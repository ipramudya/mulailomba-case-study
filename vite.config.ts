import react from "@vitejs/plugin-react-swc"
import { resolve } from "path"
import { defineConfig, splitVendorChunkPlugin } from "vite"

import manualChunks from "./src/config/vite-rollup-manual-chunk"

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => ({
    plugins: [react(), splitVendorChunkPlugin()],
    esbuild: {
        drop: mode === "production" ? ["console", "debugger"] : [],
    },
    // server: {
    //     port: 3000,
    // },
    preview: {
        port: 8080,
    },
    build: {
        rollupOptions: {
            output: {
                manualChunks,
            },
        },
    },
    resolve: {
        alias: [
            { find: "@publicService", replacement: resolve(__dirname, "src/services/public") },
            {
                find: "@organizerService",
                replacement: resolve(__dirname, "src/services/organizer"),
            },
            { find: "@seekerService", replacement: resolve(__dirname, "src/services/seeker") },
            { find: "@hooks", replacement: resolve(__dirname, "src/hooks") },
            { find: "@lib", replacement: resolve(__dirname, "src/lib") },
            { find: "@constant", replacement: resolve(__dirname, "src/constant") },
            { find: "@functions", replacement: resolve(__dirname, "src/functions") },
            { find: "@images", replacement: resolve(__dirname, "src/assets/images") },
            { find: "@modules", replacement: resolve(__dirname, "src/modules") },
            { find: "@pages", replacement: resolve(__dirname, "src/pages") },
            { find: "@blocks", replacement: resolve(__dirname, "src/components/blocks") },
            { find: "@ui", replacement: resolve(__dirname, "src/components/ui") },
            { find: "@utils", replacement: resolve(__dirname, "src/components/utils") },
            { find: "@", replacement: resolve(__dirname, "src") },
        ],
    },
}))
