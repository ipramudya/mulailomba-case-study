const PREREQUISITE_PLOTING_TYPE = new Map<
    "File" | "Paragraf" | "Teks Singkat",
    {
        icon: string
        label: string
    }
>([
    [
        "File",
        {
            icon: "heroicons:document",
            label: "File",
        },
    ],
    [
        "Paragraf",
        {
            icon: "heroicons:bars-3-bottom-left",
            label: "Paragraf",
        },
    ],
    [
        "Teks Singkat",
        {
            icon: "ph:text-aa-fill",
            label: "Teks Singkat",
        },
    ],
])

export default PREREQUISITE_PLOTING_TYPE
