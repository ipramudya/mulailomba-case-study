const mimetypeIcon = new Map([
    [
        "pdf",
        {
            icon: "bi:filetype-pdf",
            color: "text-danger-700",
        },
    ],
    [
        "png",
        {
            icon: "bi:file-earmark-image",
            color: "text-neutral-500",
        },
    ],
    [
        "jpg",
        {
            icon: "bi:file-earmark-image",
            color: "text-neutral-500",
        },
    ],
    [
        "jpeg",
        {
            icon: "bi:file-earmark-image",
            color: "text-neutral-500",
        },
    ],
    [
        "doc",
        {
            icon: "bi:filetype-doc",
            color: "text-primary-600 ",
        },
    ],
    [
        "docx",
        {
            icon: "bi:filetype-docx",
            color: "text-primary-600 ",
        },
    ],
    [
        "ppt",
        {
            icon: "bi:filetype-ppt",
            color: "text-warning-700 ",
        },
    ],
    [
        "pptx",
        {
            icon: "bi:filetype-pptx",
            color: "text-warning-700 ",
        },
    ],
    [
        "xlsx",
        {
            icon: "bi:file-earmark-excel",
            color: "text-kale-700 ",
        },
    ],
    [
        "xls",
        {
            icon: "bi:file-earmark-excel",
            color: "text-kale-700 ",
        },
    ],
])

export default mimetypeIcon
