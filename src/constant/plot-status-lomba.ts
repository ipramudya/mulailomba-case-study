const PLOT_STATUS_LOMBA = new Map([
    [
        "live",
        {
            badgeIntent: "success-low",
            badgeLabel: "Berlangsung",
        },
    ],
    [
        "will",
        {
            badgeIntent: "warning-low",
            badgeLabel: "Segera",
        },
    ],
    [
        "done",
        {
            badgeIntent: "neutral-low",
            badgeLabel: "Selesai",
        },
    ],
    [
        "archived",
        {
            badgeIntent: "kale-low",
            badgeLabel: "Diarsipkan",
        },
    ],
])

export default PLOT_STATUS_LOMBA
