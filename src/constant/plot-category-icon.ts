const categoryIcon = new Map([
    ["Musik", "heroicons:musical-note-solid"],
    ["Pendidikan", "heroicons:academic-cap-solid"],
    ["Olimpiade Sains dan Teknologi", "heroicons:beaker-solid"],
    ["Esport Game", "heroicons:puzzle-piece-solid"],
    ["Sastra dan Bahasa", "heroicons:paint-brush-solid"],
    ["Olahraga", "heroicons:sparkles-solid"],
])

export default categoryIcon
