/* cek apakah keys berada di dalam object */
export function checkExistanceKeys(obj: Record<string, string | null>, desiredKeys: string[]) {
    const keys = Object.keys(obj)
    return desiredKeys.every((key) => keys.includes(key))
}

export function checkExistanceValues(obj: Record<string, string | null>, desiredKeys?: string[]) {
    for (const key in obj) {
        if (
            Object.prototype.hasOwnProperty.call(obj, key) &&
            (!desiredKeys || desiredKeys.includes(key)) &&
            obj[key] === null
        ) {
            return true
        }
    }
    return false
}

export function checkExistanceAttributes(
    obj: Record<string, string | null>,
    desiredKeys: string[]
) {
    const keys = Object.keys(obj)

    for (const key of desiredKeys) {
        if (!keys.includes(key) || !obj[key]) {
            return false
        }
    }

    return true
}
