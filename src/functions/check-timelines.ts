import { isAfter, isWithinInterval, parseISO } from "date-fns"

interface Timeline {
    name: string | null
    description: string | null
    start_date: string | null
    end_date: string | null
    lomba_id: string | null
    id: string | null
}

export function getCurrentTimeline(givenDate: string, timelines: Timeline[]) {
    const parsedGivenDate = parseISO(givenDate)

    for (const timeline of timelines) {
        if (!timeline.start_date || !timeline.end_date) return null

        const startDate = parseISO(timeline.start_date)
        const endDate = parseISO(timeline.end_date)

        if (isWithinInterval(parsedGivenDate, { start: startDate, end: endDate })) {
            return timeline
        }
    }

    return null
}

export function getNextTimeline(givenDate: string, timelines: Timeline[]) {
    const parsedGivenDate = parseISO(givenDate)

    for (const timeline of timelines) {
        if (!timeline.start_date) return null

        const startDate = parseISO(timeline.start_date)

        if (isAfter(startDate, parsedGivenDate)) {
            return timeline
        }
    }

    return null
}
