/* eslint-disable @typescript-eslint/ban-ts-comment */
import { type AllLombaResponse } from "@organizerService/lomba/use-get-all-lomba"

export default function checkUndoneCreationLomba(lomba: AllLombaResponse): "done" | "undone" {
    const keys = Object.keys(lomba)
    const keysToCheck = keys.filter(
        (k) =>
            ![
                "total_participants",
                "held_location",
                "held_meeting_link",
                "benefits",
                "eligibilities",
            ].includes(k) // excluded keys
    )

    let status
    for (const key of Object.keys(lomba)) {
        if (keysToCheck.includes(key)) {
            if (key === "is_archived") continue
            /* @ts-ignore */
            if (lomba[key] === null || lomba[key] === undefined || lomba[key] === "") {
                status = "undone"
                break
            }

            status = "done"
        }
    }

    return status as "done" | "undone"
}
