type Param = {
    considered?: number | null
    truthy: string
    falsy: string
}

export default function plotToBoolean({ considered, truthy, falsy }: Param) {
    if (considered || considered === 0) {
        if (considered) return truthy
        else return falsy
    }

    return considered
}
