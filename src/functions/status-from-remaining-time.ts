export default function statusFromRemainingTime(remainingTime: number) {
    if (remainingTime === 0) return "live"
    if (remainingTime > 0) return "will"

    return "done"
}
