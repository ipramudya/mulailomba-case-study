export default function getFileExtension(fileName: string) {
    return fileName.match(/\.([^.]+)$/)?.[1] as string
}
