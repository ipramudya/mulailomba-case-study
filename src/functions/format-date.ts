import { differenceInDays, differenceInHours, differenceInMonths, format, parseISO } from "date-fns"
import idLocale from "date-fns/locale/id"

export function formatDate(date: string, formatter?: string) {
    return format(new Date(date), formatter || "E, dd LLL yyyy", { locale: idLocale })
}

export function formatStartAndEndDate(startDate: string, endDate: string) {
    const start = parseISO(startDate)
    const end = parseISO(endDate)

    if (start.getFullYear() === end.getFullYear()) {
        if (start.getMonth() === end.getMonth()) {
            if (start.getDate() === end.getDate()) {
                // Same day
                return `${format(end, "d MMM yyyy")}`
            }
            // Same month and year
            return `${format(start, "d")} - ${format(end, "d MMM yyyy")}`
        } else {
            // Same year, different months
            return `${format(start, "d MMM")} - ${format(end, "d MMM yyyy")}`
        }
    } else {
        // Different years
        return `${format(start, "d MMM yyyy")} - ${format(end, "d MMM yyyy")}`
    }
}

/* untuk waktu registrasi */
export function getRemainingTime(
    startDate: string,
    endDate: string
): {
    message: string
    status: string
    dateRemaining: number
    monthRemaining: number
} {
    const today = new Date()
    const start = parseISO(startDate)
    const end = parseISO(endDate)
    const inStartDays = differenceInDays(start, today)
    const inMonths = differenceInMonths(start, today)
    const inHours = differenceInHours(start, today)

    const result = {
        message: "",
        status: "will",
        dateRemaining: inStartDays,
        monthRemaining: inMonths,
    }

    if (inStartDays <= 0) {
        const inEndDays = differenceInDays(end, today)

        if (inEndDays < 0) {
            result.message = "Telah Berakhir"
            result.dateRemaining = inEndDays
            result.status = "done"
        } else if (inEndDays === 0) {
            result.message = "Segera Berakhir"
            result.dateRemaining = inEndDays
            result.status = "live"
        } else {
            result.message = "Berlangsung"
            result.dateRemaining = inEndDays
            result.status = "live"

            if (inHours >= 0) {
                result.message = "Beberapa jam lagi"
            }
        }
    } else if (inStartDays === 1) {
        result.message = "Besok"
    } else if (inStartDays < 7) {
        result.message = "Beberapa hari lagi"
    } else if (inStartDays === 7) {
        result.message = "1 minggu lagi"
    } else if (inStartDays > 7 && inMonths === 0) {
        result.message = "Kurang dari satu bulan"
    } else if (inMonths === 1) {
        result.message = "1 bulan lagi"
    } else {
        result.message = "Beberapa bulan lagi"
    }

    return result
}
