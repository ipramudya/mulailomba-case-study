import { shapes } from "@dicebear/collection"
import { createAvatar } from "@dicebear/core"

const avatar = createAvatar(shapes, {
    backgroundType: ["gradientLinear", "solid"],
}).toDataUriSync()

export default avatar
