export const toSnakeCase = (obj: Record<string, any>) => {
    const snakeKeys: Record<string, any> = {}

    for (const key in obj) {
        if (Object.prototype.hasOwnProperty.call(obj, key)) {
            const snakeKey = key.replace(/[A-Z]/g, (letter) => `_${letter.toLowerCase()}`)
            snakeKeys[snakeKey] = obj[key]
        }
    }
    return snakeKeys
}

export function replaceSpaceWithUnderscore(text: string) {
    return text.replace(/ /g, "_")
}
