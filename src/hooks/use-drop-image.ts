import { useCallback, useEffect, useState } from "react"
import { type DropzoneOptions, useDropzone } from "react-dropzone"

interface UploadedFileState {
    preview: string
    file: File
}

export default function useDropImage(param?: DropzoneOptions) {
    const [uploaded, setUploaded] = useState<UploadedFileState | null>(null)
    const [error, setError] = useState("")

    const { getInputProps, getRootProps, ...otherDropzoneVars } = useDropzone({
        maxFiles: 1,
        multiple: false,
        accept: {
            "image/*": [".png", ".jpeg", ".jpg"],
        },
        onDrop: (files) => {
            const image = files[0]
            setUploaded({
                preview: URL.createObjectURL(image),
                file: image,
            })
        },
        onError: (err) => setError(err.message),
        ...param,
    })

    useEffect(() => {
        return () => {
            if (uploaded) return URL.revokeObjectURL(uploaded.preview as string)
        }
    }, [uploaded])

    const onRemove = useCallback(() => {
        setUploaded(null)
        if (uploaded) {
            URL.revokeObjectURL(uploaded.preview as string)
        }
    }, [uploaded])

    return {
        getInputProps,
        getRootProps,
        uploaded,
        error,
        onRemove,
        dropzoneVars: otherDropzoneVars,
    }
}
