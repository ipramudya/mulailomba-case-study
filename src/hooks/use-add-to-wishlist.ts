import useAuthData from "@/services/auth-user/use-get-auth-data"
import useAddOrRemoveLombaFromFavorite from "@seekerService/lomba/use-save-lomba"
import useSeekerUserData from "@seekerService/user/use-seeker-user-data"
import { useEffect, useMemo, useState } from "react"
import toast from "react-hot-toast"
import { useNavigate } from "react-router-dom"

export default function useAddToWishlist(lombaId: string) {
    const [isAlreadySaved, setIsAlreadySaved] = useState<boolean | null>(null)
    const navigate = useNavigate()

    /* API - general usage */
    const addOrRemoveLombaFromFavorite = useAddOrRemoveLombaFromFavorite()
    const { data: authData } = useAuthData()

    const enableGetSeekerData =
        authData && authData.user && !authData.user.user_metadata.access_role
    const { data: seekerData, isLoading: seekerDataLoading } = useSeekerUserData()

    useEffect(() => {
        if (seekerData) {
            setIsAlreadySaved(() => {
                if (seekerData.saved_lomba) {
                    const found = seekerData.saved_lomba.find((s) => s === lombaId)
                    return Boolean(found)
                } else {
                    return false
                }
            })
        }
    }, [authData, lombaId, seekerData, seekerDataLoading])

    useEffect(() => {
        if (enableGetSeekerData === null) {
            setIsAlreadySaved(false)
        }
    }, [enableGetSeekerData])

    return useMemo(() => {
        const handleAddToFavorite = (e: any) => {
            e.preventDefault()

            /* temp */
            const isAlreadySavedCopied = isAlreadySaved

            setIsAlreadySaved((prev) => !prev)

            if (!authData || authData.user === null) return navigate("/entry/login")

            if (authData.user.user_metadata && seekerData && !seekerDataLoading) {
                if (authData.user.user_metadata.access_role) return // organizer

                addOrRemoveLombaFromFavorite({
                    lombaId,
                    seekerId: seekerData.id,
                    alreadySavedLomba: seekerData.saved_lomba,
                }).catch((err) => {
                    if (err) {
                        toast.error("Gagal menambahkan lomba ke dalam favorit")
                        setIsAlreadySaved(isAlreadySavedCopied)
                        return
                    }
                })
            }
        }

        return {
            isAlreadySaved,
            handleAddToFavorite,
            isRenderable: seekerData === undefined || !seekerDataLoading,
        }
    }, [
        addOrRemoveLombaFromFavorite,
        authData,
        isAlreadySaved,
        lombaId,
        navigate,
        seekerData,
        seekerDataLoading,
    ])
}
