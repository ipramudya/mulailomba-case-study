import useEmblaCarousel, { EmblaOptionsType } from "embla-carousel-react"
import { useCallback, useEffect, useMemo, useState } from "react"

export type CarouselApiType = {
    scroolPrev: () => void
    scroolNext: () => void
    isPrevBtnEnabled: boolean
    isNnextBtnEnabled: boolean
}
type Param = Partial<EmblaOptionsType>

export default function useCarousel(options?: Param | undefined) {
    useEmblaCarousel.globalOptions = { align: 0 }
    const [emblaRef, emblaAPI] = useEmblaCarousel({ loop: true, skipSnaps: false, ...options })
    const [isPrevBtnEnabled, setPrevBtnEnabled] = useState(false)
    const [isNnextBtnEnabled, setNextBtnEnabled] = useState(false)

    const scroolNext = useCallback(() => {
        if (emblaAPI) {
            emblaAPI.scrollNext()
        }
    }, [emblaAPI])

    const scroolPrev = useCallback(() => {
        if (emblaAPI) {
            emblaAPI.scrollPrev()
        }
    }, [emblaAPI])

    const onEmblaSelect = useCallback(() => {
        if (emblaAPI) {
            setNextBtnEnabled(emblaAPI.canScrollNext)
            setPrevBtnEnabled(emblaAPI.canScrollPrev)
        }
    }, [emblaAPI])

    useEffect(() => {
        if (!emblaAPI) return
        onEmblaSelect()
        emblaAPI.on("select", onEmblaSelect)
    }, [emblaAPI, onEmblaSelect])

    const carouselAPI = useMemo(
        () => ({ scroolPrev, scroolNext, isPrevBtnEnabled, isNnextBtnEnabled }),
        [isNnextBtnEnabled, isPrevBtnEnabled, scroolNext, scroolPrev]
    )

    return { carouselRef: emblaRef, carouselAPI }
}
