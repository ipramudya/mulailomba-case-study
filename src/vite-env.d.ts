/// <reference types="vite/client" />

interface ImportMetaEnv {
    readonly VITE_SUPABASE_PUBLIC_ANON_KEY: string
    readonly VITE_SUPABASE_PROJECT_URL: string
    // more env variables...
}
