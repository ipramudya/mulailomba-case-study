/* 👇 decoration */
export { default as BottomDots } from "./bottom-dots.svg"
export { default as TopDots } from "./top-dots.svg"

/* 👇 organizer sample logos */
export { default as SampleOrganizerOne } from "./organizer-example-1.svg"
export { default as SampleOrganizerTwo } from "./organizer-example-2.svg"
export { default as SampleOrganizerThree } from "./organizer-example-3.svg"
export { default as SampleOrganizerFour } from "./organizer-example-4.svg"
export { default as SampleOrganizerFive } from "./organizer-example-5.svg"
export { default as SampleOrganizerSix } from "./organizer-example-6.svg"
