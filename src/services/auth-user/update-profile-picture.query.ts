import supabaseClient from "@lib/supabase/client"

export default function updateProfilePictureQuery(path: string, newProfilePicture: File) {
    return supabaseClient.storage.from("user-profile-picture").update(path, newProfilePicture, {
        upsert: true,
    })
}
