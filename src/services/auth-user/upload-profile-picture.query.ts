import supabaseClient from "@lib/supabase/client"

export default function uploadProfilePictureQuery(path: string, profilePicture: File) {
    return supabaseClient.storage.from("user-profile-picture").upload(path, profilePicture)
}
