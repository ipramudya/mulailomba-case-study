import supabaseClient from "@lib/supabase/client"
import { useQuery } from "@tanstack/react-query"

export const AUTH_DATA_QUERY_KEY = "auth-data"

export default function useAuthData() {
    return useQuery({
        queryKey: [AUTH_DATA_QUERY_KEY],
        queryFn: () => supabaseClient.auth.getUser().then((res) => res.data),
        refetchOnWindowFocus: false,
    })
}
