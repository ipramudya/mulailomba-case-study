import supabaseClient from "@lib/supabase/client"

export default function getUserProfilePicturePublicUrl(urlPath: string) {
    return supabaseClient.storage.from("user-profile-picture").getPublicUrl(urlPath)
}
