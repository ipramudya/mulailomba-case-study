import supabaseClient from "@lib/supabase/client"

export default function updateUserMetadataQuery(param: UpdateUserMetadataParam) {
    return supabaseClient.auth.updateUser({
        email: param.email || undefined,
        password: param.password || undefined,
        data: {
            name: param.name || undefined,
            phone: param.phone || undefined,
            about: param.about || undefined,
            location: param.location || undefined,
            social_whatsapp: param.social_whatsapp || undefined,
            social_instagram: param.social_instagram || undefined,
            profile_status: param.profile_status || undefined,
            profile_image_url: param.profile_image_url || undefined,
        },
    })
}

export type UpdateUserMetadataParam = {
    /* supabase default data */
    email?: string
    password?: string
    phone?: string

    /* metadata */
    name?: string
    about?: string
    social_whatsapp?: string
    social_instagram?: string
    location?: string
    profile_status?: string
    profile_image_url?: string
    profileImage?: File
}
