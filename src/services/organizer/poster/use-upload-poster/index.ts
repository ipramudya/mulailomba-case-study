export { default } from "./use-upload-poster"

export type UploadPosterParams = {
    organizerId: string
    fileName: string
    file: File
}
