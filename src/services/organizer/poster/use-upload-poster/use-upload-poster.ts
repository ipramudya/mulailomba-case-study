import useAuthData from "@/services/auth-user/use-get-auth-data"

import { UploadPosterParams } from "./index"
import uploadPosterQuery from "./upload-poster.query"

export default function useUploadPoster() {
    const { data } = useAuthData()

    const uploadPoster = async (params: Omit<UploadPosterParams, "organizerId">) => {
        if (!data || !data.user) return { error: { message: "Data Organizer Kosong" }, data: null }

        return await uploadPosterQuery({ ...params, organizerId: data.user.id as string })
    }

    return { uploadPoster }
}
