import supabaseClient from "@lib/supabase/client"

import { UploadPosterParams } from "./index"

export default function uploadPosterQuery({ file, fileName, organizerId }: UploadPosterParams) {
    return supabaseClient.storage.from("posters").upload(`${organizerId}/${fileName}`, file, {
        cacheControl: "3600",
        upsert: false,
    })
}
