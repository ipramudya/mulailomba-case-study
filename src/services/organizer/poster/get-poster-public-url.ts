import supabaseClient from "@lib/supabase/client"

export default function getPosterPublicURL(urlPath: string) {
    return supabaseClient.storage.from("posters").getPublicUrl(urlPath)
}
