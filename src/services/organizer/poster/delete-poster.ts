import supabaseClient from "@lib/supabase/client"

export default function deletePoster(posterPath: string) {
    return supabaseClient.storage.from("posters").remove([posterPath])
}
