import supabaseClient from "@lib/supabase/client"
import { PostgrestSingleResponse } from "@supabase/supabase-js"
import { useQuery } from "@tanstack/react-query"

type Params = {
    lombaId: string
    search?: string | null
    filterMember?: string | null
}

export const GET_REGISTRATIONS_QUERY_KEY = "get_registrations"

export type RegistrationsResponse = Awaited<ReturnType<typeof useGetRegistrations>>
export type TRegistration = NonNullable<RegistrationsResponse["data"]>

export default function useGetRegistrations({ lombaId, search, filterMember }: Params) {
    return useQuery({
        queryKey: [GET_REGISTRATIONS_QUERY_KEY, search ?? undefined, filterMember ?? undefined],
        queryFn: async () => {
            // const itHasTimelines = await supabaseClient
            //     .from("lomba")
            //     .select("lomba_timelines!inner (count)")
            //     .eq("id", lombaId)
            //     .single()
            //     .then((res) => (res.data?.lomba_timelines as any)?.[0].count)

            // const selectBuilder = itHasTimelines
            //     ? ( as const)
            //     : ("*, seeker!inner (id, name, email, location, phone, profile_public_url)" as const)

            let query = supabaseClient
                .from("registrations")
                .select(
                    "*, seeker!inner (id, name, email, location, phone, profile_public_url),registrations_answer (label, value), registrations_upload (file_storage_url, file_public_url, label)"
                )
                .eq("lomba_id", lombaId)
                .eq("is_canceled", 0)

            if (search) {
                query = query.ilike("seeker.name", `%${search}%`)
            }

            if (filterMember) {
                const regex = /verified-(\d)/ // get identifier setelah kata verified-
                const filterMemberToCheck = regex.exec(filterMember)?.[1] as string

                if (filterMember.includes("verified")) {
                    query = query.eq("is_verified", filterMemberToCheck)
                } else if (filterMember.includes("eliminate")) {
                    query = query.eq("is_eliminated", filterMemberToCheck)
                }
            }

            const res = (await query) as PostgrestSingleResponse<Registration[]>
            return res.data
        },
    })
}

export interface RegistrationAnswer {
    id: string
    created_at: string
    label: string
    value: string
    registration_id: string
}

export interface RegistrationUpload {
    id: string
    created_at: string
    file_storage_url: string
    file_public_url: string
    label: string
    registration_id: string
}

export interface Seeker {
    id: string
    name: string
    phone: string
    location: string
    profile_public_url: string
}

export interface Registration {
    id: string
    created_at: string
    updated_at: string
    seeker_id: string
    lomba_id: string
    is_verified: number | null
    is_eliminated: boolean | null
    is_canceled: number
    eliminated_on_timeline: boolean | null
    registrations_answer: RegistrationAnswer[] | null
    registrations_upload: RegistrationUpload[] | null
    seeker: Seeker
}
