import supabaseClient from "@lib/supabase/client"
import { GET_REGISTRATIONS_QUERY_KEY } from "@organizerService/registrations/use-get-registrations"
import { useQueryClient } from "@tanstack/react-query"

type Payload = {
    verifyValue?: 1 | 0
}

export default function useUpdateRegistration(registrationId: string) {
    const queryClient = useQueryClient()

    return async (payload: Payload) => {
        const res = await supabaseClient
            .from("registrations")
            .update({
                is_verified: payload.verifyValue,
            })
            .eq("id", registrationId)

        if (res.status === 204) {
            queryClient.refetchQueries([GET_REGISTRATIONS_QUERY_KEY])
        }

        return res
    }
}
