import supabaseClient from "@lib/supabase/client"
import { useQueryClient } from "@tanstack/react-query"

import { GET_CANCELATIONS_QUERY_KEY } from "./use-get-cancelations"

export default function useApproveCancelation() {
    const queryClient = useQueryClient()

    return async (registrationId: string) => {
        const res = await supabaseClient
            .from("registrations")
            .update({
                is_cancelation_approved: 1,
            })
            .eq("id", registrationId)

        if (res.status === 204) {
            queryClient.refetchQueries([GET_CANCELATIONS_QUERY_KEY])
        }

        return res
    }
}
