import supabaseClient from "@lib/supabase/client"

type Payload = {
    fileName: string
    file: File
    registrationId: string
}

export default function useUploadResultFile() {
    return async ({ registrationId, file, fileName }: Payload) => {
        if (!registrationId || !file || !fileName) {
            return { error: "Terdapat data yang masih kosong", data: null }
        }

        return await supabaseClient.storage
            .from("executions-files")
            .upload(`${registrationId}/${fileName}`, file, {
                upsert: true,
                cacheControl: "3600",
            })
    }
}
