import supabaseClient from "@lib/supabase/client"
import getPublicURL from "@publicService/get-public-url"
import { useQueryClient } from "@tanstack/react-query"
import { toast } from "react-hot-toast"

import useUploadResultFile from "./use-upload-result-file"

type Payload = {
    status: "eliminate" | "pass" | "winner"
    timelineId?: string | null
    description: string
    timelineName?: string | null
    seekerId: string
    file?: File
    winnerAs?: string
    exist?: {
        executionId: string
        storageUrl?: string | null
    } | null
}

export default function useCreateExecution(registrationId: string) {
    const uploadResultFile = useUploadResultFile()
    const queryClient = useQueryClient()

    return async ({
        description,
        seekerId,
        status,
        file,
        timelineId,
        winnerAs,
        timelineName,
        exist,
    }: Payload) => {
        if (exist) {
            /* menghapus file bila ada */
            let deleteExecutionFiles: any = null
            if (exist.storageUrl) {
                deleteExecutionFiles = supabaseClient.storage
                    .from("executions-files")
                    .remove([exist.storageUrl])
            }
            const deleteExistingExecution = supabaseClient
                .from("executions")
                .delete()
                .eq("id", exist.executionId)

            await Promise.all([deleteExecutionFiles, deleteExistingExecution])
        }

        /* jike mengirimkan file tambahan */
        let uploadedFileResponsePath: string | null = null
        if (file) {
            const uploadedFileResponse = await uploadResultFile({
                registrationId,
                file,
                fileName: file.name.replace(/\s+/g, "_"),
            })

            if (uploadedFileResponse.error) {
                if (typeof uploadedFileResponse.error === "string")
                    toast.error(uploadedFileResponse.error)
            } else if (uploadedFileResponse.data) {
                uploadedFileResponsePath = uploadedFileResponse.data.path
            }
        }

        const baseExecutionDataToPush = {
            seeker_id: seekerId,
            registration_id: registrationId,
            description,
            timeline_name: timelineName,
            updated_at: new Date().toISOString(),
            on_timeline: timelineId,
            file_storage_url: uploadedFileResponsePath || null,
            file_public_url:
                uploadedFileResponsePath !== null
                    ? getPublicURL("executions-files", uploadedFileResponsePath).data.publicUrl
                    : null,
        }

        const getExecutionDataToPush = () => {
            switch (status) {
                case "eliminate":
                    return {
                        ...baseExecutionDataToPush,
                        is_eliminate: 1,
                    }
                case "pass":
                    return {
                        ...baseExecutionDataToPush,
                        is_passed: 1,
                    }
                case "winner":
                    return {
                        ...baseExecutionDataToPush,
                        is_winner: 1,
                        winner_as: winnerAs,
                    }
            }
        }

        const res = await supabaseClient.from("executions").insert(getExecutionDataToPush() as any)

        if (res.status === 201) {
            queryClient.refetchQueries({ stale: true })
        }

        return res
    }
}
