import supabaseClient from "@lib/supabase/client"
import { useQuery } from "@tanstack/react-query"

type Params = {
    registrationId: string
    timelineId?: string | null
}

export const GET_EXECUTION_BY_TIMELINE_QUERY_KEY = "get-execution-by-timeline"

export default function useGetExecutionByTimeline({ timelineId, registrationId }: Params) {
    return useQuery({
        queryKey: [GET_EXECUTION_BY_TIMELINE_QUERY_KEY, timelineId || undefined],
        queryFn: async () => {
            let query = supabaseClient
                .from("executions")
                .select("*")
                .eq("registration_id", registrationId)

            if (timelineId) {
                query = query.eq("on_timeline", timelineId)
            }

            const res = await query
            return res.data
        },
        refetchOnWindowFocus: false,
        select(data) {
            return data?.[0]
        },
    })
}
