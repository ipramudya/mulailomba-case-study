import supabaseClient from "@lib/supabase/client"
import { useQuery } from "@tanstack/react-query"

const GET_EXECUTIONS_QUERY_KEY = "get-executions"

export default function useGetExecutions(registrationId: string) {
    return useQuery({
        queryKey: [GET_EXECUTIONS_QUERY_KEY],
        queryFn: async () => {
            const res = await supabaseClient
                .from("executions")
                .select("*")
                .eq("registration_id", registrationId)
                .order("created_at", { ascending: true })

            return res.data
        },
        refetchOnWindowFocus: false,
    })
}
