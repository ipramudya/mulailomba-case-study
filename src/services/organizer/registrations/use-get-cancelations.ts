import supabaseClient from "@lib/supabase/client"
import { useQuery } from "@tanstack/react-query"

type Params = {
    lombaId: string
    search?: string | null
}

export const GET_CANCELATIONS_QUERY_KEY = "get_cancelations"

export default function useGetCancelations({ lombaId, search }: Params) {
    return useQuery({
        queryKey: [GET_CANCELATIONS_QUERY_KEY],
        queryFn: async () => {
            let query = supabaseClient
                .from("registrations")
                .select("*, seeker!inner (id, name, email, location, phone, profile_public_url)")
                .eq("lomba_id", lombaId)
                .eq("is_canceled", 1)

            if (search) {
                query = query.ilike("seeker.name", `%${search}%`)
            }

            return (await query).data
        },
    })
}
