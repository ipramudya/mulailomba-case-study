import supabaseClient from "@lib/supabase/client"
import { useQuery } from "@tanstack/react-query"

type Params = {
    registrationId: string
    timelineId: string | null
}

export const CHECK_EXECUTION_BY_TIMELINE_QUERY_KEY = "check-execution-by-timeline"

export default function useCheckExecutionByTimeline({ registrationId, timelineId }: Params) {
    return useQuery({
        queryKey: [CHECK_EXECUTION_BY_TIMELINE_QUERY_KEY, timelineId ?? undefined],
        queryFn: async () => {
            let query = supabaseClient
                .from("executions")
                .select("*")
                .eq("registration_id", registrationId)

            if (timelineId) {
                query = query.eq("on_timeline", timelineId)
            }

            return (await query.single()).data
        },
        refetchOnWindowFocus: false,
        enabled: Boolean(registrationId),
    })
}
