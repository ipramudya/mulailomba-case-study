import useAuthData from "@/services/auth-user/use-get-auth-data"
import supabaseClient from "@lib/supabase/client"
import { useQuery } from "@tanstack/react-query"

export const ORGANIZER_USER_DATA_QUERY_KEY = "organizer-user-data"

export default function useOrganizerUserData() {
    const { data: auth, error: errorAuth } = useAuthData()

    return useQuery({
        queryKey: [ORGANIZER_USER_DATA_QUERY_KEY],
        queryFn: async () => {
            const res = await supabaseClient
                .from("organizer")
                .select("*")
                .eq("id", auth?.user?.id as string)
                .single()

            if (res.error) {
                throw new Error(res.error.message)
            }

            return res.data
        },
        refetchOnWindowFocus: false,
        enabled: Boolean(auth && auth.user && auth.user.id && !errorAuth),
    })
}
