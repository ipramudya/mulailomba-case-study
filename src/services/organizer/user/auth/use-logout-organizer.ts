import { AUTH_DATA_QUERY_KEY } from "@/services/auth-user/use-get-auth-data"
import supabaseClient from "@lib/supabase/client"
import { useMutation, useQueryClient } from "@tanstack/react-query"

export default function useLogoutOrganizer() {
    const queryClient = useQueryClient()

    return useMutation(async () => await supabaseClient.auth.signOut(), {
        onSuccess() {
            queryClient.refetchQueries([AUTH_DATA_QUERY_KEY])
        },
    })
}
