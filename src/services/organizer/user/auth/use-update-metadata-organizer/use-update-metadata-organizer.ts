import getUserProfilePicturePublicUrl from "@/services/auth-user/get-user-profile-picture-public-url"
import updateProfilePictureQuery from "@/services/auth-user/update-profile-picture.query"
import updateUserMetadataQuery from "@/services/auth-user/update-user-metadata.query"
import uploadProfilePictureQuery from "@/services/auth-user/upload-profile-picture.query"
import useAuthData, { AUTH_DATA_QUERY_KEY } from "@/services/auth-user/use-get-auth-data"
import { replaceSpaceWithUnderscore } from "@functions/format-strings"
import getFileExtension from "@functions/get-file-extension"
import { useQueryClient } from "@tanstack/react-query"
import { toast } from "react-hot-toast"

import { UpdateMetadataOrganizerParam } from "./index"
import updateDataOrganizerQuery from "./update-data-organizer.query"

export default function useUpdateOrganizerMetadata() {
    const queryClient = useQueryClient()
    const { data: dataOrganizer } = useAuthData()

    return async ({ profileImage, ...restParam }: UpdateMetadataOrganizerParam) => {
        /* upload/update profile */
        let updatedProfileData: any, updatedProfileError: any
        if (dataOrganizer && profileImage) {
            const { user } = dataOrganizer

            if (user?.user_metadata.profile_image_url) {
                const { data, error } = await updateProfilePictureQuery(
                    user?.user_metadata.profile_image_url,
                    profileImage
                )

                if (data && !error) {
                    updatedProfileData = data
                } else {
                    updatedProfileError = error
                }
            } else {
                const constructFileName = `${replaceSpaceWithUnderscore(
                    user?.user_metadata.name
                )}/${user?.id}_profile.${getFileExtension(profileImage.name)}`

                const { data, error } = await uploadProfilePictureQuery(
                    constructFileName,
                    profileImage
                )

                if (data && !error) {
                    updatedProfileData = data
                } else {
                    updatedProfileError = error
                }
            }
        }

        if (updatedProfileError) {
            return {
                data: null,
                error: updatedProfileError,
            }
        }

        /* push updated/uploaded file into user meta data */
        const { data: updatedMetadataOrganizer, error: errorUpdatedMetadataOrganizer } =
            await updateUserMetadataQuery({
                ...restParam,
                profile_image_url: updatedProfileData?.path || undefined,
            })

        if (!updatedMetadataOrganizer && errorUpdatedMetadataOrganizer) {
            return {
                data: null,
                error: errorUpdatedMetadataOrganizer.message,
            }
        }

        /* push updated metadata into organizer table (which not related with auth.user.metadata) */
        if (updatedMetadataOrganizer.user) {
            const {
                name,
                about,
                location,
                profile_status,
                social_instagram,
                social_whatsapp,
                phone,
                profile_image_url,
            } = updatedMetadataOrganizer.user.user_metadata
            const profilePublicUrl =
                getUserProfilePicturePublicUrl(profile_image_url).data.publicUrl

            const { error: errorUpdatedDataOrganizer } = await updateDataOrganizerQuery({
                id: updatedMetadataOrganizer.user.id,
                email: updatedMetadataOrganizer.user.email,
                name,
                about,
                phone,
                location,
                profile_status,
                social_instagram,
                social_whatsapp,
                profile_storage_url: profile_image_url,
                profile_public_url: profilePublicUrl,
            })

            if (errorUpdatedDataOrganizer) {
                console.log("error", errorUpdatedDataOrganizer)
                toast.error("Gagal melakukan update profile, server sedang tidak stabil...")
            }

            queryClient.refetchQueries([AUTH_DATA_QUERY_KEY])
            return { data: updatedMetadataOrganizer, error: errorUpdatedMetadataOrganizer }
        }
    }
}
