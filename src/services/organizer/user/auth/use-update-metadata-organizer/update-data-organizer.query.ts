import supabaseClient from "@lib/supabase/client"

type UpdateDataOrganizerParam = {
    id: string
    email?: string
    name?: string
    location?: string
    phone?: string
    about?: string
    social_whatsapp?: string
    social_instagram?: string
    profile_status?: string
    profile_storage_url?: string
    profile_public_url?: string
}

export default async function updateDataOrganizerQuery(param: UpdateDataOrganizerParam) {
    return await supabaseClient
        .from("organizer")
        .update({
            email: param.email || undefined,
            name: param.name || undefined,
            location: param.location || undefined,
            phone: param.phone || undefined,
            social_whatsapp: param.social_whatsapp || undefined,
            social_instagram: param.social_instagram || undefined,
            profile_status: param.profile_status || undefined,
            profile_public_url: param.profile_public_url || undefined,
            profile_storage_url: param.profile_storage_url || undefined,
            about: param.about || undefined,
            updated_at: new Date().toISOString(),
        })
        .eq("id", param.id)
}
