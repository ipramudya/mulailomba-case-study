export { default } from "./use-update-metadata-organizer"

export type UpdateMetadataOrganizerParam = {
    /* supabase default data */
    email?: string
    password?: string
    phone?: string

    /* metadata */
    name?: string
    about?: string
    social_whatsapp?: string
    social_instagram?: string
    location?: string
    profile_status?: string
    profile_image_url?: string
    profileImage?: File
    interest?: string[]
}
