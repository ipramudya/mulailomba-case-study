import { AUTH_DATA_QUERY_KEY } from "@/services/auth-user/use-get-auth-data"
import supabaseClient from "@lib/supabase/client"
import { useQueryClient } from "@tanstack/react-query"
import { isEmpty } from "lodash-es"

type OrganizerLoginparams = {
    email: string
    password: string
}

export default function useLoginOrgan() {
    const queryClient = useQueryClient()

    return async (params: OrganizerLoginparams) => {
        const { data: accountOnDB, error } = await supabaseClient
            .from("organizer")
            .select("email")
            .eq("email", params.email)

        if (error) {
            return {
                data: null,
                error: "Server sedang tidak stabil",
            }
        }

        if (isEmpty(accountOnDB)) {
            return {
                data: null,
                error: "Akun tidak dapat ditemukan",
            }
        }

        const res = await supabaseClient.auth.signInWithPassword(params)

        if (!res.error) {
            queryClient.refetchQueries([AUTH_DATA_QUERY_KEY])
        }

        return res
    }
}
