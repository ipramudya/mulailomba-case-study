import supabaseClient from "@lib/supabase/client"
import { useQuery } from "@tanstack/react-query"

export const ORGANIZER_AUTH_DATA_QUERY_KEY = "organizer-auth-data"

export default function useOrganizerAuthData() {
    return useQuery({
        queryKey: [ORGANIZER_AUTH_DATA_QUERY_KEY],
        queryFn: () => supabaseClient.auth.getUser().then((res) => res.data),
        refetchOnWindowFocus: false,
    })
}
