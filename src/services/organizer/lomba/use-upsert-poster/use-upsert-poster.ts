import supabaseClient from "@lib/supabase/client"

export default function useUpsertPoster() {
    const upsertPoster = async (existingUrl: string, newFile: File) => {
        return await supabaseClient.storage.from("posters").update(existingUrl, newFile, {
            cacheControl: "3600",
            upsert: true,
        })
    }

    return { upsertPoster }
}
