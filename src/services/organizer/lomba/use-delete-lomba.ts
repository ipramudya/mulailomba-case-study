import supabaseClient from "@lib/supabase/client"
import { useQueryClient } from "@tanstack/react-query"

import { GET_ALL_ORGANIZER_LOMBA_QUERY_KEY } from "./use-get-all-lomba"

export default function useDeleteLomba() {
    const queryClient = useQueryClient()

    return async function deleteLomba(lombaId: string) {
        const res = await supabaseClient.from("lomba").delete().eq("id", lombaId)

        if (res.status === 204) {
            queryClient.refetchQueries([GET_ALL_ORGANIZER_LOMBA_QUERY_KEY])
        }

        return res
    }
}
