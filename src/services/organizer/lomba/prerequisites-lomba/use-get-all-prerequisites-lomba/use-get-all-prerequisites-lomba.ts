import { useQuery } from "@tanstack/react-query"

import getAllPrerequisitesLombaQuery from "./get-all-prerequisites-lomba.query"
import { GET_ALL_ORGANIZER_PREREQUISITES_LOMBA_QUERY_KEY } from "./index"

export default function useGetAllPrerequisitesLomba(lombaId: string) {
    return useQuery({
        queryKey: [GET_ALL_ORGANIZER_PREREQUISITES_LOMBA_QUERY_KEY, lombaId],
        queryFn: ({ queryKey }) =>
            getAllPrerequisitesLombaQuery(queryKey[1]).then((res) => res.data),
        enabled: Boolean(lombaId),
        refetchOnWindowFocus: false,
    })
}
