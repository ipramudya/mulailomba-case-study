import supabaseClient from "@lib/supabase/client"

export default async function getAllPrerequisitesLombaQuery(lombaId: string) {
    return await supabaseClient.from("lomba_prerequisites").select("*").eq("lomba_id", lombaId)
}
