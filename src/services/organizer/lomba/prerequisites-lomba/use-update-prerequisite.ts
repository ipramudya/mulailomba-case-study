import { toSnakeCase } from "@functions/format-strings"
import supabaseClient from "@lib/supabase/client"
import { useQueryClient } from "@tanstack/react-query"

import { GET_ALL_ORGANIZER_PREREQUISITES_LOMBA_QUERY_KEY } from "./use-get-all-prerequisites-lomba"

type Payload = Partial<{
    description: string | null
    isRequired: "0" | "1"
    name: string | null
    variant: string | null
}> & { id: string }

export default function useUpdatePrerequisite() {
    const queryClient = useQueryClient()

    return async (payload: Payload) => {
        const { id, ...rest } = toSnakeCase(payload)

        const res = supabaseClient.from("lomba_prerequisites").update(rest).eq("id", id).select()

        if ((await res).status === 200) {
            queryClient.refetchQueries([GET_ALL_ORGANIZER_PREREQUISITES_LOMBA_QUERY_KEY])
        }

        return res
    }
}
