import supabaseClient from "@lib/supabase/client"
import { useQueryClient } from "@tanstack/react-query"

import { GET_ALL_ORGANIZER_PREREQUISITES_LOMBA_QUERY_KEY } from "./use-get-all-prerequisites-lomba"

export default function useDeletePrerequisite() {
    const queryClient = useQueryClient()

    return async (prerequisiteId: string) => {
        const res = supabaseClient.from("lomba_prerequisites").delete().eq("id", prerequisiteId)

        if ((await res).status === 204) {
            queryClient.refetchQueries([GET_ALL_ORGANIZER_PREREQUISITES_LOMBA_QUERY_KEY])
        }
        return res
    }
}
