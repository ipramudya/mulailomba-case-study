import { useQueryClient } from "@tanstack/react-query"

import { GET_ALL_ORGANIZER_PREREQUISITES_LOMBA_QUERY_KEY } from "../use-get-all-prerequisites-lomba"
import createPrerequisiteQuery from "./create-prerequisite.query"
import { PrerequisiteParams } from "./index"

export default function useCreatePrerequisite(lombaId: string) {
    const queryClient = useQueryClient()

    const createPrerequisites = async (params: Omit<PrerequisiteParams, "lombaId">) => {
        const result = await createPrerequisiteQuery({ ...params, lombaId })

        if (result.data) {
            queryClient.refetchQueries([GET_ALL_ORGANIZER_PREREQUISITES_LOMBA_QUERY_KEY, lombaId])
        }

        return result
    }

    return { createPrerequisites }
}
