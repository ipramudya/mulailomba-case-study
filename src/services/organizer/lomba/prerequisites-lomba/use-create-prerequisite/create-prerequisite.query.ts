import supabaseClient from "@lib/supabase/client"

import { PrerequisiteParams } from "./index"

export default function createPrerequisiteQuery({
    lombaId,
    isRequired,
    ...restParams
}: PrerequisiteParams) {
    return supabaseClient
        .from("lomba_prerequisites")
        .insert({
            lomba_id: lombaId,
            is_required: Number(isRequired),
            ...restParams,
        })
        .select()
}
