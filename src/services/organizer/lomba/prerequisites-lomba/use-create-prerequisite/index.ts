export { default } from "./use-create-prerequisite"

export type PrerequisiteParams = {
    description?: string | null
    id?: string
    isRequired: "0" | "1"
    lombaId: string
    name?: string | null
    variant?: string | null
}
