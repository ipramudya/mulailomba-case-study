import supabaseClient from "@lib/supabase/client"

export default async function deleteAllPrerequisitesLombaQuery(lombaId: string) {
    return supabaseClient.from("lomba_prerequisites").delete().eq("lomba_id", lombaId)
}
