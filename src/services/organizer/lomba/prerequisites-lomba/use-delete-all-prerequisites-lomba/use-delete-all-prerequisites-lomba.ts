import deleteAllPrerequisitesLombaQuery from "./delete-all-prerequisites-lomba.query"

export default function useDeleteAllPrerequisitesLomba() {
    const deletePrerequisite = async (lombaId: string) => {
        await deleteAllPrerequisitesLombaQuery(lombaId)
    }

    return { deletePrerequisite }
}
