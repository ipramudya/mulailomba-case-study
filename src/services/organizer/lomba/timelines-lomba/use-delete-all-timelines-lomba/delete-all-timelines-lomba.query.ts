import supabaseClient from "@lib/supabase/client"

export default function deleteAllTimelinesLombaQuery(lombaId: string) {
    return supabaseClient.from("lomba_timelines").delete().eq("lomba_id", lombaId)
}
