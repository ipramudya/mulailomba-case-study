import deleteAllTimelinesLombaQuery from "./delete-all-timelines-lomba.query"

export default function useDeleteAllTimelinesLomba() {
    const deleteTimeline = async (lombaId: string) => {
        await deleteAllTimelinesLombaQuery(lombaId)
    }

    return { deleteTimeline }
}
