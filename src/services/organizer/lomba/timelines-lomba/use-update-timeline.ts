import { toSnakeCase } from "@functions/format-strings"
import supabaseClient from "@lib/supabase/client"
import { useQueryClient } from "@tanstack/react-query"

import { GET_ALL_ORGANIZER_TIMELINES_LOMBA_QUERY_KEY } from "./use-get-all-timelines-lomba"

type Payload = Partial<{
    name: string
    description: string
    startDate: string
    endDate: string
    lombaId: string
}> & { id: string }

export default function useUpdateTimeline() {
    const queryClient = useQueryClient()

    return async (payload: Payload) => {
        const { id, ...rest } = toSnakeCase(payload)

        const res = supabaseClient.from("lomba_timelines").update(rest).eq("id", id).select()

        if ((await res).status === 200) {
            queryClient.refetchQueries([GET_ALL_ORGANIZER_TIMELINES_LOMBA_QUERY_KEY])
        }

        return res
    }
}
