import supabaseClient from "@lib/supabase/client"
import { useQueryClient } from "@tanstack/react-query"

export default function useDeleteTimeline() {
    const queryClient = useQueryClient()

    return async (timelineId: string) => {
        const res = await supabaseClient.from("lomba_timelines").delete().eq("id", timelineId)

        if (res.status === 204) {
            queryClient.refetchQueries({
                stale: true,
            })
        }

        return res
    }
}
