import { useQueryClient } from "@tanstack/react-query"

import createTimelineQuery from "./create-timeline.query"
import { TimelineParams } from "./index"

export default function useCreateTimeline(lombaId: string) {
    const queryClient = useQueryClient()

    const createTimeline = async (params: Omit<TimelineParams, "lombaId">) => {
        const result = await createTimelineQuery({ ...params, lombaId })

        if (result.data) {
            queryClient.refetchQueries()
        }

        return result
    }

    return { createTimeline }
}
