import supabaseClient from "@lib/supabase/client"

import { TimelineParams } from "./index"

export default function createTimelineQuery({
    lombaId,
    startDate,
    endDate,
    ...restParams
}: TimelineParams) {
    return supabaseClient
        .from("lomba_timelines")
        .insert({
            lomba_id: lombaId,
            start_date: startDate,
            end_date: endDate,
            ...restParams,
        })
        .select()
}
