export { default } from "./use-create-timeline"

export type TimelineParams = {
    id?: string
    name: string
    description: string
    startDate: string
    endDate: string
    lombaId: string
}
