import { useQuery } from "@tanstack/react-query"

import getAllTimelinesLombaQuery from "./get-all-timelines-lomba.query"
import { GET_ALL_ORGANIZER_TIMELINES_LOMBA_QUERY_KEY } from "./index"

export default function useGetAllTimelinesLomba(lombaId: string) {
    return useQuery({
        queryKey: [GET_ALL_ORGANIZER_TIMELINES_LOMBA_QUERY_KEY, lombaId],
        queryFn: ({ queryKey }) => getAllTimelinesLombaQuery(queryKey[1]).then((res) => res.data),
        enabled: Boolean(lombaId),
        refetchOnWindowFocus: false,
    })
}
