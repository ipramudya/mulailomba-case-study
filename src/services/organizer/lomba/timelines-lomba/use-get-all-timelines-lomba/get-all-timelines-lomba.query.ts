import supabaseClient from "@lib/supabase/client"

export default async function getAllTimelinesLombaQuery(lombaId: string) {
    return await supabaseClient
        .from("lomba_timelines")
        .select("*")
        .eq("lomba_id", lombaId)
        .order("start_date")
}
