export { default } from "./use-get-all-organizer-lomba"
export const GET_ALL_ORGANIZER_LOMBA_QUERY_KEY = "get-all-organizer-lomba"

export interface AllLombaResponse {
    id: string
    created_at: Date
    updated_at: Date
    name: string
    description: null | string
    start_date: string
    end_date: string
    start_registration: string
    end_registration: string
    category: string
    is_free: number
    is_unlimited_participants: number
    total_participants: number
    is_held_online: number
    held_location: null | string
    held_meeting_link: null | string
    benefits: string[]
    eligibilities: string[]
    organizer_id: string
    rules: null | string
    poster_storage_url: string
    poster_public_url: string
    is_archived: number | null
    registrations: Registration[]
}

export interface Registration {
    count: number
}
