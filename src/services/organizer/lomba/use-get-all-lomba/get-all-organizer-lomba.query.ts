import supabaseClient from "@lib/supabase/client"

import { GetAllOrganizerLombaParams } from "./use-get-all-organizer-lomba"

export default async function getAllOrganizerLombaQuery(params?: GetAllOrganizerLombaParams) {
    let query = supabaseClient
        .from("lomba")
        .select("*, registrations(count)")
        .eq("organizer_id", params?.organizerId as string)

    if (params && params.search) {
        query = query.ilike("name", `%${params.search}%`)
    }

    return await query
}
