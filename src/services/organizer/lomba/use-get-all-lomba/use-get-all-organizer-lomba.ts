import useAuthData from "@/services/auth-user/use-get-auth-data"
import { useQuery } from "@tanstack/react-query"

import { GET_ALL_ORGANIZER_LOMBA_QUERY_KEY } from "."
import deletePoster from "../../poster/delete-poster"
import useDeleteLomba from "../use-delete-lomba"
import getAllOrganizerLombaQuery from "./get-all-organizer-lomba.query"

export type GetAllOrganizerLombaParams =
    | {
          organizerId?: string
          search?: string
      }
    | undefined

export default function useGetAllOrganizerLomba(params?: GetAllOrganizerLombaParams) {
    const { data } = useAuthData()
    const deleteLomba = useDeleteLomba()

    const whichOrganizerId = params?.organizerId || (data?.user?.id as string)

    return useQuery({
        queryKey: [GET_ALL_ORGANIZER_LOMBA_QUERY_KEY, params ? params.search : undefined],
        queryFn: () =>
            getAllOrganizerLombaQuery({ ...params, organizerId: whichOrganizerId }).then((res) => {
                if (res.status === 200 && res.data) {
                    res.data.forEach((d) => {
                        if (d.name === null) {
                            if (d.poster_public_url && d.poster_storage_url) {
                                deletePoster(d.poster_storage_url)
                            }
                            deleteLomba(d.id)
                        }
                    })
                }

                return res.data
            }),
        enabled: Boolean(data && data.user) || Boolean(params?.organizerId),
        refetchOnWindowFocus: false,
    })
}
