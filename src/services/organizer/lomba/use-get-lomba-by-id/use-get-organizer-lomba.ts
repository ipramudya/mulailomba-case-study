import useAuthData from "@/services/auth-user/use-get-auth-data"
import { useQuery } from "@tanstack/react-query"

import { GET_ORGANIZER_LOMBA_QUERY_KEY } from "."
import getOrganizerLombaQuery from "./get-specific-lomba.query"

export default function useGetOrganizerLomba(id: string) {
    const { data } = useAuthData()

    return useQuery({
        queryKey: [GET_ORGANIZER_LOMBA_QUERY_KEY],
        queryFn: () => getOrganizerLombaQuery(id, data?.user?.id as string).then((res) => res.data),
        enabled: Boolean(data && data.user),
        refetchOnWindowFocus: false,
    })
}
