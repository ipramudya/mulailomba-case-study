import supabaseClient from "@lib/supabase/client"

export default async function getOrganizerLombaQuery(id: string, organizerId: string) {
    return await supabaseClient
        .from("lomba")
        .select("*")
        .eq("id", id)
        .eq("organizer_id", organizerId)
        .single()
}
