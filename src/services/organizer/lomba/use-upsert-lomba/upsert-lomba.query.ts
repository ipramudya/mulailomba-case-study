/* eslint-disable @typescript-eslint/ban-ts-comment */
import supabaseClient from "@lib/supabase/client"

import { type UpsertLombaParams } from "./index"

export default function upsertLombaQuery(params: UpsertLombaParams) {
    const remapParams = {} as any
    for (const [key, value] of Object.entries(params)) {
        const snakeKey = key.replace(/[A-Z]/g, (letter) => `_${letter.toLowerCase()}`)

        // @ts-ignore
        if (typeof params[key] === "boolean") {
            // @ts-ignore
            remapParams[snakeKey] = value ? 1 : 0
        }
        // @ts-ignore
        else if (params[key] === "" || params[key] === undefined) {
            remapParams[snakeKey] = null
        } else {
            // @ts-ignore
            remapParams[snakeKey] = value
        }
    }

    return supabaseClient.from("lomba").upsert(remapParams).select()
}
