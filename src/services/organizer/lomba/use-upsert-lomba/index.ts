export { default } from "./use-upsert-lomba"

export type UpsertLombaParams = {
    id: string
    organizerId: string
} & RestLombaParams

export type RestLombaParams = Partial<{
    name: string
    description: string
    startDate: string
    endDate: string
    startRegistration: string
    endRegistration: string
    category: string
    posterPublicUrl: string | null
    posterStorageUrl: string | null
    isFree: boolean
    isUnlimitedParticipants: boolean
    totalParticipants?: number
    isHeldOnline: boolean
    heldLocation?: string | null
    heldMeetingLink?: string | null
    benefits: string[]
    eligibilities: string[]
    rules: string
    isArchived: boolean
}>
