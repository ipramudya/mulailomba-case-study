import useAuthData from "@/services/auth-user/use-get-auth-data"
import { useQueryClient } from "@tanstack/react-query"

import { RestLombaParams } from "./index"
import upsertLombaQuery from "./upsert-lomba.query"

export default function useUpsertLomba(lombaId: string) {
    const { data } = useAuthData()
    const queryClient = useQueryClient()

    const upsertLomba = async (params: RestLombaParams) => {
        if (!data || !data.user) return { error: { message: "Data Organizer Kosong" }, data: null }

        const res = upsertLombaQuery({
            ...params,
            id: lombaId,
            organizerId: data.user.id,
        })

        if ((await res).status === 201) {
            queryClient.refetchQueries({ stale: true })
        }

        return res
    }

    return { upsertLomba }
}
