import useAuthData from "@/services/auth-user/use-get-auth-data"
import supabaseClient from "@lib/supabase/client"
import { useQuery } from "@tanstack/react-query"

export const SEEKER_USER_DATA_QUERY_KEY = "seeker-user-data"

export default function useSeekerUserData() {
    const { data: auth } = useAuthData()

    return useQuery({
        queryKey: [SEEKER_USER_DATA_QUERY_KEY],
        queryFn: async () => {
            if (!auth || !auth.user) return null

            const res = await supabaseClient
                .from("seeker")
                .select("*")
                .eq("id", auth.user.id)
                .single()

            if (res.error) {
                throw new Error(res.error.message)
            }

            return res.data
        },
        refetchOnWindowFocus: false,
        enabled: Boolean(auth && auth.user),
    })
}
