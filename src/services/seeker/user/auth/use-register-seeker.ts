import { AUTH_DATA_QUERY_KEY } from "@/services/auth-user/use-get-auth-data"
import supabaseClient from "@lib/supabase/client"
import { useQueryClient } from "@tanstack/react-query"
import { isEmpty } from "lodash-es"

type SeekerRegisterParams = {
    name: string
    email: string
    password: string
}

export default function useRegisterSeeker() {
    const queryClient = useQueryClient()

    return async ({ name, ...restParams }: SeekerRegisterParams) => {
        const checkSeekerEmailPromise = supabaseClient
            .from("seeker")
            .select("email")
            .eq("email", restParams.email)

        const checkOrganizerEmailPromise = supabaseClient
            .from("organizer")
            .select("email")
            .eq("email", restParams.email)

        const [seekerAccountOnDB, organizerAccountOnDB] = await Promise.all([
            checkSeekerEmailPromise,
            checkOrganizerEmailPromise,
        ])

        if (!isEmpty(seekerAccountOnDB.data) || !isEmpty(organizerAccountOnDB.data)) {
            return {
                data: null,
                error: "Email telah digunakan",
            }
        }

        const res = await supabaseClient.auth.signUp({
            ...restParams,
            options: {
                data: {
                    name,
                },
            },
        })

        if (!res.error) {
            queryClient.refetchQueries([AUTH_DATA_QUERY_KEY])
        }

        return res
    }
}
