import { AUTH_DATA_QUERY_KEY } from "@/services/auth-user/use-get-auth-data"
import supabaseClient from "@lib/supabase/client"
import { useMutation, useQueryClient } from "@tanstack/react-query"

import { SEEKER_USER_DATA_QUERY_KEY } from "../use-seeker-user-data"

export default function useLogoutSeeker() {
    const queryClient = useQueryClient()

    return useMutation(async () => await supabaseClient.auth.signOut(), {
        onSuccess() {
            queryClient.refetchQueries({ queryKey: [AUTH_DATA_QUERY_KEY] })
            queryClient.resetQueries({
                queryKey: [SEEKER_USER_DATA_QUERY_KEY],
                exact: true,
            })
        },
    })
}
