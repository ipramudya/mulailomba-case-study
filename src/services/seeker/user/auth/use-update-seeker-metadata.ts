import getUserProfilePicturePublicUrl from "@/services/auth-user/get-user-profile-picture-public-url"
import updateProfilePictureQuery from "@/services/auth-user/update-profile-picture.query"
import uploadProfilePictureQuery from "@/services/auth-user/upload-profile-picture.query"
import useAuthData, { AUTH_DATA_QUERY_KEY } from "@/services/auth-user/use-get-auth-data"
import { replaceSpaceWithUnderscore } from "@functions/format-strings"
import getFileExtension from "@functions/get-file-extension"
import supabaseClient from "@lib/supabase/client"
import { UpdateMetadataOrganizerParam } from "@organizerService/user/auth/use-update-metadata-organizer"
import { useQueryClient } from "@tanstack/react-query"
import toast from "react-hot-toast"

type UpdateSeekerMetadataParam = Omit<
    UpdateMetadataOrganizerParam,
    "about" | "social_whatsapp" | "social_instagram" | "profile_status"
>

export default function useUpdateSeekerMetadata() {
    const queryClient = useQueryClient()

    const { data: seekerData } = useAuthData()

    return async ({ profileImage, ...restParams }: UpdateSeekerMetadataParam) => {
        /* upload/update profile */
        let updatedProfileData: any, updatedProfileError: any
        if (seekerData && profileImage) {
            const { user } = seekerData

            if (user && user.user_metadata.profile_image_url) {
                const { data, error } = await updateProfilePictureQuery(
                    user.user_metadata.profile_image_url,
                    profileImage
                )

                if (data && !error) {
                    updatedProfileData = data
                } else {
                    updatedProfileError = error
                }
            } else {
                const constructFileName = `${replaceSpaceWithUnderscore(
                    user?.user_metadata.name
                )}/${user?.id}_profile.${getFileExtension(profileImage.name)}`

                const { data, error } = await uploadProfilePictureQuery(
                    constructFileName,
                    profileImage
                )

                if (data && !error) {
                    updatedProfileData = data
                } else {
                    updatedProfileError = error
                }
            }
        }

        if (updatedProfileError) {
            return {
                data: null,
                error: updatedProfileError,
            }
        }

        /* push updated/uploaded file into user meta data */
        const { data: updatedMetadata, error: errorUpdatedMetadata } =
            await supabaseClient.auth.updateUser({
                email: restParams.email,
                password: restParams.password,
                data: {
                    name: restParams.name,
                    location: restParams.location,
                    profile_image_url: updatedProfileData?.path,
                    phone: restParams.phone,
                    interest: restParams.interest,
                },
            })

        if (!updatedMetadata && errorUpdatedMetadata) {
            return {
                data: null,
                error: errorUpdatedMetadata.message,
            }
        }

        if (updatedMetadata.user) {
            const { name, location, phone, profile_image_url, interest } =
                updatedMetadata.user.user_metadata
            const profilePublicUrl =
                getUserProfilePicturePublicUrl(profile_image_url).data.publicUrl

            const { error: errorUpdatedSeekerData } = await supabaseClient
                .from("seeker")
                .update({
                    email: updatedMetadata.user.email,
                    name,
                    location,
                    phone,
                    updated_at: new Date().toISOString(),
                    profile_storage_url: profile_image_url,
                    profile_public_url: profilePublicUrl,
                    interest,
                })
                .eq("id", updatedMetadata.user.id)

            if (errorUpdatedSeekerData) {
                console.log("error", errorUpdatedSeekerData)
                toast.error("Gagal melakukan update profile, server sedang tidak stabil...")
                return
            }

            queryClient.refetchQueries([AUTH_DATA_QUERY_KEY])
            return { data: updatedMetadata, error: errorUpdatedMetadata }
        }
    }
}
