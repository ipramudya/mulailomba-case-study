import supabaseClient from "@lib/supabase/client"
import { useQuery } from "@tanstack/react-query"

const GET_REGISTRATION_FILES_QUERY_KEY = "get-registration-files"

export default function useGetRegistrationFiles(registrationId: string) {
    return useQuery({
        queryKey: [GET_REGISTRATION_FILES_QUERY_KEY],
        queryFn: async () => {
            const res = await supabaseClient
                .from("registrations_upload")
                .select("*")
                .eq("registration_id", registrationId)

            return res.data
        },
        refetchOnWindowFocus: false,
    })
}
