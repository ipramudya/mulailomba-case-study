import supabaseClient from "@lib/supabase/client"

export default function getFilesPublicURL(urlPath: string) {
    return supabaseClient.storage.from("files").getPublicUrl(urlPath)
}
