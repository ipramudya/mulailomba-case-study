import supabaseClient from "@lib/supabase/client"
import { useQueryClient } from "@tanstack/react-query"

import { CHECK_OWN_REGISTRATION } from "./use-check-own-registration"

type Payload = {
    lombaId: string
    seekerId: string
}

export default function useRegisterLomba() {
    const queryClient = useQueryClient()

    return async (payload: Payload) => {
        const res = await supabaseClient
            .from("registrations")
            .insert({
                created_at: new Date().toISOString(),
                updated_at: new Date().toISOString(),
                lomba_id: payload.lombaId,
                seeker_id: payload.seekerId,
            })
            .select()
            .single()

        if (res.status === 201 && res.data) {
            queryClient.refetchQueries([CHECK_OWN_REGISTRATION])

            supabaseClient
                .from("seeker")
                .select("registered_lomba")
                .eq("id", payload.seekerId)
                .single()
                .then(async (queryResponse) => {
                    if (queryResponse.data) {
                        const existing = queryResponse.data.registered_lomba
                        await supabaseClient
                            .from("seeker")
                            .update({
                                registered_lomba:
                                    existing === null ? [res.data.id] : [...existing, res.data.id],
                            })
                            .eq("id", payload.seekerId)
                    }
                })
        }

        return res
    }
}
