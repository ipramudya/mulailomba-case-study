import supabaseClient from "@lib/supabase/client"
import { useQuery } from "@tanstack/react-query"
import { isEmpty } from "lodash-es"

type Params = {
    lombaId: string
    seekerId: string
}

export const CHECK_OWN_REGISTRATION = "check-own-registration"

export default function useCheckOwnRegistration({ lombaId, seekerId }: Params) {
    return useQuery({
        queryKey: [CHECK_OWN_REGISTRATION],
        queryFn: async () => {
            const res = await supabaseClient
                .from("registrations")
                .select("*")
                .eq("lomba_id", lombaId)
                .eq("seeker_id", seekerId)

            if (res.status === 200) {
                if (!isEmpty(res.data)) {
                    return true
                } else {
                    return false
                }
            }

            return res
        },
        refetchOnWindowFocus: false,
        enabled: Boolean(lombaId) && Boolean(seekerId),
    })
}
