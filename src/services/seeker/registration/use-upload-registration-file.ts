import supabaseClient from "@lib/supabase/client"

type Payload = {
    seekerId: string
    fileName: string
    file: File
}

export default function useUploadRegistrationFile() {
    return async ({ file, fileName, seekerId }: Payload) => {
        if (!seekerId || !file)
            return { error: { message: "Terdapat data yang belum diisi" }, data: null }

        return supabaseClient.storage.from("files").upload(`${seekerId}/${fileName}`, file, {
            cacheControl: "3600",
            upsert: false,
        })
    }
}
