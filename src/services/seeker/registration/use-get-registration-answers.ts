import supabaseClient from "@lib/supabase/client"
import { useQuery } from "@tanstack/react-query"

const GET_REGISTRATION_ANSWERS_QUERY_KEY = "get-registration-answers"

export default function useGetRegistrationAnswers(registrationId: string) {
    return useQuery({
        queryKey: [GET_REGISTRATION_ANSWERS_QUERY_KEY],
        queryFn: async () => {
            const res = await supabaseClient
                .from("registrations_answer")
                .select("*")
                .eq("registration_id", registrationId)

            return res.data
        },
    })
}
