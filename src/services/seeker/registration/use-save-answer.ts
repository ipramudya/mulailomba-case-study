import supabaseClient from "@lib/supabase/client"

type Payload = {
    label: string
    value: string
    registrationId: string
}

export default function useSaveAnswer() {
    return async ({ label, registrationId, value }: Payload) => {
        return await supabaseClient
            .from("registrations_answer")
            .insert({
                label,
                value,
                registration_id: registrationId,
            })
            .select()
            .single()
    }
}
