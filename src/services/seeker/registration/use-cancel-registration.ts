import supabaseClient from "@lib/supabase/client"
import { useQueryClient } from "@tanstack/react-query"

import { GET_REGISTRATIONS_BY_SEEKER_QUERY_KEY } from "./use-get-registrations-by-seeker"

export default function useCancelRegistration() {
    const queryClient = useQueryClient()

    return async (registrationId: string) => {
        const res = await supabaseClient
            .from("registrations")
            .update({
                is_canceled: 1,
                updated_at: new Date().toISOString(),
            })
            .eq("id", registrationId)

        if (res.status === 204) {
            queryClient.refetchQueries([GET_REGISTRATIONS_BY_SEEKER_QUERY_KEY])
        }

        return res
    }
}
