import supabaseClient from "@lib/supabase/client"

type Payload = {
    fileStorageUrl: string
    filePublicUrl: string
    label: string
    registrationId: string
}

export default function useSaveUploadedFile() {
    return async ({ filePublicUrl, fileStorageUrl, label, registrationId }: Payload) => {
        return await supabaseClient
            .from("registrations_upload")
            .insert({
                file_public_url: filePublicUrl,
                file_storage_url: fileStorageUrl,
                registration_id: registrationId,
                label,
            })
            .select()
            .single()
    }
}
