import useAuthData from "@/services/auth-user/use-get-auth-data"
import supabaseClient from "@lib/supabase/client"
import { useQuery, useQueryClient } from "@tanstack/react-query"
import { isEmpty } from "lodash-es"
import { useEffect } from "react"

type Params = {
    registeredIds?: string[] | null
    filter?: string | null
    category?: string | null
    search?: string | null
}

export const GET_REGISTRATIONS_BY_SEEKER_QUERY_KEY = "get-registrations-by-seeker" as const

export default function useGetRegistrationsBySeeker({
    registeredIds,
    filter,
    category,
    search,
}: Params) {
    const queryClient = useQueryClient()
    const { data: auth } = useAuthData()

    useEffect(() => {
        if (auth && !auth.user) {
            queryClient.removeQueries({ queryKey: [GET_REGISTRATIONS_BY_SEEKER_QUERY_KEY] })
        }
    }, [auth, queryClient])

    return useQuery({
        queryKey: [GET_REGISTRATIONS_BY_SEEKER_QUERY_KEY, filter, category, search],
        queryFn: async () => {
            if (!registeredIds || (auth && auth.user === null)) return null

            let query = supabaseClient
                .from("registrations")
                .select(
                    "*, lomba!inner(*, organizer(*), lomba_timelines(*)), registrations_answer(*), registrations_upload(*)"
                )
                .in("id", registeredIds)

            if (search) {
                query = query.ilike("lomba.name", `%${search}%`)
            }

            if (filter) {
                if (filter === "verified") {
                    query = query.eq("is_verified", 1)
                } else {
                    query = query.eq("is_verified", 0)
                }
            }

            if (category) {
                query = query.eq("lomba.category", category)
            }

            const res = await query
            return res.data
        },
        enabled: Boolean(
            (!isEmpty(registeredIds) && registeredIds) || (auth && auth.user === null)
        ),
    })
}
