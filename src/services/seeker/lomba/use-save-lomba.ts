import supabaseClient from "@lib/supabase/client"
import { SEEKER_USER_DATA_QUERY_KEY } from "@seekerService/user/use-seeker-user-data"
import { useQueryClient } from "@tanstack/react-query"

type Payload = {
    lombaId: string
    seekerId: string
    alreadySavedLomba: string[] | null
}

export default function useAddOrRemoveLombaFromFavorite() {
    const queryClient = useQueryClient()

    return async ({ lombaId, seekerId, alreadySavedLomba }: Payload) => {
        let lombaTobeSaved: string[] = []
        if (alreadySavedLomba) {
            if (alreadySavedLomba.includes(lombaId)) {
                lombaTobeSaved = alreadySavedLomba.filter((s) => s !== lombaId)
            } else {
                lombaTobeSaved = alreadySavedLomba.concat(lombaId)
            }
        } else {
            lombaTobeSaved = [lombaId]
        }

        const res = await supabaseClient
            .from("seeker")
            .update({
                saved_lomba: lombaTobeSaved,
            })
            .eq("id", seekerId)

        if (res.status === 204) {
            queryClient.refetchQueries([SEEKER_USER_DATA_QUERY_KEY])
        }

        return res
    }
}
