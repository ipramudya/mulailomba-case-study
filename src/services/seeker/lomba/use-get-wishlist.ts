import useAuthData from "@/services/auth-user/use-get-auth-data"
import supabaseClient from "@lib/supabase/client"
import useSeekerUserData from "@seekerService/user/use-seeker-user-data"
import { useQuery, useQueryClient } from "@tanstack/react-query"
import { useEffect } from "react"
import { toast } from "react-hot-toast"

const GET_WISHLIST_QUERY_KEY = "get-wishlist"

export default function useGetWishlist() {
    const queryClient = useQueryClient()
    const { data: auth } = useAuthData()
    const { data: seekerData } = useSeekerUserData()

    useEffect(() => {
        if (auth && !auth.user) {
            queryClient.removeQueries({ queryKey: [GET_WISHLIST_QUERY_KEY] })
        }
    }, [auth, queryClient])

    return useQuery({
        queryKey: [GET_WISHLIST_QUERY_KEY],
        queryFn: async () => {
            if (!seekerData || (auth && auth.user === null)) return null

            if (seekerData.saved_lomba !== null) {
                const res = await supabaseClient
                    .from("lomba")
                    .select("*")
                    .in("id", seekerData.saved_lomba)

                if (res.status !== 200) {
                    toast.error("Gagal mendapatkan informasi wishlist lomba")
                    return null
                }

                return res.data
            }

            return null
        },
        refetchOnWindowFocus: false,
        enabled: Boolean(seekerData || (auth && auth.user === null && !seekerData)),
    })
}
