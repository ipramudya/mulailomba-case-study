import supabaseClient from "@lib/supabase/client"

import { GetAllOrganizersParams } from "."

export default async function getAllOrganizersQuery(params?: GetAllOrganizersParams) {
    let query = supabaseClient
        .from("organizer")
        .select("*, lomba(name)")
        .not("lomba.name", "is", null)
        .not("lomba.description", "is", null)
        .not("lomba.start_date", "is", null)
        .not("lomba.end_date", "is", null)
        .not("lomba.start_registration", "is", null)
        .not("lomba.end_registration", "is", null)
        .not("lomba.category", "is", null)
        .not("lomba.is_free", "is", null)
        .not("lomba.is_unlimited_participants", "is", null)
        .not("lomba.is_held_online", "is", null)
        .not("lomba.benefits", "is", null)
        .not("lomba.eligibilities", "is", null)
        .not("lomba.rules", "is", null)
        .not("lomba.poster_public_url", "is", null)
        .not("lomba.poster_storage_url", "is", null)
        .eq("lomba.is_archived", 0)

    if (params && params.category) {
        query = query.eq("profile_status", params.category)
    }

    if (params && params.search) {
        query = query.ilike("name", `%${params.search}%`)
    }

    return await query
}
