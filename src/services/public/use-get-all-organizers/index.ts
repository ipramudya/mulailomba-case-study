export { default } from "./use-get-all-organizers"
export const GET_ALL_ORGANIZERS_QUERY = "get-all-organizers"

export type GetAllOrganizersParams =
    | {
          category?: string
          search?: string
      }
    | undefined
