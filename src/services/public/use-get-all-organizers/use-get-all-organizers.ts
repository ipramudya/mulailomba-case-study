import { useQuery } from "@tanstack/react-query"

import { GET_ALL_ORGANIZERS_QUERY, GetAllOrganizersParams } from "."
import getAllOrganizersQuery from "./get-all-organizers.query"

export default function useGetAllOrganizers(params?: GetAllOrganizersParams) {
    return useQuery({
        queryKey: [
            GET_ALL_ORGANIZERS_QUERY,
            params ? params.category : undefined,
            params ? params.search : undefined,
        ],
        queryFn: () => getAllOrganizersQuery(params).then((res) => res.data),
        refetchOnWindowFocus: false,
    })
}
