import supabaseClient from "@lib/supabase/client"
import { useQuery } from "@tanstack/react-query"

export const GET_LIST_FILES_QUERY_KEY = "get-list-files-query-key"

export default function useGetListFiles(storageId: string) {
    return useQuery({
        queryKey: [GET_LIST_FILES_QUERY_KEY],
        queryFn: async () => {
            return await supabaseClient.storage.from("files").list(storageId, {
                sortBy: { column: "name", order: "asc" },
            })
        },
        refetchOnWindowFocus: false,
    })
}
