import supabaseClient from "@lib/supabase/client"
import { useQuery } from "@tanstack/react-query"

export const GET_DETAIL_FILE_QUERY_KEY = "get-detail-file"

export default function useGetDetailFile(seekerId: string, searchTerms: string) {
    return useQuery({
        queryKey: [GET_DETAIL_FILE_QUERY_KEY],
        queryFn: async () => {
            const res = await supabaseClient.storage.from("files").list(seekerId)

            return res.data
        },
        select(data) {
            if (data) {
                return data.find((d) => d.name === searchTerms)
            }
        },
        refetchOnWindowFocus: false,
    })
}
