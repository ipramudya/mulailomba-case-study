import supabaseClient from "@lib/supabase/client"
import { useQuery } from "@tanstack/react-query"
import { flatten } from "lodash-es"

export const GET_ALL_LOMBA_FILTER_OPTIONS = "get-all-lomba-filter-options"

type FilterOptions = {
    benefits: any[]
    eligibilities: any[]
}

export default function useGetLombaFilterOptions() {
    return useQuery({
        queryKey: [GET_ALL_LOMBA_FILTER_OPTIONS],
        queryFn: async () => {
            const res = await supabaseClient
                .from("lomba")
                .select("benefits,eligibilities")
                .not("name", "is", null)
                .not("description", "is", null)
                .not("start_date", "is", null)
                .not("end_date", "is", null)
                .not("start_registration", "is", null)
                .not("end_registration", "is", null)
                .not("category", "is", null)
                .not("is_free", "is", null)
                .not("is_unlimited_participants", "is", null)
                .not("is_held_online", "is", null)
                .not("benefits", "is", null)
                .not("eligibilities", "is", null)
                .not("rules", "is", null)
                .not("poster_public_url", "is", null)
                .not("poster_storage_url", "is", null)

            if (res.data) {
                const combinedEachEntries = res.data.reduce<FilterOptions>((acc, current) => {
                    if (!acc.benefits) {
                        if (current.benefits) {
                            acc.benefits = [current.benefits]
                        }
                    } else if (current.benefits) {
                        acc.benefits.push(current.benefits)
                    }

                    if (!acc.eligibilities) {
                        if (current.eligibilities) {
                            acc.eligibilities = [current.eligibilities]
                        }
                    } else if (current.eligibilities) {
                        acc.eligibilities.push(current.eligibilities)
                    }

                    return acc
                }, {} as FilterOptions)

                const flattenOptions: FilterOptions = { benefits: [], eligibilities: [] }
                for (const key in combinedEachEntries) {
                    // @ts-expect-error amy
                    flattenOptions[key] = [...new Set(flatten([...combinedEachEntries[key]]))]
                }

                return flattenOptions
            }
        },
        refetchOnWindowFocus: false,
    })
}
