import supabaseClient from "@lib/supabase/client"

export default function downloadFileStorage() {
    return async (storage: string, filePath: string) => {
        return await supabaseClient.storage.from(storage).download(filePath)
    }
}
