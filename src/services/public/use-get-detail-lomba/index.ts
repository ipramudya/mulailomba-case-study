export { default } from "./use-get-detail-lomba"
export const GET_DETAIL_LOMBA_QUERY = "get-detail-lomba"
export interface DetailLombaResponse {
    id: string
    created_at: string
    updated_at: string
    name: string
    description: string
    start_date: string
    end_date: string
    start_registration: string
    end_registration: string
    category: string
    is_free: number
    is_unlimited_participants: number
    total_participants: null
    is_held_online: number
    held_location: null
    held_meeting_link: string
    benefits: string[]
    eligibilities: string[]
    organizer_id: string
    rules: string
    poster_storage_url: string
    poster_public_url: string
    lomba_timelines: any[]
    lomba_prerequisites: null
    organizer: Organizer
}

export interface Organizer {
    name: string
    id: string
    profile_public_url: string
}

export interface Timeline {
    name: string | null
    description: string | null
    start_date: string | null
    end_date: string | null
    lomba_id: string | null
    id: string | null
}
