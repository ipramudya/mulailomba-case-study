import supabaseClient from "@lib/supabase/client"

export default async function getDetailLombaQuery(lombaId: string) {
    const query = supabaseClient
        .from("lomba")
        .select(
            `*, lomba_prerequisites(*), lomba_timelines(*), organizer (name, id, profile_public_url),
            registrations(*, registrations_answer(*), registrations_upload(*), seeker(name, phone, location, profile_public_url))`
        )
        .eq("id", lombaId)
        .order("start_date", { foreignTable: "lomba_timelines", ascending: true })

    return await query.single()
}
