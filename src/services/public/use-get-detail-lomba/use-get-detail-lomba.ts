import { useQuery } from "@tanstack/react-query"

import { GET_DETAIL_LOMBA_QUERY } from "."
import getDetailLombaQuery from "./get-detail-lomba.query"

export type GetDetailLombaResponse = Awaited<ReturnType<typeof useGetDetailLomba>>

export default function useGetDetailLomba(lombaId: string) {
    return useQuery({
        queryKey: [GET_DETAIL_LOMBA_QUERY, lombaId],
        queryFn: ({ queryKey }) => getDetailLombaQuery(queryKey[1]).then((res) => res.data),
    })
}
