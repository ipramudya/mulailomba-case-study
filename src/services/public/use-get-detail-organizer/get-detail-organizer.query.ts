import supabaseClient from "@lib/supabase/client"

export default async function getDetailOrganizerQuery(organizerId: string) {
    return await supabaseClient.from("organizer").select().eq("id", organizerId).single()
}
