import { useQuery } from "@tanstack/react-query"

import { ORGANIZER_DETAIL_QUERY } from "."
import getDetailOrganizerQuery from "./get-detail-organizer.query"

export default function useGetDetailOrganizer(organizerId: string) {
    return useQuery({
        queryKey: [ORGANIZER_DETAIL_QUERY],
        queryFn: () => getDetailOrganizerQuery(organizerId).then((res) => res.data),
        enabled: Boolean(organizerId),
    })
}
