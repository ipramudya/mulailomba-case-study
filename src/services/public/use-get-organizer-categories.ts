import supabaseClient from "@lib/supabase/client"
import { useQuery } from "@tanstack/react-query"

export const GET_ORGANIZER_CATEGORIES = "use-get-organizer-categories"

export default function useGetOrganizerCategories() {
    return useQuery({
        queryKey: [GET_ORGANIZER_CATEGORIES],
        queryFn: async () => {
            const res = await supabaseClient
                .from("organizer")
                .select("profile_status")
                .not("profile_status", "is", null)

            if (res.data) {
                return Array.from(new Set(res.data.map((obj) => obj.profile_status)))
            }
        },
        refetchOnWindowFocus: false,
    })
}
