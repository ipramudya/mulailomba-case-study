import supabaseClient from "@lib/supabase/client"
import { isEmpty } from "lodash-es"

type Payload = {
    emailOrUsername: string
    rating: string
    feedback: string
    category: string
    status: string
}

export default function useSendFeedback() {
    return async ({ emailOrUsername, ...restPayload }: Payload) => {
        const { data: emailOrUsernameExist, error: foundEmailOrUsernameError } =
            await supabaseClient
                .from("feedback")
                .select("email_or_username")
                .eq("email_or_username", emailOrUsername)

        if (!isEmpty(emailOrUsernameExist)) {
            return {
                data: null,
                error: "Email atau Username telah digunakan",
            }
        }

        if (foundEmailOrUsernameError) {
            return {
                data: null,
                error: foundEmailOrUsernameError,
            }
        }

        return await supabaseClient.from("feedback").insert({
            ...restPayload,
            email_or_username: emailOrUsername,
        })
    }
}
