import { useQuery } from "@tanstack/react-query"

import { GET_ALL_LOMBA_QUERY, GetAllLombaParams } from "."
import getAllLombaQuery from "./get-all-lomba.query"

export default function useGetAllLomba(params?: GetAllLombaParams) {
    return useQuery({
        queryKey: [
            GET_ALL_LOMBA_QUERY,
            params ? params.category : undefined,
            params ? params.filter : undefined,
            params ? params.search : undefined,
            params ? params.in : undefined,
        ],
        queryFn: async () => {
            const allLomba = (await getAllLombaQuery(params)).data
            return allLomba
        },
        refetchOnWindowFocus: false,
    })
}
