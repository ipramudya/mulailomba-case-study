export { default } from "./use-get-all-lomba"
export const GET_ALL_LOMBA_QUERY = "get-all-lomba"
export type GetAllLombaParams =
    | {
          organizerId?: string
          category?: string
          filter?: {
              [key in "benefits" | "eligibilities" | "is_held_online" | "is_free"]?: string
          }
          search?: string
          in?: string[]
      }
    | undefined
