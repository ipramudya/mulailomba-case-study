import supabaseClient from "@lib/supabase/client"

import { GetAllLombaParams } from "."

export default async function getAllLombaQuery(params?: GetAllLombaParams) {
    let query = supabaseClient
        .from("lomba")
        .select("*")
        .not("name", "is", null)
        .not("description", "is", null)
        .not("start_date", "is", null)
        .not("end_date", "is", null)
        .not("start_registration", "is", null)
        .not("end_registration", "is", null)
        .not("category", "is", null)
        .not("is_free", "is", null)
        .not("is_unlimited_participants", "is", null)
        .not("is_held_online", "is", null)
        .not("benefits", "is", null)
        .not("eligibilities", "is", null)
        .not("rules", "is", null)
        .not("poster_public_url", "is", null)
        .not("poster_storage_url", "is", null)
        .eq("is_archived", 0)

    if (params && params.organizerId) {
        query = query.eq("organizer_id", params.organizerId)
    }

    if (params && params.category) {
        if (params.category !== "all") {
            query = query.eq("category", params.category)
        }
    }

    if (params && params.filter) {
        let key: keyof typeof params.filter
        for (key in params.filter) {
            if (params.filter[key]) {
                if (["eligibilities", "benefits"].includes(key)) {
                    query = query.contains(key, [params.filter[key]])
                } else {
                    query = query.eq(key, params.filter[key] as string)
                }
            }
        }
    }

    if (params && params.in) {
        query = query.in("category", params.in)
    }

    if (params && params.search) {
        query = query.ilike("name", `%${params.search}%`)
    }

    return await query
}
