import supabaseClient from "@lib/supabase/client"

export default function getPublicURL(storage: string, filePath: string) {
    return supabaseClient.storage.from(storage).getPublicUrl(filePath)
}
