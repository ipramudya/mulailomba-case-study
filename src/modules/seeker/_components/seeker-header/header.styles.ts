const headerStyles = {
    primary: {
        logo: "text-primary-500",
        links: "primary",
        buttonPriority: "primary",
        buttonSecond: "primary-low",
    },
    secondary: {
        logo: "text-white",
        links: "light",
        buttonPriority: "primary-darken",
        buttonSecond: "primary-bordered",
    },
} as const

export default headerStyles
export type HeaderStyles = keyof typeof headerStyles
