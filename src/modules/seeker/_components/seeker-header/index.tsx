import useAuthData from "@/services/auth-user/use-get-auth-data"
import Anchor from "@ui/anchor"
import Container from "@ui/container"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"
import { Link } from "react-router-dom"

import EntryButtons from "./entry-buttons"
import headerStyles, { HeaderStyles } from "./header.styles"

const navlinks = [
    {
        to: "/",
        label: "Home",
    },
    {
        to: "/lomba",
        label: "Lomba",
    },
    {
        to: "/organizer",
        label: "Organizer",
    },
] as const

interface Props {
    variant: HeaderStyles
}

const SeekerHeader: FunctionComponent<Props> = ({ variant }) => {
    const { data } = useAuthData()

    return (
        <Container size="lg" as="div" className=" py-4">
            <div className="flex items-center justify-between">
                {/* 👇 links */}
                <div className="flex items-center">
                    {/* 👇 logo */}
                    <Link to="/">
                        <Typography
                            size="h3"
                            weight={600}
                            as="h2"
                            className={headerStyles[variant].logo}
                        >
                            MulaiLomba
                        </Typography>
                    </Link>

                    {/* 👇 links */}
                    {data === undefined ? (
                        <div className="flex space-x-6 pl-8">
                            {Array.from({ length: 3 }).map((_, idx) => (
                                <Skeleton
                                    key={`nav-links-skeleton-${idx}`}
                                    className="h-[24px] w-[62px]"
                                />
                            ))}
                        </div>
                    ) : (
                        <ul className="flex space-x-6 pl-8 text-white">
                            {navlinks.map((n, idx) =>
                                n.label === "Lomba" && !data.user ? (
                                    false
                                ) : (
                                    <li key={`nav-link-${idx}`}>
                                        <Anchor
                                            to={n.to}
                                            intent={headerStyles[variant].links}
                                            size="sm"
                                            weight={500}
                                        >
                                            {n.label}
                                        </Anchor>
                                    </li>
                                )
                            )}
                        </ul>
                    )}
                </div>

                {/* 👇 entry buttons */}
                <EntryButtons variant={variant} />
            </div>
        </Container>
    )
}

export default SeekerHeader
