import useAuthData from "@/services/auth-user/use-get-auth-data"
import Button from "@ui/button"
import RouterLink from "@ui/router-link"
import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"

import headerStyles, { HeaderStyles } from "../header.styles"
import ProfileButton from "./profile-button"

interface Props {
    variant: HeaderStyles
}
const EntryButtons: FunctionComponent<Props> = ({ variant }) => {
    const { data } = useAuthData()

    return data === undefined ? (
        <Skeleton className="h-[32px] w-[160px]" />
    ) : (
        <>
            {data.user ? (
                <ProfileButton label={data.user?.user_metadata.name} />
            ) : (
                <div className="flex space-x-3">
                    <RouterLink to="/entry/login">
                        <Button
                            label="Masuk"
                            intent={headerStyles[variant].buttonPriority}
                            pill
                            className="min-w-[80px]"
                        />
                    </RouterLink>
                    <RouterLink to="/entry/register">
                        <Button
                            label="Daftar"
                            intent={headerStyles[variant].buttonSecond}
                            pill
                            className="min-w-[80px] border-transparent"
                        />
                    </RouterLink>
                </div>
            )}
        </>
    )
}

export default EntryButtons
