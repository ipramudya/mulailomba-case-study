import getUserProfilePicturePublicUrl from "@/services/auth-user/get-user-profile-picture-public-url"
import useAuthData from "@/services/auth-user/use-get-auth-data"
import { checkExistanceKeys } from "@functions/check-existance-object"
import avatar from "@functions/generate-avatar"
import useLogoutSeeker from "@seekerService/user/auth/use-logout-seeker"
import Button from "@ui/button"
import buttonStyles from "@ui/button/button.styles"
import Popover from "@ui/popover"
import Typography from "@ui/typography"
import { FunctionComponent, useMemo, useState } from "react"
import Skeleton from "react-loading-skeleton"
import { useNavigate } from "react-router-dom"
import { twMerge } from "tailwind-merge"

import SeekerSettingsModal from "../../seeker-settings"

interface Props {
    label: string
}

const ProfileButton: FunctionComponent<Props> = ({ label }) => {
    /* standalone state */
    const [isSettingModalOpen, setIsSettingModalOpen] = useState(false)

    /* API calls */
    const seekerLogout = useLogoutSeeker()
    const { data: seekerData } = useAuthData()
    const isOrganizer = seekerData?.user?.user_metadata.access_role === "organizer"

    /* router stuffs */
    const navigate = useNavigate()

    const onLogout = () => {
        seekerLogout.mutate()
    }

    const checkIsUserInfoComplete = (): boolean | null => {
        if (seekerData && seekerData.user && seekerData.user.user_metadata) {
            const desiredKeys = ["location", "name", "phone"]

            return checkExistanceKeys(seekerData.user.user_metadata, desiredKeys)
        }

        return null
    }

    const imageSource = useMemo(() => {
        if (seekerData) {
            if (seekerData.user?.app_metadata.provider === "google") {
                return seekerData.user.user_metadata?.avatar_url
            } else {
                return getUserProfilePicturePublicUrl(
                    seekerData.user?.user_metadata.profile_image_url
                ).data.publicUrl
            }
        }

        return avatar
    }, [seekerData])

    return (
        <>
            {isSettingModalOpen && (
                <SeekerSettingsModal
                    isOpen={isSettingModalOpen}
                    onOpenChange={setIsSettingModalOpen}
                />
            )}
            {checkIsUserInfoComplete() === null || !seekerData || !seekerData.user ? (
                <Skeleton className="h-[32px] w-[180px] " />
            ) : (
                <Popover
                    className="rounded-md border border-neutral-50 px-2 py-4 shadow-menu"
                    triggerEl={
                        <button
                            className={twMerge(
                                buttonStyles({ intent: "primary-low", pill: true, size: "sm" }),
                                "relative min-w-[96px] justify-start",
                                !checkIsUserInfoComplete()
                                    ? "before:absolute before:top-[-2px] before:right-[-2px] before:h-[14px] before:w-[14px] before:rounded-full before:bg-danger-400 before:shadow before:content-['']"
                                    : undefined
                            )}
                        >
                            <div className="aspect-square w-6 overflow-hidden rounded-full border-[2px] border-white">
                                <img
                                    src={imageSource}
                                    alt="profil organizer"
                                    className="w-full object-cover"
                                    referrerPolicy="no-referrer"
                                />
                            </div>
                            <Typography
                                size="sm"
                                weight={500}
                                as="span"
                                className="text-primary-700"
                            >
                                {label}
                            </Typography>
                        </button>
                    }
                >
                    <div className="flex flex-col space-y-2">
                        <Button
                            thin
                            label="Perbarui Profil"
                            size="xs"
                            intent="default"
                            className="min-w-[180px] justify-start"
                            onClick={() => setIsSettingModalOpen(true)}
                            hasDot={!checkIsUserInfoComplete() as boolean}
                        />
                        {isOrganizer && (
                            <Button
                                thin
                                label="Dasboard Organizer"
                                size="xs"
                                intent="default"
                                className="min-w-[180px] justify-start"
                                onClick={() => navigate("/dashboard")}
                            />
                        )}
                        {!isOrganizer && (
                            <>
                                <hr className="border-neutral-100" />
                                <Button
                                    thin
                                    label="Lomba Tersimpan"
                                    size="xs"
                                    intent="default"
                                    className="min-w-[180px] justify-start"
                                    onClick={() => navigate("/?tab=wishlist")}
                                    iconSize="sm"
                                    startIcon="heroicons:bookmark"
                                />
                                <Button
                                    thin
                                    label="Lomba Teregistrasi"
                                    size="xs"
                                    intent="default"
                                    className="min-w-[180px] justify-start"
                                    onClick={() => navigate("/?tab=registered")}
                                    iconSize="sm"
                                    startIcon="heroicons:document-check"
                                />
                            </>
                        )}
                        <hr className="border-neutral-100" />
                        <Button
                            thin
                            label="Keluar"
                            size="xs"
                            intent="danger-ghost"
                            className="min-w-[180px] justify-start"
                            onClick={onLogout}
                        />
                    </div>
                </Popover>
            )}
        </>
    )
}

export default ProfileButton
