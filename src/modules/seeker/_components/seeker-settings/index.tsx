import getUserProfilePicturePublicUrl from "@/services/auth-user/get-user-profile-picture-public-url"
import useAuthData from "@/services/auth-user/use-get-auth-data"
import avatar from "@functions/generate-avatar"
import useDropImage from "@hooks/use-drop-image"
import useUpdateSeekerMetadata from "@seekerService/user/auth/use-update-seeker-metadata"
import Badge from "@ui/badge"
import Button from "@ui/button"
import Form from "@ui/form"
import Modal from "@ui/modal"
import Typography from "@ui/typography"
import { FunctionComponent, useEffect, useMemo, useState } from "react"
import { SubmitHandler, useForm } from "react-hook-form"
import { toast } from "react-hot-toast"
import { twMerge } from "tailwind-merge"

import SeekerSettingsSkeleton from "./seeker-settings-skeleton"

const profileInterest = [
    "Pendidikan",
    "Musik dan Hiburan",
    "Sains dan Teknologi",
    "Game Organizer",
    "Sastra dan Bahasa",
    "Olahraga",
]

type FormFields = {
    email: string
    username: string
    telp: string
    password: string
    confirmPassword: string
    location: string
}

interface Props {
    isOpen: boolean
    onOpenChange(val: boolean): void
}

const SeekerSettingsModal: FunctionComponent<Props> = ({ isOpen, onOpenChange }) => {
    /* Local States */
    const [interest, setInterest] = useState<string[]>([])

    /* API calls */
    const { data: seekerData, isLoading: isSeekerDataLoading } = useAuthData()
    const updateSeekerData = useUpdateSeekerMetadata()

    /* hook form */
    const methods = useForm<FormFields>()

    /* side effect to update interest state */
    useEffect(() => {
        if (seekerData && seekerData.user) {
            if (!seekerData.user.user_metadata.interest) {
                setInterest([])
                return
            }

            setInterest(seekerData.user.user_metadata.interest)
        }
    }, [seekerData])

    /* upload image dropzone */
    const {
        uploaded,
        getRootProps,
        getInputProps,
        dropzoneVars: { open },
    } = useDropImage({ noClick: true })

    const imageProfileSource = useMemo(() => {
        let profileImageSource
        if (uploaded) {
            profileImageSource = uploaded.preview
        } else if (
            seekerData &&
            seekerData.user &&
            seekerData.user.user_metadata &&
            seekerData.user.user_metadata.profile_image_url
        ) {
            profileImageSource = getUserProfilePicturePublicUrl(
                seekerData.user?.user_metadata.profile_image_url
            ).data.publicUrl
        } else {
            profileImageSource = avatar
        }

        return profileImageSource
    }, [seekerData, uploaded])

    const onUpdateProfileSubmit: SubmitHandler<FormFields> = async ({
        email,
        username,
        telp,
        location,
        confirmPassword,
        password,
    }) => {
        if (password) {
            if (password !== confirmPassword) return toast.error("Password tidak sesuai")
        }

        const res = await updateSeekerData({
            email,
            name: username,
            phone: telp,
            location,
            password: password === "" ? undefined : password,
            profileImage: uploaded ? uploaded.file : undefined,
            interest,
        })

        if (res) {
            if (res.data && !res.error) {
                toast.success(`Profil ${res.data.user?.user_metadata.name} berhasil diperbarui`)
            } else {
                toast.error("Gagal memperbarui profil, silahkan coba lagi")
            }
        }
    }

    const onSetInterest = (val: string) => {
        setInterest((prev) => {
            if (prev.includes(val)) {
                return prev.filter((i) => i !== val)
            }

            return [...prev, val]
        })
    }

    return (
        <Modal
            title={false}
            open={isOpen}
            onOpenChange={onOpenChange}
            classNames={{
                content:
                    "restyle-scrollbar max-h-[642px] min-w-[628px] overflow-x-hidden overflow-y-auto pb-0",
            }}
        >
            {isSeekerDataLoading || !seekerData ? (
                <SeekerSettingsSkeleton />
            ) : (
                <Form
                    {...methods}
                    onSubmit={onUpdateProfileSubmit}
                    className="relative flex w-full flex-col space-y-4"
                >
                    {/* title */}
                    <div className="border-b border-neutral-100 pb-4">
                        <Typography weight={500}>Pengaturan</Typography>
                    </div>

                    {/* photo */}
                    {seekerData.user?.app_metadata.provider === "google" ? (
                        <div className="aspect-square w-[86px] overflow-hidden rounded-full">
                            <img
                                src={seekerData.user.user_metadata?.avatar_url}
                                alt="foto profil organizer"
                                className="w-full object-cover"
                            />
                        </div>
                    ) : (
                        <div
                            {...getRootProps({
                                className:
                                    "flex flex-col space-y-3 border-b border-neutral-100 pb-4",
                            })}
                        >
                            <Typography size="sm">Foto</Typography>
                            <div className="aspect-square w-[86px] overflow-hidden rounded-full">
                                <img
                                    src={imageProfileSource}
                                    alt="foto profil organizer"
                                    className="w-full object-cover"
                                    referrerPolicy="no-referrer"
                                />
                            </div>
                            <input {...getInputProps()} />
                            <Button
                                size="sm"
                                intent="primary-bordered"
                                className="w-fit"
                                label="Unggah Foto"
                                onClick={() => open()}
                            />
                        </div>
                    )}

                    {/* personal info */}
                    <div
                        className={twMerge(
                            "flex flex-col space-y-3",
                            seekerData.user?.app_metadata.provider !== "google" &&
                                "border-b border-neutral-100 pb-4"
                        )}
                    >
                        <Typography size="sm">Informasi personal</Typography>
                        <div className="flex flex-col space-y-1">
                            <Typography size="xs" className="text-neutral-400">
                                Email
                            </Typography>
                            <Form.TextField
                                name="email"
                                label={false}
                                size="sm"
                                classNames={{
                                    wrapper:
                                        "py-1 px-0 border-none min-h-fit focus-within:outline-none focus-within:outline-0",
                                }}
                                defaultValue={seekerData.user?.email}
                            />
                        </div>

                        <div className="flex flex-col space-y-1">
                            <Typography size="xs" className="text-neutral-400">
                                Nama pengguna
                            </Typography>
                            <Form.TextField
                                name="username"
                                label={false}
                                size="sm"
                                classNames={{
                                    wrapper:
                                        "py-1 px-0 border-none min-h-fit max-w-fit focus-within:outline-none focus-within:outline-0",
                                }}
                                defaultValue={seekerData.user?.user_metadata.name}
                            />
                        </div>

                        <div className="flex flex-col space-y-1">
                            <Typography size="xs" className="text-neutral-400">
                                Nomor Telepon
                            </Typography>
                            <Form.TextField
                                name="telp"
                                label={false}
                                size="sm"
                                classNames={{
                                    wrapper:
                                        "py-1 px-0 border-none min-h-fit max-w-fit focus-within:outline-none focus-within:outline-0",
                                }}
                                placeholder="+628xx-xxxx-xxx"
                                defaultValue={seekerData.user?.user_metadata.phone}
                            />
                        </div>

                        <div className="flex flex-col space-y-1">
                            <Typography size="xs" className="text-neutral-400">
                                Lokasi
                            </Typography>
                            <Form.TextField
                                name="location"
                                label={false}
                                size="sm"
                                classNames={{
                                    wrapper:
                                        "py-1 px-0 border-none min-h-fit focus-within:outline-none focus-within:outline-0",
                                }}
                                placeholder="Lokasi organisasi kamu"
                                defaultValue={seekerData.user?.user_metadata.location}
                            />
                        </div>

                        {/* status/profesi */}
                        <div className="flex flex-col space-y-3 border-t border-neutral-100 pt-4">
                            <div>
                                <Typography size="sm">Minat</Typography>
                                <Typography size="xs" className="mt-1 text-neutral-400">
                                    Tentukan minat anda agar kami dapat merekomendasikan lomba yang
                                    cocok untuk anda
                                </Typography>
                            </div>
                            <div className="flex flex-wrap gap-x-2 gap-y-3">
                                {profileInterest.map((s, idx) => (
                                    <Badge
                                        intent={interest.includes(s) ? "primary" : "bordered"}
                                        asElement="button"
                                        size="sm"
                                        label={s}
                                        key={"profile-status-" + idx}
                                        onClick={() => onSetInterest(s)}
                                    />
                                ))}
                            </div>
                        </div>
                    </div>

                    {/* password */}
                    {seekerData.user?.app_metadata.provider === "google" ? (
                        false
                    ) : (
                        <div className="flex flex-col space-y-3 pb-4">
                            <div>
                                <Typography size="sm">Password</Typography>
                                <Typography size="xs" className="mt-1 text-neutral-400">
                                    Agar lebih aman gunakan kata sandi dengan panjang minimal 15
                                    huruf, atau minimal 8 karakter dengan huruf dan angka.
                                </Typography>
                            </div>
                            <Form.RowLayout column={2}>
                                <Form.TextField
                                    name="password"
                                    type="password"
                                    label={false}
                                    size="xs"
                                    classNames={{
                                        input: "bg-transparent",
                                        wrapper:
                                            "p-2 border-neutral-100 min-h-fit bg-neutral-50 focus-within:outline-none focus-within:outline-0",
                                    }}
                                    placeholder="Masukan password baru"
                                />
                                <Form.TextField
                                    name="confirmPassword"
                                    type="password"
                                    label={false}
                                    size="xs"
                                    classNames={{
                                        input: "bg-transparent",
                                        wrapper:
                                            "p-2 border-neutral-100 min-h-fit bg-neutral-50 focus-within:outline-none focus-within:outline-0",
                                    }}
                                    placeholder="Tulis ulang password baru"
                                />
                            </Form.RowLayout>
                        </div>
                    )}

                    <div className="sticky bottom-0 z-10 flex justify-end space-x-3 border-t border-neutral-100 bg-white py-4">
                        <Button
                            intent="bordered"
                            label="Batalkan"
                            size="sm"
                            onClick={() => onOpenChange(false)}
                        />
                        <Button
                            type="submit"
                            label="Perbarui"
                            size="sm"
                            loading={methods.formState.isSubmitting}
                            disabled={methods.formState.isSubmitting}
                        />
                    </div>
                </Form>
            )}
        </Modal>
    )
}

export default SeekerSettingsModal
