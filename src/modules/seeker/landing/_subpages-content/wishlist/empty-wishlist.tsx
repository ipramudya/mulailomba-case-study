import NotFound from "@blocks/not-found"
import AnchorButton from "@ui/anchor-button"
import { FunctionComponent } from "react"

interface Props {
    status: "not-logged" | "empty"
}

const EmptyWishlist: FunctionComponent<Props> = ({ status }) => (
    <div className="col-span-3">
        <NotFound
            icon="heroicons:arrow-left-on-rectangle"
            spacingY={false}
            message={
                status === "empty"
                    ? "Wishlist kamu masih kosong, ketuk tombol dibawah untuk mulai bereksplorasi dan tambahkan wishlist favorit kamu"
                    : "Silahkan masuk terlebih dahulu, kemudian mulai menambahkan wishlist"
            }
        >
            {status === "empty" && (
                <AnchorButton
                    tag="Link"
                    href="/?tab=explore"
                    size="sm"
                    label="Mulai mencari lomba"
                    intent="primary"
                    pill
                />
            )}
        </NotFound>
    </div>
)

export default EmptyWishlist
