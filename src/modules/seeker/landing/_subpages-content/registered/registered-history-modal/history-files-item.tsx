import mimetypeIcon from "@constant/plot-mimetype-icon"
import formatBytes from "@functions/format-bytes"
import { formatDate } from "@functions/format-date"
import getFileExtension from "@functions/get-file-extension"
import { Icon } from "@iconify/react"
import DetailFilesMenu from "@modules/dashboard/lomba-detail/main-detail/detail-files-modal/detail-files-menu"
import useGetDetailFile from "@publicService/use-get-detail-file"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"

interface Props {
    storageUrl: string
}

const HistoryFilesItem: FunctionComponent<Props> = ({ storageUrl }) => {
    const [seekerId, fileName] = storageUrl.split("/")
    const { data: detailFile } = useGetDetailFile(seekerId, fileName)

    return (
        <>
            {!detailFile ? (
                <Skeleton className="col-span-1 min-h-[72px]" />
            ) : (
                <div className="flex flex-col space-y-4 rounded-md border border-neutral-100/50 p-3">
                    <div className="flex items-center justify-between">
                        <Icon
                            icon={
                                !detailFile || !detailFile.name
                                    ? "bi:file-earmark-text"
                                    : (mimetypeIcon.get(getFileExtension(detailFile.name) as any)
                                          ?.icon as string)
                            }
                            className={`h-7 w-7 ${
                                mimetypeIcon.get(getFileExtension(detailFile.name) as any)?.color
                            }`}
                        />
                        <DetailFilesMenu
                            filePath={`${seekerId}/${detailFile.name}`}
                            fileName={detailFile.name}
                        />
                    </div>
                    <div className="flex flex-col space-y-2">
                        <Typography size="sm" weight={500} className="truncate ">
                            {detailFile.name}
                        </Typography>
                        <div className="flex items-center space-x-2">
                            <Typography size="xs" className="text-neutral-400">
                                {formatDate(detailFile.created_at, "dd MMM yyy")}
                            </Typography>
                            <div className="h-1 w-1 rounded-full bg-neutral-300" />
                            <Typography size="xs" className="text-neutral-400">
                                {formatBytes(detailFile.metadata.size)}
                            </Typography>
                        </div>
                    </div>
                </div>
            )}
        </>
    )
}

export default HistoryFilesItem
