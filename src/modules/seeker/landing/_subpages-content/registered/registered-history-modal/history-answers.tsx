import NotFound from "@blocks/not-found"
import { Icon } from "@iconify/react"
import useGetRegistrationAnswers from "@seekerService/registration/use-get-registration-answers"
import Button from "@ui/button"
import Paper from "@ui/paper"
import Typography from "@ui/typography"
import { useSetAtom } from "jotai"
import { isEmpty } from "lodash-es"
import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"

import { RegisteredHistoryModalChooser } from "."

interface Props {
    registrationId: string
}

const HistoryAnswers: FunctionComponent<Props> = ({ registrationId }) => {
    const setRegisteredHistoryModalChooser = useSetAtom(RegisteredHistoryModalChooser)

    const {
        data: registrationAnswer,
        isLoading,
        isFetching,
    } = useGetRegistrationAnswers(registrationId)

    return (
        <div className="flex flex-col space-y-4">
            <hr className="border-neutral-100" />
            <Typography size="sm">Jawaban Terkirim</Typography>
            {isLoading || isFetching ? (
                <>
                    {Array.from({ length: 4 }).map((_, idx) => (
                        <Skeleton className="min-h-[62px]" key={"registered-files-" + idx} />
                    ))}
                </>
            ) : (
                <>
                    {isEmpty(registrationAnswer) || !registrationAnswer ? (
                        <NotFound message="Berkas pendaftaran kosong" spacingY={false} />
                    ) : (
                        <>
                            {registrationAnswer.map((file, idx) => (
                                <Paper key={file.id + idx}>
                                    <div className="flex space-x-2">
                                        <Icon
                                            icon="fluent:pin-20-filled"
                                            className="h-5 w-5 text-primary-500"
                                        />
                                        <Typography size="sm" weight={500}>
                                            {file.label}
                                        </Typography>
                                    </div>
                                    <Typography size="xs">{file.value}</Typography>
                                </Paper>
                            ))}
                        </>
                    )}
                </>
            )}
            <Button
                size="xs"
                label="Kembali"
                intent="bordered"
                className="w-fit"
                startIcon="heroicons:chevron-left-20-solid"
                onClick={() => setRegisteredHistoryModalChooser(null)}
            />
        </div>
    )
}

export default HistoryAnswers
