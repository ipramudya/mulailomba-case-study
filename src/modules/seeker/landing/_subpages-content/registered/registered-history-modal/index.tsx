import IconButton from "@ui/icon-button"
import Modal from "@ui/modal"
import Typography from "@ui/typography"
import { atom, useAtom } from "jotai"
import { FunctionComponent, useEffect } from "react"

import Chooser from "./chooser"
import HistoryAnswers from "./history-answers"
import HistoryFiles from "./history-files"

export const RegisteredHistoryModalChooser = atom<"answer" | "files" | null>(null)

interface Props {
    isOpen: boolean
    onOpenChange(val: boolean): void
    registrationId: string
}

const RegisteredHistoryModal: FunctionComponent<Props> = ({
    isOpen,
    onOpenChange,
    registrationId,
}) => {
    const [registeredHistoryModalChooser, setRegisteredHistoryModalChooser] = useAtom(
        RegisteredHistoryModalChooser
    )

    useEffect(() => {
        return function () {
            setRegisteredHistoryModalChooser(null)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <Modal open={isOpen} onOpenChange={onOpenChange} title={false}>
            <div className="flex flex-col space-y-4">
                <div className="flex items-center justify-between">
                    <div className="flex flex-col space-y-1">
                        <Typography weight={500}>Riwayat Pendaftaran</Typography>
                        <Typography size="xs">
                            Berikut adalah riwayat data yang anda masukan sebelum <br /> mendaftar
                            perlombaan
                        </Typography>
                    </div>
                    <IconButton
                        aria-label="tutup"
                        icon="heroicons:x-circle"
                        size="sm"
                        iconSize="md"
                        intent="danger-ghost"
                        circle
                        onClick={() => {
                            onOpenChange(false)
                        }}
                    />
                </div>
                {registeredHistoryModalChooser === null ? (
                    <Chooser />
                ) : (
                    <>
                        {registeredHistoryModalChooser === "files" ? (
                            <HistoryFiles registrationId={registrationId} />
                        ) : (
                            <HistoryAnswers registrationId={registrationId} />
                        )}
                    </>
                )}
            </div>
        </Modal>
    )
}

export default RegisteredHistoryModal
