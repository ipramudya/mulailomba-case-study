import { Icon } from "@iconify/react"
import Typography from "@ui/typography"
import { useAtom } from "jotai"
import { FunctionComponent } from "react"

import { RegisteredHistoryModalChooser } from "."

const Chooser: FunctionComponent = () => {
    const [registeredHistoryModalChooser, setRegisteredHistoryModalChooser] = useAtom(
        RegisteredHistoryModalChooser
    )

    return (
        <div>
            <div className="grid grid-cols-2 gap-x-6">
                <div
                    className="col-span-1 aspect-[3/2] cursor-pointer rounded-lg bg-primary-50 p-[6px] transition duration-200 hover:bg-primary-50/80 "
                    onClick={() => {
                        if (!registeredHistoryModalChooser) {
                            setRegisteredHistoryModalChooser("files")
                        }
                    }}
                >
                    <div className="flex h-full grow flex-col items-center justify-start space-y-4 rounded-lg border border-primary-500 px-3 pb-3 pt-4 ">
                        <Icon
                            icon="heroicons:document-text-solid"
                            className="h-7 w-7 text-primary-600"
                        />
                        <div className="flex flex-col items-center justify-center space-y-[2px]">
                            <Typography className="text-primary-600" weight={500}>
                                Lihat Berkas
                            </Typography>
                            <Typography
                                className="max-w-[200px] text-center text-primary-600"
                                size="xs"
                            >
                                Berkas yang pernah dikirimkan sebagai persyaratan pendaftaran lomba
                                ini
                            </Typography>
                        </div>
                    </div>
                </div>

                <div
                    className="col-span-1 aspect-[3/2] cursor-pointer rounded-lg bg-kale-50 p-[6px] transition duration-200 hover:bg-kale-50/80 "
                    onClick={() => {
                        if (!registeredHistoryModalChooser) {
                            setRegisteredHistoryModalChooser("answer")
                        }
                    }}
                >
                    <div className="flex h-full grow flex-col items-center justify-start space-y-6 rounded-lg border border-kale-500 px-3 pb-3 pt-4 ">
                        <Icon
                            icon="heroicons:identification-solid"
                            className="h-7 w-7 text-kale-600"
                        />
                        <div className="flex flex-col items-center justify-center space-y-[2px]">
                            <Typography className="text-kale-600" weight={500}>
                                Lihat Jawaban
                            </Typography>
                            <Typography
                                className="max-w-[200px] text-center text-kale-600"
                                size="xs"
                            >
                                Jawaban yang pernah kamu jawab sebelum mendaftar lomba ini
                            </Typography>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Chooser
