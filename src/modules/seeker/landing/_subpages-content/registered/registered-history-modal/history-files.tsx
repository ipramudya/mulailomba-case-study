import NotFound from "@blocks/not-found"
import useGetRegistrationFiles from "@seekerService/registration/use-get-registration-files"
import Button from "@ui/button"
import Typography from "@ui/typography"
import { useSetAtom } from "jotai"
import { isEmpty } from "lodash-es"
import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"

import { RegisteredHistoryModalChooser } from "."
import HistoryFilesItem from "./history-files-item"

interface Props {
    registrationId: string
}

const HistoryFiles: FunctionComponent<Props> = ({ registrationId }) => {
    const setRegisteredHistoryModalChooser = useSetAtom(RegisteredHistoryModalChooser)

    const {
        data: registrationFiles,
        isLoading,
        isFetching,
    } = useGetRegistrationFiles(registrationId)

    return (
        <div className="flex flex-col space-y-4">
            <hr className="border-neutral-100" />
            <Typography size="sm">Berkas Pendaftaran</Typography>
            {isLoading || isFetching ? (
                <div className="grid grid-cols-2 gap-4">
                    {Array.from({ length: 4 }).map((_, idx) => (
                        <Skeleton className="min-h-[62px]" key={"registered-files-" + idx} />
                    ))}
                </div>
            ) : (
                <>
                    {isEmpty(registrationFiles) || !registrationFiles ? (
                        <NotFound message="Berkas pendaftaran kosong" spacingY={false} />
                    ) : (
                        <div className="grid grid-cols-2 gap-4">
                            {registrationFiles.map((file) => (
                                <HistoryFilesItem
                                    key={file.id}
                                    storageUrl={file.file_storage_url as string}
                                />
                            ))}
                        </div>
                    )}
                </>
            )}
            <Button
                size="xs"
                label="Kembali"
                intent="bordered"
                className="w-fit"
                startIcon="heroicons:chevron-left-20-solid"
                onClick={() => setRegisteredHistoryModalChooser(null)}
            />
        </div>
    )
}

export default HistoryFiles
