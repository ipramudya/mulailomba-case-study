const dropdownsConstant = [
    { label: "Musik", value: "Musik" },
    { label: "Pendidikan", value: "Pendidikan" },
    { label: "Olahraga", value: "Olahraga" },
    { label: "Sastra dan Bahasa", value: "Sastra dan Bahasa" },
    { label: "Esport Game", value: "Esport Game" },
    {
        label: "Olimpiade Sains dan Teknologi",
        value: "Olimpiade Sains dan teknologi",
    },
]

export default dropdownsConstant
