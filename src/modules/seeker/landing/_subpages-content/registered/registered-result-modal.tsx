import NotFound from "@blocks/not-found"
import { Icon } from "@iconify/react"
import useGetExecutions from "@organizerService/registrations/use-get-executions"
import AnchorButton from "@ui/anchor-button"
import IconButton from "@ui/icon-button"
import Modal from "@ui/modal"
import Paper from "@ui/paper"
import Typography from "@ui/typography"
import { isEmpty } from "lodash-es"
import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"

interface Props {
    isOpen: boolean
    onOpenChange(val: boolean): void
    registrationId: string
}

const RegisteredResultModal: FunctionComponent<Props> = ({
    isOpen,
    onOpenChange,
    registrationId,
}) => {
    const {
        data: executions,
        isLoading: executionsLoading,
        isFetching,
    } = useGetExecutions(registrationId)

    return (
        <Modal open={isOpen} onOpenChange={onOpenChange} title={false}>
            <div className="flex flex-col space-y-6">
                <div className="flex items-center justify-between">
                    <div className="flex flex-col space-y-1">
                        <Typography weight={500}>Hasil Perlombaan</Typography>
                        <Typography size="xs">
                            Berikut adalah kumpulan informasi hasil perlombaan berdasarkan <br />
                            timeline (bila ada)
                        </Typography>
                    </div>
                    <IconButton
                        aria-label="tutup"
                        icon="heroicons:x-circle"
                        size="sm"
                        iconSize="md"
                        intent="danger-ghost"
                        circle
                        onClick={() => onOpenChange(false)}
                    />
                </div>

                <div className="flex flex-col space-y-4">
                    {executionsLoading || isFetching || !executions ? (
                        Array.from({ length: 4 }).map((_, idx) => (
                            <Skeleton className="h-[42px] w-full " key={"history-peserta" + idx} />
                        ))
                    ) : (
                        <>
                            {isEmpty(executions) ? (
                                <NotFound
                                    spacingY={false}
                                    icon="heroicons:archive-box-x-mark"
                                    message="Informasi terkait hasil lomba belum ada"
                                />
                            ) : (
                                executions.map((e) => (
                                    <Paper key={e.id} className="space-y-4">
                                        <div className="flex flex-col space-y-2">
                                            <div className="flex space-x-2">
                                                <Icon
                                                    icon={
                                                        e.is_eliminate
                                                            ? "heroicons:exclamation-triangle-solid"
                                                            : "heroicons:check-circle-solid"
                                                    }
                                                    className={`h-5 w-5 ${
                                                        e.is_eliminate
                                                            ? "text-danger-500"
                                                            : "text-success-600"
                                                    } `}
                                                />
                                                <Typography size="sm" weight={500}>
                                                    {e.is_eliminate ? "Gagal" : "Lolos"}
                                                    {e.timeline_name &&
                                                        ` pada ${e.timeline_name.toLowerCase()}`}
                                                </Typography>
                                            </div>
                                            <Typography size="xs">
                                                Catatan: {e.description}
                                            </Typography>
                                        </div>
                                        {e.file_public_url && (
                                            <AnchorButton
                                                intent="bordered"
                                                label="Lihat informasi yang dikirimkan"
                                                thin
                                                size="xs"
                                                className="max-w-fit border-none bg-transparent p-0 text-primary-500 hover:text-primary-400"
                                                href={e.file_public_url}
                                                newTab
                                            />
                                        )}
                                    </Paper>
                                ))
                            )}
                        </>
                    )}
                </div>
            </div>
        </Modal>
    )
}

export default RegisteredResultModal
