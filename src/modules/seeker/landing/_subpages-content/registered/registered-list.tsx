import useGetRegistrationsBySeeker from "@seekerService/registration/use-get-registrations-by-seeker"
import useSeekerUserData from "@seekerService/user/use-seeker-user-data"
import { useAtomValue } from "jotai"
import { isEmpty } from "lodash-es"
import { FunctionComponent } from "react"

import EmptyRegistration from "./registered-empty"
import { RegisteredSeekerFilterAtom } from "./registered-filters"
import RegisteredListItem from "./registered-list-item"
import RegisteredListItemSkeleton from "./registered-list-item-skeleton"
import {
    RegosteredSeekerCategoryAtom,
    RegosteredSeekerSearchAtom,
} from "./registered-search-dropdowns"

const RegisteredList: FunctionComponent = () => {
    const selectedFilter = useAtomValue(RegisteredSeekerFilterAtom)
    const selectedCategory = useAtomValue(RegosteredSeekerCategoryAtom)
    const search = useAtomValue(RegosteredSeekerSearchAtom)

    const { data: seekerData } = useSeekerUserData()
    const { data: registrations, isLoading } = useGetRegistrationsBySeeker({
        registeredIds: seekerData?.registered_lomba,
        filter: selectedFilter === "all" ? null : selectedFilter,
        category: selectedCategory,
        search,
    })

    return (
        <div className="grid grid-cols-1 gap-y-6">
            <>
                {isLoading ? (
                    <RegisteredListItemSkeleton />
                ) : (
                    <>
                        {!registrations || isEmpty(registrations) ? (
                            <EmptyRegistration
                                status={
                                    Array.isArray(registrations) && isEmpty(registrations)
                                        ? "empty"
                                        : "not-logged"
                                }
                            />
                        ) : (
                            registrations.map((r) => (
                                <RegisteredListItem key={r.id} registration={r} />
                            ))
                        )}
                    </>
                )}
            </>
        </div>
    )
}

export default RegisteredList
