import categoryIcon from "@constant/plot-category-icon"
import { getCurrentTimeline, getNextTimeline } from "@functions/check-timelines"
import { formatDate } from "@functions/format-date"
import avatar from "@functions/generate-avatar"
import { Icon } from "@iconify/react"
import useGetExecutions from "@organizerService/registrations/use-get-executions"
import Badge from "@ui/badge"
import Button from "@ui/button"
import Divider from "@ui/divider"
import RouterLink from "@ui/router-link"
import Typography from "@ui/typography"
import { isEmpty } from "lodash-es"
import { FunctionComponent, useMemo, useState } from "react"
import Skeleton from "react-loading-skeleton"

import RegisteredListItemMenu from "./registered-list-item-menu"
import RegisteredResultModal from "./registered-result-modal"

interface Props {
    registration: any
}

const RegisteredListItem: FunctionComponent<Props> = ({ registration }) => {
    console.log(registration)
    const { data: executions, isLoading: isExecutionsLoading } = useGetExecutions(registration.id)
    const [isResultModalShown, setIsResultModalShown] = useState(false)

    const currentTimeline = useMemo(() => {
        if (!isEmpty(registration.lomba.lomba_timelines)) {
            const foundCurrentTimeline = getCurrentTimeline(
                new Date().toISOString(),
                registration.lomba.lomba_timelines
            )

            if (!foundCurrentTimeline) return "Tidak ada"

            return foundCurrentTimeline
        }
        return "Tidak ada"
    }, [registration.lomba])

    const nextTimeline = useMemo(() => {
        if (!isEmpty(registration.lomba.lomba_timelines)) {
            const foundCurrentTimeline = getNextTimeline(
                new Date().toISOString(),
                registration.lomba.lomba_timelines
            )

            if (!foundCurrentTimeline) return "Tidak ada"

            return foundCurrentTimeline
        }
        return "Tidak ada"
    }, [registration.lomba])

    const renderStatusRegistration = () => {
        if (registration.is_canceled) {
            if (registration.is_cancelation_approved) return "Ajuan Pembatalan Disetujui"

            return "Mengajukan Pembatalan"
        }

        return registration.is_verified ? "Sudah terverivikasi" : "Belum terverivikasi"
    }

    return (
        <>
            {isResultModalShown && (
                <RegisteredResultModal
                    isOpen={isResultModalShown}
                    onOpenChange={setIsResultModalShown}
                    registrationId={registration.id}
                />
            )}
            <div className="flex max-h-fit flex-col rounded-md border border-neutral-100">
                {/* 👇 head */}
                <div className="flex items-center justify-between border-b border-neutral-100 px-4 py-3">
                    <div className="flex min-h-[16px] items-center space-x-3 ">
                        <div className="flex items-center space-x-2">
                            <div className="flex items-center rounded-full bg-primary-500 p-1">
                                <Icon
                                    icon={
                                        categoryIcon.get(registration.lomba.category) ||
                                        "heroicons:squares-2x2-20-solid"
                                    }
                                    className="text-white"
                                />
                            </div>
                            <Typography weight={500} className="text-primary-500" size="sm">
                                {registration.lomba.category}
                            </Typography>
                        </div>
                        <Divider orientation="vertical" className="min-h-[18px]" />
                        <Typography size="sm">
                            Mendaftar pada: {formatDate(registration.created_at)}
                        </Typography>
                    </div>
                    {/* 👇 action buttons */}
                    <div className="flex items-center space-x-2">
                        {isExecutionsLoading ? (
                            <Skeleton className="h-[32px] w-[90px]" />
                        ) : (
                            <>
                                {!isEmpty(executions) && (
                                    <Button
                                        label="Lihat Hasil"
                                        intent="primary-low"
                                        className="text-primary-500 hover:text-primary-700"
                                        onClick={() => setIsResultModalShown(true)}
                                    />
                                )}
                            </>
                        )}

                        {registration.is_canceled === 0 && (
                            <RegisteredListItemMenu registrationId={registration.id} />
                        )}
                    </div>
                </div>
                {/* 👇 body */}
                <div className="flex space-x-5 px-4 py-3">
                    {/* 👇 poster */}
                    <div>
                        {registration.lomba.poster_public_url ? (
                            <div className=" aspect-[3/4] max-w-[143px] overflow-hidden rounded-md bg-neutral-100 ">
                                <img
                                    src={registration.lomba.poster_public_url}
                                    alt={`poster-${registration.lomba.name}`}
                                    className="h-full w-full"
                                />
                            </div>
                        ) : (
                            <div className="flex aspect-[3/4] items-center justify-center bg-secondary-50">
                                <Icon icon="heroicons:photo" className="h-6 w-6 text-neutral-500" />
                            </div>
                        )}
                    </div>
                    <div className="relative flex grow flex-col">
                        {/* 👇 status registrasi */}
                        <div className="absolute top-4 right-0">
                            <Badge
                                label={renderStatusRegistration()}
                                intent={
                                    [
                                        "Mengajukan Pembatalan",
                                        "Ajuan Pembatalan Disetujui",
                                        "Belum terverivikasi",
                                    ].includes(renderStatusRegistration())
                                        ? "danger-low"
                                        : "success-low"
                                }
                            />
                        </div>
                        <div className="mb-6 flex flex-col">
                            <RouterLink
                                className="w-fit max-w-[80%]"
                                to={`/lomba/${registration.lomba.id}`}
                            >
                                <Typography
                                    as="h5"
                                    size="lg"
                                    className="mb-1 line-clamp-2"
                                    weight={500}
                                >
                                    {registration.lomba.name}
                                </Typography>
                            </RouterLink>
                            <div className="flex items-center space-x-2">
                                {/* 👇 organizer profile */}
                                <div className="flex aspect-square w-5 items-center justify-center overflow-hidden rounded-full shadow-md">
                                    <img
                                        src={
                                            registration.lomba.organizer.profile_public_url ||
                                            avatar
                                        }
                                        alt="profil organizer"
                                        className="w-full object-cover"
                                    />
                                </div>

                                {/* 👇 organizer name */}
                                <RouterLink
                                    to={`/organizer/detail/${registration.lomba.organizer.id}`}
                                >
                                    <Typography size="xs" weight={500} className="text-primary-600">
                                        {registration.lomba.organizer.name}
                                    </Typography>
                                </RouterLink>
                            </div>
                        </div>
                        <div className="flex flex-col space-y-4">
                            {/* 👇 waktu pelaksanaan */}
                            <div className="flex flex-col space-y-1">
                                <Typography as="span" size="xs" className="text-neutral-400">
                                    Waktu pelaksanaan
                                </Typography>
                                <Typography as="span" size="sm">
                                    {formatDate(registration.lomba.start_date)}
                                </Typography>
                            </div>
                            {/* 👇 status timeline */}
                            <div className="flex items-center space-x-8">
                                {/* 👇 timeline berlangsung */}
                                <div className="flex h-full flex-col space-y-1">
                                    <Typography as="span" size="xs" className="text-neutral-400">
                                        Timeline berlangsung
                                    </Typography>
                                    <div className="flex grow flex-col space-y-[2px]">
                                        <Typography as="span" size="sm">
                                            {typeof currentTimeline === "string"
                                                ? currentTimeline
                                                : currentTimeline.name}
                                        </Typography>

                                        {typeof currentTimeline !== "string" && (
                                            <Typography
                                                as="span"
                                                size="xs"
                                                className="text-neutral-400"
                                            >
                                                {formatDate(currentTimeline.start_date as string)}
                                            </Typography>
                                        )}
                                    </div>
                                </div>
                                <Divider orientation="vertical" className="min-h-[42px] " />
                                {/* 👇 timeline kemudian */}
                                <div className="flex h-full flex-col space-y-1">
                                    <Typography as="span" size="xs" className="text-neutral-400">
                                        Timeline selanjutnya
                                    </Typography>
                                    <div className="flex grow flex-col space-y-[2px] ">
                                        <Typography as="span" size="sm">
                                            {typeof nextTimeline === "string"
                                                ? nextTimeline
                                                : nextTimeline.name}
                                        </Typography>

                                        {typeof nextTimeline !== "string" && (
                                            <Typography
                                                as="span"
                                                size="xs"
                                                className="text-neutral-400"
                                            >
                                                {formatDate(nextTimeline.start_date as string)}
                                            </Typography>
                                        )}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default RegisteredListItem
