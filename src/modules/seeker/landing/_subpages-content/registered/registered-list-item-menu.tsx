import Confirmation from "@blocks/confirmation"
import useCancelRegistration from "@seekerService/registration/use-cancel-registration"
import Button from "@ui/button"
import IconButton from "@ui/icon-button"
import Popover from "@ui/popover"
import { FunctionComponent, useState } from "react"
import toast from "react-hot-toast"

import RegisteredHistoryModal from "./registered-history-modal"

interface Props {
    registrationId: string
}

const RegisteredListItemMenu: FunctionComponent<Props> = ({ registrationId }) => {
    const [isConfirmationOpen, setIsConfirmationOpen] = useState(false)
    const [isHistoryModalOpen, setIsHistoryModalOpen] = useState(false)

    const cancelRegistration = useCancelRegistration()

    const onCancelRegistration = async () => {
        const { error } = await cancelRegistration(registrationId)

        if (error) {
            console.log("error", error)
            return toast.error("Ajuan pembatalan gagal dilakukan")
        }

        toast.success("Ajuan pembatalan berhasil dilakukan")
    }

    return (
        <>
            {isHistoryModalOpen && (
                <RegisteredHistoryModal
                    isOpen={isHistoryModalOpen}
                    onOpenChange={setIsHistoryModalOpen}
                    registrationId={registrationId}
                />
            )}
            <Confirmation
                isOpen={isConfirmationOpen}
                onOpenChange={setIsConfirmationOpen}
                cb={onCancelRegistration}
                variant="delete"
                title="Ajukan pembatalan lomba"
                description="Aksi tidak bisa diurungkan, apakah anda yakin ingin mengajukan pembatalan lomba ?"
            />
            <Popover
                size="sm"
                className="overflow-hidden rounded-md border border-neutral-100 p-2 shadow-menu"
                triggerEl={
                    <IconButton
                        icon="heroicons:ellipsis-vertical"
                        size="sm"
                        intent="primary-bordered"
                        className="rd-state-open:bg-neutral-50"
                    />
                }
                align="end"
            >
                <div className="flex flex-col space-y-2">
                    <Button
                        thin
                        label="Riwayat Pendaftaran"
                        size="xs"
                        intent="default"
                        className="min-w-[140px] justify-start"
                        iconSize="sm"
                        startIcon="heroicons:clock"
                        onClick={() => setIsHistoryModalOpen(true)}
                    />
                    <Button
                        thin
                        label="Ajukan Pembatalan"
                        size="xs"
                        intent="danger-ghost"
                        className="min-w-[140px] justify-start"
                        iconSize="sm"
                        startIcon="heroicons:document-minus"
                        onClick={() => setIsConfirmationOpen(true)}
                    />
                </div>
            </Popover>
        </>
    )
}

export default RegisteredListItemMenu
