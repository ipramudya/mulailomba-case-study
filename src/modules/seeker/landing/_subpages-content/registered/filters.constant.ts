const filterConstant = [
    {
        label: "Semua",
        value: "all",
    },
    {
        label: "Sudah Terverifikasi",
        value: "verified",
    },
    {
        label: "Belum Terverifikasi",
        value: "not-verified",
    },
]

export default filterConstant
