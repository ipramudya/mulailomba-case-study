import Search from "@blocks/search"
import Menu from "@ui/menu"
import { atom, useAtom, useSetAtom } from "jotai"
import { FunctionComponent } from "react"

import dropdownsConstant from "./dropdowns.constant"

export const RegosteredSeekerSearchAtom = atom("")
export const RegosteredSeekerCategoryAtom = atom("")

const RegisteredSearchDropdowns: FunctionComponent = () => {
    const [filterCategory, setFilterCategory] = useAtom(RegosteredSeekerCategoryAtom)
    const setSearch = useSetAtom(RegosteredSeekerSearchAtom)
    return (
        <div className="flex items-center space-x-4">
            <Menu.Root
                buttonLabel={filterCategory || "Kategori"}
                classNames={{
                    button: filterCategory ? "border-success-600 bg-success-50" : undefined,
                }}
            >
                <Menu.RadioGroup
                    value={filterCategory}
                    onValueChange={(val) => {
                        if (val === filterCategory) return setFilterCategory("")
                        setFilterCategory(val)
                    }}
                >
                    {dropdownsConstant?.map((f, idx) =>
                        !f ? (
                            <></>
                        ) : (
                            <Menu.Radio label={f.label} value={f.value} key={"filter-" + f + idx} />
                        )
                    )}
                </Menu.RadioGroup>
            </Menu.Root>

            {/* 👇 search */}
            <Search
                iconPlacing="end"
                onSearchChange={(val) => {
                    setSearch(val)
                }}
                placeholder="Cari lomba terdaftar disini..."
            />
        </div>
    )
}

export default RegisteredSearchDropdowns
