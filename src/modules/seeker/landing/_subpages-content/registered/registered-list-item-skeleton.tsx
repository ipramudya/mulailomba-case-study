import Divider from "@ui/divider"
import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"

const RegisteredListItemSkeleton: FunctionComponent = () => {
    return (
        <>
            {Array.from({ length: 5 }).map((_, idx) => (
                <div
                    key={"registered-list-item-skeleton-" + idx}
                    className="flex max-h-fit flex-col rounded-md border border-neutral-100"
                >
                    <div className="flex items-center justify-between border-b border-neutral-100 px-4 py-3">
                        <div className="flex items-center space-x-2">
                            <Skeleton className="h-6 w-[100px] " />
                            <Skeleton className="h-5 w-[200px] " />
                        </div>
                        <div className="flex items-center space-x-2">
                            <Skeleton className="h-[32px] w-[140px] " />
                            <Skeleton className="aspect-square h-[32px] " />
                        </div>
                    </div>
                    <div className="flex space-x-5 px-4 py-3">
                        <div>
                            <Skeleton className="aspect-[3/4] min-w-[143px] " />
                        </div>
                        <div className="flex grow flex-col">
                            <div className="mb-6 flex flex-col">
                                <Skeleton className="h-6 w-[200px] " />
                                <Skeleton className="h-5 w-[120px] " />
                            </div>
                            <div className="flex flex-col space-y-4">
                                <div className="flex flex-col space-y-1">
                                    <Skeleton className="h-4 w-[100px] " />
                                    <Skeleton className="h-5 w-[110px] " />
                                </div>
                                <div className="flex items-center space-x-8">
                                    <div className="flex flex-col space-y-1">
                                        <Skeleton className="h-4 w-[100px] " />
                                        <div className="flex flex-col space-y-[2px]">
                                            <Skeleton className="h-5 w-[110px] " />
                                            <Skeleton className="h-4 w-[100px] " />
                                        </div>
                                    </div>
                                    <Divider orientation="vertical" className="min-h-[42px] " />
                                    <div className="flex flex-col space-y-1">
                                        <Skeleton className="h-4 w-[100px] " />
                                        <div className="flex flex-col space-y-[2px]">
                                            <Skeleton className="h-5 w-[110px] " />
                                            <Skeleton className="h-4 w-[100px] " />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            ))}
        </>
    )
}

export default RegisteredListItemSkeleton
