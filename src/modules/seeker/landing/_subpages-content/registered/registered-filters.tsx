import Button from "@ui/button"
import { atom, useAtom } from "jotai"
import { FunctionComponent } from "react"

import filterConstant from "./filters.constant"

export const RegisteredSeekerFilterAtom = atom("all")

const RegisteredFilters: FunctionComponent = () => {
    /* shareable state */
    const [selected, setSelected] = useAtom(RegisteredSeekerFilterAtom)

    return (
        <div className="flex items-center space-x-3">
            {filterConstant.map(({ label, value }, idx) => (
                <Button
                    label={label}
                    key={`${value}-${idx}`}
                    intent="bordered"
                    pill
                    className={selected === value ? undefined : "text-neutral-300"}
                    onClick={() => setSelected(value)}
                />
            ))}
        </div>
    )
}

export default RegisteredFilters
