import DropdownFilterV2 from "@blocks/dropdown-filter-v2"
import useGetLombaFilterOptions from "@publicService/use-get-lomba-filter-options"
import { atom, useAtom } from "jotai"
import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"

export const ExploreLombaEligibiliesFilterAtom = atom("")
export const ExploreLombaBenefitsFilterAtom = atom("")
export const ExploreLombaLocationFilterAtom = atom("")
export const ExploreLombaPaidFilterAtom = atom("")

const DropdownFilters: FunctionComponent = () => {
    /* universal reusable atomic state */
    const [eligibilitiesFilter, setEligibilitiesFilter] = useAtom(ExploreLombaEligibiliesFilterAtom)
    const [benefitFilter, setBenefitFilter] = useAtom(ExploreLombaBenefitsFilterAtom)
    const [locationFilter, setLocationFilter] = useAtom(ExploreLombaLocationFilterAtom)
    const [paidFilter, setPaidFilter] = useAtom(ExploreLombaPaidFilterAtom)

    /* API calls */
    const { data: filterOptions } = useGetLombaFilterOptions()

    return (
        <>
            {!filterOptions ? (
                Array.from({ length: 4 }).map((_, idx) => (
                    <Skeleton
                        key={"flatten-filter-skeleton-" + idx}
                        className=" h-[34px] w-[120px] "
                    />
                ))
            ) : (
                <>
                    <DropdownFilterV2
                        buttonLabel="Jenjang"
                        value={eligibilitiesFilter}
                        valueSetter={setEligibilitiesFilter}
                        options={filterOptions.eligibilities.map((b) => ({ label: b, value: b }))}
                    />
                    <DropdownFilterV2
                        buttonLabel="Benefit"
                        value={benefitFilter}
                        valueSetter={setBenefitFilter}
                        options={filterOptions.benefits.map((b) => ({ label: b, value: b }))}
                    />
                    <DropdownFilterV2
                        buttonLabel="Lokasi"
                        value={locationFilter}
                        valueSetter={setLocationFilter}
                        options={[
                            { label: "Tatap Muka", value: "0" },
                            { label: "Online", value: "1" },
                        ]}
                    />
                    <DropdownFilterV2
                        buttonLabel="Biaya"
                        value={paidFilter}
                        valueSetter={setPaidFilter}
                        options={[
                            { label: "Berbayar", value: "0" },
                            { label: "Gratis", value: "1" },
                        ]}
                    />
                </>
            )}
        </>
    )
}

export default DropdownFilters
