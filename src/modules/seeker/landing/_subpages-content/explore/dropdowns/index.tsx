import Search from "@blocks/search"
import { atom, useSetAtom } from "jotai"
import { type FunctionComponent } from "react"

import DropdownFilters from "./dropdown-filters"

export const ExploreLombaSearchAtom = atom("")

const ExploreDropdownFilters: FunctionComponent = () => {
    const setSearch = useSetAtom(ExploreLombaSearchAtom)

    return (
        <div className="flex items-center space-x-4">
            <Search
                wide
                onSearchChange={(val) => {
                    setSearch(val)
                }}
            />
            <DropdownFilters />
        </div>
    )
}

export default ExploreDropdownFilters
