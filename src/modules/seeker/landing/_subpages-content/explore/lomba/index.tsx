import NotFound from "@blocks/not-found"
import LandscapePoster from "@blocks/poster/landscape"
import PosterSkeleton from "@blocks/poster/poster-skeleton"
import useGetAllLomba from "@publicService/use-get-all-lomba"
import { useAtomValue } from "jotai"
import { isEmpty } from "lodash-es"
import { type FunctionComponent } from "react"

import { ExploreLombaSearchAtom } from "../dropdowns"
import {
    ExploreLombaBenefitsFilterAtom,
    ExploreLombaEligibiliesFilterAtom,
    ExploreLombaLocationFilterAtom,
    ExploreLombaPaidFilterAtom,
} from "../dropdowns/dropdown-filters"
import { ExploreLombaFilterAtom } from "../filter/explore-filter-item"

interface Props {
    shouldRecommendLomba?: boolean
    interest?: string[]
}

const LandingLomba: FunctionComponent<Props> = ({ shouldRecommendLomba, interest }) => {
    /* universal reusable atomic state */
    const exploreLombaFilter = useAtomValue(ExploreLombaFilterAtom)
    const eligibilitiesFilter = useAtomValue(ExploreLombaEligibiliesFilterAtom)
    const benefitFilter = useAtomValue(ExploreLombaBenefitsFilterAtom)
    const locationFilter = useAtomValue(ExploreLombaLocationFilterAtom)
    const paidFilter = useAtomValue(ExploreLombaPaidFilterAtom)
    const exploreSearch = useAtomValue(ExploreLombaSearchAtom)

    /* API calls */
    const { data: allLombaResponse, isLoading: isAllLombaLoading } = useGetAllLomba({
        category: exploreLombaFilter,
        filter: {
            eligibilities: eligibilitiesFilter,
            benefits: benefitFilter,
            is_free: paidFilter,
            is_held_online: locationFilter,
        },
        search: exploreSearch,
        in: interest,
    })

    return (
        <div className="grid grid-cols-[repeat(auto-fill,_minmax(370px,_1fr))] gap-x-5 gap-y-8">
            {isAllLombaLoading || !allLombaResponse ? (
                Array.from({ length: 12 }).map((_, idx) => (
                    <PosterSkeleton key={"poster-skeleton-explore-" + idx} />
                ))
            ) : (
                <>
                    {isEmpty(allLombaResponse) ? (
                        <div className="col-span-3">
                            <NotFound
                                message={
                                    shouldRecommendLomba
                                        ? "Maaf, saat ini idak ada perlombaan yang sesuai dengan minat anda"
                                        : "Lomba yang anda cari tidak dapat ditemukan"
                                }
                            />
                        </div>
                    ) : (
                        allLombaResponse.map((lomba) => (
                            <LandscapePoster
                                key={"poster-landing-" + lomba.id}
                                id={lomba.id}
                                price={lomba.is_free ? null : "-"}
                                isOnline={Boolean(lomba.is_held_online)}
                                title={lomba.name as string}
                                startDate={lomba.start_date as string}
                                endDate={lomba.end_date as string}
                                image={lomba.poster_public_url as string}
                            />
                        ))
                    )}
                </>
            )}
        </div>
    )
}

export default LandingLomba
