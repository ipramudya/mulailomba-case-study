const exploreFilterConstant = [
    {
        label: "Semua Lomba",
        icon: "heroicons:fire-solid",
        id: "all",
    },
    {
        label: "Musik",
        icon: "heroicons:musical-note-solid",
        id: "Musik",
    },
    {
        label: "Olimpiade Sains dan Teknologi",
        icon: "heroicons:beaker-solid",
        id: "Olimpiade Sains dan Teknologi",
    },
    {
        label: "Esport Game",
        icon: "heroicons:puzzle-piece-solid",
        id: "Esport Game",
    },
    {
        label: "Sastra dan Bahasa",
        icon: "heroicons:paint-brush-solid",
        id: "Sastra dan Bahasa",
    },
    {
        label: "Pendidikan",
        icon: "heroicons:academic-cap-solid",
        id: "Pendidikan",
    },
    {
        label: "Olahraga",
        icon: "heroicons:sparkles-solid",
        id: "Olahraga",
    },
]

export default exploreFilterConstant
