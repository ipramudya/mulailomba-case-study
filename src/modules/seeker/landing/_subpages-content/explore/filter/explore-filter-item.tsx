import { Icon } from "@iconify/react"
import Typography from "@ui/typography"
import { atom, useAtom } from "jotai"
import { type FunctionComponent } from "react"
import { twMerge } from "tailwind-merge"

interface Props {
    label: string
    icon: string
    id: string
}

export const ExploreLombaFilterAtom = atom("all")

const ExploreFilterItem: FunctionComponent<Props> = ({ label, icon, id }) => {
    const [exploreLombaFilter, setExploreLombaFilter] = useAtom(ExploreLombaFilterAtom)

    return (
        <button
            className={twMerge(
                "flex w-full cursor-pointer flex-col space-y-2 rounded-md border-[2px] border-primary-700 p-2 transition duration-300 hover:shadow-accent",
                exploreLombaFilter === id ? "bg-primary-50" : "bg-white"
            )}
            onClick={() => setExploreLombaFilter(id)}
        >
            <Typography
                weight={500}
                as="p"
                size="sm"
                className="w-[110px] text-start text-primary-700"
            >
                {label}
            </Typography>
            <div className="flex flex-grow items-end justify-center">
                <div className="ml-auto  w-fit rounded-full bg-primary-500 p-[6px]">
                    <Icon icon={icon} className="h-5 w-5 text-white" />
                </div>
            </div>
        </button>
    )
}

export default ExploreFilterItem
