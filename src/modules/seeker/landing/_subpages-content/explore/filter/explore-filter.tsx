import { type FunctionComponent } from "react"

import ExploreFilterItem from "./explore-filter-item"
import exploreFilterConstant from "./explore-filter.constant"

const ExploreFilter: FunctionComponent = () => (
    <div className="flex items-stretch space-x-4">
        {exploreFilterConstant.map((d, idx) => (
            <ExploreFilterItem {...d} key={`explore-filter-${idx}`} />
        ))}
    </div>
)

export default ExploreFilter
