import useAuthData from "@/services/auth-user/use-get-auth-data"
import AnchorButton from "@ui/anchor-button"
import { HEADER_HEIGHT } from "@ui/nav"
import { type FunctionComponent } from "react"
import { useSearchParams } from "react-router-dom"

const TabNav: FunctionComponent = () => {
    /* router stuffs */
    const [searchParams] = useSearchParams()
    const search = searchParams.get("tab")

    /* API calls */
    const { data: authData } = useAuthData()

    const checkVariant = (val: string) => (val === search ? "primary-low" : "bordered")
    const restyleVariant = (val: string) => (val === search ? undefined : "border-transparent")

    return (
        <ul
            className="sticky z-10 flex h-fit items-center space-x-3 border-b border-neutral-100 bg-white py-2"
            style={{ top: HEADER_HEIGHT }}
        >
            <AnchorButton
                tag="Link"
                label="Jelajahi"
                size="sm"
                href="/?tab=explore"
                intent={checkVariant("explore")}
                className={restyleVariant("explore")}
            />
            {(!authData ||
                !authData.user ||
                authData.user.user_metadata.access_role !== "organizer") && (
                <>
                    <AnchorButton
                        tag="Link"
                        label="Wishlist"
                        size="sm"
                        href="/?tab=wishlist"
                        intent={checkVariant("wishlist")}
                        className={restyleVariant("wishlist")}
                    />
                    <AnchorButton
                        tag="Link"
                        label="Terdaftar"
                        size="sm"
                        href="/?tab=registered"
                        intent={checkVariant("registered")}
                        className={restyleVariant("registered")}
                    />
                </>
            )}
        </ul>
    )
}

export default TabNav
