import Container from "@ui/container"
import { FunctionComponent } from "react"

import HighlightItem from "./highlight-item"
import lombaHighlights from "./highlights-constant"

const LombaHighlight: FunctionComponent = () => (
    <Container size="md" className="mt-6 mb-12">
        <div className="relative z-0 flex space-x-6">
            {lombaHighlights.map((d, idx) => (
                <HighlightItem {...d} key={`lomba-highlight-${idx}`} />
            ))}
        </div>
    </Container>
)

export default LombaHighlight
