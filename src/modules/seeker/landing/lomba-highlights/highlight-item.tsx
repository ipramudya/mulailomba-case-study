import Highlight from "@blocks/highlight"
import Badge from "@ui/badge"
import IconButton from "@ui/icon-button"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"
import { Link } from "react-router-dom"

interface Props {
    totalLomba: number
    title: string
    navigateTo?: string
}

const HighlightItem: FunctionComponent<Props> = ({ title, totalLomba, navigateTo }) => (
    <Highlight>
        <div className="flex min-w-[276px] flex-col space-y-3">
            <Badge label={totalLomba + " lomba"} intent="primary-low" />
            <div className="flex items-center justify-between">
                <Typography className="max-w-[200px]" size="sm" weight={500}>
                    {title}
                </Typography>
                <Link to={navigateTo || "/"}>
                    <IconButton
                        size="sm"
                        icon="heroicons:arrow-up-right"
                        intent="primary-ghost"
                        circle
                    />
                </Link>
            </div>
        </div>
    </Highlight>
)

export default HighlightItem
