import useAuthData from "@/services/auth-user/use-get-auth-data"
import Container from "@ui/container"
import { HEADER_HEIGHT } from "@ui/nav"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"

const Hero: FunctionComponent = () => {
    const { data, isLoading } = useAuthData()
    const isUserExist = data && data.user

    return (
        <Container size="md" className="pt-6" as="section">
            <div className="flex flex-col space-y-6">
                {/* 👇 text */}
                {!data || isLoading ? (
                    <div
                        className="flex flex-col space-y-[6px] "
                        style={{ marginTop: HEADER_HEIGHT }}
                    >
                        <Skeleton className="h-[36px] w-[50%]" />
                        <Skeleton className="h-[24px] w-[30%]" />
                    </div>
                ) : (
                    <div
                        className="flex flex-col space-y-[6px]"
                        style={{ marginTop: HEADER_HEIGHT }}
                    >
                        <Typography
                            weight={700}
                            size="h1"
                            as="h1"
                            before={
                                isUserExist
                                    ? `Selamat Datang Kembali, ${data.user.user_metadata.name}`
                                    : "Buruan Daftar dan Tunjukan bahwa Kamu Pemenangnya"
                            }
                            className="relative z-[1] w-full text-white before:absolute before:top-[3px] before:left-[3px] before:z-[-1] before:w-full before:text-3xl before:font-bold before:text-primary-800 before:content-[attr(before)]"
                        >
                            {isUserExist ? "Selamat Datang Kembali, " : "Buruan Daftar dan "}
                            <span className="text-secondary-200">
                                {isUserExist
                                    ? data.user.user_metadata.name
                                    : "Tunjukan bahwa Kamu Pemenangnya"}
                            </span>
                        </Typography>
                        <Typography size="lg" className="text-white">
                            Beraneka macam lomba menarik sesuai dengan hal yang kamu sukai
                        </Typography>
                    </div>
                )}
            </div>
        </Container>
    )
}

export default Hero
