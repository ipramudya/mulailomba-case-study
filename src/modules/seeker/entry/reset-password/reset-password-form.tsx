import { zodResolver } from "@hookform/resolvers/zod"
import supabaseClient from "@lib/supabase/client"
import Button from "@ui/button"
import Form from "@ui/form"
import { FunctionComponent } from "react"
import { SubmitHandler, useForm } from "react-hook-form"
import { toast } from "react-hot-toast"
import { useNavigate } from "react-router-dom"
import { z } from "zod"

const formSchema = z
    .object({
        newPassword: z.string().min(6, "*terlalu pendek"),
        confirmNewPassword: z.string().min(6, "*terlalu pendek"),
    })
    .superRefine((schema, ctx) => {
        if (schema.newPassword !== schema.confirmNewPassword) {
            ctx.addIssue({
                code: z.ZodIssueCode.custom,
                path: ["confirmPassword"],
                message: "*tidak sesuai",
            })
        }
    })
type FormFields = z.infer<typeof formSchema>

const ResetPasswordForm: FunctionComponent = () => {
    /* router dom */
    const navigate = useNavigate()

    /* form things */
    const methods = useForm<FormFields>({
        resolver: zodResolver(formSchema),
    })

    const onResetPasswordSubmit: SubmitHandler<FormFields> = async (fields) => {
        const { error } = await supabaseClient.auth.updateUser({
            password: fields.newPassword,
        })

        if (error) {
            toast.error(error.message)
            console.log("error", error)
            return
        }

        toast.success("Kata sandi berhasil diperbarui")
        navigate("/", { replace: true })
    }

    return (
        <Form {...methods} onSubmit={methods.handleSubmit(onResetPasswordSubmit)}>
            <Form.Layout>
                <Form.TextField
                    type="password"
                    name="newPassword"
                    label="Kata Sandi Baru"
                    required
                    placeholder="Masukan kata sandi baru kamu"
                />
                <Form.TextField
                    type="password"
                    name="confirmNewPassword"
                    label="Konfirmasi Kata Sandi"
                    required
                    placeholder="Konfirmasi kata sandi baru kamu"
                />
                <Button
                    type="submit"
                    label="Simpan"
                    className="min-h-[40px] w-full"
                    loading={methods.formState.isSubmitting}
                />
            </Form.Layout>
        </Form>
    )
}

export default ResetPasswordForm
