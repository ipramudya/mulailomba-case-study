import { BASE_URL } from "@constant/endpoint"
import { zodResolver } from "@hookform/resolvers/zod"
import supabaseClient from "@lib/supabase/client"
import Button from "@ui/button"
import Form from "@ui/form"
import Modal from "@ui/modal"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"
import { SubmitHandler, useForm } from "react-hook-form"
import { toast } from "react-hot-toast"
import { z } from "zod"

const formSchema = z.object({
    email: z.string().email("*email tidak valid"),
})
type FormFields = z.infer<typeof formSchema>

interface Props {
    isOpen: boolean
    onOpenChange(val: boolean): void
}

const ConfirmationResetPasswordModal: FunctionComponent<Props> = ({ isOpen, onOpenChange }) => {
    const methods = useForm<FormFields>({
        resolver: zodResolver(formSchema),
    })

    const onConfirmResetPassword: SubmitHandler<FormFields> = async (fields) => {
        const { error } = await supabaseClient.auth.resetPasswordForEmail(fields.email, {
            redirectTo: `${BASE_URL}/seeker/reset-password`,
        })

        if (error) {
            toast.error(error.message)
            console.log("error", error)
            return
        }

        onOpenChange(false)
        toast.success("Link verifikasi telah dikirim menuju email, silahkan cek inbox")
    }

    return (
        <Modal open={isOpen} onOpenChange={onOpenChange} title="Konfirmasi Reset Password">
            <Form {...methods} onSubmit={onConfirmResetPassword}>
                <Form.Layout>
                    <Typography size="sm">
                        Pastikan email yang anda masukan <strong>benar dan valid</strong>, sistem
                        akan mengirimkan link verifikasi menuju email, periksa inbox atau spam
                        secara berkala.
                    </Typography>
                    <Form.TextField
                        name="email"
                        label="Email"
                        required
                        placeholder="Masukan email kamu"
                    />
                    <Form.RowLayout column={2} className="pt-4">
                        <Button
                            thin
                            label="Batalkan"
                            intent="bordered"
                            onClick={() => onOpenChange(false)}
                        />
                        <Button
                            thin
                            label="Kirimkan"
                            type="submit"
                            loading={methods.formState.isSubmitting}
                        />
                    </Form.RowLayout>
                </Form.Layout>
            </Form>
        </Modal>
    )
}

export default ConfirmationResetPasswordModal
