import supabaseClient from "@lib/supabase/client"
import Button from "@ui/button"
import Typography from "@ui/typography"
import { FunctionComponent, useState } from "react"
import { toast } from "react-hot-toast"

import ConfirmationResetPasswordModal from "./confirmation-reset-password-modal"
import LoginForm from "./login-form"

const Login: FunctionComponent = () => {
    const [isConfirmationOpen, setConfirmationOpen] = useState(false)

    const onLoginGoogle = async () => {
        const { error } = await supabaseClient.auth.signInWithOAuth({
            provider: "google",
            options: {
                queryParams: {
                    access_type: "offline",
                    prompt: "consent",
                },
            },
        })

        if (error) {
            toast.error(error.message)
            console.log("error", error)
            return
        }
    }

    return (
        <div className="flex w-full max-w-[420px] flex-col space-y-6">
            <Typography as="h2" size="h2" weight={700} className="text-center">
                Masuk
            </Typography>

            <LoginForm />
            <hr className="border-neutral-100" />

            {/* 👇 other login methods */}
            <Button
                label="Masuk melalui Google"
                intent="bordered"
                className="min-h-[40px] w-full"
                onClick={onLoginGoogle}
            />
            {isConfirmationOpen && (
                <ConfirmationResetPasswordModal
                    isOpen={isConfirmationOpen}
                    onOpenChange={setConfirmationOpen}
                />
            )}

            <Typography
                size="sm"
                className="cursor-pointer text-center text-primary-500"
                onClick={() => setConfirmationOpen(true)}
            >
                Lupa kata sandi ?
            </Typography>
        </div>
    )
}

export default Login
