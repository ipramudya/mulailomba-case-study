import { zodResolver } from "@hookform/resolvers/zod"
import useLoginSeeker from "@seekerService/user/auth/use-login-seeker"
import { AuthError } from "@supabase/supabase-js"
import Button from "@ui/button"
import Form from "@ui/form"
import { FunctionComponent } from "react"
import { SubmitHandler, useForm } from "react-hook-form"
import toast from "react-hot-toast"
import { useNavigate } from "react-router-dom"
import { z } from "zod"

const formSchema = z.object({
    email: z
        .string({ required_error: "*tidak boleh kosong" })
        .email({ message: "*email tidak valid" }),
    password: z.string().min(6, "*terlalu pendek"),
})
type FormFields = z.infer<typeof formSchema>

const LoginForm: FunctionComponent = () => {
    /* 👇 API definitions */
    const login = useLoginSeeker()

    /* 👇 router stuff */
    const navigate = useNavigate()

    /* 👇 form things */
    const methods = useForm<FormFields>({
        resolver: zodResolver(formSchema),
    })
    const onLoginSubmit: SubmitHandler<FormFields> = async (fields) => {
        const { data, error } = await login(fields)

        if (error) {
            if (error instanceof AuthError) {
                toast.error("Email atau password anda salah")
            } else {
                toast.error(error)
            }
        } else if (data && data.user) {
            return navigate("/", { replace: true })
        }
    }

    return (
        <Form {...methods} onSubmit={onLoginSubmit}>
            <Form.Layout>
                <Form.TextField name="email" label="Email" required placeholder="Masukan email" />
                <Form.TextField
                    name="password"
                    type="password"
                    label="Kata sandi"
                    required
                    placeholder="Masukan kata sandi"
                />
                <Button
                    type="submit"
                    label="Masuk melalui email"
                    className="min-h-[40px] w-full"
                    loading={methods.formState.isSubmitting}
                />
            </Form.Layout>
        </Form>
    )
}

export default LoginForm
