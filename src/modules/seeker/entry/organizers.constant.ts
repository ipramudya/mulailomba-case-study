import {
    SampleOrganizerFive,
    SampleOrganizerFour,
    SampleOrganizerOne,
    SampleOrganizerSix,
    SampleOrganizerThree,
    SampleOrganizerTwo,
} from "@images/index"

const organizerExampleConstant = [
    SampleOrganizerOne,
    SampleOrganizerTwo,
    SampleOrganizerThree,
    SampleOrganizerFour,
    SampleOrganizerFive,
    SampleOrganizerSix,
]

export default organizerExampleConstant
