import Paper from "@ui/paper"
import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"

const DetailLombaSkeleton: FunctionComponent = () => {
    return (
        <>
            {/* 👇 back button */}
            <Skeleton className="h-[20px] w-[180px] " />

            {/* 👇 title */}
            <div>
                <Skeleton className="h-[32px] w-[300px] " />
                <div className="mt-2 flex items-center space-x-3">
                    <div className="flex items-center space-x-2">
                        <Skeleton className="aspect-square w-6 !rounded-full" />
                        <Skeleton className="h-[20px] w-[120px] " />
                    </div>
                    <Skeleton className="h-[20px] w-[180px] " />
                </div>
            </div>

            {/* 👇 detail lomba */}
            <div className="flex space-x-[5rem] pt-4">
                {/* 👇 main content */}
                <div className="flex w-full flex-col space-y-6">
                    <div className="flex flex-col space-y-3">
                        <Skeleton className="h-[20px] w-[120px] " />
                        <Paper className="space-y-1">
                            <Skeleton count={3} className="h-[18px] w-full " />
                            <Skeleton count={3} className="h-[18px] w-[60%] " />
                            <Skeleton count={3} className="h-[18px] w-[80%] " />
                        </Paper>
                    </div>
                    <div className="flex flex-col space-y-3">
                        <Skeleton className="h-[20px] w-[120px] " />
                        <Paper className="space-y-1">
                            <Skeleton count={3} className="h-[18px] w-full " />
                            <Skeleton count={2} className="h-[18px] w-[60%] " />
                            <Skeleton count={3} className="h-[18px] w-[80%] " />
                        </Paper>
                    </div>
                    <div className="flex flex-col space-y-3">
                        <Skeleton className="h-[20px] w-[120px] " />
                        <Paper className="space-y-1">
                            <Skeleton count={2} className="h-[18px] w-full " />
                            <Skeleton count={3} className="h-[18px] w-[60%] " />
                            <Skeleton count={3} className="h-[18px] w-[80%] " />
                        </Paper>
                    </div>
                </div>

                {/* 👇 aside content */}
                <div className="w-full max-w-[260px]">
                    <div className="flex flex-col space-y-6">
                        {/* 👇 action buttons */}
                        <div className="flex items-center space-x-2">
                            <Skeleton className="h-[32px] w-[180px] " />
                            <Skeleton className="aspect-square h-[32px] " />
                            <Skeleton className="aspect-square h-[32px] " />
                        </div>
                        {/* 👇 poster */}
                        <Skeleton className="aspect-[3/4] w-full" />

                        {/* 👇 general information */}
                        <div className="flex flex-col space-y-3">
                            <Skeleton className="h-[20px] w-[120px] " />
                            <Paper className="space-y-3">
                                {Array.from({ length: 4 }).map((_, idx) => (
                                    <div key={"informasi-lomba-" + idx}>
                                        <Skeleton className="mb-1 h-[20px] w-[120px] " />
                                        <Skeleton className="h-[38px] w-full " />
                                        {/*  */}
                                    </div>
                                ))}
                            </Paper>
                        </div>

                        {/* 👇 benefit */}
                        <div className="flex flex-col space-y-3">
                            <Skeleton className="h-[20px] w-[120px] " />
                            <Skeleton className="h-[38px] w-full " />
                            <Skeleton className="h-[38px] w-full " />
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default DetailLombaSkeleton
