import useGetDetailLomba, { DetailLombaResponse } from "@publicService/use-get-detail-lomba"
import Container from "@ui/container"
import { atom, useSetAtom } from "jotai"
import { FunctionComponent, useEffect } from "react"
import { useParams } from "react-router-dom"

import BackButton from "./back-button"
import DetailLombaContent from "./content"
import DetailLombaSkeleton from "./detail-lomba-skeleton"
import SimilarContent from "./similar"
import Title from "./title"

export const detailLombaAtom = atom<DetailLombaResponse | null>(null)

const DetailLomba: FunctionComponent = () => {
    /* universal states */
    const setDetailLomba = useSetAtom(detailLombaAtom)

    /* router stuffs */
    const { lombaId } = useParams()

    /* API calls */
    const { data: detailLombaData, isLoading: isDetailLombaLoading } = useGetDetailLomba(
        lombaId as string
    )
    const detailLomba: DetailLombaResponse = detailLombaData as any

    useEffect(() => {
        if (!isDetailLombaLoading || detailLombaData) {
            setDetailLomba(detailLomba)
        }
    }, [detailLomba, detailLombaData, isDetailLombaLoading, setDetailLomba])

    console.log("detailLomba", detailLomba)

    return (
        <Container as="main" className="flex flex-col space-y-6 py-8">
            {isDetailLombaLoading || !detailLombaData ? (
                <DetailLombaSkeleton />
            ) : (
                <>
                    <BackButton />
                    <Title
                        title={detailLomba.name}
                        organizer={detailLomba.organizer?.name}
                        createdAt={detailLomba.created_at}
                        organizerId={detailLomba.organizer.id}
                        organizerImage={detailLomba.organizer.profile_public_url}
                        eligibilities={detailLomba.eligibilities}
                    />
                    <DetailLombaContent />
                    <SimilarContent />
                </>
            )}
        </Container>
    )
}

export default DetailLomba
