import { adjectives, colors, names, uniqueNamesGenerator } from "unique-names-generator"

const name = () =>
    uniqueNamesGenerator({
        dictionaries: [colors, adjectives, names],
        separator: " ",
        style: "capital",
    })

const c = [
    {
        price: null,
        onSite: false,
        title: name(),
        date: "1 Jan - 31 Mar 2022",
        organizer: name(),
        status: "now",
    },
    {
        price: null,
        onSite: true,
        title: name(),
        date: "1 Jan - 31 Mar 2022",
        organizer: name(),
        status: "will",
    },
    {
        price: null,
        onSite: true,
        title: name(),
        date: "1 Jan - 31 Mar 2022",
        organizer: name(),
        status: "will",
    },
    {
        price: null,
        onSite: false,
        title: name(),
        date: "1 Jan - 31 Mar 2022",
        organizer: name(),
        status: "now",
    },
    {
        price: null,
        onSite: true,
        title: name(),
        date: "1 Jan - 31 Mar 2022",
        organizer: name(),
        status: "now",
    },
] as const

const similarLombaConstant = [...c, ...c, ...c, ...c, ...c]

export default similarLombaConstant
