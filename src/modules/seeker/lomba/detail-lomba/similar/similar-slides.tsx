import LandscapePoster from "@blocks/poster/landscape"
import useGetAllLomba from "@publicService/use-get-all-lomba"
import Carousel from "@ui/carousel"
import CarouselItem from "@ui/carousel/carousel-item"
import { FunctionComponent } from "react"

interface Props {
    carouselRef: any
}
const SimilarSlides: FunctionComponent<Props> = ({ carouselRef }) => {
    const { data: allLombaResponse } = useGetAllLomba()

    return (
        <Carousel.Root ref={carouselRef}>
            {allLombaResponse?.map((lomba) => (
                <CarouselItem key={lomba.id} slidesToShow={3}>
                    <LandscapePoster
                        id={lomba.id}
                        price={lomba.is_free ? null : "-"}
                        isOnline={Boolean(lomba.is_held_online)}
                        title={lomba.name as string}
                        startDate={lomba.start_date as string}
                        endDate={lomba.end_date as string}
                        image={lomba.poster_public_url as string}
                    />
                </CarouselItem>
            ))}
        </Carousel.Root>
    )
}

export default SimilarSlides
