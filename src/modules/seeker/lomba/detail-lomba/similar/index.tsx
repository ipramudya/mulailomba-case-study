import useCarousel from "@/hooks/use-carousel"
import { FunctionComponent } from "react"

import SimilarHeader from "./similar-header"
import SimilarSlides from "./similar-slides"

const SimilarContent: FunctionComponent = () => {
    const { carouselRef, carouselAPI } = useCarousel({ loop: true })

    return (
        <div className="flex flex-col space-y-3">
            <SimilarHeader carouselAPI={carouselAPI} />
            <SimilarSlides carouselRef={carouselRef} />
        </div>
    )
}

export default SimilarContent
