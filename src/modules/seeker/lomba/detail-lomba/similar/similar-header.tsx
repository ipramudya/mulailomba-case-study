import { CarouselApiType } from "@/hooks/use-carousel"
import IconButton from "@ui/icon-button"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"

interface Props {
    carouselAPI: CarouselApiType
}
const SimilarHeader: FunctionComponent<Props> = ({ carouselAPI }) => {
    return (
        <div className="flex w-full flex-col space-y-2 pt-4">
            <div className="flex items-center justify-between">
                <Typography as="h5" weight={500}>
                    Lomba serupa
                </Typography>

                {/* 👇 next/prev buttons */}
                <div className="flex space-x-2">
                    <IconButton
                        icon="heroicons:chevron-left"
                        size="sm"
                        intent="primary-bordered"
                        onClick={carouselAPI.scroolPrev}
                    />
                    <IconButton
                        icon="heroicons:chevron-right"
                        size="sm"
                        intent="primary-bordered"
                        onClick={carouselAPI.scroolNext}
                    />
                </div>
            </div>
        </div>
    )
}

export default SimilarHeader
