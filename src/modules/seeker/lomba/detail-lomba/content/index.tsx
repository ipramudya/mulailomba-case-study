import { FunctionComponent } from "react"

import AsideContent from "./aside-content"
import MainContent from "./main-content"

const DetailLombaContent: FunctionComponent = () => {
    return (
        <div className="flex space-x-[5rem] pt-4">
            <MainContent />
            <AsideContent />
        </div>
    )
}

export default DetailLombaContent
