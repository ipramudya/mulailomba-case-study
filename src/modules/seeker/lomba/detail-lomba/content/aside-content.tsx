import useAuthData from "@/services/auth-user/use-get-auth-data"
import { checkExistanceKeys } from "@functions/check-existance-object"
import { formatStartAndEndDate } from "@functions/format-date"
import useAddToWishlist from "@hooks/use-add-to-wishlist"
import { Icon } from "@iconify/react"
import useCheckOwnRegistration from "@seekerService/registration/use-check-own-registration"
import useRegisterLomba from "@seekerService/registration/use-register-lomba"
import Button from "@ui/button"
import Callout from "@ui/callout"
import IconButton from "@ui/icon-button"
import Paper from "@ui/paper"
import Typography from "@ui/typography"
import { isBefore, parseISO } from "date-fns"
import { useAtomValue } from "jotai"
import { isEmpty } from "lodash-es"
import { FunctionComponent, useMemo, useState } from "react"
import { toast } from "react-hot-toast"
import Skeleton from "react-loading-skeleton"
import { useNavigate, useParams } from "react-router-dom"

import { detailLombaAtom } from ".."
import JoinLombaModal from "../join-lomba-modal"

const AsideContent: FunctionComponent = () => {
    /* standalone states */
    const [isJoinModalOpen, setIsJoinModalOpen] = useState(false)
    const [isSubmitting, setIsSubmitting] = useState(false)

    /* router stuff */
    const navigate = useNavigate()
    const params = useParams()
    const lombaId = params.lombaId as string

    /* universal states */
    const detailLomba = useAtomValue(detailLombaAtom)

    /* custom hooks */
    const { handleAddToFavorite, isAlreadySaved, isRenderable } = useAddToWishlist(lombaId)

    /* API calls */
    const { data: seekerData } = useAuthData()
    const { data: isRegistered } = useCheckOwnRegistration({
        lombaId,
        seekerId: seekerData?.user?.id as string,
    })
    const registerLomba = useRegisterLomba()

    const formattedDate = useMemo(() => {
        if (!detailLomba) return null

        return {
            registration: formatStartAndEndDate(
                detailLomba.start_registration,
                detailLomba.end_registration
            ),
            entry: formatStartAndEndDate(detailLomba.start_date, detailLomba.end_date),
        }
    }, [detailLomba])

    const checkIsUserInfoComplete = (): boolean | null => {
        if (seekerData && seekerData.user && seekerData.user.user_metadata) {
            const desiredKeys = ["location", "name", "phone"]

            return checkExistanceKeys(seekerData.user.user_metadata, desiredKeys)
        }

        return null
    }

    const getJoinButtonProps = () => {
        if (!detailLomba) return null
        const end = parseISO(detailLomba.end_registration)

        if (isRegistered) {
            return {
                label: "Sudah Bergabung",
                disabled: true,
            }
        } else {
            if (!isBefore(new Date(), end)) {
                return {
                    label: "Pendaftaran telah ditutup",
                    disabled: true,
                }
            } else {
                return {
                    label: "Bergabung",
                    disabled: false,
                }
            }
        }
    }

    const onRegisterLomba = async () => {
        if (!checkIsUserInfoComplete())
            return toast.error("Silahkan lengkapi profil terlebih dahulu")

        if (seekerData && seekerData.user) {
            if (detailLomba && !isEmpty(detailLomba.lomba_prerequisites)) {
                setIsJoinModalOpen(true)
            } else {
                setIsSubmitting(true)
                const res = await registerLomba({
                    lombaId,
                    seekerId: seekerData.user.id,
                })

                if (res.status == 201 && res.data) {
                    toast.success("Proses registrasi lomba telah berhasil")
                    setIsJoinModalOpen(false)
                } else {
                    toast.success("Proses registrasi lomba gagal, silahkan coba lagi")
                }
                setIsSubmitting(false)
            }
        }
    }

    return (
        <>
            {detailLomba &&
                seekerData &&
                seekerData.user &&
                isJoinModalOpen &&
                !isEmpty(detailLomba.lomba_prerequisites) &&
                detailLomba.lomba_prerequisites && (
                    <JoinLombaModal
                        lombaId={detailLomba.id}
                        seekerId={seekerData.user.id}
                        isOpen={isJoinModalOpen}
                        onOpenChange={setIsJoinModalOpen}
                        prerequisites={detailLomba.lomba_prerequisites}
                    />
                )}
            <aside className="w-full max-w-[260px] shrink-0">
                <div className="flex flex-col space-y-6">
                    <div className="flex items-center space-x-2">
                        {/* 👇 redirect to login */}
                        {!seekerData ||
                            (seekerData.user === null && (
                                <Button
                                    label="Bergabung"
                                    className="flex-grow"
                                    endIcon="heroicons:arrow-right-on-rectangle"
                                    onClick={() => navigate("/entry/login")}
                                />
                            ))}
                        {/* 👇 proceed register */}
                        {seekerData &&
                            seekerData.user &&
                            seekerData.user.user_metadata.access_role !== "organizer" && (
                                <>
                                    {!getJoinButtonProps() ? (
                                        <Skeleton className="h-[32px] w-[200px]" />
                                    ) : (
                                        <Button
                                            {...(getJoinButtonProps() as any)}
                                            className="flex-grow"
                                            endIcon="heroicons:arrow-right-on-rectangle"
                                            onClick={onRegisterLomba}
                                            loading={isSubmitting}
                                        />
                                    )}
                                </>
                            )}
                        {/* 👇 bookmark button */}
                        <div className="ml-auto flex space-x-2">
                            {isRenderable ? (
                                <IconButton
                                    intent="primary-bordered"
                                    className="flex-shrink-0"
                                    icon={
                                        isAlreadySaved
                                            ? "heroicons:bookmark-20-solid"
                                            : "heroicons:bookmark"
                                    }
                                    onClick={handleAddToFavorite}
                                />
                            ) : (
                                <Skeleton className="h-5 w-5" />
                            )}
                        </div>
                    </div>

                    {/* 👇 poster */}
                    {detailLomba?.poster_public_url ? (
                        <div className="flex overflow-hidden rounded-md shadow-card">
                            <img
                                src={detailLomba.poster_public_url}
                                alt={"poster" + detailLomba.name}
                                className="aspect-[3/4]"
                            />
                        </div>
                    ) : (
                        <div className="flex aspect-[3/4] w-full items-center justify-center rounded-md bg-secondary-50 ">
                            <Icon icon="heroicons:photo" className="h-8 w-8 text-neutral-500" />
                        </div>
                    )}

                    {/* 👇 general information */}
                    <div className="flex flex-col space-y-3">
                        <Typography as="h5" weight={500}>
                            Informasi Lomba
                        </Typography>
                        <Paper className="space-y-3">
                            <div>
                                <Typography as="h6" size="sm" className="mb-1">
                                    Tanggal Pendaftaran
                                </Typography>
                                <Callout>
                                    <Typography size="sm">
                                        {!formattedDate ? "Loading..." : formattedDate.registration}
                                    </Typography>
                                </Callout>
                            </div>
                            <div>
                                <Typography as="h6" size="sm" className="mb-1">
                                    Waktu pelaksanaan
                                </Typography>
                                <Callout>
                                    <Typography size="sm">
                                        {!formattedDate ? "Loading..." : formattedDate.entry}
                                    </Typography>
                                </Callout>
                            </div>
                            <div>
                                <Typography as="h6" size="sm" className="mb-1">
                                    Batas Kuota Pendaftar
                                </Typography>
                                <Callout>
                                    <Typography size="sm">
                                        {detailLomba?.is_unlimited_participants &&
                                        !detailLomba.total_participants
                                            ? "Tidak terbatas"
                                            : detailLomba?.total_participants}
                                    </Typography>
                                </Callout>
                            </div>
                            <div>
                                <Typography as="h6" size="sm" className="mb-1">
                                    Biaya Pendaftaran
                                </Typography>
                                <Callout>
                                    <Typography size="sm">
                                        {detailLomba?.is_free
                                            ? "Gratis tidak dipungut biaya"
                                            : "Berbayar"}
                                    </Typography>
                                </Callout>
                            </div>
                            <div>
                                <Typography as="h6" size="sm" className="mb-1">
                                    Lokasi
                                </Typography>
                                <Callout>
                                    <Typography size="sm">
                                        {detailLomba?.is_held_online && !detailLomba.held_location
                                            ? "Online"
                                            : detailLomba?.held_location}
                                    </Typography>
                                </Callout>
                            </div>
                        </Paper>
                    </div>

                    {/* 👇 benefit */}
                    <div className="flex flex-col space-y-3">
                        <Typography as="h5" weight={500}>
                            Keuntungan
                        </Typography>
                        {detailLomba?.benefits.map((benefit, idx) => (
                            <Callout key={"benefit-" + benefit + idx}>
                                <Typography size="sm">{benefit}</Typography>
                            </Callout>
                        ))}
                    </div>
                </div>
            </aside>
        </>
    )
}

export default AsideContent
