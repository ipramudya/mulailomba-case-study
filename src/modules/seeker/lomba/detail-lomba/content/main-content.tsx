import { formatStartAndEndDate } from "@functions/format-date"
import Paper from "@ui/paper"
import TimelineItem from "@ui/timeline"
import Typography from "@ui/typography"
import { useAtomValue } from "jotai"
import { isEmpty } from "lodash-es"
import { FunctionComponent } from "react"

import { detailLombaAtom } from ".."

/* TODO: Buat Timemline Components  */
const MainContent: FunctionComponent = () => {
    /* universal states */
    const detailLomba = useAtomValue(detailLombaAtom)

    return (
        <div className="flex grow flex-col space-y-6">
            {/* 👇 description */}
            <div className="flex flex-col space-y-3">
                <Typography as="h5" weight={500}>
                    Deskripsi Lomba
                </Typography>
                <Paper>
                    {detailLomba?.description.split("\n").map((t, idx) => (
                        <Typography
                            as="span"
                            size="sm"
                            key={"lomba-desc-" + idx}
                            className="min-h-[20px]"
                        >
                            {t}
                        </Typography>
                    ))}
                </Paper>
            </div>

            {/* 👇 rules */}
            <div className="flex flex-col space-y-3">
                <Typography as="h5" weight={500}>
                    Aturan dan Persyaratan Lomba
                </Typography>
                <Paper>
                    {detailLomba?.rules.split("\n").map((t, idx) => (
                        <Typography
                            as="span"
                            size="sm"
                            key={"lomba-desc-" + idx}
                            className="min-h-[20px]"
                        >
                            {t}
                        </Typography>
                    ))}
                </Paper>
            </div>

            {/* 👇 timeline */}
            {detailLomba &&
                !isEmpty(detailLomba.lomba_timelines) &&
                detailLomba.lomba_timelines && (
                    <div className="flex flex-col space-y-3">
                        <Typography as="h5" weight={500}>
                            Timeline Lomba
                        </Typography>
                        <Paper className="list-disc space-y-0">
                            {detailLomba.lomba_timelines.map((timeline, idx) => (
                                <TimelineItem
                                    key={timeline.id}
                                    spacing="md"
                                    isEnd={idx + 1 === detailLomba.lomba_timelines.length}
                                >
                                    <div className="flex flex-col space-y-2">
                                        <div className="flex flex-col space-y-1">
                                            <Typography
                                                size="sm"
                                                as="span"
                                                weight={500}
                                                className="text-neutral-600"
                                            >
                                                {timeline.name}
                                            </Typography>
                                            <Typography
                                                size="xs"
                                                as="span"
                                                className="text-neutral-400"
                                            >
                                                {formatStartAndEndDate(
                                                    timeline.start_date,
                                                    timeline.end_date
                                                )}
                                            </Typography>
                                        </div>
                                        <div className="rounded-md border border-neutral-100 bg-white p-2">
                                            <Typography size="sm">
                                                {timeline.description}
                                            </Typography>
                                        </div>
                                    </div>
                                </TimelineItem>
                            ))}
                        </Paper>
                    </div>
                )}
        </div>
    )
}

export default MainContent
