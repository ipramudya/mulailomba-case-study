import { formatDate } from "@functions/format-date"
import avatar from "@functions/generate-avatar"
import RouterLink from "@ui/router-link"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"

interface Props {
    title: string
    organizer: string
    organizerId: string
    organizerImage?: string
    createdAt: string
    eligibilities: string[]
}

const Title: FunctionComponent<Props> = ({
    title,
    organizer,
    organizerId,
    organizerImage,
    createdAt,
    eligibilities,
}) => {
    const formattedDate = formatDate(createdAt)

    return (
        <div>
            <Typography as="h2" size="h2" weight={600}>
                {title}
            </Typography>
            <div className="mt-2 flex items-center space-x-2">
                <Typography size="sm">Cocok bagi</Typography>
                {eligibilities.map((e, idx) => (
                    <Typography
                        size="sm"
                        key={"eligibilities_" + idx}
                        weight={500}
                        intent="primary"
                    >
                        {`${e}${idx !== eligibilities.length - 1 ? "," : ""}`}
                    </Typography>
                ))}
            </div>
            <div className="mt-2 flex items-center space-x-3">
                {/* 👇 organizer info */}
                <div className="flex items-center space-x-2">
                    {/* 👇 organizer profile */}
                    <div className="flex aspect-square w-6 items-center justify-center overflow-hidden rounded-full shadow-md">
                        <img
                            src={organizerImage || avatar}
                            alt="profil organizer"
                            className="w-full object-cover"
                        />
                    </div>

                    {/* 👇 organizer name */}
                    <RouterLink to={`/organizer/detail/${organizerId}`}>
                        <Typography size="sm" weight={500} className="text-neutral">
                            {organizer}
                        </Typography>
                    </RouterLink>
                </div>

                {/* 👇 dot */}
                <div className="h-1 w-1 rounded-full bg-neutral-300" />

                <Typography size="sm">Diunggah pada {formattedDate}</Typography>
            </div>
        </div>
    )
}

export default Title
