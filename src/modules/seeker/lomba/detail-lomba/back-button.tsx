import Button from "@ui/button"
import { FunctionComponent } from "react"
import { useLocation, useNavigate } from "react-router-dom"

const BackButton: FunctionComponent = () => {
    const navigate = useNavigate()
    const location = useLocation()

    return (
        <Button
            label="Halaman sebelumnya"
            startIcon="heroicons:arrow-left-solid"
            intent="unstyled"
            onClick={() => navigate(location.state.from.pathname)}
            className="max-w-fit p-0 text-neutral-400 hover:text-opacity-80"
        />
    )
}

export default BackButton
