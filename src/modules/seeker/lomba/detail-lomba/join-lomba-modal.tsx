import { replaceSpaceWithUnderscore } from "@functions/format-strings"
import getFilesPublicURL from "@seekerService/registration/get-files-public-url"
import useRegisterLomba from "@seekerService/registration/use-register-lomba"
import useSaveAnswer from "@seekerService/registration/use-save-answer"
import useSaveUploadedFile from "@seekerService/registration/use-save-uploaded-file"
import useUploadRegistrationFile from "@seekerService/registration/use-upload-registration-file"
import Button from "@ui/button"
import Form from "@ui/form"
import Modal from "@ui/modal"
import { Fragment, FunctionComponent } from "react"
import { useForm } from "react-hook-form"
import toast from "react-hot-toast"

interface Props {
    onOpenChange(val: boolean): void
    isOpen: boolean
    lombaId: string
    seekerId: string
    prerequisites: {
        description: string
        id: string
        is_required: number
        lomba_id: string
        name: string
        variant: string
    }[]
}

const JoinLombaModal: FunctionComponent<Props> = ({
    isOpen,
    onOpenChange,
    prerequisites,
    lombaId,
    seekerId,
}) => {
    const registerLomba = useRegisterLomba()
    const uploadRegistrationFile = useUploadRegistrationFile()
    const saveUploadedFile = useSaveUploadedFile()
    const saveAnswer = useSaveAnswer()

    const methods = useForm()

    const onJoinLomba = async (fields: any) => {
        for (const [idx, p] of prerequisites.entries()) {
            const fieldKeyName = replaceSpaceWithUnderscore(p.name).toLowerCase() + idx

            if (p.is_required && !fields[fieldKeyName]) {
                toast.error(`Prasyarat ${p.name} wajib dipenuhi`)
                return
            }
        }
        const res = await registerLomba({
            lombaId,
            seekerId,
        })

        if (res.status === 201 && res.data) {
            for (const [idx, key] of Object.keys(fields).entries()) {
                if (typeof fields[key] === "object") {
                    const uploadFileResponse = await uploadRegistrationFile({
                        file: fields[key],
                        fileName: fields[key].name.replace(/\s+/g, "_"),
                        seekerId,
                    })

                    if (uploadFileResponse.error) {
                        return toast.error(uploadFileResponse.error.message)
                    }

                    const saveUploadedFileResponse = await saveUploadedFile({
                        fileStorageUrl: uploadFileResponse.data.path,
                        filePublicUrl: getFilesPublicURL(uploadFileResponse.data.path).data
                            .publicUrl,
                        label: prerequisites[idx].name,
                        registrationId: res.data.id,
                    })

                    if (saveUploadedFileResponse.error) {
                        return toast.error("Gagal mengirim dan menyimpan berkas")
                    }
                } else {
                    const saveAnswerResponse = await saveAnswer({
                        label: prerequisites[idx].name,
                        value: fields[key],
                        registrationId: res.data.id,
                    })

                    if (saveAnswerResponse.error) {
                        return toast.error("Gagal mengirim dan menyimpan jawaban persyaratan")
                    }
                }
            }

            toast.success("Proses registrasi lomba telah berhasil")
            onOpenChange(false)
        }
    }

    return (
        <Modal open={isOpen} onOpenChange={onOpenChange} title="Selesaikan Prasyarat">
            <Form {...methods} onSubmit={onJoinLomba}>
                <Form.Layout>
                    {prerequisites.map((p, idx) => (
                        <Fragment key={p.id}>
                            {p.variant === "Paragraf" && (
                                <Form.TextareaField
                                    key={p.id}
                                    label={p.name as string}
                                    required={Boolean(p.is_required)}
                                    name={
                                        replaceSpaceWithUnderscore(p.name as string).toLowerCase() +
                                        idx
                                    }
                                    maxLength={1000}
                                    hasCount
                                    classNames={{ input: "min-h-[120px] resize-y" }}
                                    placeholder={`Masukan ${p.name}`}
                                    hint={p.description || undefined}
                                    hideHintIcon
                                />
                            )}

                            {p.variant === "Teks Singkat" && (
                                <Form.TextField
                                    key={p.id}
                                    hint={p.description || undefined}
                                    label={p.name as string}
                                    required={Boolean(p.is_required)}
                                    name={
                                        replaceSpaceWithUnderscore(p.name as string).toLowerCase() +
                                        idx
                                    }
                                    placeholder={`Masukan ${p.name}`}
                                    hideHintIcon
                                />
                            )}

                            {p.variant === "File" && (
                                <Form.FileField
                                    key={p.id}
                                    hint={p.description || undefined}
                                    label={p.name as string}
                                    required={Boolean(p.is_required)}
                                    name={
                                        replaceSpaceWithUnderscore(p.name as string).toLowerCase() +
                                        idx
                                    }
                                    placeholder={`Silahkan ${p.name}`}
                                    hideHintIcon
                                />
                            )}
                        </Fragment>
                    ))}
                </Form.Layout>
                <Button
                    label="Bergabung Lomba"
                    type="submit"
                    className="mt-6 w-full"
                    loading={methods.formState.isSubmitting}
                />
            </Form>
        </Modal>
    )
}

export default JoinLombaModal
