import useGetAllLomba from "@publicService/use-get-all-lomba"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"

const AllLombaTitle: FunctionComponent = () => {
    const { data, isLoading } = useGetAllLomba()

    return (
        <div>
            {isLoading || !data ? (
                <>
                    <Skeleton className="mb-1 h-[28px] w-[180px]" />
                    <Skeleton className="h-[18px] w-[200px]" />
                </>
            ) : (
                <div>
                    <Typography as="h3" weight={600} size="h3">
                        Seluruh Lomba
                    </Typography>
                    <Typography as="span" size="sm">
                        Total {data.length} lomba ditemukan
                    </Typography>
                </div>
            )}
        </div>
    )
}

export default AllLombaTitle
