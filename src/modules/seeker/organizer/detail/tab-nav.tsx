import anchorStyles from "@ui/anchor/anchor.styles"
import Badge from "@ui/badge"
import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"
import { useSearchParams } from "react-router-dom"
import { twMerge } from "tailwind-merge"

interface Props {
    isLoading?: boolean
    totalLomba: number
}

const TabNav: FunctionComponent<Props> = ({ isLoading, totalLomba }) => {
    /* router stuffs */
    const [searchParams, setSearchParams] = useSearchParams()
    const search = searchParams.get("tab")

    console.log("totalLomba", totalLomba)

    return (
        <ul className="flex items-center space-x-6">
            {isLoading ? (
                <>
                    <Skeleton className="h-[28px] w-[120px] " />
                    <Skeleton className="h-[28px] w-[120px] " />
                </>
            ) : (
                <>
                    <li
                        onClick={() => setSearchParams({ tab: "description" })}
                        className={twMerge(
                            anchorStyles({
                                active: Boolean(search === "description"),
                                ignoreUnderline: true,
                            }),
                            "font-medium"
                        )}
                    >
                        Deskripsi
                    </li>
                    <li
                        onClick={() => setSearchParams({ tab: "lomba" })}
                        className={twMerge(
                            anchorStyles({
                                active: Boolean(search === "lomba"),
                                ignoreUnderline: true,
                            }),
                            "flex items-center font-medium hover:!no-underline"
                        )}
                    >
                        Lomba
                        <Badge
                            intent="primary-low"
                            className="ml-2 aspect-square"
                            label={totalLomba.toString()}
                        />
                    </li>
                </>
            )}
        </ul>
    )
}

export default TabNav
