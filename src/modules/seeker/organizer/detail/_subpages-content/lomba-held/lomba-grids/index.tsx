import LandscapePoster from "@blocks/poster/landscape"
import PosterSkeleton from "@blocks/poster/poster-skeleton"
import useGetAllLomba from "@publicService/use-get-all-lomba"
import { useAtomValue } from "jotai"
import { type FunctionComponent } from "react"
import { useParams } from "react-router-dom"

import {
    DetailOrganizerBenefitsFilterAtom,
    DetailOrganizerEligibiliesFilterAtom,
    DetailOrganizerLocationFilterAtom,
    DetailOrganizerPaidFilterAtom,
    DetailOrganizerSearchAtom,
} from "../lomba-filter"

const LombaGrids: FunctionComponent = () => {
    /* router stuffs */
    const { organizerId } = useParams()

    /* universal reusable atomic state */
    const eligibilitiesFilter = useAtomValue(DetailOrganizerEligibiliesFilterAtom)
    const benefitFilter = useAtomValue(DetailOrganizerBenefitsFilterAtom)
    const locationFilter = useAtomValue(DetailOrganizerLocationFilterAtom)
    const paidFilter = useAtomValue(DetailOrganizerPaidFilterAtom)
    const exploreSearch = useAtomValue(DetailOrganizerSearchAtom)

    /* API calls */
    const { data: organizerLomba, isLoading: isOrganizerLombaLoading } = useGetAllLomba({
        organizerId,
        filter: {
            eligibilities: eligibilitiesFilter,
            benefits: benefitFilter,
            is_free: paidFilter,
            is_held_online: locationFilter,
        },
        search: exploreSearch,
    })

    return (
        <div className="grid grid-cols-[repeat(auto-fill,_minmax(370px,_1fr))] gap-x-5 gap-y-8 ">
            {isOrganizerLombaLoading || !organizerLomba
                ? Array.from({ length: 12 }).map((_, idx) => (
                      <PosterSkeleton key={"poster-skeleton-" + idx} />
                  ))
                : organizerLomba.map((lomba) => (
                      <LandscapePoster
                          key={lomba.id}
                          id={lomba.id}
                          price={lomba.is_free ? null : "-"}
                          isOnline={Boolean(lomba.is_held_online)}
                          title={lomba.name as string}
                          startDate={lomba.start_date as string}
                          endDate={lomba.end_date as string}
                          image={lomba.poster_public_url as string}
                      />
                  ))}
        </div>
    )
}

export default LombaGrids
