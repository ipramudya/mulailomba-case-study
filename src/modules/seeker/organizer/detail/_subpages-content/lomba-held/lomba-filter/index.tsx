import DropdownFilterV2 from "@blocks/dropdown-filter-v2"
import Search from "@blocks/search"
import useGetLombaFilterOptions from "@publicService/use-get-lomba-filter-options"
import { atom, useAtom, useSetAtom } from "jotai"
import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"

export const DetailOrganizerEligibiliesFilterAtom = atom("")
export const DetailOrganizerBenefitsFilterAtom = atom("")
export const DetailOrganizerLocationFilterAtom = atom("")
export const DetailOrganizerPaidFilterAtom = atom("")
export const DetailOrganizerSearchAtom = atom("")

const LombaFilter: FunctionComponent = () => {
    /* universal reusable atomic state */
    const [eligibilitiesFilter, setEligibilitiesFilter] = useAtom(
        DetailOrganizerEligibiliesFilterAtom
    )
    const [benefitFilter, setBenefitFilter] = useAtom(DetailOrganizerBenefitsFilterAtom)
    const [locationFilter, setLocationFilter] = useAtom(DetailOrganizerLocationFilterAtom)
    const [paidFilter, setPaidFilter] = useAtom(DetailOrganizerPaidFilterAtom)
    const setSearch = useSetAtom(DetailOrganizerSearchAtom)

    /* API calls */
    const { data: filterOptions } = useGetLombaFilterOptions()

    return (
        <div className="flex items-center space-x-4">
            {/* 👇 search */}
            <Search
                iconPlacing="start"
                wide
                onSearchChange={(val) => {
                    setSearch(val)
                }}
            />

            {/* 👇 dropdown filters */}
            {!filterOptions ? (
                Array.from({ length: 4 }).map((_, idx) => (
                    <Skeleton
                        key={"flatten-filter-skeleton-" + idx}
                        className=" h-[34px] w-[120px] "
                    />
                ))
            ) : (
                <>
                    <DropdownFilterV2
                        buttonLabel="Jenjang"
                        value={eligibilitiesFilter}
                        valueSetter={setEligibilitiesFilter}
                        options={filterOptions.eligibilities.map((b) => ({ label: b, value: b }))}
                    />
                    <DropdownFilterV2
                        buttonLabel="Benefit"
                        value={benefitFilter}
                        valueSetter={setBenefitFilter}
                        options={filterOptions.benefits.map((b) => ({ label: b, value: b }))}
                    />
                    <DropdownFilterV2
                        buttonLabel="Lokasi"
                        value={locationFilter}
                        valueSetter={setLocationFilter}
                        options={[
                            { label: "Online", value: "1" },
                            { label: "Tatap Muka", value: "0" },
                        ]}
                    />
                    <DropdownFilterV2
                        buttonLabel="Biaya"
                        value={paidFilter}
                        valueSetter={setPaidFilter}
                        options={[
                            { label: "Gratis", value: "1" },
                            { label: "Berbayar", value: "0" },
                        ]}
                    />
                </>
            )}
        </div>
    )
}

export default LombaFilter
