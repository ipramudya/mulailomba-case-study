import useGetDetailOrganizer from "@publicService/use-get-detail-organizer"
import Button from "@ui/button"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"
import { useParams } from "react-router-dom"

const DescriptionAsideContact: FunctionComponent = () => {
    /* router stuffs */
    const { organizerId } = useParams()

    /* API calls */
    const { data: organizderDetailData, isLoading: isOrganizderDetailLoading } =
        useGetDetailOrganizer(organizerId as string)

    return (
        <div>
            <Typography as="h5" weight={500} className="mb-4">
                Kontak
            </Typography>
            <div className="flex flex-col space-y-3">
                {!organizderDetailData || isOrganizderDetailLoading ? (
                    <>
                        <Skeleton count={3} className="mb-3 h-[42px] w-[260px] " />
                    </>
                ) : (
                    <>
                        <Button
                            size="md"
                            className="justify-start border border-primary-300"
                            startIcon="tabler:brand-whatsapp"
                            label={organizderDetailData.social_whatsapp || "Belum terdaftar"}
                            intent="primary-low"
                        />
                        <Button
                            size="md"
                            className="justify-start border border-primary-300"
                            startIcon="lucide:mail"
                            label={organizderDetailData.email || "Belum terdaftar"}
                            intent="primary-low"
                        />
                        <Button
                            size="md"
                            className="justify-start border border-primary-300"
                            startIcon="lucide:instagram"
                            label={organizderDetailData.social_instagram || "Belum terdaftar"}
                            intent="primary-low"
                        />
                    </>
                )}
            </div>
        </div>
    )
}

export default DescriptionAsideContact
