import useGetAllLomba from "@publicService/use-get-all-lomba"
import useGetDetailOrganizer from "@publicService/use-get-detail-organizer"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"
import { useParams } from "react-router-dom"

import AdvancedInfoItem from "./advanced-info-item"

const DescriptionAdvancedInfo: FunctionComponent = () => {
    /* router stuffs */
    const { organizerId } = useParams()

    /* API calls */
    const { data: organizderDetailData, isLoading: isOrganizderDetailLoading } =
        useGetDetailOrganizer(organizerId as string)
    const { data: organizerLomba, isLoading: isOrganizerLombaLoading } = useGetAllLomba({
        organizerId,
    })

    const isLoading =
        !organizderDetailData ||
        isOrganizderDetailLoading ||
        isOrganizerLombaLoading ||
        !organizerLomba

    return (
        <div>
            <Typography as="h5" weight={500} className="mb-4">
                Informasi Lanjut
            </Typography>
            <div className="space-y-3 rounded-md border border-neutral-100 bg-neutral-50 px-3 py-4">
                {isLoading ? (
                    Array.from({ length: 3 }).map((_, idx) => (
                        <div key={"advanced-info-skeleton-" + idx}>
                            <Skeleton className="mb-1 h-[20px] w-[40%] " />
                            <Skeleton className="h-[32px] w-full " />
                        </div>
                    ))
                ) : (
                    <>
                        <AdvancedInfoItem
                            title="Alamat organizer"
                            content={organizderDetailData.location || "Tidak memiliki alamat"}
                        />
                        <AdvancedInfoItem
                            title="Lomba telah terselenggara"
                            content={organizerLomba.length + " lomba"}
                        />
                        <AdvancedInfoItem title="Total pendaftar" content="Belum ada pendaftar" />
                    </>
                )}
            </div>
        </div>
    )
}

export default DescriptionAdvancedInfo
