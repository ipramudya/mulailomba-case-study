import Callout from "@ui/callout"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"

interface Props {
    title: string
    content: string
}

const AdvancedInfoItem: FunctionComponent<Props> = ({ content, title }) => {
    return (
        <div>
            <Typography as="h6" size="sm" className="mb-1">
                {title}
            </Typography>
            <Callout>
                <Typography size="sm">{content}</Typography>
            </Callout>
        </div>
    )
}

export default AdvancedInfoItem
