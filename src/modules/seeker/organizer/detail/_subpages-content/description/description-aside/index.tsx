import { FunctionComponent } from "react"

import DescriptionAdvancedInfo from "./advanced-info"
import DescriptionAsideContact from "./contact"

const DescriptionAside: FunctionComponent = () => (
    <div className="flex flex-col space-y-6">
        <DescriptionAsideContact />
        <DescriptionAdvancedInfo />
    </div>
)

export default DescriptionAside
