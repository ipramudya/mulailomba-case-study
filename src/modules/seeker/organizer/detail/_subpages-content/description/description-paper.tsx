import useGetDetailOrganizer from "@publicService/use-get-detail-organizer"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"
import { useParams } from "react-router-dom"

const DescriptionPaper: FunctionComponent = () => {
    /* router stuffs */
    const { organizerId } = useParams()

    /* API calls */
    const { data: organizderDetailData, isLoading: isOrganizderDetailLoading } =
        useGetDetailOrganizer(organizerId as string)

    return (
        <div className="boder flex flex-col space-y-2 rounded-md border border-neutral-100 bg-neutral-50 p-3">
            {isOrganizderDetailLoading || !organizderDetailData ? (
                <Skeleton count={4} className="h-[20px] w-[90%] " />
            ) : (
                <Typography size="sm">
                    {organizderDetailData.about || "Tidak ada deskripsi mengenai organizer"}
                </Typography>
            )}
        </div>
    )
}

export default DescriptionPaper
