import Button from "@ui/button"
import { FunctionComponent } from "react"
import { useNavigate } from "react-router-dom"

const BackButton: FunctionComponent = () => {
    const navigate = useNavigate()

    return (
        <Button
            label="Halaman daftar organizer"
            startIcon="heroicons:arrow-left-solid"
            intent="unstyled"
            onClick={() => navigate({ pathname: "/organizer" })}
            className="max-w-fit p-0 text-white hover:text-opacity-80"
        />
    )
}

export default BackButton
