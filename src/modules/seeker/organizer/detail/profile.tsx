import avatar from "@functions/generate-avatar"
import useGetDetailOrganizer from "@publicService/use-get-detail-organizer"
import { isEmpty } from "lodash-es"
import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"
import { useParams } from "react-router-dom"

interface Props {
    img?: string
}
const Profile: FunctionComponent<Props> = () => {
    const { organizerId } = useParams()
    const { data: organizerDetailData, isLoading: isOrganizerDetailLoading } =
        useGetDetailOrganizer(organizerId as string)

    return (
        <div className="flex aspect-square w-[6rem] items-center justify-center rounded-lg border-[2px] border-primary-800 bg-white p-3">
            {isOrganizerDetailLoading || !organizerDetailData || isEmpty(organizerDetailData) ? (
                <Skeleton className="aspect-square w-[90%] overflow-hidden " />
            ) : (
                <div className="aspect-square w-[90%] overflow-hidden rounded-md">
                    <img
                        src={
                            organizerDetailData.profile_public_url
                                ? (organizerDetailData.profile_public_url as string)
                                : avatar
                        }
                        alt="foto profil organizer"
                    />
                </div>
            )}
        </div>
    )
}

export default Profile
