import avatar from "@functions/generate-avatar"
import Badge from "@ui/badge"
import RouterLink from "@ui/router-link"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"

interface Props {
    id: string
    image: string
    name: string
    about: string
    status: string
    lombaLength: number
}

const OrganizersItem: FunctionComponent<Props> = ({
    id,
    image,
    name,
    about,
    status,
    lombaLength,
}) => {
    return (
        <RouterLink to={"/organizer/detail/" + id}>
            <div className="flex h-full flex-col space-y-3 rounded-md border border-neutral-100 px-4 py-3 transition duration-300 hover:shadow-card">
                {/* 👇 top section */}
                <div className="flex items-center space-x-3">
                    {/* 👇 profile image */}
                    <div className="flex aspect-square w-8 items-center justify-center overflow-hidden rounded bg-white shadow-sm">
                        <img
                            src={image || avatar}
                            alt="organizer picture"
                            className="w-full object-cover"
                        />
                    </div>
                    {/* 👇 identity */}
                    <div>
                        <Typography weight={500} as="h3">
                            {name}
                        </Typography>
                        <Typography size="sm" as="span" className="text-primary-600">
                            {status || "Belum memiliki kategori"}
                        </Typography>
                    </div>
                </div>
                {/* 👇 desc */}
                <Typography size="sm" className="max-w-[90%] flex-grow line-clamp-2 ">
                    {about || "Tidak ada deskripsi mengenai organizer"}
                </Typography>
                <Badge
                    label={lombaLength + " lomba terselenggara"}
                    size="sm"
                    intent="success-low"
                />
            </div>
        </RouterLink>
    )
}

export default OrganizersItem
