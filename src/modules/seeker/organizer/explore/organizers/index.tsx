import avatar from "@functions/generate-avatar"
import useGetAllOrganizers from "@publicService/use-get-all-organizers"
import { useAtomValue } from "jotai"
import { FunctionComponent } from "react"

import {
    ExploreOrganizerCategoryFilterAtom,
    ExploreOrganizerSearchAtom,
} from "../title/title-filter"
import OrganizersItem from "./organizers-item"
import OrganizersItemSkeleton from "./organizers-item-skeleton"

const Organizers: FunctionComponent = () => {
    /* shareable state */
    const filterCategory = useAtomValue(ExploreOrganizerCategoryFilterAtom)
    const searchOrganizer = useAtomValue(ExploreOrganizerSearchAtom)

    /* API calls */
    const { data: allOrganizersResponse, isLoading: isAllOrganizersLoading } = useGetAllOrganizers({
        category: filterCategory,
        search: searchOrganizer,
    })

    return (
        <div className="grid grid-cols-[repeat(auto-fill,_minmax(300px,_1fr))] gap-x-5 gap-y-8 ">
            {isAllOrganizersLoading || !allOrganizersResponse
                ? Array.from({ length: 12 }).map((_, idx) => (
                      <OrganizersItemSkeleton key={"organizer-item-skeleton-" + idx} />
                  ))
                : allOrganizersResponse.map(
                      ({ id, about, profile_public_url, name, profile_status, lomba }) => (
                          <OrganizersItem
                              key={id}
                              id={id}
                              about={about as string}
                              image={profile_public_url ? (profile_public_url as string) : avatar}
                              name={name as string}
                              status={profile_status as string}
                              lombaLength={lomba && Array.isArray(lomba) ? lomba.length : 0}
                          />
                      )
                  )}
        </div>
    )
}

export default Organizers
