import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"

const OrganizersItemSkeleton: FunctionComponent = () => {
    return (
        <div className="flex flex-col space-y-3 rounded-md border border-neutral-100 px-4 py-3">
            <div className="flex items-center space-x-3">
                <Skeleton className="aspect-square w-8 rounded " />
                <div>
                    <Skeleton count={2} className="h-[18px] w-[180px] " />
                </div>
            </div>
            <Skeleton count={2} className="h-[18px] w-full " />
            <Skeleton className="h-[18px] w-[60%] " />
            {/*  */}
        </div>
    )
}

export default OrganizersItemSkeleton
