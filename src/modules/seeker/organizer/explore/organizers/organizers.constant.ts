import avatar from "@functions/generate-avatar"
import { colors, starWars, uniqueNamesGenerator } from "unique-names-generator"

const name = () =>
    uniqueNamesGenerator({
        dictionaries: [colors, starWars],
        separator: " ",
        style: "capital",
    })

const c = [
    {
        name: name(),
        category: "Pendidikan",
        description:
            "Eu dolore sit et occaecat sit do. Deserunt non ut occaecat nisi Lorem. Esse eu cillum culpa est officia ea duis eu Lorem qui.",
        totalLomba: 20,
        img: avatar,
    },
    {
        name: name(),
        category: "Olimpiade Sains & Teknologi",
        description:
            "Adipisicing amet ad commodo fugiat duis id veniam. Ea deserunt aute aliquip aliquip veniam ad laborum ut et laborum et pariatur magna anim. Sint consectetur velit est officia commodo.",
        totalLomba: 4,
        img: avatar,
    },
    {
        name: name(),
        category: "Musik",
        description:
            "Adipisicing amet ad commodo fugiat duis id veniam. Ea deserunt aute aliquip aliquip veniam ad laborum ut et laborum et pariatur magna anim. Sint consectetur velit est officia commodo.",
        totalLomba: 7,
        img: avatar,
    },
    {
        name: name(),
        category: "Olahraga",
        description:
            "Occaecat aliquip Lorem officia commodo elit cupidatat exercitation sint velit dolor. Ullamco occaecat dolor do culpa tempor magna.",
        totalLomba: 12,
        img: avatar,
    },
]

const organizersConstant = [...c, ...c, ...c, ...c, ...c]

export default organizersConstant
