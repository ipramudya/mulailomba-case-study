import useGetAllOrganizers from "@publicService/use-get-all-organizers"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"

import TitleDropdownFilter from "./title-filter"

const ExploreTitle: FunctionComponent = () => {
    const { data: allOrganizersResponse, isLoading: isAllOrganizersLoading } = useGetAllOrganizers()

    return (
        <div className="flex items-center justify-between">
            {/* 👇 title */}
            <div>
                {isAllOrganizersLoading || !allOrganizersResponse ? (
                    <>
                        <Skeleton className="mb-1 h-[28px] w-[180px]" />
                        <Skeleton className="h-[18px] w-[200px]" />
                    </>
                ) : (
                    <div>
                        <Typography as="h3" weight={600} size="h3">
                            Seluruh Organizer
                        </Typography>
                        <Typography as="span" size="sm">
                            Total {allOrganizersResponse.length} organizer ditemukan
                        </Typography>
                    </div>
                )}
            </div>

            {/* 👇 dropdown filters */}
            <TitleDropdownFilter />
        </div>
    )
}

export default ExploreTitle
