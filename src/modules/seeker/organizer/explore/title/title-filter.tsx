import Search from "@blocks/search"
import useGetOrganizerCategories from "@publicService/use-get-organizer-categories"
import Menu from "@ui/menu"
import { atom, useAtom, useSetAtom } from "jotai"
import { FunctionComponent } from "react"

export const ExploreOrganizerCategoryFilterAtom = atom("")
export const ExploreOrganizerSearchAtom = atom("")

const TitleDropdownFilter: FunctionComponent = () => {
    const [filterCategory, setFilterCategory] = useAtom(ExploreOrganizerCategoryFilterAtom)
    const setSearch = useSetAtom(ExploreOrganizerSearchAtom)

    /* API calls */
    const { data: filterOptions } = useGetOrganizerCategories()

    return (
        <div className="flex items-center space-x-4">
            {/* 👇 dropdown filters */}
            <Menu.Root
                buttonLabel={filterCategory || "Kategori"}
                classNames={{
                    button: filterCategory ? "border-success-600 bg-success-50" : undefined,
                }}
            >
                <Menu.RadioGroup
                    value={filterCategory}
                    onValueChange={(val) => {
                        if (val === filterCategory) return setFilterCategory("")
                        setFilterCategory(val)
                    }}
                >
                    {filterOptions?.map((f, idx) =>
                        !f ? <></> : <Menu.Radio label={f} value={f} key={"filter-" + f + idx} />
                    )}
                </Menu.RadioGroup>
            </Menu.Root>

            {/* 👇 search */}
            <Search
                iconPlacing="end"
                onSearchChange={(val) => {
                    setSearch(val)
                }}
                placeholder="Cari organizer favorit..."
            />
        </div>
    )
}

export default TitleDropdownFilter
