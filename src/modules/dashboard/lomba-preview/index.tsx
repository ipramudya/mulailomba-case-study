import { formatDate, formatStartAndEndDate } from "@functions/format-date"
import avatar from "@functions/generate-avatar"
import { Icon } from "@iconify/react"
import DetailLombaSkeleton from "@modules/seeker/lomba/detail-lomba/detail-lomba-skeleton"
import useGetDetailLomba, { DetailLombaResponse } from "@publicService/use-get-detail-lomba"
import Callout from "@ui/callout"
import Container from "@ui/container"
import Paper from "@ui/paper"
import TimelineItem from "@ui/timeline"
import Typography from "@ui/typography"
import { isEmpty } from "lodash-es"
import { FunctionComponent, useMemo } from "react"
import { useParams } from "react-router-dom"

const LombaPreview: FunctionComponent = () => {
    /* router stuffs */
    const { lombaId } = useParams()

    /* API calls */
    const { data: detailLombaData, isLoading: isDetailLombaLoading } = useGetDetailLomba(
        lombaId as string
    )
    const detailLomba: DetailLombaResponse = detailLombaData as any

    const formattedDate = useMemo(() => {
        if (!detailLomba) return null

        return {
            registration: formatStartAndEndDate(
                detailLomba.start_registration,
                detailLomba.end_registration
            ),
            entry: formatStartAndEndDate(detailLomba.start_date, detailLomba.end_date),
        }
    }, [detailLomba])

    return (
        <Container as="main" className="flex flex-col space-y-6 py-8">
            {isDetailLombaLoading || !detailLombaData ? (
                <DetailLombaSkeleton />
            ) : (
                <>
                    {/* 👇 title */}
                    <div>
                        <Typography as="h2" size="h2" weight={600}>
                            {detailLomba.name}
                        </Typography>
                        <div className="mt-2 flex items-center space-x-2">
                            <Typography size="sm">Cocok bagi</Typography>
                            {detailLomba.eligibilities.map((e, idx) => (
                                <Typography
                                    size="sm"
                                    key={"eligibilities_" + idx}
                                    weight={500}
                                    intent="primary"
                                >
                                    {`${e}${
                                        idx !== detailLomba.eligibilities.length - 1 ? "," : ""
                                    }`}
                                </Typography>
                            ))}
                        </div>
                        <div className="mt-2 flex items-center space-x-3">
                            {/* 👇 organizer info */}
                            <div className="flex items-center space-x-2">
                                {/* 👇 organizer profile */}
                                <div className="flex aspect-square w-6 items-center justify-center overflow-hidden rounded-full shadow-md">
                                    <img
                                        src={detailLomba.organizer.profile_public_url || avatar}
                                        alt="profil organizer"
                                        className="w-full object-cover"
                                    />
                                </div>

                                {/* 👇 organizer name */}
                                <Typography size="sm" weight={500} className="text-primary-600">
                                    {detailLomba.organizer?.name}
                                </Typography>
                            </div>

                            {/* 👇 dot */}
                            <div className="h-1 w-1 rounded-full bg-neutral-300" />

                            <Typography size="sm">
                                Diunggah pada {formatDate(detailLomba.created_at)}
                            </Typography>
                        </div>
                    </div>
                    <div className="flex space-x-[5rem] pt-4">
                        {/* 👇 main content */}
                        <div className="flex grow flex-col space-y-6">
                            {/* 👇 description */}
                            <div className="flex flex-col space-y-3">
                                <Typography as="h5" weight={500}>
                                    Deskripsi Lomba
                                </Typography>
                                <Paper className="space-y-2">
                                    {detailLomba?.description.split("\n").map((t, idx) => (
                                        <Typography
                                            as="span"
                                            size="sm"
                                            key={"lomba-desc-" + idx}
                                            className="min-h-[20px]"
                                        >
                                            {t}
                                        </Typography>
                                    ))}
                                </Paper>
                            </div>

                            {/* 👇 rules */}
                            <div className="flex flex-col space-y-3">
                                <Typography as="h5" weight={500}>
                                    Aturan dan Persyaratan Lomba
                                </Typography>
                                <Paper className="space-y-2">
                                    {detailLomba?.rules.split("\n").map((t, idx) => (
                                        <Typography
                                            as="span"
                                            size="sm"
                                            key={"lomba-desc-" + idx}
                                            className="min-h-[20px]"
                                        >
                                            {t}
                                        </Typography>
                                    ))}
                                </Paper>
                            </div>

                            {/* 👇 timeline */}
                            {detailLomba &&
                                !isEmpty(detailLomba.lomba_timelines) &&
                                detailLomba.lomba_timelines && (
                                    <div className="flex flex-col space-y-3">
                                        <Typography as="h5" weight={500}>
                                            Timeline Lomba
                                        </Typography>
                                        <Paper className="list-disc space-y-0">
                                            {detailLomba.lomba_timelines.map((timeline, idx) => (
                                                <TimelineItem
                                                    key={timeline.id}
                                                    spacing="md"
                                                    isEnd={
                                                        idx + 1 ===
                                                        detailLomba.lomba_timelines.length
                                                    }
                                                >
                                                    <div className="flex flex-col space-y-2">
                                                        <div className="flex flex-col space-y-1">
                                                            <Typography
                                                                size="sm"
                                                                as="span"
                                                                weight={500}
                                                                className="text-neutral-600"
                                                            >
                                                                {timeline.name}
                                                            </Typography>
                                                            <Typography
                                                                size="xs"
                                                                as="span"
                                                                className="text-neutral-400"
                                                            >
                                                                {formatStartAndEndDate(
                                                                    timeline.start_date,
                                                                    timeline.end_date
                                                                )}
                                                            </Typography>
                                                        </div>
                                                        <div className="rounded-md border border-neutral-100 bg-white p-2">
                                                            <Typography size="sm">
                                                                {timeline.description}
                                                            </Typography>
                                                        </div>
                                                    </div>
                                                </TimelineItem>
                                            ))}
                                        </Paper>
                                    </div>
                                )}
                        </div>

                        {/* 👇 aside content */}
                        <aside className="w-full max-w-[260px] shrink-0">
                            <div className="flex flex-col space-y-6">
                                {/* 👇 poster */}
                                {detailLomba?.poster_public_url ? (
                                    <div className="flex overflow-hidden rounded-md shadow-card">
                                        <img
                                            src={detailLomba.poster_public_url}
                                            alt={"poster" + detailLomba.name}
                                        />
                                    </div>
                                ) : (
                                    <div className="flex aspect-[3/4] w-full items-center justify-center rounded-md bg-secondary-50 ">
                                        <Icon
                                            icon="heroicons:photo"
                                            className="h-8 w-8 text-neutral-500"
                                        />
                                    </div>
                                )}

                                {/* 👇 general information */}
                                <div className="flex flex-col space-y-3">
                                    <Typography as="h5" weight={500}>
                                        Informasi Lomba
                                    </Typography>
                                    <Paper className="space-y-3">
                                        <div>
                                            <Typography as="h6" size="sm" className="mb-1">
                                                Tanggal Pendaftaran
                                            </Typography>
                                            <Callout>
                                                <Typography size="sm">
                                                    {!formattedDate
                                                        ? "Loading..."
                                                        : formattedDate.registration}
                                                </Typography>
                                            </Callout>
                                        </div>
                                        <div>
                                            <Typography as="h6" size="sm" className="mb-1">
                                                Waktu pelaksanaan
                                            </Typography>
                                            <Callout>
                                                <Typography size="sm">
                                                    {!formattedDate
                                                        ? "Loading..."
                                                        : formattedDate.entry}
                                                </Typography>
                                            </Callout>
                                        </div>
                                        <div>
                                            <Typography as="h6" size="sm" className="mb-1">
                                                Batas Kuota Pendaftar
                                            </Typography>
                                            <Callout>
                                                <Typography size="sm">
                                                    {detailLomba?.is_unlimited_participants &&
                                                    !detailLomba.total_participants
                                                        ? "Tidak terbatas"
                                                        : detailLomba?.total_participants}
                                                </Typography>
                                            </Callout>
                                        </div>
                                        <div>
                                            <Typography as="h6" size="sm" className="mb-1">
                                                Biaya Pendaftaran
                                            </Typography>
                                            <Callout>
                                                <Typography size="sm">
                                                    {detailLomba?.is_free
                                                        ? "Gratis tidak dipungut biaya"
                                                        : "Berbayar"}
                                                </Typography>
                                            </Callout>
                                        </div>
                                        <div>
                                            <Typography as="h6" size="sm" className="mb-1">
                                                Lokasi
                                            </Typography>
                                            <Callout>
                                                <Typography size="sm">
                                                    {detailLomba?.is_held_online &&
                                                    !detailLomba.held_location
                                                        ? "Online"
                                                        : detailLomba?.held_location}
                                                </Typography>
                                            </Callout>
                                        </div>
                                    </Paper>
                                </div>

                                {/* 👇 benefit */}
                                <div className="flex flex-col space-y-3">
                                    <Typography as="h5" weight={500}>
                                        Keuntungan
                                    </Typography>
                                    {detailLomba?.benefits.map((benefit, idx) => (
                                        <Callout key={"benefit-" + benefit + idx}>
                                            <Typography size="sm">{benefit}</Typography>
                                        </Callout>
                                    ))}
                                </div>
                            </div>
                        </aside>
                    </div>
                </>
            )}
        </Container>
    )
}

export default LombaPreview
