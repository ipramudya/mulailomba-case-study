import checkUndoneCreationLomba from "@functions/check-undone-creation-lomba"
import useGetOrganizerLomba from "@organizerService/lomba/use-get-lomba-by-id"
import useUpsertLomba from "@organizerService/lomba/use-upsert-lomba"
import Button from "@ui/button"
import Form from "@ui/form"
import Typography from "@ui/typography"
import { isEmpty } from "lodash-es"
import { FunctionComponent, useMemo, useState } from "react"
import { SubmitHandler, useForm } from "react-hook-form"
import toast from "react-hot-toast"
import Skeleton from "react-loading-skeleton"
import { useParams } from "react-router-dom"
import { twMerge } from "tailwind-merge"

type FormFields = {
    rules: string
}

const Rules: FunctionComponent = () => {
    const [isEdited, setIsEdited] = useState(false)

    /* router stuff */
    const params = useParams()
    const lombaId = params.lombaId as string

    /* API calls */
    const { data: lombaOnDB } = useGetOrganizerLomba(lombaId)
    const { upsertLomba } = useUpsertLomba(lombaId)

    /* RH form */
    const methods = useForm<FormFields>()
    const { setValue, formState } = methods

    const onEditOrSave: SubmitHandler<FormFields> = async ({ rules }) => {
        if (!lombaOnDB) return

        if (!isEdited) {
            /* on edit */
            setIsEdited(true)

            if (lombaOnDB && !isEmpty(lombaOnDB)) {
                const { rules } = lombaOnDB

                setValue("rules", rules as string)
            }
        } else {
            /* on save */
            const { data: updateLombaData, error: updateLombaError } = await upsertLomba({ rules })

            if (!updateLombaData || updateLombaError) {
                return toast.error("Informasi aturan lomba gagal diubah, silahkan coba lagi")
            }

            toast.success("Informasi aturan lomba berhasil diubah")

            setIsEdited(false)
        }
    }

    const checkUndone = useMemo(() => {
        if (lombaOnDB) {
            return checkUndoneCreationLomba({
                rules: lombaOnDB.rules,
            } as any)
        }

        return null
    }, [lombaOnDB])

    return (
        <Form {...methods} onSubmit={onEditOrSave}>
            <div
                className={twMerge(
                    "w-full rounded-xl border border-neutral-100/50 p-4",
                    checkUndone === "undone" ? "border-warning-400/50" : undefined
                )}
            >
                <div className="flex flex-col space-y-6">
                    <div className="flex items-center justify-between">
                        <div className="flex flex-col space-y-1">
                            <Typography weight={600} size="sm">
                                Aturan
                            </Typography>
                            <Typography size="xs" className="text-neutral-400">
                                Anda dapat menjelaskan berbagai aturan untuk perlombaan anda
                            </Typography>
                            {checkUndone === "undone" && (
                                <Typography size="xs" className="text-warning-600">
                                    * Informasi perlu diatur
                                </Typography>
                            )}
                        </div>

                        <div className="flex space-x-3">
                            {isEdited && (
                                <Button
                                    size="xs"
                                    label="Batalkan"
                                    endIcon="heroicons:x-mark"
                                    intent="bordered"
                                    className="shrink-0"
                                    type="button"
                                    thin
                                    onClick={() => setIsEdited(false)}
                                />
                            )}
                            <Button
                                size="xs"
                                label={isEdited ? "Simpan" : "Edit"}
                                endIcon={
                                    isEdited
                                        ? "heroicons:check-solid"
                                        : "ph:pencil-simple-line-light"
                                }
                                intent={isEdited ? "primary-low" : "bordered"}
                                className="shrink-0"
                                type="submit"
                                thin
                                loading={formState.isSubmitting}
                            />
                        </div>
                    </div>

                    {!lombaOnDB ? (
                        <Skeleton count={4} className="h-5 w-[120px] " />
                    ) : (
                        <>
                            {isEdited ? (
                                <Form.TextareaField
                                    name="rules"
                                    label={false}
                                    placeholder="Jelaskan aturan perlombaan yang akan diadakan"
                                    maxLength={1000}
                                    hasCount
                                    classNames={{ input: "min-h-[120px] resize-y" }}
                                />
                            ) : (
                                <>
                                    {lombaOnDB.rules === null ? (
                                        <Typography
                                            as="span"
                                            size="sm"
                                            className="text-warning-600"
                                        >
                                            Belum diatur
                                        </Typography>
                                    ) : (
                                        lombaOnDB.rules.split("\n\n").map((r, idx) => (
                                            <Typography
                                                as="span"
                                                size="sm"
                                                key={"lomba-rules-" + idx}
                                            >
                                                {r}
                                            </Typography>
                                        ))
                                    )}
                                </>
                            )}
                        </>
                    )}
                </div>
            </div>
        </Form>
    )
}

export default Rules
