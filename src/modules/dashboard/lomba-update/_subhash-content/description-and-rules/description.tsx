import checkUndoneCreationLomba from "@functions/check-undone-creation-lomba"
import useGetOrganizerLomba from "@organizerService/lomba/use-get-lomba-by-id"
import useUpsertLomba from "@organizerService/lomba/use-upsert-lomba"
import Button from "@ui/button"
import Form from "@ui/form"
import Typography from "@ui/typography"
import { isEmpty } from "lodash-es"
import { FunctionComponent, useMemo, useState } from "react"
import { SubmitHandler, useForm } from "react-hook-form"
import toast from "react-hot-toast"
import Skeleton from "react-loading-skeleton"
import { useParams } from "react-router-dom"
import { twMerge } from "tailwind-merge"

type FormFields = {
    description: string
}

const Description: FunctionComponent = () => {
    const [isEdited, setIsEdited] = useState(false)

    /* router stuff */
    const params = useParams()
    const lombaId = params.lombaId as string

    /* API calls */
    const { data: lombaOnDB } = useGetOrganizerLomba(lombaId)
    const { upsertLomba } = useUpsertLomba(lombaId)

    /* RH form */
    const methods = useForm<FormFields>()
    const { setValue, formState } = methods

    const onEditOrSave: SubmitHandler<FormFields> = async ({ description }) => {
        if (!lombaOnDB) return

        if (!isEdited) {
            /* on edit */
            setIsEdited(true)

            if (lombaOnDB && !isEmpty(lombaOnDB)) {
                const { description } = lombaOnDB

                setValue("description", description as string)
            }
        } else {
            /* on save */

            const { data: updateLombaData, error: updateLombaError } = await upsertLomba({
                description,
            })

            if (!updateLombaData || updateLombaError) {
                return toast.error("Deskripsi lomba gagal diubah, silahkan coba lagi")
            }

            toast.success("Deskripsi lomba berhasil diubah")

            setIsEdited(false)
        }
    }

    const checkUndone = useMemo(() => {
        if (lombaOnDB) {
            return checkUndoneCreationLomba({
                description: lombaOnDB.description,
            } as any)
        }

        return null
    }, [lombaOnDB])

    return (
        <Form {...methods} onSubmit={onEditOrSave}>
            <div
                className={twMerge(
                    "w-full rounded-xl border border-neutral-100/50 p-4",
                    checkUndone === "undone" ? "border-warning-400/50" : undefined
                )}
            >
                <div className="flex flex-col space-y-6">
                    <div className="flex items-center justify-between">
                        <div className="flex flex-col space-y-1">
                            <Typography weight={600} size="sm">
                                Deskripsi
                            </Typography>
                            <Typography size="xs" className="text-neutral-400">
                                Deskripsikan bentuk perlombaan yang ingin kamu adakan
                            </Typography>
                            {checkUndone === "undone" && (
                                <Typography size="xs" className="text-warning-600">
                                    * Informasi perlu diatur
                                </Typography>
                            )}
                        </div>

                        <div className="flex space-x-3">
                            {isEdited && (
                                <Button
                                    size="xs"
                                    label="Batalkan"
                                    endIcon="heroicons:x-mark"
                                    intent="bordered"
                                    className="shrink-0"
                                    type="button"
                                    thin
                                    onClick={() => setIsEdited(false)}
                                />
                            )}
                            <Button
                                size="xs"
                                label={isEdited ? "Simpan" : "Edit"}
                                endIcon={
                                    isEdited
                                        ? "heroicons:check-solid"
                                        : "ph:pencil-simple-line-light"
                                }
                                intent={isEdited ? "primary-low" : "bordered"}
                                className="shrink-0"
                                type="submit"
                                thin
                                loading={formState.isSubmitting}
                            />
                        </div>
                    </div>

                    {!lombaOnDB ? (
                        <Skeleton count={4} className="h-5 w-[120px] " />
                    ) : (
                        <>
                            {isEdited ? (
                                <Form.TextareaField
                                    name="description"
                                    label={false}
                                    placeholder="Jelaskan aturan perlombaan yang akan diadakan"
                                    maxLength={1000}
                                    hasCount
                                    classNames={{ input: "min-h-[120px] resize-y" }}
                                />
                            ) : (
                                <>
                                    {lombaOnDB.description === null ? (
                                        <Typography
                                            as="span"
                                            size="sm"
                                            className="text-warning-600"
                                        >
                                            Belum diatur
                                        </Typography>
                                    ) : (
                                        <div className="flex flex-col space-y-2">
                                            {lombaOnDB.description.split("\n").map((d, idx) => (
                                                <Typography
                                                    as="span"
                                                    size="sm"
                                                    key={"lomba-desc-" + idx}
                                                >
                                                    {d}
                                                </Typography>
                                            ))}
                                        </div>
                                    )}
                                </>
                            )}
                        </>
                    )}
                </div>
            </div>
        </Form>
    )
}

export default Description
