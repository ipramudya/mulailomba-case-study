import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"

const ImplementationTypeSkeleton: FunctionComponent = () => {
    return (
        <>
            <div className="flex flex-col space-y-2">
                <Skeleton className="h-5 w-[120px] " />
                <div className="flex items-center space-x-3">
                    <Skeleton className="h-5 w-[80px] " />
                    <Skeleton className="h-5 w-[80px] " />
                </div>
            </div>
            <div className="flex flex-col space-y-2">
                <Skeleton className="h-5 w-[120px] " />
                <div className="flex items-center space-x-3">
                    <Skeleton className="h-5 w-[80px] " />
                    <Skeleton className="h-5 w-[80px] " />
                </div>
            </div>
        </>
    )
}

export default ImplementationTypeSkeleton
