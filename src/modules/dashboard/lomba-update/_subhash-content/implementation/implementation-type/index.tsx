import checkUndoneCreationLomba from "@functions/check-undone-creation-lomba"
import plotToBoolean from "@functions/plot-to-boolean"
import useGetOrganizerLomba from "@organizerService/lomba/use-get-lomba-by-id"
import useUpsertLomba from "@organizerService/lomba/use-upsert-lomba"
import Button from "@ui/button"
import Form from "@ui/form"
import Typography from "@ui/typography"
import { isEmpty } from "lodash-es"
import { FunctionComponent, useMemo, useState } from "react"
import { SubmitHandler, useForm } from "react-hook-form"
import toast from "react-hot-toast"
import { useParams } from "react-router-dom"
import { twMerge } from "tailwind-merge"

import ImplementationTypeSkeleton from "./implementation-type-skeleton"

type FormFields = {
    heldType: "online" | "offline"
    meetingLink?: string
    location?: string
}

const ImplementationType: FunctionComponent = () => {
    const [isEdited, setIsEdited] = useState(false)

    /* router stuff */
    const params = useParams()
    const lombaId = params.lombaId as string

    /* API calls */
    const { data: lombaOnDB } = useGetOrganizerLomba(lombaId)
    const { upsertLomba } = useUpsertLomba(lombaId)

    /* RH form */
    const methods = useForm<FormFields>()
    const { watch, setValue, formState } = methods
    const heldTypeFieldWatcher = watch("heldType")

    const onEditOrSave: SubmitHandler<FormFields> = async ({ heldType, location, meetingLink }) => {
        if (!isEdited) {
            /* on edit */
            setIsEdited(true)

            if (lombaOnDB && !isEmpty(lombaOnDB)) {
                const { is_held_online, held_location, held_meeting_link } = lombaOnDB

                setValue(
                    "heldType",
                    plotToBoolean({
                        considered: is_held_online,
                        truthy: "online",
                        falsy: "offline",
                    }) as any
                )
                setValue("meetingLink", held_meeting_link as string)
                setValue("location", held_location as string)
            }
        } else {
            /* on save */
            const { data: updateLombaData, error: updateLombaError } = await upsertLomba({
                isHeldOnline: Boolean(heldType === "online"),
                heldLocation: heldType === "offline" ? location : null,
                heldMeetingLink: heldType === "online" ? meetingLink : null,
            })

            if (!updateLombaData || updateLombaError) {
                return toast.error("Informasi tipe pelaksanaan gagal diubah")
            }

            toast.success("Informasi tipe pelaksanaan berhasil diubah")

            setIsEdited(false)
        }
    }

    const isHeldOnline = useMemo(() => {
        if (isEdited) {
            return heldTypeFieldWatcher === "online" ? 1 : 0
        }

        if (lombaOnDB) {
            return lombaOnDB.is_held_online
        }

        return null
    }, [heldTypeFieldWatcher, isEdited, lombaOnDB])

    const checkUndone = useMemo(() => {
        if (lombaOnDB) {
            return checkUndoneCreationLomba({
                is_held_online: lombaOnDB.is_held_online,
                held_location: lombaOnDB.held_location,
                held_meeting_link: lombaOnDB.held_meeting_link,
            } as any)
        }

        return null
    }, [lombaOnDB])

    return (
        <Form {...methods} onSubmit={onEditOrSave}>
            <div
                className={twMerge(
                    "w-full rounded-xl border border-neutral-100/50 p-4",
                    checkUndone === "undone" ? "border-warning-400/50" : undefined
                )}
            >
                <div className="flex flex-col space-y-6">
                    <div className="flex items-center justify-between">
                        <div className="flex flex-col space-y-1">
                            <Typography weight={600} size="sm">
                                Tipe Pelaksanaan
                            </Typography>
                            <Typography size="xs" className="text-neutral-400">
                                Atur bagaimana lomba akan diselenggarakan (daring / luring)
                            </Typography>
                            {checkUndone === "undone" && (
                                <Typography size="xs" className="text-warning-600">
                                    * Beberapa informasi perlu diatur
                                </Typography>
                            )}
                        </div>

                        <div className="flex space-x-3">
                            {isEdited && (
                                <Button
                                    size="xs"
                                    label="Batalkan"
                                    endIcon="heroicons:x-mark"
                                    intent="bordered"
                                    className="shrink-0"
                                    type="button"
                                    thin
                                    onClick={() => setIsEdited(false)}
                                />
                            )}
                            <Button
                                size="xs"
                                label={isEdited ? "Simpan" : "Edit"}
                                endIcon={
                                    isEdited
                                        ? "heroicons:check-solid"
                                        : "ph:pencil-simple-line-light"
                                }
                                intent={isEdited ? "primary-low" : "bordered"}
                                className="shrink-0"
                                type="submit"
                                thin
                                loading={formState.isSubmitting}
                            />
                        </div>
                    </div>

                    {!lombaOnDB ? (
                        <ImplementationTypeSkeleton />
                    ) : (
                        <div className="flex flex-col space-y-6">
                            {/* 👇 radio type */}
                            <div className="flex flex-col space-y-1">
                                <Typography as="span" size="xs" className="mb-1 text-neutral-400">
                                    Tipe
                                </Typography>
                                {isEdited ? (
                                    <Form.RadioField
                                        label={false}
                                        name="heldType"
                                        split
                                        options={[
                                            { label: "Tatap Muka", value: "offline" },
                                            { label: "Daring", value: "online" },
                                        ]}
                                    />
                                ) : (
                                    <>
                                        {lombaOnDB.is_held_online === null ? (
                                            <Typography
                                                as="span"
                                                size="sm"
                                                className="text-warning-600"
                                            >
                                                Belum diatur
                                            </Typography>
                                        ) : (
                                            <Typography as="span" size="sm">
                                                {isHeldOnline
                                                    ? "Daring (Online)"
                                                    : "Luring (Tatap Muka)"}
                                            </Typography>
                                        )}
                                    </>
                                )}
                            </div>

                            {/* 👇  link meeting or location */}
                            <div className="flex flex-col space-y-1">
                                <Typography as="span" size="xs" className="mb-1 text-neutral-400">
                                    {isHeldOnline ? "Link Meeting" : "Lokasi Penyelenggaraan"}
                                </Typography>
                                {isEdited ? (
                                    <>
                                        {isHeldOnline ? (
                                            <Form.TextField
                                                name="meetingLink"
                                                label="Link meeting"
                                                placeholder="Contoh: https://www.meeting.com/foo-bar-baz"
                                                required
                                                defaultValue={
                                                    lombaOnDB.held_meeting_link || undefined
                                                }
                                            />
                                        ) : (
                                            <Form.TextareaField
                                                name="location"
                                                label="Lokasi Penyelenggaraan"
                                                placeholder="Masukan lokasi penyelenggaraan lomba"
                                                required
                                                defaultValue={lombaOnDB.held_location || undefined}
                                            />
                                        )}
                                    </>
                                ) : (
                                    <>
                                        {lombaOnDB.is_held_online === null ? (
                                            <Typography
                                                as="span"
                                                size="sm"
                                                className="text-warning-600"
                                            >
                                                Belum diatur
                                            </Typography>
                                        ) : (
                                            <Typography as="span" size="sm">
                                                {isHeldOnline
                                                    ? lombaOnDB.held_meeting_link
                                                    : lombaOnDB.held_location}
                                            </Typography>
                                        )}
                                    </>
                                )}
                            </div>
                        </div>
                    )}
                </div>
            </div>
        </Form>
    )
}

export default ImplementationType
