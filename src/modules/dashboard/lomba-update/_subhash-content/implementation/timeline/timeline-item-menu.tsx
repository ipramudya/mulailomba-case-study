import Confirmation from "@blocks/confirmation"
import useDeleteTimeline from "@organizerService/lomba/timelines-lomba/use-delete-timeline"
import IconButton from "@ui/icon-button"
import Menu from "@ui/menu"
import { FunctionComponent, useState } from "react"
import toast from "react-hot-toast"

import { type TimelineFromDB } from "."
import TimelineModal from "./timeline-modal"

interface Props {
    index: number
    timeline: TimelineFromDB
}

const TimelineItemMenu: FunctionComponent<Props> = ({ timeline, index }) => {
    /* react states */
    const [isOpen, setOpen] = useState(false)
    const [isConfirmationOpen, setIsConfirmationOpen] = useState(false)

    const deleteTimeline = useDeleteTimeline()

    const onRemoveTimeline = async () => {
        const { error: deleteTimelineError } = await deleteTimeline(timeline.id)

        if (deleteTimelineError) {
            return toast.error("Gagal memproses penghapusan timeline")
        }

        toast.success(`Timeline ${timeline.name.toLowerCase()} berhasil dihapus`)
    }

    return (
        <>
            {isOpen && (
                <TimelineModal
                    isOpen={isOpen}
                    onOpenChange={setOpen}
                    timeline={timeline}
                    index={index}
                />
            )}
            {isConfirmationOpen && (
                <Confirmation
                    cb={onRemoveTimeline}
                    variant="delete"
                    isOpen={isConfirmationOpen}
                    onOpenChange={setIsConfirmationOpen}
                    title={`Menghapus ${timeline.name}`}
                    description="Apakah anda yakin untuk menghapus prasyarat tersebut ?"
                />
            )}
            <Menu.Root
                contentAlign="end"
                triggerButton={
                    <IconButton
                        aria-label="update-timeline-menu"
                        icon="heroicons:ellipsis-vertical"
                        size="sm"
                        circle
                        intent="bordered"
                    />
                }
            >
                <Menu.Item
                    label="Perbarui"
                    icon="heroicons:pencil-square"
                    onClick={() => setOpen(true)}
                />
                <Menu.Item
                    label="Hapus"
                    icon="heroicons:trash"
                    className="rd-highlighted:bg-danger-50 rd-highlighted:text-danger-600"
                    onClick={() => setIsConfirmationOpen(true)}
                />
            </Menu.Root>
        </>
    )
}

export default TimelineItemMenu
