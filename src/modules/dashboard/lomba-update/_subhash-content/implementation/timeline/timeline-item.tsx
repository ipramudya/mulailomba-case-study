import { formatDate } from "@functions/format-date"
import Typography from "@ui/typography"
import { useAtomValue } from "jotai"
import { FunctionComponent } from "react"

import { EditableTimelineAtom, TimelineFromDB } from "."
import TimelineItemMenu from "./timeline-item-menu"

interface Props {
    timeline: TimelineFromDB
    index: number
}

const TimelineItem: FunctionComponent<Props> = ({ timeline, index }) => {
    const isTimelineEdited = useAtomValue(EditableTimelineAtom)

    return (
        <div className="col-span-1 flex flex-col space-y-4 overflow-hidden rounded-lg border border-neutral-100/50 p-3">
            <div className="flex w-full items-center justify-between ">
                <Typography size="sm" className="max-w-[80%] ">
                    {timeline.name}
                </Typography>
                {isTimelineEdited && <TimelineItemMenu index={index} timeline={timeline} />}
            </div>
            <Typography size="xs">{timeline.description}</Typography>
            <div className="grid grid-cols-2 gap-x-4">
                <div className="col-span-1 flex flex-col space-y-1">
                    <Typography as="span" size="xs" className="mb-1 text-neutral-400">
                        Waktu Timeline Dimulai
                    </Typography>
                    <Typography as="span" size="sm">
                        {formatDate(timeline.start_date, "E, dd MMMM y")}
                    </Typography>
                </div>
                <div className="col-span-1 flex flex-col space-y-1">
                    <Typography as="span" size="xs" className="mb-1 text-neutral-400">
                        Waktu Timeline Berakhir
                    </Typography>
                    <Typography as="span" size="sm">
                        {formatDate(timeline.end_date, "E, dd MMMM y")}
                    </Typography>
                </div>
            </div>
        </div>
    )
}

export default TimelineItem
