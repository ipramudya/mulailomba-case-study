import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"

const TimelineItemSkeleton: FunctionComponent = () => {
    return (
        <>
            {Array.from({ length: 4 }).map((_, idx) => (
                <div
                    className="col-span-1 flex flex-col space-y-4 overflow-hidden rounded-lg border border-neutral-100/50 p-3"
                    key={"timeline-skeleton-" + idx}
                >
                    <Skeleton className="h-5 w-[120px] " />
                    <Skeleton className="h-5 w-full " count={3} />
                    <div className="grid grid-cols-2 gap-x-3">
                        <Skeleton className="h-10" />
                        <Skeleton className="h-10" />
                    </div>
                    {/*  */}
                </div>
            ))}
        </>
    )
}

export default TimelineItemSkeleton
