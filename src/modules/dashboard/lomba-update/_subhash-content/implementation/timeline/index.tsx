import NotFound from "@blocks/not-found"
import useGetAllTimelinesLomba from "@organizerService/lomba/timelines-lomba/use-get-all-timelines-lomba"
import Button from "@ui/button"
import Form from "@ui/form"
import Typography from "@ui/typography"
import { atom, useAtom } from "jotai"
import { isEmpty } from "lodash-es"
import { FunctionComponent, useRef, useState } from "react"
import { SubmitHandler, useForm } from "react-hook-form"
import { useParams } from "react-router-dom"

import TimelineItem from "./timeline-item"
import TimelineItemSkeleton from "./timeline-item-skeleton"
import TimelineModal from "./timeline-modal"

export type TimelineFromDB = {
    id: string
    name: string
    description: string
    start_date: string
    end_date: string
}

type FormFields = {
    timelines?: Omit<TimelineFromDB, "start_date" | "end_date"> &
        {
            startDate: string
            endDate: string
        }[]
}

export const EditableTimelineAtom = atom(false)

const Timeline: FunctionComponent = () => {
    const [isOpen, setIsOpen] = useState(false)
    const formRef = useRef()

    /* shareable state */
    const [isEdited, setIsEdited] = useAtom(EditableTimelineAtom)

    /* router stuff */
    const params = useParams()
    const lombaId = params.lombaId as string

    /* API calls */
    const { data: timelineOnDB } = useGetAllTimelinesLomba(lombaId)

    /* RH form */
    const methods = useForm<FormFields>()

    const onEditOrSave: SubmitHandler<FormFields> = (_, event) => {
        if ((event && event.target) !== formRef.current) return

        if (!isEdited) {
            if (timelineOnDB === null || timelineOnDB === undefined) return

            /* on edit */
            setIsEdited(true)
            const remapTimelineFromDB = timelineOnDB.map((t) => {
                const { start_date, end_date, ...rest } = t
                return {
                    ...rest,
                    startDate: start_date,
                    endDate: end_date,
                }
            })
            methods.setValue("timelines", remapTimelineFromDB as any)
            return
        }

        setIsEdited(false)
    }

    return (
        <>
            {isOpen && <TimelineModal isOpen={isOpen} onOpenChange={setIsOpen} />}
            <Form {...methods} onSubmit={onEditOrSave} ref={formRef}>
                <div className="w-full rounded-xl border border-neutral-100/50 p-4">
                    <div className="flex flex-col space-y-6">
                        <div className="flex items-center justify-between">
                            <div className="flex flex-col space-y-1">
                                <Typography weight={600} size="sm">
                                    Timeline
                                </Typography>
                                <Typography size="xs" className="text-neutral-400">
                                    Pecah informasi pelaksanaan lomba berupa timeline agar
                                    memudahkan pendaftar
                                </Typography>
                            </div>
                            <div className="flex space-x-3">
                                {isEdited && (
                                    <Button
                                        size="xs"
                                        label="Batalkan"
                                        endIcon="heroicons:x-mark"
                                        intent="bordered"
                                        className="shrink-0"
                                        thin
                                        onClick={() => setIsEdited(false)}
                                    />
                                )}
                                <Button
                                    size="xs"
                                    label={isEdited ? "Selesai" : "Edit"}
                                    endIcon={
                                        isEdited
                                            ? "heroicons:check-solid"
                                            : "ph:pencil-simple-line-light"
                                    }
                                    intent={isEdited ? "primary-low" : "bordered"}
                                    className="shrink-0"
                                    type="submit"
                                    thin
                                />
                            </div>
                        </div>

                        {isEmpty(timelineOnDB) ? (
                            <NotFound message="Lomba ini belum memiliki timeline">
                                {isEdited && (
                                    <Button
                                        size="sm"
                                        label="Atur Timeline"
                                        thin
                                        onClick={() => setIsOpen(true)}
                                    />
                                )}
                            </NotFound>
                        ) : (
                            <div className="grid grid-cols-1 gap-x-4 gap-y-6">
                                {timelineOnDB === null || timelineOnDB === undefined ? (
                                    <TimelineItemSkeleton />
                                ) : (
                                    <>
                                        {isEdited && (
                                            <div className="flex items-center justify-center rounded-lg py-4 outline-dashed outline-1 outline-neutral-100">
                                                <Button
                                                    size="xs"
                                                    pill
                                                    label="Tambah Timeline"
                                                    className="px-3"
                                                    endIcon="heroicons:plus"
                                                    iconSize="sm"
                                                    thin
                                                    onClick={() => setIsOpen(true)}
                                                />
                                            </div>
                                        )}
                                        {timelineOnDB.map((t, idx) => (
                                            <TimelineItem
                                                index={idx}
                                                timeline={t as any}
                                                key={"timeline-item-" + idx}
                                            />
                                        ))}
                                    </>
                                )}
                            </div>
                        )}
                    </div>
                </div>
            </Form>
        </>
    )
}

export default Timeline
