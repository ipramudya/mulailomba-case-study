import useCreateTimeline from "@organizerService/lomba/timelines-lomba/use-create-timeline"
import useUpdateTimeline from "@organizerService/lomba/timelines-lomba/use-update-timeline"
import useGetOrganizerLomba from "@organizerService/lomba/use-get-lomba-by-id"
import Button from "@ui/button"
import Form from "@ui/form"
import Modal from "@ui/modal"
import { isEmpty } from "lodash-es"
import { FunctionComponent } from "react"
import { SubmitHandler, useForm } from "react-hook-form"
import { toast } from "react-hot-toast"
import { useParams } from "react-router-dom"

import { TimelineFromDB } from "."

type FormFields = Omit<TimelineFromDB, "start_date" | "end_date"> & {
    startDate: string
    endDate: string
}

interface Props {
    isOpen: boolean
    onOpenChange(val: boolean): void
    timeline?: TimelineFromDB
    index?: number
}

const TimelineModal: FunctionComponent<Props> = ({
    isOpen,
    onOpenChange,
    timeline,
    index = null,
}) => {
    /* router stuff */
    const params = useParams()
    const lombaId = params.lombaId as string

    /* API calls */
    const { data: lombaOnDB } = useGetOrganizerLomba(lombaId)
    const updateTimeline = useUpdateTimeline()
    const { createTimeline } = useCreateTimeline(lombaId)

    /* hook form */
    const methods = useForm<FormFields>()
    const getDefaultValue = (key: keyof TimelineFromDB) => {
        if (timeline && index !== null) {
            return timeline[key]
        }

        return undefined
    }

    /* append fields handler */
    const onTimelineAdded: SubmitHandler<FormFields> = async (fields) => {
        if (!lombaOnDB || isEmpty(lombaOnDB) || !(lombaOnDB.start_date && lombaOnDB.end_date)) {
            return toast.error("Atur deadline lomba terlebih dahulu pada tab pendaftaran")
        }

        if (timeline) {
            const { data: updatePrerequisiteData, error: updatePrerequisiteError } =
                await updateTimeline({
                    ...fields,
                    id: timeline.id,
                })

            if (!updatePrerequisiteData || updatePrerequisiteError) {
                return toast.error("Informasi timeline gagal diubah, silahkan coba lagi")
            }

            toast.success("Informasi timeline berhasil diubah")
        } else {
            const { data: createPrerequisiteData, error: createPrerequisiteError } =
                await createTimeline({
                    ...fields,
                })

            if (!createPrerequisiteData || createPrerequisiteError) {
                return toast.error("Informasi timeline gagal dibuat, silahkan coba lagi")
            }

            toast.success("Informasi timeline berhasil dibuat")
        }
        onOpenChange(false)
    }

    return (
        <Modal open={isOpen} onOpenChange={onOpenChange} title="Atur Timeline Pelaksanaan">
            <Form {...methods} onSubmit={onTimelineAdded}>
                <Form.Layout>
                    <Form.TextField
                        name="name"
                        label="Nama Timeline"
                        required
                        placeholder="Masukan nama timeline"
                        defaultValue={getDefaultValue("name")}
                    />
                    <Form.TextareaField
                        name="description"
                        label="Deskripsi Timeline"
                        required
                        placeholder="Jelaskan mengenai timeline yang dibuat"
                        maxLength={256}
                        hasCount
                        defaultValue={getDefaultValue("description")}
                    />
                    <Form.RowLayout column={2}>
                        <Form.TextField
                            label="Waktu dimulai"
                            name="startDate"
                            required
                            type="datetime-local"
                            defaultValue={getDefaultValue("start_date")}
                        />
                        <Form.TextField
                            label="Waktu berakhir"
                            name="endDate"
                            required
                            type="datetime-local"
                            defaultValue={getDefaultValue("end_date")}
                        />
                    </Form.RowLayout>
                </Form.Layout>
                <div className="mt-9 flex items-center justify-end space-x-3 ">
                    <Button
                        label="Batalkan"
                        intent="bordered"
                        onClick={() => onOpenChange(false)}
                    />
                    <Button
                        type="submit"
                        label={timeline ? "Perbarui Timeline" : "Buat Timeline"}
                    />
                </div>
            </Form>
        </Modal>
    )
}

export default TimelineModal
