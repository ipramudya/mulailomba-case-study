import { Icon } from "@iconify/react"
import useGetOrganizerLomba from "@organizerService/lomba/use-get-lomba-by-id"
import useUpsertLomba from "@organizerService/lomba/use-upsert-lomba"
import Badge from "@ui/badge"
import Button from "@ui/button"
import Form from "@ui/form"
import Typography from "@ui/typography"
import { isEmpty } from "lodash-es"
import { FunctionComponent, useMemo, useState } from "react"
import { SubmitHandler, useFieldArray, useForm } from "react-hook-form"
import toast from "react-hot-toast"
import { useParams } from "react-router-dom"

import FieldArraySkeleton from "./field-array-skeleton"

type FormFields = {
    eligibilities: string[]
}

const Eligibility: FunctionComponent = () => {
    const [isEdited, setIsEdited] = useState(false)

    /* router stuff */
    const params = useParams()
    const lombaId = params.lombaId as string

    /* API calls */
    const { data: lombaOnDB } = useGetOrganizerLomba(lombaId)
    const { upsertLomba } = useUpsertLomba(lombaId)

    /* RH form */
    const methods = useForm()
    const { watch, setValue, control, handleSubmit, formState } = methods
    const eligibilitiesFieldWatcher: string[] = watch("eligibilities")
    const { remove } = useFieldArray({ control, name: "eligibilities" })

    const eligibilitiesToRender = useMemo(() => {
        if (isEdited) return eligibilitiesFieldWatcher

        if (lombaOnDB && !isEmpty(lombaOnDB)) {
            return lombaOnDB.eligibilities
        }

        return null
    }, [eligibilitiesFieldWatcher, isEdited, lombaOnDB])

    const onEditOrSave: SubmitHandler<FormFields> = async ({ eligibilities }) => {
        if (!lombaOnDB) return

        if (!isEdited) {
            /* on edit */
            setIsEdited(true)

            if (lombaOnDB && !isEmpty(lombaOnDB)) {
                const { eligibilities } = lombaOnDB
                setValue("eligibilities", eligibilities as string[])
            }
        } else {
            /* on save */
            const { data: updateLombaData, error: updateLombaError } = await upsertLomba({
                eligibilities,
            })

            if (!updateLombaData || updateLombaError) {
                return toast.error("Gagal mengubah data kelayakan")
            }

            toast.success("Data kelayakan lomba berhasil diubah")

            setIsEdited(false)
        }
    }

    return (
        <Form {...methods} onSubmit={handleSubmit(onEditOrSave as any)}>
            <div className="w-full rounded-xl border border-neutral-100/50 p-4">
                <div className="flex flex-col space-y-6">
                    <div className="flex items-center justify-between">
                        <div className="flex flex-col space-y-1">
                            <Typography weight={600} size="sm">
                                Kelayakan
                            </Typography>
                            <Typography size="xs" className="text-neutral-400">
                                Tentukan peserta yang dapat mengikuti perlombaan
                            </Typography>
                        </div>

                        <div className="flex space-x-3">
                            {isEdited && (
                                <Button
                                    size="xs"
                                    label="Batalkan"
                                    endIcon="heroicons:x-mark"
                                    intent="bordered"
                                    className="shrink-0"
                                    type="button"
                                    thin
                                    onClick={() => setIsEdited(false)}
                                />
                            )}
                            <Button
                                size="xs"
                                label={isEdited ? "Simpan" : "Edit"}
                                endIcon={
                                    isEdited
                                        ? "heroicons:check-solid"
                                        : "ph:pencil-simple-line-light"
                                }
                                intent={isEdited ? "primary-low" : "bordered"}
                                className="shrink-0"
                                type="submit"
                                thin
                                loading={formState.isSubmitting}
                            />
                        </div>
                    </div>

                    {isEdited && (
                        <Form.CreatableSelectField
                            label={false}
                            name="eligibilities"
                            placeholder="Cari atau tambahakan kelayakan baru"
                            required
                            options={OPTIONS}
                            controlShouldRenderValue={false}
                            icon="heroicons:magnifying-glass"
                            defaultValue={lombaOnDB?.eligibilities}
                        />
                    )}

                    {!lombaOnDB || !eligibilitiesToRender ? (
                        <FieldArraySkeleton />
                    ) : (
                        <div className="flex items-center space-x-2">
                            {eligibilitiesToRender.map((eligibility, idx) => (
                                <Badge
                                    key={"eligibility-" + idx}
                                    label={eligibility}
                                    intent="primary-low"
                                    endElement={
                                        isEdited ? (
                                            <button
                                                className="ml-1"
                                                type="button"
                                                onClick={() => {
                                                    methods
                                                        .getValues("eligibilities")
                                                        .forEach((v: any, idx: number) => {
                                                            if (v === eligibility) {
                                                                remove(idx)
                                                            }
                                                        })
                                                }}
                                            >
                                                <Icon icon="heroicons:x-mark" />
                                            </button>
                                        ) : undefined
                                    }
                                />
                            ))}
                        </div>
                    )}
                </div>
            </div>
        </Form>
    )
}

export default Eligibility

const OPTIONS = [
    { label: "Pelajar", value: "Pelajar" },
    { label: "Umum", value: "Umum" },
]
