import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"

const BenefitSkeleton: FunctionComponent = () => {
    return (
        <div className="flex items-center space-x-2">
            {Array.from({ length: 6 }).map((_, idx) => (
                <Skeleton key={"benefit-skeleton-" + idx} className="h-5 w-[80px] " />
            ))}
        </div>
    )
}

export default BenefitSkeleton
