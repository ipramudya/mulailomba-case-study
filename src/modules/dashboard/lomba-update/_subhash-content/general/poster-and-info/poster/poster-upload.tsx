import formatBytes from "@functions/format-bytes"
import useDropImage from "@hooks/use-drop-image"
import { Icon } from "@iconify/react"
import Button from "@ui/button"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"
import { useFormContext } from "react-hook-form"

import { FormFields } from ".."

const PosterUpload: FunctionComponent = () => {
    /* hook form */
    const { setValue } = useFormContext<FormFields>()

    /* dropzone */
    const { uploaded, getRootProps, getInputProps, dropzoneVars } = useDropImage({
        noClick: true,
        onDropAccepted(file) {
            if (file) {
                setValue("file", file[0])
            }
        },
    })

    return (
        <div {...getRootProps()} className="flex flex-col">
            <input {...getInputProps()} />
            {uploaded ? (
                <div className="aspect-[3/4] w-[140px] overflow-hidden rounded">
                    <img src={uploaded.preview} className="h-full w-full object-cover" />
                </div>
            ) : (
                <div className="flex aspect-[3/4] w-[140px] flex-col items-center justify-center space-y-2 overflow-hidden rounded border border-dashed border-neutral-100">
                    <Icon icon="heroicons:photo" className="h-6 w-6 text-neutral-500 " />
                    <Typography size="xs" className="text-center text-neutral-500">
                        Jatuhkan file di sini
                    </Typography>
                </div>
            )}

            {uploaded && (
                <>
                    <div className="mt-3 flex flex-col">
                        <Typography as="span" size="xs" className="mb-1 text-neutral-400">
                            Nama File
                        </Typography>
                        <Typography size="sm" className="max-w-[140px] truncate ">
                            {uploaded.file.name}
                        </Typography>
                    </div>
                    <div className="mt-3 flex flex-col">
                        <Typography as="span" size="xs" className="mb-1 text-neutral-400">
                            Besar File
                        </Typography>
                        <Typography size="sm" className="max-w-[140px] truncate ">
                            {formatBytes(uploaded.file.size)}
                        </Typography>
                    </div>
                </>
            )}
            <Button
                label="Unggah Poster"
                className="mt-2"
                endIcon="heroicons:arrow-up-on-square"
                size="xs"
                intent="bordered"
                thin
                onClick={() => dropzoneVars.open()}
            />
        </div>
    )
}

export default PosterUpload
