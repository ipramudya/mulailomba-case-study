import { Icon } from "@iconify/react"
import Button from "@ui/button"
import Typography from "@ui/typography"
import { useAtomValue } from "jotai"
import { FunctionComponent } from "react"
import { useFormContext } from "react-hook-form"

import { EditablePosterAndInfoAtom, FormFields } from ".."

interface Props {
    poster?: string
    name?: string
}

const PosterCloud: FunctionComponent<Props> = ({ name, poster }) => {
    const isEdited = useAtomValue(EditablePosterAndInfoAtom)

    /* hook form */
    const { setValue } = useFormContext<FormFields>()

    return (
        <div className="flex flex-col">
            <div className="aspect-[3/4] w-[140px] overflow-hidden rounded">
                {poster ? (
                    <img src={poster} className="h-full w-full object-cover" />
                ) : (
                    <div className="flex h-full flex-col items-center justify-center space-y-2 bg-secondary-50">
                        <Icon icon="heroicons:photo" className=" h-6 w-6 " />
                        <Typography size="xs" weight={500}>
                            Belum ada poster
                        </Typography>
                    </div>
                )}
            </div>
            <div className="mt-3 flex flex-col">
                {name && (
                    <div className="flex flex-col">
                        <Typography as="span" size="xs" className="mb-1 text-neutral-400">
                            Nama File
                        </Typography>
                        <Typography size="sm" className="max-w-[140px] truncate ">
                            {name.split("/").at(-1)}
                        </Typography>
                    </div>
                )}
                {isEdited && (
                    <Button
                        label="Hapus Poster"
                        className="mt-1"
                        endIcon="heroicons:x-mark"
                        size="xs"
                        intent="danger-low"
                        thin
                        onClick={() => setValue("file", "")}
                    />
                )}
            </div>
        </div>
    )
}

export default PosterCloud
