import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"

const PosterAndInfoSkeleton: FunctionComponent = () => {
    return (
        <div className="flex space-x-6">
            <div className="flex flex-col">
                <Skeleton className="aspect-[3/4] w-[120px] rounded" />
                <div className="mt-3 flex flex-col">
                    <Skeleton className="mb-1 h-5 w-[80px]" />
                    <Skeleton className="h-[30px] w-[120px]" />
                </div>
            </div>

            <div className="flex flex-col space-y-3">
                <div className="flex flex-col">
                    <Skeleton className="h-5 w-[100px] " />
                    <Skeleton className="mt-2 h-5 w-[160px] " />
                </div>
                <div className="flex flex-col">
                    <Skeleton className="h-5 w-[100px] " />
                    <Skeleton className="mt-2 h-5 w-[160px] " />
                </div>
            </div>
        </div>
    )
}

export default PosterAndInfoSkeleton
