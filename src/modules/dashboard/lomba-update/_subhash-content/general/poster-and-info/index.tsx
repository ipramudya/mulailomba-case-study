import useGetOrganizerLomba from "@organizerService/lomba/use-get-lomba-by-id"
import useUpsertLomba from "@organizerService/lomba/use-upsert-lomba"
import deletePoster from "@organizerService/poster/delete-poster"
import getPosterPublicURL from "@organizerService/poster/get-poster-public-url"
import useUploadPoster from "@organizerService/poster/use-upload-poster"
import Button from "@ui/button"
import Form from "@ui/form"
import Typography from "@ui/typography"
import { atom, useAtom } from "jotai"
import { isEmpty } from "lodash-es"
import { FunctionComponent } from "react"
import { SubmitHandler, useForm } from "react-hook-form"
import toast from "react-hot-toast"
import { useParams } from "react-router-dom"

import PosterAndInfoSkeleton from "./poster-and-info-skeleton"
import PosterCloud from "./poster/poster-cloud"
import PosterUpload from "./poster/poster-upload"

export const EditablePosterAndInfoAtom = atom(false)

export type FormFields = {
    file: File | string
    name: string
    category: string
}

const PosterAndInfo: FunctionComponent = () => {
    /* router stuff */
    const params = useParams()
    const lombaId = params.lombaId as string

    /* API calls */
    const { upsertLomba } = useUpsertLomba(lombaId)
    const { data: lombaOnDB } = useGetOrganizerLomba(lombaId)
    const { uploadPoster } = useUploadPoster()
    const posterPublicURL = lombaOnDB?.poster_public_url

    /* shareable state */
    const [isEdited, setIsEdited] = useAtom(EditablePosterAndInfoAtom)

    /* RH Form */
    const methods = useForm<FormFields>()
    const { formState, setValue } = methods
    const fileFieldWatcher = methods.watch("file")

    const onEditOrSave: SubmitHandler<FormFields> = async ({ category, file, name }) => {
        if (!lombaOnDB) return

        if (!isEdited) {
            /* on edit */
            setIsEdited(true)

            if (lombaOnDB && !isEmpty(lombaOnDB)) {
                const { name, poster_public_url, category } = lombaOnDB
                setValue("file", poster_public_url as string)
                setValue("name", name as string)
                setValue("category", category as string)
            }
        } else {
            /* on save */
            const { poster_storage_url } = lombaOnDB

            /* hapus poster di db/storage ketika payload file 👇 */
            if (!file || typeof file !== "string") {
                if (poster_storage_url) {
                    deletePoster(poster_storage_url)
                }

                upsertLomba({
                    posterPublicUrl: null,
                    posterStorageUrl: null,
                })
            }

            /* ketika poster baru ditambahkan */
            if (typeof file !== "string") {
                const response = await uploadPoster({
                    file: file,
                    fileName: file.name.replace(/\s+/g, "_"),
                })

                if (!response.data || response.error) {
                    return toast.error("Upload poster gagal, silahkan coba kembali")
                }

                await upsertLomba({
                    posterStorageUrl: response.data.path,
                    posterPublicUrl: getPosterPublicURL(response.data.path).data.publicUrl,
                })
            }

            /* update lomba on db*/
            const { data: updateLombaData, error: updateLombaError } = await upsertLomba({
                name,
                category,
            })

            /* ketika error, hapus data terkait yg telah diupload */
            if (!updateLombaData || updateLombaError) {
                return toast.error("Gagal menambahkan informasi tahap umum")
            }

            toast.success("Informasi umum berhasil diubah")

            setIsEdited(false)
        }
    }

    return (
        <Form {...methods} onSubmit={onEditOrSave}>
            <div className="w-full rounded-xl border border-neutral-100/50 p-4">
                <div className="flex items-start justify-between gap-[5rem] ">
                    {!lombaOnDB ? (
                        <PosterAndInfoSkeleton />
                    ) : (
                        <div className="flex grow space-x-6">
                            {!isEdited ||
                            (fileFieldWatcher && typeof fileFieldWatcher === "string") ? (
                                <PosterCloud
                                    name={lombaOnDB.poster_public_url as string}
                                    poster={posterPublicURL as string}
                                />
                            ) : (
                                <PosterUpload />
                            )}

                            <div className="flex grow flex-col space-y-6">
                                <div className="flex flex-col">
                                    <Typography
                                        as="span"
                                        size="xs"
                                        className="mb-1 text-neutral-400"
                                    >
                                        Nama Lomba
                                    </Typography>
                                    {isEdited ? (
                                        <Form.TextField
                                            label={false}
                                            name="name"
                                            size="sm"
                                            classNames={{ wrapper: "w-full" }}
                                        />
                                    ) : (
                                        <Typography as="span" size="sm">
                                            {lombaOnDB.name}
                                        </Typography>
                                    )}
                                </div>

                                <div className="flex flex-col">
                                    <Typography
                                        as="span"
                                        size="xs"
                                        className="mb-1 text-neutral-400"
                                    >
                                        Kategori Lomba
                                    </Typography>
                                    {isEdited ? (
                                        <Form.SelectField
                                            isClearable
                                            label={false}
                                            name="category"
                                            placeholder="Pilih kategori"
                                            required
                                            options={OPTIONS}
                                        />
                                    ) : (
                                        <Typography as="span" size="sm">
                                            {lombaOnDB.category}
                                        </Typography>
                                    )}
                                </div>
                            </div>
                        </div>
                    )}

                    <div className="flex flex-col space-y-3">
                        <Button
                            size="xs"
                            label={isEdited ? "Simpan" : "Edit"}
                            endIcon={
                                isEdited ? "heroicons:check-solid" : "ph:pencil-simple-line-light"
                            }
                            intent={isEdited ? "primary-low" : "bordered"}
                            className="shrink-0"
                            type="submit"
                            thin
                            loading={formState.isSubmitting}
                        />
                        {isEdited && (
                            <Button
                                size="xs"
                                label="Batalkan"
                                endIcon="heroicons:x-mark"
                                intent="bordered"
                                className="shrink-0"
                                type="button"
                                thin
                                onClick={() => setIsEdited(false)}
                            />
                        )}
                    </div>
                </div>
            </div>
        </Form>
    )
}

export default PosterAndInfo

const OPTIONS = [
    { label: "Musik", value: "Musik" },
    { label: "Pendidikan", value: "Pendidikan" },
    { label: "Olahraga", value: "Olahraga" },
    {
        label: "Sastra dan Bahasa",
        value: "Sastra dan Bahasa",
    },
    { label: "Esport Game", value: "Esport Game" },
    {
        label: "Olimpiade Sains dan Teknologi",
        value: "Olimpiade Sains dan teknologi",
    },
]
