import { Icon } from "@iconify/react"
import useGetOrganizerLomba from "@organizerService/lomba/use-get-lomba-by-id"
import useUpsertLomba from "@organizerService/lomba/use-upsert-lomba"
import Badge from "@ui/badge"
import Button from "@ui/button"
import Form from "@ui/form"
import Typography from "@ui/typography"
import { isEmpty } from "lodash-es"
import { FunctionComponent, useMemo, useState } from "react"
import { SubmitHandler, useFieldArray, useForm } from "react-hook-form"
import toast from "react-hot-toast"
import { useParams } from "react-router-dom"

import FieldArraySkeleton from "./field-array-skeleton"

type FormFields = {
    benefits: string[]
}

const Benefit: FunctionComponent = () => {
    const [isEdited, setIsEdited] = useState(false)

    /* router stuff */
    const params = useParams()
    const lombaId = params.lombaId as string

    /* RH form */
    const methods = useForm()
    const { watch, setValue, control, handleSubmit, formState } = methods
    const benefitsFieldWatcher: string[] = watch("benefits")
    const { remove } = useFieldArray({ control, name: "benefits" })

    /* API calls */
    const { data: lombaOnDB } = useGetOrganizerLomba(lombaId)
    const { upsertLomba } = useUpsertLomba(lombaId)

    const benefitsToRender = useMemo(() => {
        if (isEdited) return benefitsFieldWatcher

        if (lombaOnDB && !isEmpty(lombaOnDB)) {
            return lombaOnDB.benefits
        }

        return null
    }, [benefitsFieldWatcher, isEdited, lombaOnDB])

    const onEditOrSave: SubmitHandler<FormFields> = async ({ benefits }) => {
        if (!lombaOnDB) return

        if (!isEdited) {
            /* on edit */
            setIsEdited(true)

            if (lombaOnDB && !isEmpty(lombaOnDB)) {
                const { benefits } = lombaOnDB
                setValue("benefits", benefits)
            }
        } else {
            /* on save */
            const { data: updateLombaData, error: updateLombaError } = await upsertLomba({
                benefits,
            })

            if (!updateLombaData || updateLombaError) {
                return toast.error("Gagal mengubah data benefit")
            }

            toast.success("Benefit lomba berhasil diubah")

            setIsEdited(false)
        }
    }

    return (
        <Form {...methods} onSubmit={handleSubmit(onEditOrSave as any)}>
            <div className="w-full rounded-xl border border-neutral-100/50 p-4">
                <div className="flex flex-col space-y-6">
                    <div className="flex items-center justify-between">
                        <div className="flex flex-col space-y-1">
                            <Typography weight={600} size="sm">
                                Benefit
                            </Typography>
                            <Typography size="xs" className="text-neutral-400">
                                Keuntungan yang akan diperoleh peserta lomba
                            </Typography>
                        </div>
                        <div className="flex space-x-3">
                            {isEdited && (
                                <Button
                                    size="xs"
                                    label="Batalkan"
                                    endIcon="heroicons:x-mark"
                                    intent="bordered"
                                    className="shrink-0"
                                    type="button"
                                    thin
                                    onClick={() => setIsEdited(false)}
                                />
                            )}
                            <Button
                                size="xs"
                                label={isEdited ? "Simpan" : "Edit"}
                                endIcon={
                                    isEdited
                                        ? "heroicons:check-solid"
                                        : "ph:pencil-simple-line-light"
                                }
                                intent={isEdited ? "primary-low" : "bordered"}
                                className="shrink-0"
                                type="submit"
                                thin
                                loading={formState.isSubmitting}
                            />
                        </div>
                    </div>

                    {isEdited && (
                        <Form.CreatableSelectField
                            label={false}
                            name="benefits"
                            placeholder="Cari atau tambahakan benefit baru"
                            required
                            options={OPTIONS}
                            controlShouldRenderValue={false}
                            icon="heroicons:magnifying-glass"
                            defaultValue={lombaOnDB?.benefits}
                        />
                    )}

                    {!benefitsToRender ? (
                        <FieldArraySkeleton />
                    ) : (
                        <div className="flex items-center space-x-2">
                            {benefitsToRender.map((benefit, idx) => (
                                <Badge
                                    key={"benefit-" + idx}
                                    label={benefit}
                                    intent="primary-low"
                                    endElement={
                                        isEdited ? (
                                            <button
                                                className="ml-1"
                                                type="button"
                                                onClick={() => {
                                                    methods
                                                        .getValues("benefits")
                                                        .forEach((v: any, idx: number) => {
                                                            if (v === benefit) {
                                                                remove(idx)
                                                            }
                                                        })
                                                }}
                                            >
                                                <Icon icon="heroicons:x-mark" />
                                            </button>
                                        ) : undefined
                                    }
                                />
                            ))}
                        </div>
                    )}
                </div>
            </div>
        </Form>
    )
}

export default Benefit

const OPTIONS = [
    { label: "Uang Tunai", value: "Uang Tunai" },
    { label: "Sertifikat", value: "Sertifikat" },
]
