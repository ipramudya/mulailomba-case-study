import checkUndoneCreationLomba from "@functions/check-undone-creation-lomba"
import { formatDate } from "@functions/format-date"
import useGetOrganizerLomba from "@organizerService/lomba/use-get-lomba-by-id"
import useUpsertLomba from "@organizerService/lomba/use-upsert-lomba"
import Button from "@ui/button"
import Form from "@ui/form"
import Typography from "@ui/typography"
import { isEmpty } from "lodash-es"
import { FunctionComponent, useMemo, useState } from "react"
import { SubmitHandler, useForm } from "react-hook-form"
import toast from "react-hot-toast"
import { useParams } from "react-router-dom"
import { twMerge } from "tailwind-merge"

import DeadlineSkeleton from "./deadline-skeleton"

type FormFields = {
    registrationStartDate: string
    registrationEndDate: string
    lombaStart: string
    lombaEnd: string
}

const Deadline: FunctionComponent = () => {
    const [isEdited, setIsEdited] = useState(false)

    /* router stuff */
    const params = useParams()
    const lombaId = params.lombaId as string

    /* API calls */
    const { data: lombaOnDB } = useGetOrganizerLomba(lombaId)
    const { upsertLomba } = useUpsertLomba(lombaId)

    /* RH form */
    const methods = useForm<FormFields>()
    const { setValue, formState } = methods

    const onEditOrSave: SubmitHandler<FormFields> = async ({
        lombaEnd,
        lombaStart,
        registrationEndDate,
        registrationStartDate,
    }) => {
        if (!lombaOnDB) return

        if (!isEdited) {
            /* on edit */
            setIsEdited(true)
            if (lombaOnDB && !isEmpty(lombaOnDB)) {
                const { start_date, end_date, start_registration, end_registration } = lombaOnDB

                setValue("registrationStartDate", start_registration as string)
                setValue("registrationEndDate", end_registration as string)
                setValue("lombaStart", start_date as string)
                setValue("lombaEnd", end_date as string)
            }
        } else {
            /* on save */

            const { data: updateLombaData, error: updateLombaError } = await upsertLomba({
                startDate: lombaStart,
                endDate: lombaEnd,
                startRegistration: registrationStartDate,
                endRegistration: registrationEndDate,
            })

            if (!updateLombaData || updateLombaError) {
                return toast.error("Deadline gagal diubah, silahkan coba lagi")
            }

            toast.success("Informasi deadline berhasil diubah")

            setIsEdited(false)
        }
    }

    const checkUndone = useMemo(() => {
        if (lombaOnDB) {
            return checkUndoneCreationLomba({
                start_registration: lombaOnDB.start_registration,
                end_registration: lombaOnDB.end_registration,
                start_date: lombaOnDB.start_date,
                end_date: lombaOnDB.end_date,
            } as any)
        }

        return null
    }, [lombaOnDB])

    return (
        <Form {...methods} onSubmit={onEditOrSave}>
            <div
                className={twMerge(
                    "w-full rounded-xl border border-neutral-100/50 p-4",
                    checkUndone === "undone" ? "border-warning-400/50" : undefined
                )}
            >
                <div className="flex flex-col space-y-6">
                    <div className="flex items-center justify-between">
                        <div className="flex flex-col space-y-1">
                            <Typography weight={600} size="sm">
                                Deadline
                            </Typography>
                            <Typography size="xs" className="text-neutral-400">
                                Batas waktu untuk masa registrasi dan pelaksanaan lomba
                            </Typography>
                            {checkUndone === "undone" && (
                                <Typography size="xs" className="text-warning-600">
                                    * Beberapa informasi perlu diatur
                                </Typography>
                            )}
                        </div>
                        <div className="flex space-x-3">
                            {isEdited && (
                                <Button
                                    size="xs"
                                    label="Batalkan"
                                    endIcon="heroicons:x-mark"
                                    intent="bordered"
                                    className="shrink-0"
                                    type="button"
                                    thin
                                    onClick={() => setIsEdited(false)}
                                />
                            )}
                            <Button
                                size="xs"
                                label={isEdited ? "Simpan" : "Edit"}
                                endIcon={
                                    isEdited
                                        ? "heroicons:check-solid"
                                        : "ph:pencil-simple-line-light"
                                }
                                intent={isEdited ? "primary-low" : "bordered"}
                                className="shrink-0"
                                type="submit"
                                thin
                                loading={formState.isSubmitting}
                            />
                        </div>
                    </div>

                    {!lombaOnDB ? (
                        <DeadlineSkeleton />
                    ) : (
                        <div className="grid grid-cols-2 gap-y-6 gap-x-4">
                            {/* 👇 start registration */}
                            <div className="col-span-1 flex flex-col space-y-1">
                                <Typography as="span" size="xs" className="mb-1 text-neutral-400">
                                    Tanggal Mulai Registrasi Peserta
                                </Typography>
                                {isEdited ? (
                                    <Form.TextField
                                        label={false}
                                        required
                                        name="registrationStartDate"
                                        type="datetime-local"
                                    />
                                ) : (
                                    <>
                                        {lombaOnDB.start_registration === null ? (
                                            <Typography
                                                as="span"
                                                size="sm"
                                                className="text-warning-600"
                                            >
                                                Belum diatur
                                            </Typography>
                                        ) : (
                                            <Typography as="span" size="sm">
                                                {formatDate(
                                                    lombaOnDB.start_registration,
                                                    "E, dd MMMM y"
                                                )}
                                            </Typography>
                                        )}
                                    </>
                                )}
                            </div>
                            {/* 👇 end registration */}
                            <div className="col-span-1 flex flex-col space-y-1">
                                <Typography as="span" size="xs" className="mb-1 text-neutral-400">
                                    Tanggal Selesai Registrasi Peserta
                                </Typography>
                                {isEdited ? (
                                    <Form.TextField
                                        label={false}
                                        required
                                        name="registrationEndDate"
                                        type="datetime-local"
                                    />
                                ) : (
                                    <>
                                        {lombaOnDB.end_registration === null ? (
                                            <Typography
                                                as="span"
                                                size="sm"
                                                className="text-warning-600"
                                            >
                                                Belum diatur
                                            </Typography>
                                        ) : (
                                            <Typography as="span" size="sm">
                                                {formatDate(
                                                    lombaOnDB.end_registration,
                                                    "E, dd MMMM y"
                                                )}
                                            </Typography>
                                        )}
                                    </>
                                )}
                            </div>
                            {/* 👇 start event */}
                            <div className="col-span-1 flex flex-col space-y-1">
                                <Typography as="span" size="xs" className="mb-1 text-neutral-400">
                                    Tanggal Perlombaan Dimulai
                                </Typography>
                                {isEdited ? (
                                    <Form.TextField
                                        label={false}
                                        required
                                        name="lombaStart"
                                        type="datetime-local"
                                    />
                                ) : (
                                    <>
                                        {lombaOnDB.start_date === null ? (
                                            <Typography
                                                as="span"
                                                size="sm"
                                                className="text-warning-600"
                                            >
                                                Belum diatur
                                            </Typography>
                                        ) : (
                                            <Typography as="span" size="sm">
                                                {formatDate(lombaOnDB.start_date, "E, dd MMMM y")}
                                            </Typography>
                                        )}
                                    </>
                                )}
                            </div>
                            {/* 👇 end event */}
                            <div className="col-span-1 flex flex-col space-y-1">
                                <Typography as="span" size="xs" className="mb-1 text-neutral-400">
                                    Tanggal Perlombaan Selesai
                                </Typography>
                                {isEdited ? (
                                    <Form.TextField
                                        label={false}
                                        required
                                        name="lombaEnd"
                                        type="datetime-local"
                                    />
                                ) : (
                                    <>
                                        {lombaOnDB.end_date === null ? (
                                            <Typography
                                                as="span"
                                                size="sm"
                                                className="text-warning-600"
                                            >
                                                Belum diatur
                                            </Typography>
                                        ) : (
                                            <Typography as="span" size="sm">
                                                {formatDate(lombaOnDB.end_date, "E, dd MMMM y")}
                                            </Typography>
                                        )}
                                    </>
                                )}
                            </div>
                        </div>
                    )}
                </div>
            </div>
        </Form>
    )
}

export default Deadline
