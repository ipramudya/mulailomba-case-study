import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"

const DeadlineSkeleton: FunctionComponent = () => {
    return (
        <div className="grid grid-cols-2 gap-y-6 gap-x-4">
            <Skeleton className="h-5 w-full " />
            <Skeleton className="h-5 w-full " />
            <Skeleton className="h-5 w-full " />
            <Skeleton className="h-5 w-full " />
        </div>
    )
}

export default DeadlineSkeleton
