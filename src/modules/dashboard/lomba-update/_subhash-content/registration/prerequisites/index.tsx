import NotFound from "@blocks/not-found"
import PrerequisitePreview from "@blocks/prerequisite-preview"
import useGetAllPrerequisitesLomba from "@organizerService/lomba/prerequisites-lomba/use-get-all-prerequisites-lomba"
import Button from "@ui/button"
import Form from "@ui/form"
import Typography from "@ui/typography"
import { atom, useAtom } from "jotai"
import { isEmpty } from "lodash-es"
import { FunctionComponent, useRef, useState } from "react"
import { SubmitHandler, useForm } from "react-hook-form"
import { useParams } from "react-router-dom"

import PrerequisiteItem from "./prerequisite-item"
import PrerequisiteModal from "./prerequisite-modal"

export type PrerequisiteFromDB = {
    id: string
    name: string
    variant: "File" | "Paragraf" | "Teks Singkat"
    description: string
    is_required: "0" | "1"
}

type FormFields = {
    prerequisites?: Omit<PrerequisiteFromDB, "is_required"> &
        {
            isRequired: "0" | "1"
        }[]
}

export const EditablePrerequisiteAtom = atom(false)

const Prerequisites: FunctionComponent = () => {
    const [isOpen, setIsOpen] = useState(false)
    const [isPreviewOpen, setIsPreviewOpen] = useState(false)
    const formRef = useRef()

    /* shareable state */
    const [isEdited, setIsEdited] = useAtom(EditablePrerequisiteAtom)

    /* router stuff */
    const params = useParams()
    const lombaId = params.lombaId as string

    /* API calls */
    const { data: prerequisitesOnDB } = useGetAllPrerequisitesLomba(lombaId)

    /* RH form */
    const methods = useForm<FormFields>()

    const onEditOrSave: SubmitHandler<any> = (_, event) => {
        if ((event && event.target) !== formRef.current) return

        if (!isEdited) {
            /* on edit */
            setIsEdited(true)
            methods.setValue("prerequisites", prerequisitesOnDB as any)
            return
        }

        setIsEdited(false)
    }

    return (
        <>
            {isOpen && <PrerequisiteModal isOpen={isOpen} onOpenChange={setIsOpen} />}
            {isPreviewOpen && (
                <PrerequisitePreview
                    isOpen={isPreviewOpen}
                    onOpenChange={setIsPreviewOpen}
                    prerequisitesOnDB={prerequisitesOnDB as any}
                />
            )}
            <Form {...methods} onSubmit={onEditOrSave} ref={formRef}>
                <div className="w-full rounded-xl border border-neutral-100/50 p-4">
                    <div className="flex flex-col space-y-6">
                        <div className="flex items-center justify-between">
                            <div className="flex flex-col space-y-1">
                                <Typography weight={600} size="sm">
                                    Prasyarat
                                </Typography>
                                <Typography size="xs" className="text-neutral-400">
                                    Tambahkan persyaratan untuk pendaftar sebelum begabung dalam
                                    perlombaan
                                </Typography>
                            </div>
                            <div className="flex space-x-3">
                                {isEdited && (
                                    <Button
                                        size="xs"
                                        label="Batalkan"
                                        endIcon="heroicons:x-mark"
                                        intent="bordered"
                                        className="shrink-0"
                                        thin
                                        onClick={() => setIsEdited(false)}
                                    />
                                )}
                                <Button
                                    size="xs"
                                    label={isEdited ? "Selesai" : "Edit"}
                                    endIcon={
                                        isEdited
                                            ? "heroicons:check-solid"
                                            : "ph:pencil-simple-line-light"
                                    }
                                    intent={isEdited ? "primary-low" : "bordered"}
                                    className="shrink-0"
                                    type="submit"
                                    thin
                                />
                            </div>
                        </div>

                        {isEmpty(prerequisitesOnDB) ? (
                            <NotFound message="Lomba ini belum memiliki prasyarat khusus">
                                {isEdited && (
                                    <Button
                                        size="sm"
                                        label="Tambahkan Prasyarat"
                                        thin
                                        onClick={() => setIsOpen(true)}
                                    />
                                )}
                            </NotFound>
                        ) : (
                            <>
                                {isEdited && (
                                    <div className="flex items-center justify-center space-x-3 rounded-lg py-4 outline-dashed outline-1 outline-neutral-100">
                                        <Button
                                            size="xs"
                                            pill
                                            intent="bordered"
                                            label="Preview Prasyarat"
                                            className="px-3"
                                            endIcon="heroicons:eye"
                                            iconSize="sm"
                                            thin
                                            disabled={isEmpty(prerequisitesOnDB)}
                                            onClick={() => {
                                                console.log(prerequisitesOnDB)
                                                setIsPreviewOpen(true)
                                            }}
                                        />
                                        <Button
                                            size="xs"
                                            pill
                                            label="Tambah Prasyarat"
                                            className="px-3"
                                            endIcon="heroicons:plus"
                                            iconSize="sm"
                                            thin
                                            onClick={() => setIsOpen(true)}
                                        />
                                    </div>
                                )}
                                <div className="grid grid-cols-2 gap-x-4 gap-y-6">
                                    {prerequisitesOnDB?.map((p, idx) => (
                                        <PrerequisiteItem
                                            key={p.id}
                                            prerequisite={p as any}
                                            index={idx}
                                        />
                                    ))}
                                </div>
                            </>
                        )}
                    </div>
                </div>
            </Form>
        </>
    )
}

export default Prerequisites
