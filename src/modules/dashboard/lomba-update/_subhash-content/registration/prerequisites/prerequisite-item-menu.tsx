import Confirmation from "@blocks/confirmation"
import useDeletePrerequisite from "@organizerService/lomba/prerequisites-lomba/use-delete-prerequisite"
import IconButton from "@ui/icon-button"
import Menu from "@ui/menu"
import { FunctionComponent, useState } from "react"
import toast from "react-hot-toast"

import { PrerequisiteFromDB } from "."
import PrerequisiteModal from "./prerequisite-modal"

interface Props {
    index: number
    prerequisite: PrerequisiteFromDB
}
const PrerequisiteItemMenu: FunctionComponent<Props> = ({ index, prerequisite }) => {
    /* API calls */
    const deletePrerequisite = useDeletePrerequisite()

    /* react states */
    const [isConfirmationOpen, setIsConfirmationOpen] = useState(false)
    const [isEditPrerequisiteOpen, setIsEditPrerequisiteOpen] = useState(false)

    const onRemovePrerequisite = async () => {
        const { error: deletePrerequisiteError } = await deletePrerequisite(prerequisite.id)

        if (deletePrerequisiteError) {
            return toast.error("Gagal memproses penghapusan prasyarat")
        }

        toast.success(`Prasyarat ${prerequisite.name.toLowerCase()} berhasil dihapus`)
    }

    return (
        <>
            {isEditPrerequisiteOpen && (
                <PrerequisiteModal
                    isOpen={isEditPrerequisiteOpen}
                    onOpenChange={setIsEditPrerequisiteOpen}
                    prerequisite={prerequisite}
                    index={index}
                />
            )}
            {isConfirmationOpen && (
                <Confirmation
                    cb={onRemovePrerequisite}
                    variant="delete"
                    isOpen={isConfirmationOpen}
                    onOpenChange={setIsConfirmationOpen}
                    title={`Menghapus ${prerequisite.name}`}
                    description="Apakah anda yakin untuk menghapus prasyarat tersebut ?"
                />
            )}
            <Menu.Root
                triggerButton={
                    <IconButton
                        aria-label="update-timeline-menu"
                        icon="heroicons:ellipsis-vertical"
                        size="sm"
                        circle
                        intent="bordered"
                    />
                }
                contentAlign="end"
            >
                <Menu.Item
                    label="Perbarui"
                    icon="heroicons:pencil-square"
                    onClick={() => setIsEditPrerequisiteOpen(true)}
                />
                <Menu.Item
                    label="Hapus"
                    icon="heroicons:trash"
                    className="rd-highlighted:bg-danger-50 rd-highlighted:text-danger-600"
                    onClick={() => setIsConfirmationOpen(true)}
                />
            </Menu.Root>
        </>
    )
}

export default PrerequisiteItemMenu
