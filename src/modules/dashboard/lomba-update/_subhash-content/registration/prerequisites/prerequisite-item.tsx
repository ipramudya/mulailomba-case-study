import PREREQUISITE_PLOTING_TYPE from "@constant/prerequisite.plot"
import Badge from "@ui/badge"
import Typography from "@ui/typography"
import { useAtomValue } from "jotai"
import { FunctionComponent } from "react"

import { EditablePrerequisiteAtom, PrerequisiteFromDB } from "."
import PrerequisiteItemMenu from "./prerequisite-item-menu"

interface Props {
    prerequisite: PrerequisiteFromDB
    index: number
}

const PrerequisiteItem: FunctionComponent<Props> = ({ prerequisite, index }) => {
    const { name, variant, description, is_required } = prerequisite
    const isPrerequisiteEdited = useAtomValue(EditablePrerequisiteAtom)

    return (
        <div className="col-span-1 flex flex-col space-y-4 overflow-hidden rounded-lg border border-neutral-100/50 p-3">
            <div className="flex items-center justify-between ">
                <Badge
                    intent="primary-low"
                    label={PREREQUISITE_PLOTING_TYPE.get(variant)?.label || "error"}
                    icon={PREREQUISITE_PLOTING_TYPE.get(variant)?.icon || "ph:placeholder-thin"}
                />
                {isPrerequisiteEdited && (
                    <PrerequisiteItemMenu index={index} prerequisite={prerequisite} />
                )}
            </div>
            <div className="flex flex-col space-y-2 ">
                <Typography size="sm">{name}</Typography>
                <Typography size="xs">{description}</Typography>
            </div>
            <hr className="border-neutral-100/50" />
            <Typography size="xs">
                Prasyarat bersifat {is_required ? "wajib" : "opsional"}
            </Typography>
        </div>
    )
}

export default PrerequisiteItem
