import useCreatePrerequisite from "@organizerService/lomba/prerequisites-lomba/use-create-prerequisite"
import useUpdatePrerequisite from "@organizerService/lomba/prerequisites-lomba/use-update-prerequisite"
import Button from "@ui/button"
import Form from "@ui/form"
import Modal from "@ui/modal"
import { FunctionComponent } from "react"
import { SubmitHandler, useForm } from "react-hook-form"
import toast from "react-hot-toast"
import { useParams } from "react-router-dom"

import { PrerequisiteFromDB } from "."

interface Props {
    isOpen: boolean
    onOpenChange(val: boolean): void
    prerequisite?: PrerequisiteFromDB
    index?: number
}
type PrerequisiteFields = Omit<PrerequisiteFromDB, "is_required"> & {
    isRequired: "1" | "0"
}

const PrerequisiteModal: FunctionComponent<Props> = ({
    isOpen,
    onOpenChange,
    prerequisite,
    index = null,
}) => {
    /* router stuff */
    const params = useParams()
    const lombaId = params.lombaId as string

    /* API calls */
    const updatePrerequisite = useUpdatePrerequisite()
    const { createPrerequisites } = useCreatePrerequisite(lombaId)

    /* hook form */
    const methods = useForm<PrerequisiteFields>()
    const { formState } = methods
    const getDefaultValue = (key: keyof PrerequisiteFromDB) => {
        if (prerequisite && index !== null) {
            return prerequisite[key]
        }

        return undefined
    }

    /* append fields handler */
    const onPrerequisiteAdded: SubmitHandler<PrerequisiteFields> = async (fields) => {
        if (prerequisite) {
            const { data: updatePrerequisiteData, error: updatePrerequisiteError } =
                await updatePrerequisite({
                    ...fields,
                    id: prerequisite.id,
                })

            if (!updatePrerequisiteData || updatePrerequisiteError) {
                return toast.error("Informasi prasyarat gagal diubah, silahkan coba lagi")
            }

            toast.success("Informasi prasyarat berhasil diubah")
        } else {
            const { data: createPrerequisiteData, error: createPrerequisiteError } =
                await createPrerequisites({
                    ...fields,
                })

            if (!createPrerequisiteData || createPrerequisiteError) {
                return toast.error("Informasi prasyarat gagal dibuat, silahkan coba lagi")
            }

            toast.success("Informasi prasyarat berhasil dibuat")
        }
        onOpenChange(false)
    }

    return (
        <Modal
            open={isOpen}
            onOpenChange={onOpenChange}
            title={prerequisite ? "Perbarui Prasyarat Lomba" : "Tambah Prasyarat Pendaftaran"}
        >
            <Form {...methods} onSubmit={onPrerequisiteAdded}>
                <Form.Layout>
                    <Form.TextField
                        name="name"
                        label="Nama Persyaratan"
                        required
                        placeholder="Masukan nama persyaratan"
                        defaultValue={getDefaultValue("name")}
                    />
                    <Form.TextareaField
                        name="description"
                        label="Deskripsi Persyaratan"
                        placeholder="Jelaskan mengenai persyaratan tersebut"
                        maxLength={256}
                        hasCount
                        defaultValue={getDefaultValue("description")}
                    />
                    <Form.SelectField
                        name="variant"
                        label="Varian Jawaban"
                        required
                        options={[
                            { label: "File", value: "File" },
                            { label: "Paragraf", value: "Paragraf" },
                            { label: "Teks Singkat", value: "Teks Singkat" },
                        ]}
                        defaultValue={getDefaultValue("variant")}
                    />
                    <Form.RadioField
                        name="isRequired"
                        label="Bentuk Prasyarat"
                        required
                        options={[
                            { label: "Prasyarat bersifat wajib diisi", value: "1" },
                            { label: "Prasyarat bersifat opsional", value: "0" },
                        ]}
                        split
                        defaultValue={getDefaultValue("is_required")?.toString()}
                    />
                </Form.Layout>
                <div className="mt-9 flex items-center justify-end space-x-3 ">
                    <Button
                        label="Batalkan"
                        intent="bordered"
                        onClick={() => onOpenChange(false)}
                    />
                    <Button
                        type="submit"
                        label={prerequisite ? "Perbarui Prasyarat" : "Tambahkan Prasyarat"}
                        loading={formState.isSubmitting}
                    />
                </div>
            </Form>
        </Modal>
    )
}

export default PrerequisiteModal
