import checkUndoneCreationLomba from "@functions/check-undone-creation-lomba"
import plotToBoolean from "@functions/plot-to-boolean"
import useGetOrganizerLomba from "@organizerService/lomba/use-get-lomba-by-id"
import useUpsertLomba from "@organizerService/lomba/use-upsert-lomba"
import Button from "@ui/button"
import Form from "@ui/form"
import Typography from "@ui/typography"
import { isEmpty } from "lodash-es"
import { FunctionComponent, useEffect, useMemo, useState } from "react"
import { SubmitHandler, useForm } from "react-hook-form"
import toast from "react-hot-toast"
import { useParams } from "react-router-dom"
import { twMerge } from "tailwind-merge"

import VariantAndParticipantSkeleton from "./variant-and-participant-skeleton"

type FormFields = {
    registrationVariant: "paid" | "free"
    participant: "limited" | "unlimited"
    totalParticipants?: number
}

const VariantAndParticipant: FunctionComponent = () => {
    const [isEdited, setIsEdited] = useState(false)

    /* router stuff */
    const params = useParams()
    const lombaId = params.lombaId as string

    /* API calls */
    const { data: lombaOnDB } = useGetOrganizerLomba(lombaId)
    const { upsertLomba } = useUpsertLomba(lombaId)

    /* RH form */
    const methods = useForm<FormFields>()
    const { watch, setValue, formState } = methods
    const participantFieldWathcer = watch("participant")

    const onEditOrSave: SubmitHandler<FormFields> = async ({
        participant,
        totalParticipants,
        registrationVariant,
    }) => {
        if (!lombaOnDB) return

        if (!isEdited) {
            /* on edit */
            setIsEdited(true)

            if (lombaOnDB && !isEmpty(lombaOnDB)) {
                const { is_free, is_unlimited_participants, total_participants } = lombaOnDB

                setValue(
                    "registrationVariant",
                    plotToBoolean({ considered: is_free, truthy: "free", falsy: "paid" }) as any
                )
                setValue(
                    "participant",
                    plotToBoolean({
                        considered: is_unlimited_participants,
                        truthy: "unlimited",
                        falsy: "limited",
                    }) as any
                )
                setValue("totalParticipants", total_participants || undefined)
            }
        } else {
            /* on save */
            const { data: updateLombaData, error: updateLombaError } = await upsertLomba({
                isUnlimitedParticipants: participant === "unlimited",
                totalParticipants: totalParticipants || undefined,
                isFree: registrationVariant === "free",
            })

            if (!updateLombaData || updateLombaError) {
                return toast.error("Varian registrasi atau partisipan gagal diubah")
            }

            toast.success("Varian registrasi atau partisipan berhasil diubah")

            setIsEdited(false)
        }
    }

    const checkUndone = useMemo(() => {
        if (lombaOnDB) {
            return checkUndoneCreationLomba({
                is_unlimited_participants: lombaOnDB.is_unlimited_participants,
                is_free: lombaOnDB.is_free,
                total_participants: lombaOnDB.total_participants,
            } as any)
        }

        return null
    }, [lombaOnDB])

    useEffect(() => {
        if (participantFieldWathcer === "limited") {
            setValue("totalParticipants", lombaOnDB?.total_participants as number)
        }
    }, [lombaOnDB, participantFieldWathcer, setValue])

    return (
        <Form {...methods} onSubmit={onEditOrSave}>
            <div
                className={twMerge(
                    "w-full rounded-xl border border-neutral-100/50 p-4",
                    checkUndone === "undone" ? "border-warning-400/50" : undefined
                )}
            >
                <div className="flex flex-col space-y-6">
                    <div className="flex items-center justify-between">
                        <div className="flex flex-col space-y-1">
                            <Typography weight={600} size="sm">
                                Varian Registrasi dan Partisipan
                            </Typography>
                            <Typography size="xs" className="text-neutral-400">
                                Tentukan banyaknya peserta lomba yang dapat bergabung
                            </Typography>
                            {checkUndone === "undone" && (
                                <Typography size="xs" className="text-warning-600">
                                    * Beberapa informasi perlu diatur
                                </Typography>
                            )}
                        </div>
                        <div className="flex space-x-3">
                            {isEdited && (
                                <Button
                                    size="xs"
                                    label="Batalkan"
                                    endIcon="heroicons:x-mark"
                                    intent="bordered"
                                    className="shrink-0"
                                    type="button"
                                    thin
                                    onClick={() => setIsEdited(false)}
                                />
                            )}
                            <Button
                                size="xs"
                                label={isEdited ? "Simpan" : "Edit"}
                                endIcon={
                                    isEdited
                                        ? "heroicons:check-solid"
                                        : "ph:pencil-simple-line-light"
                                }
                                intent={isEdited ? "primary-low" : "bordered"}
                                className="shrink-0"
                                type="submit"
                                thin
                                loading={formState.isSubmitting}
                            />
                        </div>
                    </div>

                    {!lombaOnDB ? (
                        <VariantAndParticipantSkeleton />
                    ) : (
                        <div className="flex flex-col space-y-6">
                            {/* 👇 variant */}
                            <div className="flex flex-col space-y-1">
                                <Typography as="span" size="xs" className="mb-1 text-neutral-400">
                                    Varian Registrasi Peserta
                                </Typography>
                                {isEdited ? (
                                    <Form.RadioField
                                        label={false}
                                        name="registrationVariant"
                                        split
                                        options={[
                                            { label: "Gratis", value: "free" },
                                            {
                                                label: "Lomba Berbayar",
                                                value: "paid",
                                                disabled: true,
                                            },
                                        ]}
                                    />
                                ) : (
                                    <>
                                        {lombaOnDB.is_free === null ? (
                                            <Typography
                                                as="span"
                                                size="sm"
                                                className="text-warning-600"
                                            >
                                                Belum diatur
                                            </Typography>
                                        ) : (
                                            <Typography as="span" size="sm">
                                                {lombaOnDB.is_free
                                                    ? "Tidak Dipungut Biaya"
                                                    : "Berbayar"}
                                            </Typography>
                                        )}
                                    </>
                                )}
                            </div>

                            {/* 👇 participant */}
                            <div className="flex flex-col space-y-1">
                                <Typography as="span" size="xs" className="mb-1 text-neutral-400">
                                    Total Partisipan
                                </Typography>
                                {isEdited ? (
                                    <div className="flex flex-col space-y-3">
                                        <Form.RadioField
                                            label={false}
                                            name="participant"
                                            options={[
                                                { label: "Peserta Terbatas", value: "limited" },
                                                {
                                                    label: "Peserta Tidak Terbatas",
                                                    value: "unlimited",
                                                },
                                            ]}
                                            split
                                        />

                                        {participantFieldWathcer === "limited" && (
                                            <Form.TextField
                                                name="totalParticipants"
                                                label={false}
                                                placeholder="Masukan total partisipan"
                                                type="number"
                                                required
                                            />
                                        )}
                                    </div>
                                ) : (
                                    <>
                                        {lombaOnDB.is_unlimited_participants === null ? (
                                            <Typography
                                                as="span"
                                                size="sm"
                                                className="text-warning-600"
                                            >
                                                Belum diatur
                                            </Typography>
                                        ) : (
                                            <Typography as="span" size="sm">
                                                {lombaOnDB.is_unlimited_participants
                                                    ? "Peserta Tidak Terbatas"
                                                    : `${lombaOnDB.total_participants} Peserta`}
                                            </Typography>
                                        )}
                                    </>
                                )}
                            </div>
                        </div>
                    )}
                </div>
            </div>
        </Form>
    )
}

export default VariantAndParticipant
