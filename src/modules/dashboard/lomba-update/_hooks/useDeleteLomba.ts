import useDeleteLombaService from "@organizerService/lomba/use-delete-lomba"
import useGetLombaById from "@organizerService/lomba/use-get-lomba-by-id"
import deletePoster from "@organizerService/poster/delete-poster"
import { useParams } from "react-router-dom"

export default function useDeleteLomba() {
    const params = useParams()
    const lombaId = params.lombaId as string
    const { data: lombaOnDB } = useGetLombaById(lombaId)
    const deleteLombaService = useDeleteLombaService()

    return async () => {
        if (lombaOnDB && lombaOnDB) {
            if (lombaOnDB.poster_storage_url) {
                deletePoster(lombaOnDB.poster_storage_url)
            }
            const res = await deleteLombaService(lombaId)
            return res
        }
    }
}
