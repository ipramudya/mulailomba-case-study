import Confirmation from "@blocks/confirmation"
import useGetDetailLomba from "@publicService/use-get-detail-lomba"
import Button from "@ui/button"
import { FunctionComponent, useState } from "react"
import { toast } from "react-hot-toast"
import { useLocation, useNavigate, useParams } from "react-router-dom"
import { scroller } from "react-scroll"

import useDeleteLomba from "../_hooks/useDeleteLomba"
import SidebarConstant from "./sidebar.constant"

const Sidebar: FunctionComponent = () => {
    const [isConfirmationOpen, setConfirmationOpen] = useState(false)
    const [isLoading, setIsLoading] = useState(false)

    /* router stuffs */
    const navigate = useNavigate()
    const { hash } = useLocation()
    const { lombaId } = useParams()

    /* API calls */
    const { data } = useGetDetailLomba(lombaId as string)

    /* local custom hooks */
    const deleteLomba = useDeleteLomba()

    const onDeleteLomba = async () => {
        setIsLoading(true)
        const res = await deleteLomba()

        if (res) {
            navigate("/dashboard/home")
            if (res.error) {
                setIsLoading(false)
                return toast.error("Gagal menghapus lomba")
            }
            toast.success(`Data perlombaan ${data?.name} berhasil dihapus`)
        }
        setIsLoading(false)
    }

    return (
        <>
            {isConfirmationOpen && (
                <Confirmation
                    isOpen={isConfirmationOpen}
                    onOpenChange={setConfirmationOpen}
                    variant="delete"
                    cb={onDeleteLomba}
                    title={`Menghapus ${data?.name}`}
                    description="Apakah anda yakin untuk menghapus perlombaan tersebut ?"
                />
            )}
            <div className="sticky top-[10%] flex h-fit flex-shrink-0 flex-col space-y-3 ">
                {SidebarConstant.map((d, idx) => (
                    <Button
                        key={"update-lomba-menu-" + idx}
                        label={d.title}
                        size="sm"
                        intent={hash === `#${d.query}` ? "primary-low" : "default"}
                        className="justify-start"
                        thin
                        onClick={() => {
                            navigate(`#${d.query}`)
                            scroller.scrollTo(d.query, {
                                offset: -100,
                            })
                        }}
                    />
                ))}
                <hr className="border-neutral-100" />
                <Button
                    label="Kembali"
                    className="justify-start"
                    size="sm"
                    intent="default"
                    thin
                    onClick={() => navigate("..", { relative: "path" })}
                />
                <Button
                    label="Hapus Lomba"
                    className="justify-start"
                    size="sm"
                    intent="danger-ghost"
                    thin
                    onClick={() => setConfirmationOpen(true)}
                    loading={isLoading}
                />
            </div>
        </>
    )
}

export default Sidebar
