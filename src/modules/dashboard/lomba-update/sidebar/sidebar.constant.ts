const SidebarConstant = [
    {
        query: "general",
        title: "Informasi Umum",
    },
    {
        query: "registration",
        title: "Pendaftaran",
    },
    {
        query: "description-and-rules",
        title: "Deskripsi dan Aturan",
    },
    {
        query: "implementation",
        title: "Pelaksanaan",
    },
] as const

export default SidebarConstant
