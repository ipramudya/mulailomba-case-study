import { checkExistanceAttributes } from "@functions/check-existance-object"
import avatar from "@functions/generate-avatar"
import useLogoutOrganizer from "@organizerService/user/auth/use-logout-organizer"
import useOrganizerUserData from "@organizerService/user/use-organizer-user-data"
import Button from "@ui/button"
import buttonStyles from "@ui/button/button.styles"
import Container from "@ui/container"
import Popover from "@ui/popover"
import Typography from "@ui/typography"
import { FunctionComponent, useState } from "react"
import Skeleton from "react-loading-skeleton"
import { Link, useNavigate } from "react-router-dom"
import { twMerge } from "tailwind-merge"

import DashboardSettingsModal from "./dashboard-settings"

interface Props {
    variant?: "primary" | "secondary"
}

const DashboardHeader: FunctionComponent<Props> = ({ variant = "primary" }) => {
    /* react states */
    const [isProfileModalOpen, setIsProfileModalOpen] = useState(false)

    /* router stuffs */
    const navigate = useNavigate()

    /* api definitions */
    const { data: organizerData } = useOrganizerUserData()
    const organizerLogout = useLogoutOrganizer()
    const isUserExist = organizerData && organizerData

    const onLogout = () => {
        organizerLogout.mutate()
    }

    const checkIsUserInfoComplete = (): boolean | null => {
        if (organizerData) {
            const desiredKeys = [
                "about",
                "location",
                "name",
                "phone",
                "profile_public_url",
                "profile_status",
                "social_instagram",
                "social_whatsapp",
            ]
            return checkExistanceAttributes(organizerData, desiredKeys)
        }

        return null
    }

    return (
        <>
            {/* dashboard header modal registries */}
            {isProfileModalOpen && (
                <DashboardSettingsModal
                    isOpen={isProfileModalOpen}
                    onOpenChange={setIsProfileModalOpen}
                />
            )}
            <Container size="lg" as="div" className="flex h-full items-center">
                <div className="flex w-full items-center justify-between">
                    {/* 👇 logo */}
                    <Link to="/dashboard/home">
                        <Typography
                            size="lg"
                            weight={600}
                            as="h2"
                            className={variant === "primary" ? "text-primary-500" : "text-white"}
                        >
                            MulaiLomba Organizer
                        </Typography>
                    </Link>

                    {checkIsUserInfoComplete() === null || !organizerData ? (
                        <Skeleton className="h-[32px] w-[180px] " />
                    ) : (
                        <Popover
                            size="md"
                            align="end"
                            className="rounded-md border border-neutral-50 px-2 py-4 shadow-menu"
                            triggerEl={
                                <button
                                    className={twMerge(
                                        buttonStyles({
                                            intent: "primary-low",
                                            pill: true,
                                            size: "sm",
                                        }),
                                        "relative",
                                        !checkIsUserInfoComplete()
                                            ? "before:absolute before:top-[-2px] before:right-[-2px] before:h-[14px] before:w-[14px] before:rounded-full before:bg-danger-400 before:shadow before:content-['']"
                                            : undefined
                                    )}
                                >
                                    <div className="aspect-square w-6 overflow-hidden rounded-full border-[2px] border-white ">
                                        <img
                                            src={
                                                organizerData.profile_public_url
                                                    ? organizerData.profile_public_url
                                                    : avatar
                                            }
                                            alt="profil organizer"
                                            className="w-full object-cover"
                                        />
                                    </div>
                                    <Typography
                                        size="sm"
                                        weight={500}
                                        as="span"
                                        className="max-w-[120px] truncate text-primary-700"
                                    >
                                        {isUserExist ? organizerData.name : "Loading..."}
                                    </Typography>
                                </button>
                            }
                        >
                            <div className="flex flex-col space-y-2">
                                <Button
                                    thin
                                    label="Profil"
                                    size="xs"
                                    intent="default"
                                    className="min-w-[180px] justify-start"
                                    onClick={() => navigate("/dashboard/profile")}
                                />
                                <Button
                                    thin
                                    label="Perbarui Profil"
                                    size="xs"
                                    intent="default"
                                    className="min-w-[180px] justify-start"
                                    hasDot={!checkIsUserInfoComplete() as boolean}
                                    onClick={() => setIsProfileModalOpen(true)}
                                />
                                <hr className="border-neutral-100" />
                                <Button
                                    thin
                                    label="Telusuri Lomba"
                                    size="xs"
                                    intent="default"
                                    className="min-w-[180px] justify-start"
                                    onClick={() => navigate("/")}
                                    iconSize="sm"
                                />
                                <hr className="border-neutral-100" />
                                <Button
                                    thin
                                    label="Keluar"
                                    size="xs"
                                    intent="danger-ghost"
                                    className="min-w-[180px] justify-start"
                                    onClick={onLogout}
                                />
                            </div>
                        </Popover>
                    )}
                </div>
            </Container>
        </>
    )
}

export default DashboardHeader
