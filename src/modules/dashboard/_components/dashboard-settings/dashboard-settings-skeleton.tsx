import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"

const DashboardProfileSkeleton: FunctionComponent = () => {
    return (
        <>
            <div className="border-b border-neutral-100 pb-4">
                <Skeleton className="h-[20px] w-[92px] " />
            </div>
            <div className="flex flex-col space-y-3 border-b border-neutral-100 py-4">
                <Skeleton className="h-[20px] w-[40px] " />
                <Skeleton className="aspect-square w-[86px] !rounded-full" />
                <Skeleton className="h-[32px] w-[120px] " />
            </div>
            <div className="flex flex-col space-y-3 border-b border-neutral-100 pb-4 ">
                <Skeleton className="h-[20px] w-[140px] " />
                <Skeleton className="h-[20px] w-[200px] " count={2} />
                <Skeleton className="h-[20px] w-[180px] " count={2} />
                <Skeleton className="h-[20px] w-[240px] " count={2} />
            </div>
        </>
    )
}

export default DashboardProfileSkeleton
