import avatar from "@functions/generate-avatar"
import useDropImage from "@hooks/use-drop-image"
import useUpdateMetadataOrganizer from "@organizerService/user/auth/use-update-metadata-organizer"
import useOrganizerUserData from "@organizerService/user/use-organizer-user-data"
import Badge from "@ui/badge"
import Button from "@ui/button"
import Form from "@ui/form"
import Modal from "@ui/modal"
import Typography from "@ui/typography"
import { FunctionComponent, useEffect, useMemo } from "react"
import { SubmitHandler, useForm } from "react-hook-form"
import { toast } from "react-hot-toast"

import DashboardSettingsSkeleton from "./dashboard-settings-skeleton"

const profileStatus = [
    "Pendidikan",
    "Musik dan Hiburan",
    "Sains dan Teknologi",
    "Game Organizer",
    "Sastra dan Bahasa",
    "Olahraga",
]

type FormFields = {
    email: string
    username: string
    telp: string
    password: string
    confirmPassword: string
    status: string
    location: string
    about: string
    whatsapp: string
    instagram: string
}

interface Props {
    isOpen: boolean
    onOpenChange(val: boolean): void
}

const DashboardSettingsModal: FunctionComponent<Props> = ({ isOpen, onOpenChange }) => {
    /* API calls */
    const { data: organizerData, isLoading: organizerDataLoading } = useOrganizerUserData()
    const updateOrganizerMetadata = useUpdateMetadataOrganizer()

    /* hook form */
    const methods = useForm<FormFields>()
    const statusFieldWatcher = methods.watch("status")

    /* upload image dropzone */
    const {
        uploaded,
        getRootProps,
        getInputProps,
        dropzoneVars: { open },
    } = useDropImage({ noClick: true })

    const imageProfileSource = useMemo(() => {
        let profileImageSource
        if (uploaded) {
            profileImageSource = uploaded.preview
        } else if (organizerData && organizerData.profile_public_url) {
            profileImageSource = organizerData.profile_public_url
        } else {
            profileImageSource = avatar
        }

        return profileImageSource
    }, [organizerData, uploaded])

    const onSetBadgeStatus = (val: string) => {
        methods.setValue("status", val)
    }

    const onUpdateProfileSubmit: SubmitHandler<FormFields> = async ({
        email,
        username,
        telp,
        password,
        confirmPassword,
        status,
        location,
        about,
        whatsapp,
        instagram,
    }) => {
        if (password !== confirmPassword) return toast.error("Konfirmasi password tidak sesuai")

        const res = await updateOrganizerMetadata({
            email,
            name: username,
            password,
            phone: telp,
            profile_status: status,
            about,
            location,
            social_whatsapp: whatsapp,
            social_instagram: instagram,
            profileImage: uploaded?.file,
        })

        if (res) {
            if (res.data && !res.error) {
                toast.success(`Profil ${res.data.user?.user_metadata.name} berhasil diperbarui`)
            } else {
                toast.error("Gagal memperbarui profil, silahkan coba lagi")
            }
        }
    }

    useEffect(() => {
        if (organizerData && organizerData.profile_status) {
            methods.setValue("status", organizerData.profile_status)
        }
    }, [methods, organizerData])

    return (
        <Modal
            title={false}
            open={isOpen}
            onOpenChange={onOpenChange}
            classNames={{
                content:
                    "restyle-scrollbar max-h-[642px] min-w-[628px] overflow-x-hidden overflow-y-auto pb-0",
            }}
        >
            {organizerDataLoading || !organizerData ? (
                <DashboardSettingsSkeleton />
            ) : (
                <Form
                    className="relative flex w-full flex-col space-y-4"
                    onSubmit={onUpdateProfileSubmit}
                    {...methods}
                >
                    {/* title */}
                    <div className="border-b border-neutral-100 pb-4">
                        <Typography weight={500}>Pengaturan</Typography>
                    </div>

                    {/* photo */}
                    <div
                        {...getRootProps({
                            className: "flex flex-col space-y-3 border-b border-neutral-100 pb-4",
                        })}
                    >
                        <Typography size="sm">Foto</Typography>
                        <div className="aspect-square w-[86px] overflow-hidden rounded-full">
                            <img
                                src={imageProfileSource}
                                alt="foto profil organizer"
                                className="w-full object-cover"
                            />
                        </div>
                        <input {...getInputProps()} />
                        <Button
                            size="sm"
                            intent="primary-bordered"
                            className="w-fit"
                            label="Unggah Foto"
                            onClick={() => open()}
                        />
                    </div>

                    {/* personal info */}
                    <div className="flex flex-col space-y-3 border-b border-neutral-100 pb-4 ">
                        <Typography size="sm">Informasi personal</Typography>
                        <div className="flex flex-col space-y-1">
                            <Typography size="xs" className="text-neutral-400">
                                Email
                            </Typography>
                            <Form.TextField
                                name="email"
                                label={false}
                                size="sm"
                                classNames={{
                                    wrapper:
                                        "py-1 px-0 border-none min-h-fit focus-within:outline-none focus-within:outline-0",
                                }}
                                defaultValue={organizerData.email || undefined}
                            />
                        </div>
                        <div className="flex flex-col space-y-1">
                            <Typography size="xs" className="text-neutral-400">
                                Nama pengguna
                            </Typography>
                            <Form.TextField
                                name="username"
                                label={false}
                                size="sm"
                                classNames={{
                                    wrapper:
                                        "py-1 px-0 border-none min-h-fit max-w-fit focus-within:outline-none focus-within:outline-0",
                                }}
                                defaultValue={organizerData.name || undefined}
                            />
                        </div>
                        <div className="flex flex-col space-y-1">
                            <Typography size="xs" className="text-neutral-400">
                                Nomor Telepon
                            </Typography>
                            <Form.TextField
                                name="telp"
                                label={false}
                                size="sm"
                                classNames={{
                                    wrapper:
                                        "py-1 px-0 border-none min-h-fit max-w-fit focus-within:outline-none focus-within:outline-0",
                                }}
                                placeholder="+628xx-xxxx-xxx"
                                defaultValue={organizerData.phone || undefined}
                            />
                        </div>
                        <div className="flex flex-col space-y-1">
                            <Typography size="xs" className="text-neutral-400">
                                Lokasi
                            </Typography>
                            <Form.TextField
                                name="location"
                                label={false}
                                size="sm"
                                classNames={{
                                    wrapper:
                                        "py-1 px-0 border-none min-h-fit focus-within:outline-none focus-within:outline-0",
                                }}
                                placeholder="Lokasi organisasi kamu"
                                defaultValue={organizerData.location || undefined}
                            />
                        </div>
                        <div className="flex flex-col space-y-1">
                            <Typography size="xs" className="text-neutral-400">
                                Tentang
                            </Typography>
                            <Form.TextareaField
                                label={false}
                                name="about"
                                placeholder="Berikan deskripsi mengenai organisasi kamu"
                                classNames={{
                                    input: "bg-transparent resize-y",
                                    wrapper:
                                        "p-2 border-neutral-100 min-h-[160px] bg-neutral-50 focus-within:outline-none focus-within:outline-0",
                                }}
                                defaultValue={organizerData.about || undefined}
                            />
                        </div>
                    </div>

                    {/* password */}
                    <div className="flex flex-col space-y-3 border-b border-neutral-100 pb-4">
                        <div>
                            <Typography size="sm">Password</Typography>
                            <Typography size="xs" className="mt-1 text-neutral-400">
                                Agar lebih aman gunakan kata sandi dengan panjang minimal 15 huruf,
                                atau minimal 8 karakter dengan huruf dan angka.
                            </Typography>
                        </div>
                        <Form.RowLayout column={2}>
                            <Form.TextField
                                name="password"
                                type="password"
                                label={false}
                                size="xs"
                                classNames={{
                                    input: "bg-transparent",
                                    wrapper:
                                        "p-2 border-neutral-100 min-h-fit bg-neutral-50 focus-within:outline-none focus-within:outline-0",
                                }}
                                placeholder="Masukan password baru"
                            />
                            <Form.TextField
                                name="confirmPassword"
                                type="password"
                                label={false}
                                size="xs"
                                classNames={{
                                    input: "bg-transparent",
                                    wrapper:
                                        "p-2 border-neutral-100 min-h-fit bg-neutral-50 focus-within:outline-none focus-within:outline-0",
                                }}
                                placeholder="Tulis ulang password baru"
                            />
                        </Form.RowLayout>
                    </div>

                    {/* status/profesi */}
                    <div className="flex flex-col space-y-3 border-b border-neutral-100 pb-4">
                        <div>
                            <Typography size="sm">Kategori</Typography>
                            <Typography size="xs" className="mt-1 text-neutral-400">
                                Kategori organizer untuk mempermudah peserta lomba dalam menemukan
                                perlombaan menarik milik anda
                            </Typography>
                        </div>
                        <div className="flex flex-wrap gap-x-2 gap-y-3">
                            {profileStatus.map((s, idx) => (
                                <Badge
                                    intent={statusFieldWatcher === s ? "primary" : "bordered"}
                                    asElement="button"
                                    size="sm"
                                    label={s}
                                    key={"profile-status-" + idx}
                                    onClick={() => onSetBadgeStatus(s)}
                                />
                            ))}
                        </div>
                    </div>

                    {/* social media */}
                    <div className="flex flex-col space-y-3 pb-4">
                        <div>
                            <Typography size="sm">Sosial Media</Typography>
                        </div>
                        <div className="flex flex-col space-y-1">
                            <Typography size="xs" className="text-neutral-400">
                                Whatsapp
                            </Typography>
                            <Form.TextField
                                name="whatsapp"
                                label={false}
                                size="sm"
                                classNames={{
                                    wrapper:
                                        "py-1 px-0 border-none min-h-fit max-w-fit focus-within:outline-none focus-within:outline-0",
                                }}
                                placeholder="Masukan nomor whatsapp"
                                defaultValue={organizerData.social_whatsapp || undefined}
                            />
                        </div>
                        <div className="flex flex-col space-y-1">
                            <Typography size="xs" className="text-neutral-400">
                                Instagram
                            </Typography>
                            <Form.TextField
                                name="instagram"
                                label={false}
                                size="sm"
                                classNames={{
                                    wrapper:
                                        "py-1 px-0 border-none min-h-fit max-w-fit focus-within:outline-none focus-within:outline-0",
                                }}
                                placeholder="Masukan username instagram"
                                defaultValue={organizerData.social_instagram || undefined}
                            />
                        </div>
                    </div>

                    <div className="sticky bottom-0 z-10 flex justify-end space-x-3 border-t border-neutral-100 bg-white py-4">
                        <Button
                            intent="bordered"
                            label="Batalkan"
                            size="sm"
                            onClick={() => onOpenChange(false)}
                        />
                        <Button
                            type="submit"
                            label="Perbarui"
                            size="sm"
                            loading={methods.formState.isSubmitting}
                            disabled={methods.formState.isSubmitting}
                        />
                    </div>
                </Form>
            )}
        </Modal>
    )
}

export default DashboardSettingsModal
