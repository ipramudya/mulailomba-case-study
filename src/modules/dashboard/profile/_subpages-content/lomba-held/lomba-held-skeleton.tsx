import PosterSkeleton from "@blocks/poster/poster-skeleton"
import { FunctionComponent } from "react"

const LombaHeldSkeleton: FunctionComponent = () => {
    return (
        <>
            {Array.from({ length: 12 }).map((_, idx) => (
                <PosterSkeleton key={"poster-skeleton-" + idx} />
            ))}
        </>
    )
}

export default LombaHeldSkeleton
