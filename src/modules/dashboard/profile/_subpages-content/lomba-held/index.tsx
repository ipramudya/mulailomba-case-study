import LandscapePoster from "@blocks/poster/landscape"
import PosterSkeleton from "@blocks/poster/poster-skeleton"
import useGetAllOrganizerLomba from "@organizerService/lomba/use-get-all-lomba"
import { FunctionComponent } from "react"

const LombaHeldSubpage: FunctionComponent = () => {
    /* API calls */
    const { data: allLombaResponse, isLoading: isAllLombaLoading } = useGetAllOrganizerLomba()

    return (
        // <div className="grid grid-cols-3 gap-x-5 gap-y-8 ">
        <div className="grid grid-cols-[repeat(auto-fill,_minmax(370px,_1fr))] gap-x-5 gap-y-8 ">
            {isAllLombaLoading || !allLombaResponse
                ? Array.from({ length: 12 }).map((_, idx) => (
                      <PosterSkeleton key={"poster-skeleton-" + idx} />
                  ))
                : allLombaResponse.map((lomba) => (
                      <LandscapePoster
                          id={lomba.id}
                          key={lomba.id}
                          price={lomba.is_free ? null : "-"}
                          isOnline={Boolean(lomba.is_held_online)}
                          title={lomba.name as string}
                          startDate={lomba.start_date as string}
                          endDate={lomba.end_date as string}
                          image={lomba.poster_public_url as string}
                      />
                  ))}
        </div>
    )
}

export default LombaHeldSubpage
