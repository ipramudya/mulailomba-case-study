import useOrganizerUserData from "@organizerService/user/use-organizer-user-data"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"

import DescriptionAside from "./description-aside"

const DescriptionSubpage: FunctionComponent = () => {
    const { data: organizerData, isLoading: isOrganizerDataLoading } = useOrganizerUserData()

    return (
        <div className="flex space-x-[5rem]">
            <div className="w-full">
                <div className="boder flex flex-col space-y-2 rounded-md border border-neutral-100 bg-neutral-50 p-3">
                    {isOrganizerDataLoading || !organizerData ? (
                        <Skeleton />
                    ) : (
                        <Typography size="sm">{organizerData.about}</Typography>
                    )}
                </div>
            </div>

            <aside className="w-full max-w-[260px]">
                <DescriptionAside />
            </aside>
        </div>
    )
}

export default DescriptionSubpage
