import useGetAllOrganizerLomba from "@organizerService/lomba/use-get-all-lomba"
import useOrganizerUserData from "@organizerService/user/use-organizer-user-data"
import Button from "@ui/button"
import Paper from "@ui/paper"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"

import AdvancedInfoItem from "./advanced-info-item"

const DescriptionAside: FunctionComponent = () => {
    /* API calls */
    const { data: allLombaResponse, isLoading: isAllLombaLoading } = useGetAllOrganizerLomba()
    const { data: organizerData, isLoading: isOrganizerDataLoading } = useOrganizerUserData()

    return (
        <div className="flex flex-col space-y-6">
            {/* 👇 kontak */}
            <div>
                {isOrganizerDataLoading || !organizerData ? (
                    <>
                        <Skeleton className=" mb-4 h-[20px] w-[60px] " />
                        <div className="flex flex-col space-y-3">
                            {Array.from({ length: 3 }).map((_, idx) => (
                                <Skeleton
                                    className="h-[42px] w-full "
                                    key={"contact-buttons-" + idx}
                                />
                            ))}
                        </div>
                    </>
                ) : (
                    <>
                        <Typography as="h5" weight={500} className="mb-4">
                            Kontak
                        </Typography>
                        <div className="flex flex-col space-y-3">
                            {organizerData.social_whatsapp && (
                                <Button
                                    size="md"
                                    className="justify-start border border-primary-300"
                                    startIcon="tabler:brand-whatsapp"
                                    label={organizerData.social_whatsapp}
                                    intent="primary-low"
                                />
                            )}
                            <Button
                                size="md"
                                className="justify-start border border-primary-300"
                                startIcon="lucide:mail"
                                label={organizerData.email || ""}
                                intent="primary-low"
                            />
                            {organizerData.social_instagram && (
                                <Button
                                    size="md"
                                    className="justify-start border border-primary-300"
                                    startIcon="lucide:instagram"
                                    label={organizerData.social_instagram}
                                    intent="primary-low"
                                />
                            )}
                        </div>
                    </>
                )}
            </div>

            {/* 👇 advanced item */}
            <div>
                {isAllLombaLoading ||
                !allLombaResponse ||
                isOrganizerDataLoading ||
                !organizerData ? (
                    <Paper className="space-y-3 px-3 py-4">
                        {Array.from({ length: 3 }).map((_, idx) => (
                            <div key={"advanced-item-" + idx} className="mb-2">
                                <Skeleton className="mb-1 h-[20px] w-[120px] " />
                                <Skeleton className="h-[38px] w-[200px]" />
                            </div>
                        ))}
                    </Paper>
                ) : (
                    <>
                        <Typography as="h5" weight={500} className="mb-4">
                            Informasi Lanjut
                        </Typography>
                        <Paper className="space-y-3 px-3 py-4">
                            <AdvancedInfoItem
                                title="Alamat organizer"
                                content={organizerData.location || "Tidak memiliki alamat"}
                            />
                            <AdvancedInfoItem
                                title="Lomba telah terselenggara"
                                content={`${allLombaResponse?.length} Lomba`}
                            />
                            <AdvancedInfoItem
                                title="Total pendaftar"
                                content="Belum ada pendaftar"
                            />
                        </Paper>
                    </>
                )}
            </div>
        </div>
    )
}

export default DescriptionAside
