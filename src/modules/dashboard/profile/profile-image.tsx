import avatar from "@functions/generate-avatar"
import useOrganizerUserData from "@organizerService/user/use-organizer-user-data"
import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"

const ProfileImage: FunctionComponent = () => {
    const { data: organizerData } = useOrganizerUserData()

    return (
        <div className="flex aspect-square w-[6rem] items-center justify-center rounded-lg border-[2px] border-primary-800 bg-white p-3">
            {!organizerData ? (
                <Skeleton className="aspect-square w-full overflow-hidden rounded-md" />
            ) : (
                <div className="aspect-square w-[90%] overflow-hidden rounded-md">
                    <img
                        src={organizerData.profile_public_url || avatar}
                        alt="foto profil organizer"
                    />
                </div>
            )}
        </div>
    )
}

export default ProfileImage
