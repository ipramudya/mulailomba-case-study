import useOrganizerUserData from "@organizerService/user/use-organizer-user-data"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"

const ProfileLabel: FunctionComponent = () => {
    const { data: organizerData, isLoading: isOrganizerDataLoading } = useOrganizerUserData()

    return (
        <div>
            {!organizerData || isOrganizerDataLoading ? (
                <>
                    <Skeleton className="mb-1 h-[32px] w-[120px]" />
                    <Skeleton className="h-[20px] w-[60px]" />
                </>
            ) : (
                <>
                    <Typography size="h2" as="h2" weight={600}>
                        {organizerData.name}
                    </Typography>
                    <Typography size="sm" as="span">
                        {organizerData.profile_status}
                    </Typography>
                </>
            )}
        </div>
    )
}

export default ProfileLabel
