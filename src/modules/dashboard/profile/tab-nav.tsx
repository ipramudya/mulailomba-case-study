import useGetAllOrganizerLomba from "@organizerService/lomba/use-get-all-lomba"
import anchorStyles from "@ui/anchor/anchor.styles"
import Badge from "@ui/badge"
import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"
import { useSearchParams } from "react-router-dom"
import { twMerge } from "tailwind-merge"

const TabNav: FunctionComponent = () => {
    /* router stuffs */
    const [searchParams, setSearchParams] = useSearchParams()
    const search = searchParams.get("tab")

    /* API calls */
    const { data: allLombaResponse, isLoading: isAllLombaLoading } = useGetAllOrganizerLomba()

    return (
        <ul className="flex items-center space-x-6">
            {isAllLombaLoading || !allLombaResponse ? (
                <>
                    <Skeleton className="h-[24px] w-[80px] " />
                    <Skeleton className="h-[24px] w-[80px] " />
                </>
            ) : (
                <>
                    <li
                        onClick={() => setSearchParams({ tab: "description" })}
                        className={twMerge(
                            anchorStyles({
                                active: Boolean(search === "description"),
                                ignoreUnderline: true,
                            }),
                            "font-medium"
                        )}
                    >
                        Deskripsi
                    </li>
                    <li
                        onClick={() => setSearchParams({ tab: "lomba" })}
                        className={twMerge(
                            anchorStyles({
                                active: Boolean(search === "lomba"),
                                ignoreUnderline: true,
                            }),
                            "flex items-center font-medium hover:!no-underline"
                        )}
                    >
                        Lomba
                        <Badge
                            intent="primary-low"
                            className="ml-2 aspect-square"
                            label={allLombaResponse.length.toString()}
                        />
                    </li>
                </>
            )}
        </ul>
    )
}

export default TabNav
