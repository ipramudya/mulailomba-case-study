import { Icon } from "@iconify/react"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"

interface Props {
    icon: string
    label: string
    value: string
    time: string | false
}

const SummaryBlockItem: FunctionComponent<Props> = ({ icon, label, value, time }) => {
    return (
        <div className="flex flex-col space-y-3 rounded-md border border-neutral-100 px-3 py-6">
            <div className="flex items-center space-x-2">
                <Icon icon={icon} className="h-5 w-5 flex-shrink-0 text-neutral-500" />
                <Typography as="span" size="xs">
                    {label}
                </Typography>
            </div>
            <div className="flex grow flex-col space-y-1">
                <Typography as="h3" size="lg" weight={500} className="grow">
                    {value}
                </Typography>
                {time && (
                    <Typography as="span" size="xs" className="text-neutral-400">
                        {time}
                    </Typography>
                )}
            </div>
        </div>
    )
}

export default SummaryBlockItem
