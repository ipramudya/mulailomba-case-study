import { getCurrentTimeline, getNextTimeline } from "@functions/check-timelines"
import { formatDate } from "@functions/format-date"
import { isEmpty } from "lodash-es"
import { FunctionComponent, useMemo } from "react"

import { LombaDetailType } from ".."
import SummaryBlocksSkeleton from "./skeleton"
import SummaryBlockItem from "./summary-block-item"

interface Props {
    lombaResponse: LombaDetailType
}

const SummaryBlocks: FunctionComponent<Props> = ({ lombaResponse }) => {
    const { data: lomba, isLoading } = lombaResponse

    const getTimelineValues = useMemo(() => {
        if (!lomba || isLoading) return null

        const now = new Date().toISOString()
        const includedTimeline = getCurrentTimeline(now, lomba.lomba_timelines as any)
        const nextTimeline = getNextTimeline(now, lomba.lomba_timelines as any)

        return {
            includedTimeline,
            nextTimeline,
        }
    }, [isLoading, lomba])

    return (
        <div className="grid grid-cols-4 gap-6">
            {isLoading || !lomba || !getTimelineValues ? (
                Array.from({ length: 4 }).map((_, idx) => (
                    <SummaryBlocksSkeleton key={"summary blocks skeleton " + idx} />
                ))
            ) : (
                <>
                    <SummaryBlockItem
                        icon="heroicons:user-group"
                        label="Peserta terdaftar"
                        value={
                            isEmpty(lomba.registrations) || !Array.isArray(lomba.registrations)
                                ? "Belum Ada Peserta"
                                : lomba.registrations.length + " Orang Peserta"
                        }
                        time={
                            lomba.updated_at === null
                                ? "Belum diatur"
                                : "Diperbarui pada " +
                                  formatDate(lomba.updated_at, "EEEE, dd LLL y")
                        }
                    />
                    <SummaryBlockItem
                        icon="heroicons:ticket"
                        label="Kuota tersisa"
                        value={
                            lomba.is_unlimited_participants
                                ? "Kuota Tidak Terbatas"
                                : `${
                                      Number(lomba.total_participants) -
                                      (lomba.registrations as any).length
                                  } Orang Kuota`
                        }
                        time={
                            "Diperbarui pada " +
                            formatDate(lomba.updated_at as string, "EEEE, dd LLL y")
                        }
                    />
                    <SummaryBlockItem
                        icon="heroicons:clock"
                        label="Timeline berlangsung"
                        value={
                            getTimelineValues.includedTimeline === null
                                ? "Belum Ada Timeline"
                                : (getTimelineValues.includedTimeline.name as string)
                        }
                        time={
                            getTimelineValues.includedTimeline === null
                                ? false
                                : formatDate(
                                      getTimelineValues.includedTimeline.end_date as string,
                                      "EEEE, dd LLL y"
                                  )
                        }
                    />
                    <SummaryBlockItem
                        icon="heroicons:clock"
                        label="Timeline selanjutnya"
                        value={
                            getTimelineValues.nextTimeline === null
                                ? "Belum Ada Timeline"
                                : (getTimelineValues.nextTimeline.name as string)
                        }
                        time={
                            getTimelineValues.nextTimeline === null
                                ? false
                                : formatDate(
                                      getTimelineValues.nextTimeline.end_date as string,
                                      "EEEE, dd LLL y"
                                  )
                        }
                    />
                </>
            )}
        </div>
    )
}

export default SummaryBlocks
