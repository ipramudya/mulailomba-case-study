import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"

const SummaryBlocksSkeleton: FunctionComponent = () => {
    return (
        <div className="flex flex-col space-y-3 rounded-md border border-neutral-100 px-3 py-6">
            <div className="flex items-center space-x-2">
                <Skeleton className="h-5 w-[140px] " />
            </div>
            <div className="flex flex-col space-y-1">
                <Skeleton className="h-[28px] w-[80px] " />
                <Skeleton className="h-5 w-full " />
            </div>
        </div>
    )
}

export default SummaryBlocksSkeleton
