import Pattern from "@blocks/pattern"
import { getCurrentTimeline, getNextTimeline } from "@functions/check-timelines"
import useGetRegistrations from "@organizerService/registrations/use-get-registrations"
import useGetDetailLomba, { Timeline } from "@publicService/use-get-detail-lomba"
import Container from "@ui/container"
import { atom, useAtomValue, useSetAtom } from "jotai"
import { isEmpty } from "lodash-es"
import { FunctionComponent, useEffect } from "react"
import { Outlet, useParams } from "react-router-dom"

import LombaDetailHeader from "./lomba-detail-header"
import { FilterMemberAtom, SearchMemberAtom } from "./main-detail"
import SummaryBlocks from "./summary-blocks"

type TimelineAtom = {
    current: Timeline | null
    next: Timeline | null
    noTimelineShouldBeOK?: boolean | null
}
export const TimelineOnAtom = atom<TimelineAtom | null>(null)

export type LombaDetailType = ReturnType<typeof useGetDetailLomba>

const LombaDetail: FunctionComponent = () => {
    const params = useParams()
    const lombaId = params.lombaId as string

    /* shareable states */
    const searchMember = useAtomValue(SearchMemberAtom)
    const filterMember = useAtomValue(FilterMemberAtom)
    const setTimelineOn = useSetAtom(TimelineOnAtom)

    const lombaDetailResponse = useGetDetailLomba(lombaId)
    const lombaRegistrations = useGetRegistrations({
        lombaId,
        search: searchMember,
        filterMember,
    })

    useEffect(() => {
        if (lombaDetailResponse && lombaDetailResponse.data) {
            const { lomba_timelines } = lombaDetailResponse.data
            if (lomba_timelines && !isEmpty(lomba_timelines) && Array.isArray(lomba_timelines)) {
                const currentTimeline = getCurrentTimeline(
                    new Date().toISOString(),
                    lomba_timelines
                )
                const nextTimeline = getNextTimeline(new Date().toISOString(), lomba_timelines)

                setTimelineOn({
                    current: currentTimeline,
                    next: nextTimeline,
                })
            } else if (isEmpty(lomba_timelines)) {
                setTimelineOn({
                    current: null,
                    next: null,
                    noTimelineShouldBeOK: true,
                })
            }
        }
    }, [lombaDetailResponse, setTimelineOn])

    /* cleanup/unmount  */
    useEffect(() => {
        return () => setTimelineOn(null)
    }, [setTimelineOn])

    return (
        <>
            <Pattern />
            <div className="flex flex-col space-y-14 pb-14">
                {/* title */}
                <LombaDetailHeader
                    lombaResponse={lombaDetailResponse}
                    lombaRegistrations={lombaRegistrations}
                />

                <Container as="section" className="flex flex-col space-y-14">
                    {/* four blocks */}
                    <SummaryBlocks lombaResponse={lombaDetailResponse} />

                    {/* tables */}
                    <Outlet
                        context={{
                            lombaRegistrations,
                            detailLomba: lombaDetailResponse,
                        }}
                    />
                    {/* <LombaDetailTable
                    lombaRegistrations={lombaRegistrations}
                    detailLomba={lombaDetailResponse}
                /> */}
                </Container>
            </div>
        </>
    )
}

export default LombaDetail
