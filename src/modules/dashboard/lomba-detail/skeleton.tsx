import { Fragment, FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"

const LombaDetailTableSkeleton: FunctionComponent = () => {
    return (
        <div className="flex flex-col overflow-hidden rounded-md border border-neutral-100">
            <div className="grid w-full grid-cols-4 gap-8 border-b border-neutral-100 bg-neutral-50 px-6 py-3">
                {Array.from({ length: 4 }).map((_, idx) => (
                    <Skeleton key={`lomba detail table skeleton ${idx}`} className="h-6 " />
                ))}
            </div>
            {Array.from({ length: 7 }).map((_, idx) => (
                <Fragment key={`lomba table item skeleton ${idx}`}>
                    <div className="grid grid-cols-4 gap-8 px-6 py-3">
                        <div className="flex items-center space-x-2">
                            <Skeleton className="aspect-square w-9" />
                            <div className="flex flex-col">
                                <Skeleton className="h-4 w-[162px] " />
                                <Skeleton className="h-4 w-[62px] " />
                            </div>
                        </div>
                        <div className="flex items-center">
                            <Skeleton className="h-5 w-[200px] " />
                        </div>
                        <div className="flex items-center">
                            <Skeleton className="h-5 w-[200px] " />
                        </div>
                        <div className="flex items-center justify-between">
                            <Skeleton className="h-5 w-[146px] " />
                            <Skeleton className="aspect-square w-[32px] " />
                        </div>
                    </div>
                    {idx !== 6 && <hr className="border-neutral-100" />}
                </Fragment>
            ))}
        </div>
    )
}

export default LombaDetailTableSkeleton
