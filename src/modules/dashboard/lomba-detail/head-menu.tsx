import Confirmation from "@blocks/confirmation"
import Button from "@ui/button"
import IconButton from "@ui/icon-button"
import Popover from "@ui/popover"
import { FunctionComponent, useState } from "react"
import toast from "react-hot-toast"
import { useNavigate } from "react-router-dom"

import useDeleteLomba from "../lomba-update/_hooks/useDeleteLomba"

interface Props {
    lombaName: string
}

const HeadMenu: FunctionComponent<Props> = ({ lombaName }) => {
    const [isConfirmationOpen, setConfirmationOpen] = useState(false)
    const [isLoading, setIsLoading] = useState(false)

    /* router stuff */
    const navigate = useNavigate()

    /* local custom hooks */
    const deleteLomba = useDeleteLomba()

    const onDeleteLomba = async () => {
        setIsLoading(true)
        const res = await deleteLomba()

        if (res) {
            navigate("/dashboard/home")
            if (res.error) {
                setIsLoading(false)
                return toast.error("Gagal menghapus lomba")
            }
            toast.success("Data perlombaan berhasil dihapus")
        }
        setIsLoading(false)
    }

    return (
        <>
            {isConfirmationOpen && (
                <Confirmation
                    isOpen={isConfirmationOpen}
                    onOpenChange={setConfirmationOpen}
                    variant="delete"
                    cb={onDeleteLomba}
                    title={`Menghapus ${lombaName}`}
                    description="Apakah anda yakin untuk menghapus perlombaan tersebut ?"
                />
            )}
            <Popover
                size="sm"
                className="overflow-hidden rounded-md border border-neutral-100 p-2 shadow-menu"
                triggerEl={<IconButton size="sm" intent="bordered" icon="heroicons:bars-3" />}
                align="end"
            >
                <div className="flex flex-col space-y-2">
                    <Button
                        thin
                        label="Lihat Detail Lomba"
                        size="xs"
                        intent="default"
                        className="min-w-[140px] justify-start"
                        iconSize="sm"
                        startIcon="heroicons:eye"
                        onClick={() => navigate("preview")}
                    />
                    <Button
                        thin
                        label="Perbarui Lomba"
                        size="xs"
                        intent="default"
                        className="min-w-[140px] justify-start"
                        iconSize="sm"
                        startIcon="heroicons:pencil-square"
                        onClick={() => navigate("update")}
                    />
                    <div className="flex flex-col space-y-1">
                        <hr className="border-neutral-100/50" />
                        <Button
                            thin
                            label="Hapus Lomba"
                            size="xs"
                            intent="danger-ghost"
                            className="min-w-[140px] justify-start"
                            iconSize="sm"
                            startIcon="heroicons:trash"
                            onClick={() => setConfirmationOpen(true)}
                            loading={isLoading}
                        />
                    </div>
                </div>
            </Popover>
        </>
    )
}

export default HeadMenu
