import DropdownFilterV2 from "@blocks/dropdown-filter-v2"
import NotFound from "@blocks/not-found"
import Search from "@blocks/search"
import useGetCancelations from "@organizerService/registrations/use-get-cancelations"
import { RegistrationsResponse } from "@organizerService/registrations/use-get-registrations"
import { GetDetailLombaResponse } from "@publicService/use-get-detail-lomba/use-get-detail-lomba"
import AnchorButton from "@ui/anchor-button"
import Typography from "@ui/typography"
import { isBefore, parseISO } from "date-fns"
import { atom, useAtom, useSetAtom } from "jotai"
import { isEmpty } from "lodash-es"
import { Fragment, FunctionComponent, useMemo } from "react"
import Skeleton from "react-loading-skeleton"
import { useOutletContext, useParams } from "react-router-dom"

import { TimelineOnAtom } from ".."
import LombaDetailTableSkeleton from "../skeleton"
import TableHeadButton from "./table-head-button"
import TableItem from "./table-item"

export type LombaDetailOutletType = {
    lombaRegistrations: RegistrationsResponse
    detailLomba: GetDetailLombaResponse
}

export const SearchMemberAtom = atom<string | null>(null)
export const FilterMemberAtom = atom<string | null>(null)

const MainDetail: FunctionComponent = () => {
    const { lombaRegistrations, detailLomba } = useOutletContext<LombaDetailOutletType>()
    const { isLoading: registrationsLoading, data: registrations } = lombaRegistrations
    const { isLoading: lombaLoading, data: lomba } = detailLomba

    /* shareable state */
    const setSearchMember = useSetAtom(SearchMemberAtom)
    const [filterMember, setFilterMember] = useAtom(FilterMemberAtom)
    const [timelineOn, setTimelineOn] = useAtom(TimelineOnAtom)

    /* router */
    const params = useParams()

    /* api calls */
    const { data: cancelations } = useGetCancelations({ lombaId: params.lombaId as string })

    const canceledCount = useMemo(() => {
        if (cancelations) {
            return cancelations.length
        }
        return null
    }, [cancelations])

    return (
        <div className="flex flex-col space-y-6">
            <div className="flex items-center justify-between">
                <div className="flex flex-col space-y-1">
                    <Typography as="h3" size="h3" weight={500}>
                        Daftar Peserta Lomba
                    </Typography>
                    {timelineOn && timelineOn.current && (
                        <Typography size="xs" as="span" className="text-primary-400">
                            Pada timeline: {timelineOn.current?.name?.toLowerCase() || "-"}
                        </Typography>
                    )}
                </div>

                <div className="flex items-center space-x-3">
                    <Search
                        placeholder="Cari peserta..."
                        onSearchChange={(val) => {
                            setSearchMember(val)
                        }}
                    />
                    <DropdownFilterV2
                        buttonLabel="Filter"
                        value={filterMember ?? ""}
                        contentAlign="end"
                        ignoreValueAsLabel
                        valueSetter={setFilterMember}
                        options={[
                            { label: "Terverifikasi", value: "verified-1" },
                            { label: "Belum Terverifikasi", value: "verified-0" },
                            { label: "Tereliminasi", value: "eliminate-1" },
                        ]}
                    />
                    {lomba &&
                        lomba.lomba_timelines &&
                        Array.isArray(lomba.lomba_timelines) &&
                        !isEmpty(lomba.lomba_timelines) &&
                        timelineOn &&
                        timelineOn.current && (
                            <DropdownFilterV2
                                buttonLabel="History Timeline"
                                value={timelineOn.current?.id ?? ""}
                                contentAlign="end"
                                ignoreValueAsLabel
                                preventSetEmptyWhenValueSame
                                options={
                                    lomba.lomba_timelines
                                        .filter((t) => {
                                            if (timelineOn.next === null) {
                                                return true // skip proses filtering
                                            } else {
                                                return isBefore(
                                                    parseISO(t.start_date as any),
                                                    parseISO(timelineOn.next.start_date as any)
                                                )
                                            }
                                        })
                                        .map((t) => ({ label: t.name, value: t.id })) as any
                                }
                                valueSetter={(e) => {
                                    if (Array.isArray(lomba.lomba_timelines)) {
                                        const found = lomba.lomba_timelines.find((t) => t.id === e)
                                        setTimelineOn((currentState) => ({
                                            next: currentState?.next || null,
                                            current: found || null,
                                        }))
                                    }
                                }}
                            />
                        )}
                    {Boolean(canceledCount) && (
                        <div className="relative">
                            <AnchorButton
                                tag="Link"
                                label="Ajuan Pembatalan"
                                intent="danger-bordered"
                                relative="path"
                                href="../cancelation"
                            />
                            <div className="absolute top-[-0.5rem] right-[-0.5rem] flex h-5 w-5 items-center justify-center rounded-full bg-danger-500 text-xs text-white">
                                {canceledCount}
                            </div>
                        </div>
                    )}
                </div>
            </div>

            {/* table itself */}
            {registrations === undefined || registrationsLoading || lombaLoading ? (
                <LombaDetailTableSkeleton />
            ) : (
                <>
                    {!Array.isArray(registrations) || isEmpty(registrations) ? (
                        <NotFound
                            spacingY={false}
                            message="Belum ada peserta yang mendaftar pada perlombaan ini"
                        />
                    ) : (
                        <div className="flex flex-col overflow-hidden rounded-md border border-neutral-100 bg-white">
                            <div className="grid w-full grid-cols-5 gap-8 border-b border-neutral-100 bg-neutral-50 px-6 py-3">
                                <TableHeadButton>Nama</TableHeadButton>
                                <TableHeadButton>Alamat</TableHeadButton>
                                <TableHeadButton>Tanggal Mendaftar</TableHeadButton>
                                <TableHeadButton>Hasil</TableHeadButton>
                                <TableHeadButton>Status</TableHeadButton>
                            </div>
                            {registrations.map((r, idx) => (
                                <Fragment key={r.id}>
                                    {r && <TableItem registration={r as any} />}
                                    {idx !== (registrations as any).length - 1 && (
                                        <hr className="border-neutral-100" />
                                    )}
                                </Fragment>
                            ))}
                        </div>
                    )}
                </>
            )}
            <div className="flex items-center justify-between">
                {!lomba || registrationsLoading || lombaLoading ? (
                    <Skeleton className="h-5 w-[80px] " />
                ) : (
                    <Typography size="xs" as="span">
                        Menampilkan{" "}
                        {isEmpty(registrations) || !Array.isArray(registrations)
                            ? "0 dari 0 data"
                            : `${registrations.length} dari ${registrations.length} data`}
                    </Typography>
                )}
            </div>
        </div>
    )
}

export default MainDetail
