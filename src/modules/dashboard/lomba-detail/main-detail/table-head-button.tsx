import Typography from "@ui/typography"
import { FunctionComponent, PropsWithChildren } from "react"

const TableHeadButton: FunctionComponent<PropsWithChildren> = ({ children }) => {
    return (
        <div className="flex items-center justify-between">
            <Typography size="xs" as="span" weight={500}>
                {children}
            </Typography>
        </div>
    )
}

export default TableHeadButton
