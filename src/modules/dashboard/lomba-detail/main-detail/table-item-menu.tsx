import Confirmation from "@blocks/confirmation"
import useGetExecutionByTimeline from "@organizerService/registrations/use-get-execution-by-timeline"
import { Registration } from "@organizerService/registrations/use-get-registrations"
import useUpdateRegistration from "@organizerService/registrations/use-update-registration"
import useGetDetailLomba from "@publicService/use-get-detail-lomba"
import Button from "@ui/button"
import IconButton from "@ui/icon-button"
import Popover from "@ui/popover"
import { useAtomValue } from "jotai"
import { isEmpty } from "lodash-es"
import { FunctionComponent, useMemo, useState } from "react"
import { toast } from "react-hot-toast"
import { useParams } from "react-router-dom"

import { TimelineOnAtom } from ".."
import DetailFiles from "./detail-files-modal"
import DetailPrerequisites from "./detail-prerequisites-modal"
import HistorySetterModal from "./history-setter-modal"
import MemberSetterModal from "./member-setter-modal"

interface Props {
    registration: Registration
}

const TableItemMenu: FunctionComponent<Props> = ({ registration }) => {
    console.log("registration", registration)

    const [isMemberSetterOpen, setIsMemberSetterOpen] = useState(false)
    const [isFilesOpen, setIsFilesOpen] = useState(false)
    const [isPrerequisitesOpen, setIsPrerequisitesOpen] = useState(false)
    const [isVerifyOpen, setIsVerifyOpen] = useState(false)
    const [isHistoryOpen, setIsHistoryOpen] = useState(false)

    /* shareable state */
    const timelineOn = useAtomValue(TimelineOnAtom)

    /* router stuffs */
    const params = useParams()
    const lombaId = params.lombaId as string

    /* API calls & cloud data */
    const updateRegistration = useUpdateRegistration(registration.id)
    const { data: detailLomba } = useGetDetailLomba(lombaId)
    const { data: execution } = useGetExecutionByTimeline({
        registrationId: registration.id,
        timelineId: timelineOn?.current?.id,
    })
    const lombaTimelines =
        detailLomba && Array.isArray(detailLomba.lomba_timelines)
            ? detailLomba.lomba_timelines
            : null
    const isPrerequisiteContainsFiles = useMemo((): boolean | null => {
        if (
            detailLomba &&
            Array.isArray(detailLomba.lomba_prerequisites) &&
            !isEmpty(detailLomba.lomba_prerequisites)
        ) {
            return detailLomba.lomba_prerequisites.some((lp) => lp.variant === "File")
        }

        return null
    }, [detailLomba])

    const onVerifyUser = async () => {
        if (registration.is_verified) return toast.success("Peserta telah terverifikasi")
        const { error } = await updateRegistration({ verifyValue: 1 })

        if (error) {
            return toast.error(`Gagal melakukan verifikasi pada ${registration.seeker.name}`)
        }

        return toast.success(`Pendaftaran ${registration.seeker.name} berhasil diverifikasi`)
    }

    const isSetMemberRendered = (): boolean => {
        return !!(
            registration.is_verified === 1 &&
            !registration.is_eliminated &&
            (timelineOn?.current || timelineOn?.noTimelineShouldBeOK)
        )
    }

    return (
        <>
            {isFilesOpen && registration.registrations_upload && (
                <DetailFiles
                    isOpen={isFilesOpen}
                    onOpenChange={setIsFilesOpen}
                    seekerId={registration.seeker_id}
                />
            )}
            {isPrerequisitesOpen && registration.registrations_answer && (
                <DetailPrerequisites
                    isOpen={isPrerequisitesOpen}
                    onOpenChange={setIsPrerequisitesOpen}
                    registrationAnswer={registration.registrations_answer}
                />
            )}
            {isVerifyOpen && (
                <Confirmation
                    variant="accept"
                    isOpen={isVerifyOpen}
                    onOpenChange={setIsVerifyOpen}
                    cb={onVerifyUser}
                    title={`Verifikasi Pendaftaran ${registration.seeker.name}`}
                    description={`Apakah anda menyetujui berkas dan persyaratan milik ${registration.seeker.name} ?`}
                />
            )}
            {isMemberSetterOpen && (
                <MemberSetterModal
                    isOpen={isMemberSetterOpen}
                    onOpenChange={setIsMemberSetterOpen}
                    registration={registration}
                />
            )}
            {isHistoryOpen && (
                <HistorySetterModal
                    isOpen={isHistoryOpen}
                    onOpenChange={setIsHistoryOpen}
                    registrationId={registration.id}
                    memberName={registration.seeker.name}
                />
            )}
            <Popover
                size="sm"
                className="overflow-hidden rounded-md border border-neutral-100 p-2 shadow-menu"
                triggerEl={
                    <IconButton
                        icon="heroicons:ellipsis-horizontal"
                        size="sm"
                        intent="primary-ghost"
                        className="rd-state-open:bg-neutral-50"
                    />
                }
                align="end"
            >
                <div className="flex flex-col space-y-2">
                    {isSetMemberRendered() && (
                        <>
                            <Button
                                thin
                                label={execution ? "Hasil Telah Diatur" : "Atur Hasil Peserta"}
                                size="xs"
                                intent="default"
                                className="min-w-[140px] justify-start"
                                iconSize="sm"
                                startIcon="heroicons:adjustments-vertical"
                                onClick={() => setIsMemberSetterOpen(true)}
                                disabled={Boolean(execution)}
                            />
                            {!isEmpty(lombaTimelines) && (
                                <Button
                                    thin
                                    label="History"
                                    size="xs"
                                    intent="default"
                                    className="min-w-[140px] justify-start"
                                    iconSize="sm"
                                    startIcon="heroicons:clock"
                                    onClick={() => setIsHistoryOpen(true)}
                                />
                            )}
                        </>
                    )}
                    {registration.registrations_upload && isPrerequisiteContainsFiles && (
                        <Button
                            thin
                            label="Berkas Peserta"
                            size="xs"
                            intent="default"
                            className="min-w-[140px] justify-start"
                            iconSize="sm"
                            startIcon="heroicons:document-text"
                            onClick={() => setIsFilesOpen(true)}
                        />
                    )}
                    {registration.registrations_answer && (
                        <Button
                            thin
                            label="Persyaratan Peserta"
                            size="xs"
                            intent="default"
                            className="min-w-[140px] justify-start"
                            iconSize="sm"
                            startIcon="heroicons:identification"
                            onClick={() => setIsPrerequisitesOpen(true)}
                        />
                    )}
                    {registration.is_verified !== 1 && (
                        <Button
                            thin
                            label="Verifikasi Peserta"
                            size="xs"
                            intent="default"
                            className="min-w-[140px] justify-start"
                            iconSize="sm"
                            startIcon="heroicons:check-circle"
                            onClick={() => setIsVerifyOpen(true)}
                        />
                    )}
                </div>
            </Popover>
        </>
    )
}

export default TableItemMenu
