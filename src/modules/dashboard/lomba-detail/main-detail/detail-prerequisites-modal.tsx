import { Icon } from "@iconify/react"
import { RegistrationAnswer } from "@organizerService/registrations/use-get-registrations"
import IconButton from "@ui/icon-button"
import Modal from "@ui/modal"
import Paper from "@ui/paper"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"

interface Props {
    isOpen: boolean
    onOpenChange(val: boolean): void
    registrationAnswer: RegistrationAnswer[]
}

const DetailPrerequisites: FunctionComponent<Props> = ({
    isOpen,
    onOpenChange,
    registrationAnswer,
}) => (
    <Modal
        open={isOpen}
        onOpenChange={onOpenChange}
        title={false}
        classNames={{ content: "max-w-[612px]" }}
    >
        <div className="flex flex-col space-y-6">
            <div className="flex items-start justify-between">
                <div className="flex flex-col space-y-1">
                    <Typography weight={500}>Prasyarat Peserta</Typography>
                    <Typography size="xs">
                        Daftar informasi prasyarat yang diberikan kepada peserta <br /> sebelum
                        mendaftar lomba
                    </Typography>
                </div>
                <IconButton
                    aria-label="tutup"
                    icon="heroicons:x-circle"
                    size="sm"
                    iconSize="md"
                    intent="danger-ghost"
                    circle
                    onClick={() => onOpenChange(false)}
                />
            </div>

            <div className="flex flex-col space-y-4">
                {registrationAnswer.map((a, idx) => (
                    <Paper key={a.label + idx}>
                        <div className="flex space-x-2">
                            <Icon
                                icon="fluent:pin-20-filled"
                                className="h-5 w-5 text-primary-500"
                            />
                            <Typography size="sm" weight={500}>
                                {a.label}
                            </Typography>
                        </div>
                        <Typography size="xs">{a.value}</Typography>
                    </Paper>
                ))}
            </div>
        </div>
    </Modal>
)

export default DetailPrerequisites
