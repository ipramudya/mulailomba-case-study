import getFileExtension from "@functions/get-file-extension"
import downloadFileStorage from "@publicService/download-file-storage"
import getPublicURL from "@publicService/get-public-url"
import AnchorButton from "@ui/anchor-button"
import Button from "@ui/button"
import IconButton from "@ui/icon-button"
import Popover from "@ui/popover"
import { FunctionComponent, useEffect, useState } from "react"

interface Props {
    filePath: string
    hidePreview?: boolean
    fileName: string
}

const DetailFilesMenu: FunctionComponent<Props> = ({ filePath, fileName }) => {
    const downloadFile = downloadFileStorage()
    const [blobUrl, setBlobUrl] = useState<string>()
    const isPreviewVisible = ["pdf", "jpg", "jpeg", "png"].includes(getFileExtension(fileName))

    useEffect(() => {
        ;(async () => {
            downloadFile("files", filePath).then((res) => {
                if (res.data) {
                    setBlobUrl(URL.createObjectURL(res.data))
                }
            })
        })()

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <Popover
            size="sm"
            className="overflow-hidden rounded-md border border-neutral-100 p-2 shadow-menu"
            triggerEl={
                <IconButton
                    icon="heroicons:ellipsis-vertical"
                    size="xs"
                    intent="unstyled"
                    className="rd-state-open:bg-neutral-50"
                />
            }
            align="end"
        >
            <div className="flex flex-col space-y-2">
                {isPreviewVisible && (
                    <Button
                        thin
                        label="Lihat Berkas"
                        size="xs"
                        intent="default"
                        className="min-w-[140px] justify-start"
                        iconSize="sm"
                        startIcon="heroicons:document-text"
                        onClick={() => {
                            window.open(getPublicURL("files", filePath).data.publicUrl)
                        }}
                    />
                )}
                {blobUrl && (
                    <AnchorButton
                        href={blobUrl}
                        thin
                        label="Download"
                        size="xs"
                        intent="default"
                        className="min-w-[140px] justify-start"
                        iconSize="sm"
                        startIcon="heroicons:arrow-down-tray"
                        download={fileName}
                    />
                )}
            </div>
        </Popover>
    )
}

export default DetailFilesMenu
