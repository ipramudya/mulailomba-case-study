import mimetypeIcon from "@constant/plot-mimetype-icon"
import formatBytes from "@functions/format-bytes"
import { formatDate } from "@functions/format-date"
import getFileExtension from "@functions/get-file-extension"
import { Icon } from "@iconify/react"
import useGetListFiles from "@publicService/use-get-list-files"
import IconButton from "@ui/icon-button"
import Modal from "@ui/modal"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"

import DetailFilesMenu from "./detail-files-menu"

interface Props {
    isOpen: boolean
    onOpenChange(val: boolean): void
    seekerId: string
}

const DetailFiles: FunctionComponent<Props> = ({ isOpen, onOpenChange, seekerId }) => {
    const { data: files, isLoading, isFetching } = useGetListFiles(seekerId)

    return (
        <Modal
            open={isOpen}
            onOpenChange={onOpenChange}
            title={false}
            classNames={{ content: "max-w-[612px]" }}
        >
            <div className="flex flex-col space-y-4">
                <div className="flex items-center justify-between">
                    <Typography weight={500}>Berkas Peserta</Typography>
                    <IconButton
                        aria-label="tutup"
                        icon="heroicons:x-circle"
                        size="sm"
                        iconSize="md"
                        intent="danger-ghost"
                        circle
                        onClick={() => onOpenChange(false)}
                    />
                </div>
                <div className="grid grid-cols-2 gap-4">
                    {!files || !files.data || isLoading || isFetching
                        ? Array.from({ length: 4 }).map((_, idx) => (
                              <Skeleton
                                  key={`file from storage skeleton ${idx}`}
                                  className="col-span-1 min-h-[72px]"
                              />
                          ))
                        : files.data.map((file) => (
                              <div
                                  key={file.id}
                                  className="flex flex-col space-y-4 rounded-md border border-neutral-100/50 p-3"
                              >
                                  <div className="flex items-center justify-between">
                                      <Icon
                                          icon={
                                              !file || !file.name
                                                  ? "bi:file-earmark-text"
                                                  : (mimetypeIcon.get(
                                                        getFileExtension(file.name) as any
                                                    )?.icon as string)
                                          }
                                          className={`h-7 w-7 ${
                                              mimetypeIcon.get(getFileExtension(file.name) as any)
                                                  ?.color
                                          }`}
                                      />
                                      <DetailFilesMenu
                                          filePath={`${seekerId}/${file.name}`}
                                          fileName={file.name}
                                      />
                                  </div>
                                  <div className="flex flex-col space-y-2">
                                      <Typography size="sm" weight={500} className="truncate ">
                                          {file.name}
                                      </Typography>
                                      <div className="flex items-center space-x-2">
                                          <Typography size="xs" className="text-neutral-400">
                                              {formatDate(file.updated_at, "dd MMM yyy")}
                                          </Typography>
                                          <div className="h-1 w-1 rounded-full bg-neutral-300" />
                                          <Typography size="xs" className="text-neutral-400">
                                              {formatBytes(file.metadata.size)}
                                          </Typography>
                                      </div>
                                  </div>
                              </div>
                          ))}
                </div>
            </div>
        </Modal>
    )
}

export default DetailFiles
