import NotFound from "@blocks/not-found"
import { Icon } from "@iconify/react"
import useGetExecutions from "@organizerService/registrations/use-get-executions"
import AnchorButton from "@ui/anchor-button"
import IconButton from "@ui/icon-button"
import Modal from "@ui/modal"
import Paper from "@ui/paper"
import Typography from "@ui/typography"
import { isEmpty } from "lodash-es"
import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"

interface Props {
    isOpen: boolean
    onOpenChange(val: boolean): void
    memberName: string
    registrationId: string
}

const HistorySetterModal: FunctionComponent<Props> = ({
    isOpen,
    onOpenChange,
    memberName,
    registrationId,
}) => {
    const { data: executions, isLoading: executionsLoading } = useGetExecutions(registrationId)
    return (
        <Modal open={isOpen} onOpenChange={onOpenChange} title={false}>
            <div className="flex flex-col space-y-6">
                <div className="flex items-center justify-between">
                    <div className="flex flex-col space-y-1">
                        <Typography weight={500}>History {memberName}</Typography>
                        <Typography size="xs">
                            Berikut adalah informasi peserta yang telah dinyatakan lolos <br /> pada
                            timeline terkait
                        </Typography>
                    </div>
                    <IconButton
                        aria-label="tutup"
                        icon="heroicons:x-circle"
                        size="sm"
                        iconSize="md"
                        intent="danger-ghost"
                        circle
                        onClick={() => onOpenChange(false)}
                    />
                </div>

                <div className="flex flex-col space-y-4">
                    {executionsLoading || !executions ? (
                        Array.from({ length: 4 }).map((_, idx) => (
                            <Skeleton className="h-[42px] w-full " key={"history-peserta" + idx} />
                        ))
                    ) : (
                        <>
                            {isEmpty(executions) ? (
                                <NotFound
                                    spacingY={false}
                                    icon="heroicons:clock"
                                    message={`Informasi terkait history ${memberName} tidak ada`}
                                />
                            ) : (
                                executions.map((e) => (
                                    <Paper key={e.id} className="space-y-4">
                                        <div className="flex flex-col space-y-2">
                                            <div className="flex space-x-2">
                                                <Icon
                                                    icon={
                                                        e.is_eliminate
                                                            ? "heroicons:exclamation-triangle-solid"
                                                            : "heroicons:clock-20-solid"
                                                    }
                                                    className={`h-5 w-5 ${
                                                        e.is_eliminate
                                                            ? "text-danger-500"
                                                            : "text-success-600"
                                                    } `}
                                                />
                                                <Typography size="sm" weight={500}>
                                                    {e.is_eliminate ? "Gagal pada" : "Lolos pada"}{" "}
                                                    {e.timeline_name?.toLowerCase()}
                                                </Typography>
                                            </div>
                                            <Typography size="xs">
                                                Pesan bagi peserta: {e.description}
                                            </Typography>
                                        </div>
                                        {e.file_public_url && (
                                            <AnchorButton
                                                intent="bordered"
                                                label="Lihat informasi yang dikirimkan"
                                                thin
                                                size="xs"
                                                className="max-w-fit border-none bg-transparent p-0 text-primary-500 hover:text-primary-400"
                                                href={e.file_public_url}
                                                newTab
                                            />
                                        )}
                                    </Paper>
                                ))
                            )}
                        </>
                    )}
                </div>
            </div>
        </Modal>
    )
}

export default HistorySetterModal
