import useCreateExecution from "@organizerService/registrations/use-create-execution"
import Button from "@ui/button"
import Form from "@ui/form"
import { useAtom, useAtomValue } from "jotai"
import { FunctionComponent } from "react"
import { type SubmitHandler, useForm } from "react-hook-form"
import { toast } from "react-hot-toast"

import { ChoosenStatusAtom } from "."
import { TimelineOnAtom } from "../.."

type FormFields = {
    resultOn: "auto" | "manual"
    description: string
    timeline: string | null
    advanceInformation: File
    winnerAs?: string
}

interface Props {
    memberId: string
    registrationId: string
    onOpenChange(val: boolean): void
    exist?: {
        executionId: string
        storageUrl?: string | null
    } | null
}

const MemberSetterForm: FunctionComponent<Props> = ({
    memberId,
    registrationId,
    onOpenChange,
    exist,
}) => {
    const [choosenStatus, setChoosenStatus] = useAtom(ChoosenStatusAtom)
    const timelineOn = useAtomValue(TimelineOnAtom)

    const methods = useForm<FormFields>({ defaultValues: { timeline: null } })

    /* API calls & cloud data */
    const createExecution = useCreateExecution(registrationId)

    const onMemberSet: SubmitHandler<FormFields> = async ({
        description,
        advanceInformation,
        winnerAs,
    }) => {
        if (choosenStatus !== null) {
            const res = await createExecution({
                description,
                seekerId: memberId,
                status: choosenStatus,
                file: advanceInformation,
                timelineId: timelineOn?.current?.id,
                timelineName: timelineOn?.current?.name,
                winnerAs,
                exist,
            })

            if (res.error) {
                toast.error(res.error.message)
            } else {
                toast.success("Informasi peserta berhasil diatur")
                onOpenChange(false)
            }
        }
    }

    const getSubmitButtonProps = () => {
        if (choosenStatus !== null) {
            if (choosenStatus === "eliminate") {
                return {
                    label: "Eliminasi",
                    intent: "danger-low",
                } as const
            }

            return {
                label: "Simpan",
                intent: "success-low",
            } as const
        }

        return { label: "", intent: "" }
    }

    return (
        <Form {...methods} onSubmit={onMemberSet}>
            <Form.Layout>
                {/* tahap pemilihan juara */}
                {choosenStatus === "winner" && (
                    <Form.TextField
                        label="Atur sebagai juara ke-?"
                        name="winnerAs"
                        placeholder="tuliskan informasi peringkat peserta"
                        type="number"
                        required
                    />
                )}
                <Form.TextareaField
                    label="Deskripsi"
                    required
                    name="description"
                    placeholder="tuliskan pesan kepada peserta terkait hasil yang akan mereka peroleh "
                />
                {choosenStatus !== null && ["winner", "pass"].includes(choosenStatus) && (
                    <Form.FileField
                        label="Berkas pendukung"
                        hint="Anda dapat melampirkan informasi pendukung (berbentuk PDF)"
                        hideHintIcon
                        isOptional
                        name="advanceInformation"
                        placeholder="file sebagai informasi pendukung"
                    />
                )}
                <div className="flex w-full space-x-4 pt-4">
                    <Button
                        thin
                        label="Kembali"
                        intent="bordered"
                        onClick={() => setChoosenStatus(null)}
                        className="grow"
                    />
                    <Button
                        thin
                        label={getSubmitButtonProps().label}
                        intent={getSubmitButtonProps().intent as any}
                        type="submit"
                        className="grow"
                        loading={methods.formState.isSubmitting}
                    />
                </div>
            </Form.Layout>
        </Form>
    )
}

export default MemberSetterForm
