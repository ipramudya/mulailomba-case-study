import useCheckExecutionByTimeline from "@organizerService/registrations/use-check-execution-by-timeline"
import { Registration } from "@organizerService/registrations/use-get-registrations"
import IconButton from "@ui/icon-button"
import Modal from "@ui/modal"
import Typography from "@ui/typography"
import { atom, useAtom, useAtomValue } from "jotai"
import { isEmpty } from "lodash-es"
import { FunctionComponent, useEffect } from "react"
import { twMerge } from "tailwind-merge"

import { TimelineOnAtom } from "../.."
import Chooser from "./chooser"
import MemberSetterForm from "./member-setter-form"
import MemberSetterFormSkeleton from "./member-setter-form-skeleton"

export const ChoosenStatusAtom = atom<"eliminate" | "pass" | "winner" | null>(null)

interface Props {
    isOpen: boolean
    onOpenChange(val: boolean): void
    registration: Registration
}

const MemberSetterModal: FunctionComponent<Props> = ({ isOpen, onOpenChange, registration }) => {
    const [choosenStatus, setChoosenStatus] = useAtom(ChoosenStatusAtom)
    const timelineOn = useAtomValue(TimelineOnAtom)

    const { data: existingExecution, isLoading } = useCheckExecutionByTimeline({
        registrationId: registration.id,
        timelineId: timelineOn?.current?.id as any,
    })

    useEffect(() => {
        return () => {
            setChoosenStatus(null)
        }
    }, [isOpen, setChoosenStatus])

    return (
        <Modal
            open={isOpen}
            onOpenChange={onOpenChange}
            title={false}
            classNames={{ content: "relative" }}
        >
            <>
                {isLoading ? (
                    <MemberSetterFormSkeleton />
                ) : (
                    <>
                        {choosenStatus !== null && (
                            <div
                                className={twMerge(
                                    "absolute top-0 left-0 h-1 w-full",
                                    ["winner", "pass"].includes(choosenStatus)
                                        ? "bg-success-500"
                                        : "bg-danger-500"
                                )}
                            />
                        )}

                        <div className="flex flex-col space-y-6">
                            <div
                                className={twMerge(
                                    "flex items-center justify-between",
                                    choosenStatus !== null && "border-b border-neutral-100/50 pb-4"
                                )}
                            >
                                <div className="flex flex-col space-y-1">
                                    <Typography weight={500}>
                                        {!isEmpty(existingExecution)
                                            ? "Perbarui Hasil "
                                            : "Atur Hasil "}
                                        Peserta
                                    </Typography>
                                    <Typography size="xs">
                                        Anda dapat mengatur peserta untuk menjadi pemenang <br />{" "}
                                        atau bahkan mengeliminasi mereka dari perlombaan
                                    </Typography>
                                </div>
                                <IconButton
                                    aria-label="tutup"
                                    icon="heroicons:x-circle"
                                    size="sm"
                                    iconSize="md"
                                    intent="danger-ghost"
                                    circle
                                    onClick={() => onOpenChange(false)}
                                />
                            </div>

                            {/* chooser */}

                            {choosenStatus === null ? (
                                <Chooser memberName={registration.seeker.name} />
                            ) : (
                                <MemberSetterForm
                                    memberId={registration.seeker.id}
                                    registrationId={registration.id}
                                    onOpenChange={onOpenChange}
                                    exist={
                                        existingExecution
                                            ? {
                                                  executionId: existingExecution.id,
                                                  storageUrl: existingExecution.file_storage_url,
                                              }
                                            : null
                                    }
                                />
                            )}
                        </div>
                    </>
                )}
            </>
        </Modal>
    )
}

export default MemberSetterModal
