import { Icon } from "@iconify/react"
import useGetDetailLomba from "@publicService/use-get-detail-lomba"
import Typography from "@ui/typography"
import { useAtomValue, useSetAtom } from "jotai"
import { FunctionComponent, useMemo } from "react"
import { useParams } from "react-router-dom"

import { ChoosenStatusAtom } from "."
import { TimelineOnAtom } from "../.."

interface Props {
    memberName: string
}

const Chooser: FunctionComponent<Props> = ({ memberName }) => {
    const setChoosenStatus = useSetAtom(ChoosenStatusAtom)
    const timelineOn = useAtomValue(TimelineOnAtom)

    const params = useParams()
    const lombaId = params.lombaId as string

    const { data: lombaDetailResponse } = useGetDetailLomba(lombaId)

    const isPassOrWinner = useMemo(() => {
        if (timelineOn) {
            if (timelineOn.noTimelineShouldBeOK) {
                return "winner"
            }
            if (lombaDetailResponse && Array.isArray(lombaDetailResponse.lomba_timelines)) {
                const found = lombaDetailResponse.lomba_timelines.findIndex(
                    (el) => el.id === timelineOn.current?.id
                )
                if (found === lombaDetailResponse.lomba_timelines.length - 1) {
                    return "winner"
                }
                return "pass"
            }
        }
        return null
    }, [lombaDetailResponse, timelineOn])

    const getPassOrWinnerLabel = () => {
        if (isPassOrWinner === "pass") {
            return "Untuk Lolos Tahap Ini"
        } else if (isPassOrWinner === "winner") {
            return "Sebagai Pemenang"
        }
        return "Telah Usai"
    }

    return (
        <div>
            <div className="grid grid-cols-2 gap-x-6">
                {/* atur sebagai pemenang */}
                <div
                    className="col-span-1 aspect-[3/2] cursor-pointer rounded-lg bg-success-50 p-[6px] transition duration-200 hover:bg-success-50/80 "
                    onClick={() => {
                        if (isPassOrWinner) {
                            setChoosenStatus(isPassOrWinner)
                        }
                    }}
                >
                    <div className="flex h-full grow flex-col items-center justify-center space-y-6 rounded-lg border border-success-500 p-3 ">
                        <Icon icon="heroicons:trophy-solid" className="h-7 w-7 text-success-600" />
                        <div className="flex flex-col items-center justify-center space-y-[2px]">
                            <Typography
                                className="max-w-[200px] overflow-hidden truncate text-center text-success-600"
                                size="xs"
                            >
                                Buat {memberName}
                            </Typography>
                            <Typography className="text-success-600" weight={500}>
                                {getPassOrWinnerLabel()}
                            </Typography>
                        </div>
                    </div>
                </div>

                {/* atur untuk dieliminasi */}
                <div
                    className="col-span-1 aspect-[3/2] cursor-pointer rounded-lg bg-danger-50 p-[6px] transition duration-200 hover:bg-danger-50/80 "
                    onClick={() => setChoosenStatus("eliminate")}
                >
                    <div className="flex h-full grow flex-col items-center justify-center space-y-6 rounded-lg border border-danger-500 p-3 ">
                        <Icon
                            icon="heroicons:user-minus-solid"
                            className="h-7 w-7 text-danger-600"
                        />
                        <div className="flex flex-col items-center justify-center space-y-[2px]">
                            <Typography
                                className="max-w-[200px] overflow-hidden truncate text-center text-danger-600"
                                size="xs"
                            >
                                Atur {memberName}
                            </Typography>
                            <Typography className="text-danger-600" weight={500}>
                                Untuk Dieliminasi
                            </Typography>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Chooser
