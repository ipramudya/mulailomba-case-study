import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"

const MemberSetterFormSkeleton: FunctionComponent = () => {
    return (
        <div className="flex flex-col space-y-6">
            <div className="flex items-center justify-between">
                <div className="flex flex-col space-y-1">
                    <Skeleton className=" h-[20px] w-[120px] " />
                    <Skeleton className=" h-[32px] w-[300px] " />
                </div>
                <Skeleton className="h-6 w-6 " />
            </div>

            <div className="grid grid-cols-2 gap-x-6">
                <Skeleton className="h-[140px]" />
                <Skeleton className="h-[140px]" />
            </div>
        </div>
    )
}

export default MemberSetterFormSkeleton
