import { formatDate } from "@functions/format-date"
import avatar from "@functions/generate-avatar"
import useGetExecutionByTimeline from "@organizerService/registrations/use-get-execution-by-timeline"
import { Registration } from "@organizerService/registrations/use-get-registrations"
import Badge from "@ui/badge"
import Typography from "@ui/typography"
import { useAtomValue } from "jotai"
import { FunctionComponent } from "react"

import { TimelineOnAtom } from ".."
import TableItemMenu from "./table-item-menu"

interface Props {
    registration: Registration
}

const TableItem: FunctionComponent<Props> = ({ registration }) => {
    const timelineOn = useAtomValue(TimelineOnAtom)

    const { data: execution } = useGetExecutionByTimeline({
        registrationId: registration.id,
        timelineId: timelineOn?.current?.id,
    })

    const getMemberResultBadge = () => {
        if (execution) {
            if (execution.is_eliminate) {
                return {
                    msg: "Tereliminasi",
                    intent: "danger-low",
                }
            } else if (execution.is_passed) {
                return {
                    msg: "Lolos",
                    intent: "success-low",
                }
            } else if (execution.is_winner) {
                return {
                    msg: `Juara ${execution.winner_as}`,
                    intent: "success-low",
                }
            }
        }

        return { msg: "Belum dinilai", intent: "neutral-low" }
    }

    const getProfileSource = () => {
        if (registration.seeker.profile_public_url) {
            if (registration.seeker.profile_public_url.includes("undefined")) {
                return avatar
            }

            return registration.seeker.profile_public_url
        }

        return avatar
    }

    return (
        <div className="grid grid-cols-5 gap-8 px-6 py-3">
            <div className="flex items-center space-x-2">
                <div className="aspect-square w-8 flex-shrink-0 overflow-hidden rounded-full border-[2px] border-white ">
                    <img
                        src={getProfileSource()}
                        alt="profil organizer"
                        className="w-full object-cover"
                    />
                </div>
                <div className="flex flex-col space-y-1">
                    <Typography
                        size="sm"
                        as="span"
                        className="max-w-full overflow-hidden line-clamp-1"
                        weight={500}
                    >
                        {registration.seeker.name}
                    </Typography>
                    <Typography size="xs" as="span" className="text-neutral-400">
                        {registration.seeker.phone}
                    </Typography>
                </div>
            </div>
            <div className="flex items-center">
                <Typography size="sm" as="span" className="line-clamp-2">
                    {registration.seeker.location}
                </Typography>
            </div>
            <div className="flex items-center">
                <Typography size="sm" as="span">
                    {formatDate(registration.created_at, "EEEE, dd LLL y")}
                </Typography>
            </div>
            <div className="flex items-center">
                <Badge
                    label={getMemberResultBadge().msg}
                    size="sm"
                    intent={getMemberResultBadge().intent as any}
                />
            </div>
            <div className="flex items-center justify-between">
                <Badge
                    label={registration.is_verified ? "Sudah terverifikasi" : "Belum terverifikasi"}
                    size="sm"
                    intent={registration.is_verified ? "kale-low" : "danger-low"}
                />
                <TableItemMenu registration={registration} />
            </div>
        </div>
    )
}

export default TableItem
