import IconButton from "@ui/icon-button"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"
import { twMerge } from "tailwind-merge"

const BUTTON_STYLES =
    "w-8 h-8 flex flex-shrink-0 items-center justify-center overflow-hidden font-medium transition duration-300 rounded focus:outline-dashed focus:outline-1 focus:outline-offset-2 focus:outline-neutral-400"

const BORDERED =
    "border border-neutral-100 text-neutral-600 bg-white hover:bg-neutral-50 hover:text-neutral-700 hover:shadow"

const PRIMARY = "bg-primary-600 text-white hover:bg-primary-500 hover:shadow-md"

const TablePagination: FunctionComponent = () => {
    return (
        <div className="flex items-center space-x-2">
            <IconButton iconSize="xs" size="sm" icon="heroicons:chevron-left" intent="bordered" />
            <button className={twMerge(BUTTON_STYLES, PRIMARY)}>
                <Typography size="xs" as="span" className="text-inherit">
                    1
                </Typography>
            </button>
            <button className={twMerge(BUTTON_STYLES, BORDERED)}>
                <Typography size="xs" as="span" className="text-inherit">
                    2
                </Typography>
            </button>
            <button className={twMerge(BUTTON_STYLES, BORDERED)}>
                <Typography size="xs" as="span" className="text-inherit">
                    ...
                </Typography>
            </button>
            <button className={twMerge(BUTTON_STYLES, BORDERED)}>
                <Typography size="xs" as="span" className="text-inherit">
                    16
                </Typography>
            </button>
            <IconButton iconSize="xs" size="sm" icon="heroicons:chevron-right" intent="bordered" />
        </div>
    )
}

export default TablePagination
