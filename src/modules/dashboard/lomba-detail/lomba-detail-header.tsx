import checkUndoneCreationLomba from "@functions/check-undone-creation-lomba"
import useUpsertLomba from "@organizerService/lomba/use-upsert-lomba"
import { RegistrationsResponse } from "@organizerService/registrations/use-get-registrations"
import Button from "@ui/button"
import Container from "@ui/container"
import { HEADER_HEIGHT } from "@ui/nav"
import Typography from "@ui/typography"
import { isEmpty } from "lodash-es"
import { FunctionComponent, useEffect, useMemo, useState } from "react"
import { toast } from "react-hot-toast"
import Skeleton from "react-loading-skeleton"
import { useNavigate, useParams } from "react-router-dom"
import { twMerge } from "tailwind-merge"

import { type LombaDetailType } from "."
import HeadMenu from "./head-menu"

interface Props {
    lombaResponse: LombaDetailType
    lombaRegistrations: RegistrationsResponse
}

const LombaDetailHeader: FunctionComponent<Props> = ({ lombaResponse, lombaRegistrations }) => {
    const { data: lomba, isLoading } = lombaResponse
    const { data: registrations } = lombaRegistrations
    const [isArchived, setIsArchived] = useState<boolean | null>(null)

    /* router stuff */
    const navigate = useNavigate()
    const params = useParams()
    const lombaId = params.lombaId as string

    const { upsertLomba } = useUpsertLomba(lombaId)

    const isUndone = useMemo(() => {
        if (lomba) {
            return checkUndoneCreationLomba(lomba as any)
        }

        return null
    }, [lomba])

    const onArchivedLomba = async (val: boolean) => {
        if (!isEmpty(registrations)) {
            return toast.error(
                "Gagal mengarsipkan lomba. Terdapat peserta yang telah mendaftar lomba"
            )
        }

        const { error } = await upsertLomba({
            isArchived: val,
        })

        if (error) {
            return toast.error("Gagal mengubah data arsipan lomba")
        }

        setIsArchived(val)
        toast.success(
            isArchived
                ? "Proses pembatalan arsipan lomba telah berhasil"
                : "Proses pengarsipan lomba berhasil"
        )
    }

    useEffect(() => {
        if (lomba && !isLoading) {
            setIsArchived(Boolean(lomba.is_archived))
        }
    }, [isLoading, lomba])

    return (
        <div
            className="sticky top-0 z-10 h-fit border-b border-neutral-100 bg-white py-8"
            style={{ top: HEADER_HEIGHT }}
        >
            <Container as="section" className="flex items-center justify-between">
                {isLoading || !lomba ? (
                    <Skeleton className="h-[32px] w-[250px]" />
                ) : (
                    <Typography as="h2" size="h2" weight={600}>
                        {lomba.name}
                    </Typography>
                )}
                <div className="flex items-center space-x-3">
                    {isUndone === null || isArchived === null ? (
                        <Skeleton className="h-[32px] w-[200px] " />
                    ) : (
                        <>
                            {isUndone === "done" ? (
                                <button
                                    type="button"
                                    className="flex flex-shrink-0 items-center justify-center space-x-2 rounded border border-neutral-100 bg-white px-[10px] py-[6px] text-sm font-medium text-neutral-600 transition duration-300 hover:bg-neutral-50 hover:text-neutral-700 hover:shadow focus:outline-dashed focus:outline-1 focus:outline-offset-2 focus:outline-neutral-400"
                                    onClick={() => onArchivedLomba(!isArchived)}
                                >
                                    <span className="max-w-fit overflow-hidden text-sm ">
                                        Arsipkan Lomba
                                    </span>
                                    <div
                                        className={twMerge(
                                            "flex rounded-full bg-primary-50 py-1 transition-all duration-300 ",
                                            isArchived ? "pr-1 pl-4" : "pr-4 pl-1"
                                        )}
                                    >
                                        <div
                                            className={twMerge(
                                                "aspect-square w-3 rounded-full",
                                                isArchived ? "bg-primary-500" : "bg-neutral-300"
                                            )}
                                        />
                                    </div>
                                </button>
                            ) : (
                                <Button
                                    label="Selesaikan pengisian informasi"
                                    intent="warning-low"
                                    onClick={() => navigate("update")}
                                />
                            )}
                        </>
                    )}
                    <HeadMenu lombaName={lomba?.name as string} />
                </div>
            </Container>
        </div>
    )
}

export default LombaDetailHeader
