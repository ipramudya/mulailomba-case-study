import Search from "@blocks/search"
import useGetCancelations from "@organizerService/registrations/use-get-cancelations"
import AnchorButton from "@ui/anchor-button"
import Typography from "@ui/typography"
import { isEmpty } from "lodash-es"
import { Fragment, FunctionComponent, useState } from "react"
import Skeleton from "react-loading-skeleton"
import { useParams } from "react-router-dom"

import TableHeadButton from "../main-detail/table-head-button"
import LombaDetailTableSkeleton from "../skeleton"
import CancelationItem from "./cancelation-item"

const DetailCancelation: FunctionComponent = () => {
    /* router */
    const params = useParams()

    /* shareable state */
    const [searchCancelations, setSearchCancelations] = useState<string | null>(null)

    /* api calls */
    const { data: cancelations, isLoading: cancelationsLoading } = useGetCancelations({
        lombaId: params.lombaId as string,
        search: searchCancelations,
    })

    return (
        <div className="flex flex-col space-y-6">
            {/* header */}
            <div className="flex items-center justify-between">
                <Typography as="h3" size="h3" weight={500}>
                    Peserta Pembatalan Lomba
                </Typography>

                <div className="flex items-center space-x-3">
                    <Search
                        placeholder="Cari peserta..."
                        onSearchChange={(val) => {
                            setSearchCancelations(val)
                        }}
                    />
                    <AnchorButton
                        tag="Link"
                        label="Lihat daftar peserta"
                        intent="primary-bordered"
                        relative="path"
                        href="../main"
                    />
                </div>
            </div>

            {/* table itself */}
            {!cancelations || cancelationsLoading ? (
                <LombaDetailTableSkeleton />
            ) : (
                <div className="flex flex-col overflow-hidden rounded-md border border-neutral-100">
                    <div className="grid w-full grid-cols-5 gap-8 border-b border-neutral-100 bg-neutral-50 px-6 py-3">
                        <TableHeadButton>Nama</TableHeadButton>
                        <TableHeadButton>Alamat</TableHeadButton>
                        <TableHeadButton>Tanggal Mendaftar</TableHeadButton>
                        <TableHeadButton>Tanggal Pembatalan</TableHeadButton>
                    </div>
                    {cancelations.map((c, idx) => (
                        <Fragment key={c.id}>
                            {c && <CancelationItem cancelation={c as any} />}
                            {idx !== (cancelations as any).length - 1 && (
                                <hr className="border-neutral-100" />
                            )}
                        </Fragment>
                    ))}
                </div>
            )}
            <div className="flex items-center justify-between">
                {cancelationsLoading ? (
                    <Skeleton className="h-5 w-[80px] " />
                ) : (
                    <Typography size="xs" as="span">
                        Menampilkan{" "}
                        {isEmpty(cancelations) || !Array.isArray(cancelations)
                            ? "0 dari 0 data"
                            : `${cancelations.length} dari ${cancelations.length} data`}
                    </Typography>
                )}
            </div>
        </div>
    )
}

export default DetailCancelation
