import Confirmation from "@blocks/confirmation"
import { formatDate } from "@functions/format-date"
import avatar from "@functions/generate-avatar"
import useApproveCancelation from "@organizerService/registrations/use-approve-cancelation"
import Badge from "@ui/badge"
import Button from "@ui/button"
import Typography from "@ui/typography"
import { FunctionComponent, useState } from "react"
import toast from "react-hot-toast"

interface Props {
    cancelation: any
}

const CancelationItem: FunctionComponent<Props> = ({ cancelation }) => {
    const [isConfirmationOpen, setIsConfirmationOpen] = useState(false)

    /* api calls */
    const approveCancelation = useApproveCancelation()

    const onApproveCancelation = async () => {
        const { error } = await approveCancelation(cancelation.id)

        if (error) {
            console.log("error", error)
            return toast.error("Ajuan pembatalan gagal terkonfirmasi")
        }

        toast.success("Pembatalan lomba berhasil disetujui")
    }

    const getProfileSource = () => {
        if (cancelation.seeker.profile_public_url) {
            if (cancelation.seeker.profile_public_url.includes("undefined")) {
                return avatar
            }
            return cancelation.seeker.profile_public_url
        }

        return avatar
    }

    return (
        <>
            <Confirmation
                cb={onApproveCancelation}
                isOpen={isConfirmationOpen}
                onOpenChange={setIsConfirmationOpen}
                variant="delete"
                title="Konfirmasi Pembatalan Lomba"
                description={`Apakah anda yakin ingin mengonfirmasi pembatalan lomba ${cancelation.seeker.name} ? Tindakan ini tidak bisa diurungkan`}
            />

            <div className="grid grid-cols-5 gap-8 px-6 py-3">
                <div className="flex items-center space-x-2">
                    <div className="aspect-square w-8 flex-shrink-0 overflow-hidden rounded-full border-[2px] border-white ">
                        <img
                            src={getProfileSource()}
                            alt="profil organizer"
                            className="w-full object-cover"
                        />
                    </div>
                    <div className="flex flex-col space-y-1">
                        <Typography
                            size="sm"
                            as="span"
                            className="max-w-full overflow-hidden line-clamp-1"
                            weight={500}
                        >
                            {cancelation.seeker.name}
                        </Typography>
                        <Typography size="xs" as="span" className="text-neutral-400">
                            {cancelation.seeker.phone}
                        </Typography>
                    </div>
                </div>

                <div className="flex items-center">
                    <Typography size="sm" as="span" className="line-clamp-2">
                        {cancelation.seeker.location}
                    </Typography>
                </div>
                <div className="flex items-center">
                    <Typography size="sm" as="span">
                        {formatDate(cancelation.created_at, "EEEE, dd LLL y")}
                    </Typography>
                </div>
                <div className="flex items-center">
                    <Typography size="sm" as="span">
                        {formatDate(cancelation.updated_at, "EEEE, dd LLL y")}
                    </Typography>
                </div>
                <div className="flex items-center">
                    {cancelation.is_cancelation_approved ? (
                        <Badge label="Pembatalan Disetujui" size="sm" intent="danger-low" />
                    ) : (
                        <Button
                            label="Konfirmasi Pembatalan"
                            intent="danger-ghost"
                            onClick={() => setIsConfirmationOpen(true)}
                        />
                    )}
                </div>
            </div>
        </>
    )
}

export default CancelationItem
