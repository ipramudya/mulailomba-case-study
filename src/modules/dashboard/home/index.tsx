import NotFound from "@blocks/not-found"
import Pattern from "@blocks/pattern"
import Search from "@blocks/search"
import useGetAllOrganizerLomba from "@organizerService/lomba/use-get-all-lomba"
import useOrganizerUserData from "@organizerService/user/use-organizer-user-data"
import Button from "@ui/button"
import Container from "@ui/container"
import RouterLink from "@ui/router-link"
import Typography from "@ui/typography"
import { atom } from "jotai"
import { isEmpty } from "lodash-es"
import { FunctionComponent, useState } from "react"
import Skeleton from "react-loading-skeleton"
import { v4 as uuidV4 } from "uuid"

import LombaGrids from "./lomba-grids"
import LombaGridsSkeleton from "./lomba-grids/lomba-grids.skeleton"

export const DashboardLombaSearchAtom = atom("")

const Home: FunctionComponent = () => {
    const [search, setSearch] = useState("")

    /* generate id for lomba */
    const lombaId = uuidV4()

    /* organizer data */
    const { data: organizerData, isLoading: organizerDataLoading } = useOrganizerUserData()
    const { data: allLombaResponse, isLoading: isAllLombaLoading } = useGetAllOrganizerLomba({
        search,
    })

    return (
        <>
            <Pattern />
            <Container as="main" className="flex flex-col space-y-6 pb-8 pt-[3rem]">
                {/* 👇 title */}
                <div className="flex items-center justify-between">
                    {organizerDataLoading || !organizerData ? (
                        <Skeleton className="h-[32px] w-[120px]" />
                    ) : (
                        <Typography as="h2" size="h2" weight={600}>
                            Selamat datang, {organizerData.name}
                        </Typography>
                    )}

                    {/* 👇 add lomba */}
                    <div className="flex space-x-3">
                        <Search
                            iconPlacing="end"
                            onSearchChange={(val) => {
                                setSearch(val)
                            }}
                        />
                        <RouterLink to="create" state={{ lombaId }}>
                            <Button label="Buat Lomba Baru" size="sm" endIcon="heroicons:plus" />
                        </RouterLink>
                    </div>
                </div>

                {/* 👇 content, could be empty or full of lomba */}
                {isAllLombaLoading ? (
                    <LombaGridsSkeleton />
                ) : (
                    <>
                        {allLombaResponse && isEmpty(allLombaResponse) ? (
                            <NotFound />
                        ) : (
                            <LombaGrids allLomba={allLombaResponse as any} />
                        )}
                    </>
                )}
            </Container>
        </>
    )
}

export default Home
