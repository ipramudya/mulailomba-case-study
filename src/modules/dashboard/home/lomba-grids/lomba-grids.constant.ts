import { adjectives, colors, names, uniqueNamesGenerator } from "unique-names-generator"

const name = () =>
    uniqueNamesGenerator({
        dictionaries: [colors, adjectives, names],
        separator: " ",
        style: "capital",
    })

const c = [
    {
        name: name(),
        description:
            "Amet incididunt anim voluptate laboris et occaecat elit. Voluptate et occaecat aliquip veniam incididunt ex ut cillum dolore dolore exercitation culpa officia labore. Anim ea labore irure sint in veniam in sunt enim velit. Aliqua eiusmod est adipisicing sint incididunt amet mollit.",
        totalRegistered: 112,
        date: "1 Jam - 31 Mar 2022",
        status: "will",
    },
    {
        name: name(),
        description:
            "Nulla est ad ut enim. Adipisicing commodo consequat occaecat sit non ex incididunt reprehenderit. Ex consectetur magna aliquip anim officia minim laboris Lorem sint consequat qui. Id est ipsum tempor minim proident cillum voluptate duis ea id velit ad in irure. Aliqua culpa eiusmod laborum Lorem incididunt dolor. Labore fugiat adipisicing elit cupidatat adipisicing incididunt. Occaecat commodo nulla dolore officia ex ipsum commodo.",
        totalRegistered: 120,
        date: "1 Jam - 31 Mar 2022",
        status: "live",
    },
    {
        name: name(),
        description:
            "Lorem exercitation culpa laborum elit eu laborum eiusmod tempor adipisicing culpa irure. Ipsum eiusmod et sit quis aute incididunt est amet sunt nulla consequat consequat. Pariatur pariatur dolore ut adipisicing excepteur ipsum laboris irure. Deserunt irure cillum anim sint laborum excepteur elit mollit incididunt officia magna in dolore id. Ea in sunt est ad elit culpa elit qui consectetur sit et deserunt. Eu cillum deserunt elit in in et nostrud amet Lorem amet officia proident nisi. Culpa consequat sunt sunt ut et nostrud in pariatur.",
        totalRegistered: 700,
        date: "1 Jam - 31 Mar 2022",
        status: "live",
    },
    {
        name: name(),
        description:
            "Nisi commodo excepteur id qui. Est enim sit eu labore minim elit laborum ut anim. Minim irure irure mollit ut anim quis amet anim tempor consectetur dolore amet aliqua Lorem.",
        totalRegistered: 1200,
        date: "1 Jam - 31 Mar 2022",
        status: "done",
    },
    {
        name: name(),
        description:
            "Amet eu laborum veniam commodo occaecat ipsum mollit Lorem enim nostrud. Culpa pariatur enim reprehenderit labore nulla. Fugiat incididunt ullamco do mollit esse sit. Duis consequat duis laborum et mollit magna. Nulla qui sint enim tempor deserunt veniam. Ullamco eu quis ipsum nulla incididunt exercitation anim est.",
        totalRegistered: 1700,
        date: "1 Jam - 31 Mar 2022",
        status: "done",
    },
    {
        name: name(),
        description:
            "Duis elit Lorem reprehenderit laboris esse veniam minim magna sit eiusmod minim. Culpa magna aliqua aute deserunt laborum exercitation dolore. Ipsum proident adipisicing sit irure mollit. Laboris esse quis duis duis in commodo nulla pariatur sint labore do. Irure reprehenderit nulla ea eu aliqua labore aliquip fugiat occaecat. Amet proident labore ut cillum velit quis consequat minim aliquip consequat dolore in eiusmod.",
        totalRegistered: null,
        date: "1 Jam - 31 Mar 2022",
        status: "archived",
    },
    {
        name: name(),
        description:
            "Eu sunt nulla voluptate velit officia. In dolor occaecat mollit sit laboris aliqua mollit irure occaecat commodo consectetur sunt quis occaecat. Nulla laborum minim magna ipsum ut. Et pariatur officia enim nisi fugiat magna exercitation. Amet aliquip commodo ex sunt voluptate pariatur fugiat exercitation quis veniam eu. Enim esse adipisicing voluptate velit duis velit sint laborum do dolore proident.",
        totalRegistered: null,
        date: "1 Jam - 31 Mar 2022",
        status: "undone",
    },
    {
        name: name(),
        description:
            "Dolor ut voluptate officia amet veniam commodo mollit labore occaecat exercitation sint. Dolore enim in exercitation occaecat eiusmod ea Lorem culpa do. Commodo dolore mollit minim ex minim amet sunt incididunt veniam velit elit cillum. Elit mollit laborum laborum tempor aliqua magna culpa irure do. Sint Lorem occaecat magna consequat aliqua exercitation velit ad enim incididunt. Aute labore magna reprehenderit labore in nisi culpa consectetur. Ex proident exercitation fugiat pariatur ea mollit enim.",
        totalRegistered: null,
        date: "1 Jam - 31 Mar 2022",
        status: "undone",
    },
] as const

const lombaGridsConstant = [...c, ...c]

export default lombaGridsConstant
