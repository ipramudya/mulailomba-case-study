import NotFound from "@blocks/not-found"
import checkUndoneCreationLomba from "@functions/check-undone-creation-lomba"
import { getRemainingTime } from "@functions/format-date"
import statusFromRemainingTime from "@functions/status-from-remaining-time"
import { type AllLombaResponse } from "@organizerService/lomba/use-get-all-lomba"
import { useAtomValue } from "jotai"
import { isEmpty } from "lodash-es"
import { FunctionComponent, useMemo } from "react"

import LombaGridsFilter, { LombaFilterValueAtom } from "./lomba-grids-filter"
import LombaGridItem from "./lomba-grids-item"

interface Props {
    allLomba: AllLombaResponse[]
}

const LombaGrids: FunctionComponent<Props> = ({ allLomba }) => {
    /* jotai */
    const filterValue = useAtomValue(LombaFilterValueAtom)

    const lombaWithStatus = allLomba.map((lomba) => {
        const isLombaUndoned = checkUndoneCreationLomba(lomba)
        if (!!lomba.start_date && !!lomba.end_date) {
            const remainingTime = getRemainingTime(lomba.start_date, lomba.end_date)

            return {
                ...lomba,
                remainingTimeStatus: statusFromRemainingTime(remainingTime.dateRemaining),
                isUndone: isLombaUndoned === "undone",
            }
        } else {
            return {
                ...lomba,
                remainingTimeStatus: null,
                isUndone: isLombaUndoned === "undone",
            }
        }
    })

    const filteredLomba = useMemo(() => {
        return lombaWithStatus.filter((lomba) => {
            if (filterValue === "all") return true
            if (filterValue === "undone") return lomba.isUndone === true
            if (filterValue === "is_archived") return lomba.is_archived === 1
            return lomba.remainingTimeStatus === filterValue
        })
    }, [filterValue, lombaWithStatus])

    return (
        <div className="flex w-full flex-col space-y-8 py-[2rem]">
            {/* 👇 filters */}
            <LombaGridsFilter />

            {/* 👇 lomba */}
            {isEmpty(filteredLomba) ? (
                <NotFound message="Tidak ada data ajang perlombaan" />
            ) : (
                <div className="grid grid-cols-[repeat(auto-fill,_minmax(300px,_1fr))] gap-x-5 gap-y-6 ">
                    {filteredLomba.map((lomba) => (
                        <LombaGridItem key={lomba.id} lomba={lomba} />
                    ))}
                </div>
            )}
        </div>
    )
}

export default LombaGrids
