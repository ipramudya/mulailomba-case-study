import PLOT_STATUS_LOMBA from "@constant/plot-status-lomba"
import checkUndoneCreationLomba from "@functions/check-undone-creation-lomba"
import { formatStartAndEndDate, getRemainingTime } from "@functions/format-date"
import statusFromRemainingTime from "@functions/status-from-remaining-time"
import { Icon } from "@iconify/react"
import { type AllLombaResponse } from "@organizerService/lomba/use-get-all-lomba"
import Badge from "@ui/badge"
import RouterLink from "@ui/router-link"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"
import { twMerge } from "tailwind-merge"

interface Props {
    lomba: AllLombaResponse
}

const LombaGridItem: FunctionComponent<Props> = ({ lomba }) => {
    const { id, name, description, start_date, end_date, poster_public_url } = lomba

    /* formatter */
    let formattedDate, remainingTime, remainingTimeStatus
    if (!!start_date || !!end_date) {
        formattedDate = formatStartAndEndDate(start_date, end_date)
        remainingTime = getRemainingTime(start_date, end_date)
        remainingTimeStatus = statusFromRemainingTime(remainingTime.dateRemaining)
    } else {
        formattedDate = "-"
        remainingTime = null
        remainingTimeStatus = null
    }
    const lombaCreationStatus = checkUndoneCreationLomba(lomba)

    return (
        <RouterLink to={"lomba/" + id}>
            <div className="relative grid h-full grid-cols-3 gap-3 rounded-md border border-neutral-300 bg-white px-4 transition duration-300 hover:border-primary-500 hover:shadow-accent">
                {/* poster */}
                <div className="col-span-1 py-3">
                    <img
                        src={poster_public_url}
                        alt={`poster-${name}`}
                        className="h-full w-full overflow-hidden rounded-md"
                    />
                </div>

                {/* info */}
                <div
                    className={twMerge(
                        "relative col-span-2 flex h-full flex-col space-y-4 ",
                        lombaCreationStatus === "undone" ? "pb-[2.5rem] pt-3" : "py-3"
                    )}
                >
                    {/* 👇 top card */}
                    <div>
                        <Typography as="h3" weight={500} className="mb-1 line-clamp-1">
                            {name || "-"}
                        </Typography>
                        <Typography size="sm" className="line-clamp-2">
                            {description || "-"}
                        </Typography>
                    </div>

                    {/* 👇 main info */}
                    <div className="flex flex-col space-y-2">
                        {/* 👇 date */}
                        <div className="flex items-center space-x-2">
                            <Icon
                                icon="lucide:calendar-range"
                                className="h-5 w-5 flex-shrink-0 text-neutral-500"
                            />
                            <Typography as="span" size="xs">
                                {formattedDate}
                            </Typography>
                        </div>

                        {/* 👇 total registered people */}
                        <div className="flex items-center space-x-2">
                            <Icon
                                icon="heroicons:user-group-solid"
                                className="h-5 w-5 flex-shrink-0 text-neutral-500"
                            />
                            <Typography as="span" size="xs">
                                {lomba.registrations[0].count === 0
                                    ? "Belum ada pendaftar"
                                    : `${lomba.registrations[0].count} pendaftar`}
                            </Typography>
                        </div>
                    </div>

                    {remainingTimeStatus && remainingTime && lombaCreationStatus === "done" && (
                        <Badge
                            label={remainingTime.message}
                            intent={PLOT_STATUS_LOMBA.get(remainingTimeStatus)?.badgeIntent as any}
                            size="xs"
                            className="min-w-[96px] px-2"
                        />
                    )}

                    {lombaCreationStatus === "undone" && (
                        <div className="absolute bottom-0 left-0 w-full bg-warning-50 px-4 py-[2px] ">
                            <Typography
                                as="span"
                                size="xs"
                                weight={500}
                                className="text-warning-600"
                            >
                                Beberapa informasi perlu dilengkapi
                            </Typography>
                        </div>
                    )}
                </div>
            </div>
        </RouterLink>
    )
}

export default LombaGridItem
