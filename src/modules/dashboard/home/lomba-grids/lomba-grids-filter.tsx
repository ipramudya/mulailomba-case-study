import checkUndoneCreationLomba from "@functions/check-undone-creation-lomba"
import { getRemainingTime } from "@functions/format-date"
import statusFromRemainingTime from "@functions/status-from-remaining-time"
import useGetAllOrganizerLomba from "@organizerService/lomba/use-get-all-lomba"
import Button from "@ui/button"
import { atom, useAtom } from "jotai"
import { FunctionComponent, useCallback, useMemo } from "react"

import filterConstant, { type FilterConstantKey } from "./filter.constant"

export const LombaFilterValueAtom = atom<(typeof filterConstant)[number]["key"]>("all")

type RemapResult = {
    [key in FilterConstantKey]: number
}
type RemapObject = {
    undone: boolean | null
    remainingTimeStatus: ReturnType<typeof statusFromRemainingTime>
    is_archived: boolean | null
}

const LombaGridsFilter: FunctionComponent = () => {
    /* jotai */
    const [selected, setSelected] = useAtom(LombaFilterValueAtom)

    /* API calls */
    const { data: allLombaResponse } = useGetAllOrganizerLomba()

    const remapLombaToGetStatus: RemapObject[] | null = useMemo(() => {
        if (!allLombaResponse) return null

        return allLombaResponse.reduce((acc: any[], current) => {
            const obj = {} as any
            if (!!current.start_date && !!current.end_date) {
                const remainingTime = getRemainingTime(current.start_date, current.end_date)
                obj["remainingTimeStatus"] = statusFromRemainingTime(remainingTime.dateRemaining)
            }

            const isLombaUndoned = checkUndoneCreationLomba(current as any)
            if (isLombaUndoned === "undone") {
                obj["undone"] = true
            }

            if (current.is_archived) {
                obj["is_archived"] = true
            }

            acc.push(obj)
            return acc
        }, [])
    }, [allLombaResponse])

    const getFilterLabel = useCallback(
        (key: FilterConstantKey, label: string) => {
            if (remapLombaToGetStatus === null) return key

            const afterRemapValueCounted = remapLombaToGetStatus.reduce<RemapResult>(
                (acc, current) => {
                    if (current.remainingTimeStatus) {
                        if (acc[current.remainingTimeStatus] !== undefined) {
                            acc[current.remainingTimeStatus]++
                        } else {
                            acc[current.remainingTimeStatus] = 1
                        }
                    }

                    if (current.undone) {
                        if (acc["undone"] !== undefined) {
                            acc["undone"]++
                        } else {
                            acc["undone"] = 1
                        }
                    }

                    if (current.is_archived) {
                        if (acc["is_archived"] !== undefined) {
                            acc["is_archived"]++
                        } else {
                            acc["is_archived"] = 1
                        }
                    }

                    return acc
                },
                {} as RemapResult
            )
            if (key === "all") return `${label}(${remapLombaToGetStatus.length})`

            if (afterRemapValueCounted[key]) {
                return `${label}(${afterRemapValueCounted[key]})`
            } else {
                return `${label}(0)`
            }
        },
        [remapLombaToGetStatus]
    )

    return (
        <div className="flex items-center space-x-3">
            {filterConstant.map(({ label, key }) => (
                <Button
                    label={getFilterLabel(key, label)}
                    key={key}
                    intent="bordered"
                    pill
                    className={selected === key ? undefined : "text-neutral-300"}
                    onClick={() => setSelected(key)}
                />
            ))}
        </div>
    )
}

export default LombaGridsFilter
