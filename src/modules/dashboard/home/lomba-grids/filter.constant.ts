export type FilterConstantKey = "all" | "live" | "will" | "done" | "is_archived" | "undone"

const filterConstant = [
    { label: "Semua", key: "all" },
    { label: "Sedang berlangsung", key: "live" },
    { label: "Mendatang", key: "will" },
    { label: "Selesai", key: "done" },
    { label: "Diarsipkan", key: "is_archived" },
    { label: "Belum memenuhi", key: "undone" },
] as const

export default filterConstant
