import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"

const LombaGridsSkeleton: FunctionComponent = () => {
    return (
        <div className="flex w-full flex-col space-y-6 py-[2rem]">
            {/* 👇 filter skeleton */}
            <div className="flex items-center space-x-3">
                {Array.from({ length: 6 }).map((_, idx) => (
                    <Skeleton
                        key={"lomba-grids-filter-skeleton-" + idx}
                        className="h-[34px] w-[120px]"
                    />
                ))}
            </div>

            <div className="grid grid-cols-[repeat(auto-fill,_minmax(300px,_1fr))] gap-x-5 gap-y-8 ">
                {Array.from({ length: 12 }).map((_, idx) => (
                    <div
                        key={"lomba-grids-item-skeleton-" + idx}
                        className="flex h-full flex-col space-y-4 rounded-md border border-neutral-100 px-4 py-3"
                    >
                        <div>
                            <Skeleton className="mb-1 h-[20px] w-[160px]" />
                            <Skeleton className="h-[18px] w-full" count={2} />
                        </div>
                        <div className="flex flex-col space-y-2">
                            <Skeleton className="h-[26px] w-[120px]" />
                            <Skeleton className="h-[26px] w-[120px]" />
                        </div>
                        <Skeleton className="h-[20px] w-[96px]" />
                    </div>
                ))}
            </div>
        </div>
    )
}

export default LombaGridsSkeleton
