import Paper from "@ui/paper"
import Typography from "@ui/typography"
import { FunctionComponent, PropsWithChildren } from "react"

interface Props extends PropsWithChildren {
    title: string
    stepNumber: number
}

const PaperLayout: FunctionComponent<Props> = ({ title, stepNumber, children }) => {
    return (
        <Paper className="space-y-8 bg-white p-6 ">
            {/* 👇 paper title */}
            <div className="flex items-center space-x-3">
                <div className="flex aspect-square w-6 items-center justify-center rounded-full bg-primary-500 text-white">
                    <Typography as="span" weight={500} size="xs" className="text-inherit">
                        {stepNumber}
                    </Typography>
                </div>
                <Typography as="h3" size="h3" weight={500}>
                    {title}
                </Typography>
            </div>

            {/* 👇 paper content */}
            {children}
        </Paper>
    )
}

export default PaperLayout
