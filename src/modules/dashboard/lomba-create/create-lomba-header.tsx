import useDeleteLomba from "@organizerService/lomba/use-delete-lomba"
import deletePoster from "@organizerService/poster/delete-poster"
import useGetDetailLomba from "@publicService/use-get-detail-lomba"
import Button from "@ui/button"
import Container from "@ui/container"
import RouterLink from "@ui/router-link"
import { Fragment, FunctionComponent } from "react"
import { useSearchParams } from "react-router-dom"

import StepIndicator from "./step-indicator"
import stepIndicatorConstant from "./step-indicator/step-indicator.constant"

const CreateLombaHeader: FunctionComponent = () => {
    /* router things */
    const [searchParams] = useSearchParams()
    const lombaId = searchParams.get("lid") as string

    /* API calls */
    const { data: detailLomba } = useGetDetailLomba(lombaId)
    const deleteLomba = useDeleteLomba()

    const onNavigateBack = () => {
        deleteLomba(lombaId)
        if (detailLomba) {
            deletePoster(detailLomba.poster_storage_url as string)
        }
    }

    return (
        <Container size="lg" as="div" className="flex h-full items-center">
            <div className="flex w-full items-center justify-between">
                {/* 👇 back button */}
                <RouterLink to=".." relative="path" onClick={onNavigateBack}>
                    <Button
                        label="Buang perubahan dan kembali"
                        startIcon="heroicons:arrow-left-solid"
                        intent="unstyled"
                        className="max-w-fit p-0 text-neutral-400 hover:text-neutral-300"
                        thin
                    />
                </RouterLink>

                {/* 👇 steps indicator */}
                <div className="flex items-center space-x-3">
                    {stepIndicatorConstant.map((d, idx) => (
                        <Fragment key={"step-indicator-" + d.identifier}>
                            <StepIndicator {...d} />
                            {idx !== stepIndicatorConstant.length - 1 && (
                                <span className="text-neutral-100">&#8212;</span>
                            )}
                        </Fragment>
                    ))}
                </div>
            </div>
        </Container>
    )
}

export default CreateLombaHeader
