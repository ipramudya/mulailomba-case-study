import PrerequisitePreview from "@blocks/prerequisite-preview"
import plotToBoolean from "@functions/plot-to-boolean"
import { zodResolver } from "@hookform/resolvers/zod"
import useCreatePrerequisite from "@organizerService/lomba/prerequisites-lomba/use-create-prerequisite"
import useDeletePrerequisiteByLombaId from "@organizerService/lomba/prerequisites-lomba/use-delete-all-prerequisites-lomba"
import useGetAllPrerequisitesLomba from "@organizerService/lomba/prerequisites-lomba/use-get-all-prerequisites-lomba"
import useGetOrganizerLomba from "@organizerService/lomba/use-get-lomba-by-id"
import useUpsertLomba from "@organizerService/lomba/use-upsert-lomba"
import Button from "@ui/button"
import Form from "@ui/form"
import Typography from "@ui/typography"
import { isEmpty } from "lodash-es"
import { FunctionComponent, useEffect, useRef, useState } from "react"
import { SubmitHandler, useForm } from "react-hook-form"
import { toast } from "react-hot-toast"
import { useLocation, useSearchParams } from "react-router-dom"
import { z } from "zod"

import RegistrationPrerequisiteModal from "./registration-prerequisite/modal"
import PrerequisiteItem from "./registration-prerequisite/prerequisite-item"

const formPrerequisiteSchema = z.object({
    id: z.string({ required_error: "*tidak boleh kosong" }),
    name: z.string({ required_error: "*tidak boleh kosong" }),
    variant: z.enum(["File", "Paragraf", "Teks Singkat"], {
        required_error: "*tidak boleh kosong",
    }),
    description: z.string({ required_error: "*tidak boleh kosong" }),
    isRequired: z.enum(["0", "1"], { required_error: "*tidak boleh kosong" }),
})
export type Prerequisite = z.infer<typeof formPrerequisiteSchema>

const formSchema = z.object({
    registrationVariant: z.string({ required_error: "*tidak boleh kosong" }),
    participant: z.string({ required_error: "*tidak boleh kosong" }),
    totalParticipants: z.any({ required_error: "*tidak boleh kosong" }).optional(),
    registrationStartDate: z.string({ required_error: "*tidak boleh kosong" }),
    registrationEndDate: z.string({ required_error: "*tidak boleh kosong" }),
    lombaStart: z.string({ required_error: "*tidak boleh kosong" }),
    lombaEnd: z.string({ required_error: "*tidak boleh kosong" }),
    prerequisites: z.array(formPrerequisiteSchema).optional(),
})
export type FormFields = z.infer<typeof formSchema>

const Registration: FunctionComponent = () => {
    /* react states */
    const [isOpen, setOpen] = useState(false)
    const [isPreviewPrerequisiteOpen, setIsPreviewPrerequisiteOpen] = useState(false)
    const formRef = useRef()

    /* router stuffs */
    const { state: routerState } = useLocation()
    const [searchParams, setSearchParams] = useSearchParams()
    const lombaId = searchParams.get("lid") as string

    /* API calls */
    const { upsertLomba } = useUpsertLomba(lombaId)
    const { createPrerequisites } = useCreatePrerequisite(lombaId)
    const { deletePrerequisite } = useDeletePrerequisiteByLombaId()
    const { data: lombaOnDB } = useGetOrganizerLomba(lombaId)
    const { data: prerequisitesOnDB } = useGetAllPrerequisitesLomba(lombaId)

    /* hook form */
    const methods = useForm<FormFields>({
        mode: "onChange",
        defaultValues: {
            prerequisites: undefined,
        },
        resolver: zodResolver(formSchema),
    })

    /* set default value */
    useEffect(() => {
        if (lombaOnDB && !isEmpty(lombaOnDB)) {
            const {
                is_free,
                is_unlimited_participants,
                total_participants,
                start_date,
                end_date,
                start_registration,
                end_registration,
            } = lombaOnDB

            methods.setValue(
                "participant",
                plotToBoolean({
                    considered: is_unlimited_participants,
                    truthy: "unlimited",
                    falsy: "limited",
                }) as any
            )
            methods.setValue(
                "registrationVariant",
                plotToBoolean({ considered: is_free, truthy: "free", falsy: "paid" }) as any
            )
            methods.setValue("totalParticipants", total_participants || undefined)
            methods.setValue("registrationStartDate", start_registration as string)
            methods.setValue("registrationEndDate", end_registration as string)
            methods.setValue("lombaStart", start_date as string)
            methods.setValue("lombaEnd", end_date as string)
            methods.setValue("prerequisites", prerequisitesOnDB as any)
        }
    }, [lombaOnDB, methods, prerequisitesOnDB])

    /* event handler */
    const onRegistrationSubmit: SubmitHandler<FormFields> = async (
        {
            registrationVariant,
            participant,
            totalParticipants,
            lombaStart,
            lombaEnd,
            registrationStartDate,
            registrationEndDate,
            prerequisites,
        },
        event
    ) => {
        if ((event && event.target) !== formRef.current) return

        const { data, error } = await upsertLomba({
            isFree: Boolean(registrationVariant === "free"),
            isUnlimitedParticipants: Boolean(participant === "unlimited"),
            totalParticipants,
            startDate: lombaStart,
            endDate: lombaEnd,
            startRegistration: registrationStartDate,
            endRegistration: registrationEndDate,
        })

        if (error || !data) {
            return toast.error("Gagal menambahkan informasi tahap pendaftaran")
        }

        if (prerequisites) {
            const promises = prerequisites.map(async (prerequisite, idx) => {
                // untuk membatasi pemanggilan tunggal
                if (idx === 0) {
                    await deletePrerequisite(lombaId)
                }
                await createPrerequisites(prerequisite)
            })
            Promise.all(promises).then((values) => values)
        }

        // /* navigate to the next step */
        setSearchParams({ step: "3", lid: routerState.lombaId }, { state: routerState })
    }

    return (
        <>
            {isOpen && (
                <RegistrationPrerequisiteModal
                    isOpen={isOpen}
                    onOpenChange={setOpen}
                    control={methods.control}
                />
            )}
            {isPreviewPrerequisiteOpen && (
                <PrerequisitePreview
                    isOpen={isPreviewPrerequisiteOpen}
                    onOpenChange={setIsPreviewPrerequisiteOpen}
                    prerequisitesOnDB={prerequisitesOnDB as any}
                />
            )}
            <Form {...methods} onSubmit={onRegistrationSubmit} ref={formRef}>
                <div className="grid grid-cols-3 gap-8">
                    <hr className="col-span-3 border-neutral-100/50" />

                    {/* 👇 registration variant */}
                    <div className="col-span-1 pl-3">
                        <Typography as="h6" className="text-neutral-800" weight={500}>
                            Varian Registrasi
                        </Typography>
                    </div>
                    <div className="col-span-2 pr-3">
                        <Form.RadioField
                            label={false}
                            name="registrationVariant"
                            split
                            options={[
                                { label: "Gratis", value: "free" },
                                { label: "Lomba Berbayar", value: "paid", disabled: true },
                            ]}
                        />
                    </div>

                    <hr className="col-span-3 border-neutral-100/50" />

                    {/* 👇 participant */}
                    <div className="col-span-1 pl-3">
                        <Typography as="h6" className="text-neutral-800" weight={500}>
                            Partisipan
                        </Typography>
                        <Typography size="sm" className="mt-1 max-w-[260px] ">
                            Tentukan banyaknya peserta lomba yang dapat bergabung
                        </Typography>
                    </div>
                    <div className="col-span-2 pr-3">
                        <div className="flex flex-col space-y-4">
                            <Form.RadioField
                                label={false}
                                name="participant"
                                options={[
                                    { label: "Peserta Terbatas", value: "limited" },
                                    { label: "Peserta Tidak Terbatas", value: "unlimited" },
                                ]}
                                split
                            />

                            {methods.watch("participant") === "limited" && (
                                <Form.TextField
                                    name="totalParticipants"
                                    label="Total partisipan"
                                    placeholder="Masukan total partisipan"
                                    type="number"
                                    required
                                />
                            )}
                        </div>
                    </div>

                    <hr className="col-span-3 border-neutral-100/50" />

                    {/* 👇 deadline */}
                    <div className="col-span-1 pl-3">
                        <Typography as="h6" className="text-neutral-800" weight={500}>
                            Deadline
                        </Typography>
                        <Typography size="sm" className="mt-1 max-w-[260px] ">
                            Batas waktu untuk masa registrasi dan pelaksanaan lomba
                        </Typography>
                    </div>
                    <div className="col-span-2 pr-3">
                        <Form.Layout>
                            {/* 👇 registrasi */}
                            <Form.RowLayout column={2}>
                                <Form.TextField
                                    label="Tanggal Mulai Registrasi"
                                    required
                                    name="registrationStartDate"
                                    type="datetime-local"
                                />
                                <Form.TextField
                                    label="Tanggal Selesai Registrasi"
                                    required
                                    name="registrationEndDate"
                                    type="datetime-local"
                                />
                            </Form.RowLayout>

                            {/* 👇 lomba dimulai */}
                            <Form.RowLayout column={2}>
                                <Form.TextField
                                    label="Perlombaan Dimulai"
                                    required
                                    name="lombaStart"
                                    type="datetime-local"
                                />
                                <Form.TextField
                                    label="Perlombaan Selesai"
                                    required
                                    name="lombaEnd"
                                    type="datetime-local"
                                />
                            </Form.RowLayout>
                        </Form.Layout>
                    </div>

                    <hr className="col-span-3 border-neutral-100/50" />

                    {/* 👇 prerequisite */}
                    <div className="col-span-1 pl-3">
                        <Typography as="h6" className="text-neutral-800" weight={500}>
                            Prasyarat <span className="text-xs">(opsional)</span>
                        </Typography>
                        <Typography size="sm" className="mt-1 max-w-[260px] ">
                            Tambahkan persyaratan untuk pendaftar sebelum begabung dalam perlombaan
                        </Typography>
                    </div>

                    <div className="col-span-2 pr-3">
                        <div className="flex flex-col space-y-4">
                            {!isEmpty(methods.watch("prerequisites")) &&
                                methods.watch("prerequisites") &&
                                methods
                                    .watch("prerequisites")
                                    ?.map((p, idx) => (
                                        <PrerequisiteItem
                                            key={"prerequisite-" + idx}
                                            prerequisite={p}
                                            index={idx}
                                        />
                                    ))}
                            <div className="flex items-center space-x-4">
                                <Button
                                    label="Buat dan tambahkan prasyarat baru"
                                    startIcon="heroicons:plus-small"
                                    intent="bordered"
                                    thin
                                    className="flex-grow justify-start"
                                    onClick={() => setOpen(true)}
                                />
                                <Button
                                    label="Preview prasyarat"
                                    startIcon="heroicons:eye"
                                    intent="bordered"
                                    thin
                                    disabled={isEmpty(prerequisitesOnDB)}
                                    onClick={() => {
                                        console.log(prerequisitesOnDB)
                                        setIsPreviewPrerequisiteOpen(true)
                                    }}
                                />
                            </div>
                        </div>
                    </div>
                </div>

                <div className="mt-8 flex w-full justify-end space-x-3 pr-3">
                    <Button
                        type="button"
                        label="Batalkan"
                        intent="bordered"
                        className="text-neutral-400"
                    />
                    <Button
                        type="submit"
                        label="Simpan dan Lanjutkan"
                        endIcon="heroicons:document-check-20-solid"
                        loading={methods.formState.isSubmitting}
                        disabled={methods.formState.isSubmitting}
                    />
                </div>
            </Form>
        </>
    )
}

export default Registration
