import Menu from "@ui/menu"
import { FunctionComponent, useState } from "react"
import { useFieldArray, useFormContext } from "react-hook-form"

import { Prerequisite } from ".."
import RegistrationPrerequisiteModal from "./modal"

interface Props {
    index: number
    prerequisite: Prerequisite
}
const PrerequisiteItemMenu: FunctionComponent<Props> = ({ index, prerequisite }) => {
    /* react states */
    const [isOpen, setOpen] = useState(false)

    /* hook form */
    const { control } = useFormContext()
    const { remove } = useFieldArray({
        control,
        name: "prerequisites",
    })

    return (
        <>
            {isOpen && (
                <RegistrationPrerequisiteModal
                    isOpen={isOpen}
                    onOpenChange={setOpen}
                    control={control as any}
                    prerequisite={prerequisite}
                    index={index}
                />
            )}
            <Menu.Root
                triggerType="iconButton"
                icon="heroicons:ellipsis-vertical"
                contentAlign="end"
            >
                <Menu.Item
                    label="Perbarui"
                    icon="heroicons:pencil-square"
                    onClick={() => setOpen(true)}
                />
                <Menu.Item
                    label="Hapus"
                    icon="heroicons:trash"
                    className="rd-highlighted:bg-danger-50 rd-highlighted:text-danger-600"
                    onClick={() => remove(index)}
                />
            </Menu.Root>
        </>
    )
}

export default PrerequisiteItemMenu
