import PREREQUISITE_PLOTING_TYPE from "@constant/prerequisite.plot"
import Badge from "@ui/badge"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"

import { Prerequisite } from ".."
import PrerequisiteItemMenu from "./prerequisite-item-menu"

interface Props {
    prerequisite: Prerequisite
    index: number
}

const PrerequisiteItem: FunctionComponent<Props> = ({ prerequisite, index }) => {
    const { name, variant, description, isRequired } = prerequisite

    return (
        <div className="flex flex-col space-y-3 rounded-md border border-neutral-200 p-3">
            <div className="flex items-center justify-between">
                <Badge
                    size="md"
                    intent="primary-low"
                    label={PREREQUISITE_PLOTING_TYPE.get(variant)?.label || "error"}
                    icon={PREREQUISITE_PLOTING_TYPE.get(variant)?.icon || "ph:placeholder-thin"}
                />
                <PrerequisiteItemMenu index={index} prerequisite={prerequisite} />
            </div>

            <div className="flex flex-col space-y-1">
                <Typography as="span" weight={500}>
                    {name}
                </Typography>
                <Typography size="sm" className="max-w-[80%] line-clamp-4">
                    {description}
                </Typography>
            </div>
            <hr className="border-neutral-100" />
            <Typography size="sm">
                Prasyarat bersifat {isRequired === "1" ? "wajib" : "opsional"}
            </Typography>
        </div>
    )
}

export default PrerequisiteItem
