import Button from "@ui/button"
import Form from "@ui/form"
import Modal from "@ui/modal"
import { FunctionComponent } from "react"
import { type Control, SubmitHandler, useFieldArray, useForm } from "react-hook-form"
import toast from "react-hot-toast"
import { v4 as uuidV4 } from "uuid"

import { FormFields, Prerequisite } from ".."

const PREREQUISITE_ID = uuidV4()

type PrerequisiteFields = NonNullable<FormFields["prerequisites"]>[0]

interface Props {
    isOpen: boolean
    onOpenChange(val: boolean): void
    control: Control<FormFields>
    prerequisite?: Prerequisite
    index?: number
}

const RegistrationPrerequisiteModal: FunctionComponent<Props> = ({
    isOpen,
    onOpenChange,
    control,
    prerequisite,
    index = null,
}) => {
    /* hook form */
    const methods = useForm<PrerequisiteFields>({
        mode: "onChange",
        defaultValues: prerequisite && index !== null ? { ...prerequisite } : undefined,
    })
    const { append, update } = useFieldArray({
        control,
        name: "prerequisites",
    })

    /* append fields handler */
    const onPrerequisiteAdded: SubmitHandler<PrerequisiteFields> = (fields) => {
        /* validator */
        const fieldKeys = Object.keys(fields) as [keyof typeof fields]
        for (const k of fieldKeys) {
            if (["isRequired", "id"].includes(k)) continue

            if (!fields[k]) {
                toast.error("Beberapa kolom masih kosong")
                return
            }
        }

        /* add to array of prerequisite */
        if (index !== null && prerequisite) {
            update(index, { ...fields, id: PREREQUISITE_ID })
        } else {
            append({ ...fields, id: PREREQUISITE_ID })
        }
        onOpenChange(false)
    }

    return (
        <Modal open={isOpen} onOpenChange={onOpenChange} title="Buat Prasyarat Lomba">
            <Form {...methods} onSubmit={onPrerequisiteAdded}>
                <Form.Layout>
                    <Form.TextField
                        name="name"
                        label="Nama Persyaratan"
                        required
                        placeholder="Masukan nama persyaratan"
                    />
                    <Form.TextareaField
                        name="description"
                        label="Deskripsi Persyaratan"
                        placeholder="Jelaskan mengenai persyaratan tersebut"
                        maxLength={256}
                        hasCount
                    />
                    <Form.SelectField
                        name="variant"
                        label="Varian Jawaban"
                        required
                        options={[
                            { label: "File", value: "File" },
                            { label: "Paragraf", value: "Paragraf" },
                            { label: "Teks Singkat", value: "Teks Singkat" },
                        ]}
                    />
                    <Form.RadioField
                        defaultValue="0"
                        name="isRequired"
                        label={false}
                        options={[
                            { label: "Prasyarat bersifat wajib diisi", value: "1" },
                            { label: "Prasyarat bersifat opsional", value: "0" },
                        ]}
                        split
                    />
                </Form.Layout>
                <Button type="submit" className="mt-6 w-full" label="Konfirmasi Prasyarat" />
            </Form>
        </Modal>
    )
}

export default RegistrationPrerequisiteModal
