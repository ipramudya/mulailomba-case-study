import { zodResolver } from "@hookform/resolvers/zod"
import useGetOrganizerLomba from "@organizerService/lomba/use-get-lomba-by-id"
import useUpsertLomba from "@organizerService/lomba/use-upsert-lomba"
import Button from "@ui/button"
import Form from "@ui/form"
import Typography from "@ui/typography"
import { isEmpty } from "lodash-es"
import { FunctionComponent, useEffect } from "react"
import { SubmitHandler, useForm } from "react-hook-form"
import toast from "react-hot-toast"
import { useLocation, useSearchParams } from "react-router-dom"
import { z } from "zod"

const formSchema = z.object({
    description: z.string().min(1, "*tidak boleh kosong"),
    rules: z.string().min(1, "*tidak boleh kosong"),
})
type FormFields = z.infer<typeof formSchema>

const DescAndRules: FunctionComponent = () => {
    /* router stuffs */
    const [searchParams, setSearchParams] = useSearchParams()
    const { state: routerState } = useLocation()
    const lombaId = searchParams.get("lid") as string

    /* API calls */
    const { upsertLomba } = useUpsertLomba(lombaId)
    const { data } = useGetOrganizerLomba(lombaId)

    const methods = useForm<FormFields>({
        mode: "onChange",
        resolver: zodResolver(formSchema),
    })

    useEffect(() => {
        if (data && !isEmpty(data)) {
            methods.setValue("description", data.description as string)
            methods.setValue("rules", data.rules as string)
        }
    }, [data, methods])

    const onDescAndRulesSubmit: SubmitHandler<FormFields> = async (fields) => {
        const { data: upsertLombaData, error } = await upsertLomba(fields)

        if (error || !upsertLombaData) {
            return toast.error("Gagal menambahkan informasi tahap deskripsi dan aturan")
        }

        /* navigate to the next step */
        setSearchParams({ step: "4", lid: routerState.lombaId }, { state: routerState })
    }

    return (
        <Form {...methods} onSubmit={methods.handleSubmit(onDescAndRulesSubmit)}>
            <div className="grid grid-cols-3 gap-8">
                <hr className="col-span-3 border-neutral-100/50" />

                {/* 👇 description */}
                <div className="col-span-1 pl-3">
                    <Typography as="h6" className="text-neutral-800" weight={500}>
                        Deskripsi
                    </Typography>
                </div>
                <div className="col-span-2 pr-3">
                    <Form.TextareaField
                        name="description"
                        label={false}
                        placeholder="Deksripsikan poin penting perlombaan yang akan diadakan"
                        maxLength={1000}
                        hasCount
                        classNames={{ input: "min-h-[120px] resize-y" }}
                    />
                </div>

                <hr className="col-span-3 border-neutral-100/50" />

                {/* 👇 rules */}
                <div className="col-span-1 pl-3">
                    <Typography as="h6" className="text-neutral-800" weight={500}>
                        Aturan
                    </Typography>
                </div>
                <div className="col-span-2 pr-3">
                    <Form.TextareaField
                        name="rules"
                        label={false}
                        placeholder="Jelaskan aturan perlombaan yang akan diadakan"
                        maxLength={1000}
                        hasCount
                        classNames={{ input: "min-h-[120px] resize-y" }}
                    />
                </div>
            </div>

            <div className="mt-8 flex w-full justify-end space-x-3 pr-3">
                <Button label="Batalkan" intent="bordered" className="text-neutral-400" />
                <Button
                    type="submit"
                    label="Simpan dan Lanjutkan"
                    endIcon="heroicons:document-check-20-solid"
                />
            </div>
        </Form>
    )
}

export default DescAndRules
