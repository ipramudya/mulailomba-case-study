import { Icon } from "@iconify/react"
import Badge from "@ui/badge"
import Form from "@ui/form"
import { isEmpty } from "lodash-es"
import { FunctionComponent } from "react"
import { Control, useFieldArray, useFormContext, useWatch } from "react-hook-form"

interface Props {
    name: string
    control?: Control
    options: {
        label: string
        value: string
    }[]
}

const CreatableMultiSelect: FunctionComponent<Props> = ({ name, options }) => {
    const { getValues, control } = useFormContext()
    const watcher: string[] = useWatch({ name, control })
    const { remove } = useFieldArray({ control, name })

    return (
        <div className="flex flex-col space-y-4">
            {/* 👇 select field */}
            <Form.CreatableSelectField
                label={false}
                name={name}
                placeholder={`Cari atau tambahkan ${name} baru`}
                required
                options={options}
                controlShouldRenderValue={false}
                icon="heroicons:magnifying-glass"
            />

            {/* 👇 selected options */}
            {!isEmpty(watcher) && (
                <div className="flex items-center space-x-2">
                    {watcher.map((benefit, idx) => (
                        <Badge
                            label={benefit}
                            intent="primary-low"
                            size="sm"
                            key={name + "-" + idx}
                            className="flex min-w-[60px] justify-center space-x-2"
                            endElement={
                                <button
                                    type="button"
                                    onClick={() => {
                                        getValues(name).forEach((v: any, idx: number) => {
                                            if (v === benefit) {
                                                remove(idx)
                                            }
                                        })
                                    }}
                                >
                                    <Icon icon="heroicons:x-mark" />
                                </button>
                            }
                        />
                    ))}
                </div>
            )}
        </div>
    )
}

export default CreatableMultiSelect
