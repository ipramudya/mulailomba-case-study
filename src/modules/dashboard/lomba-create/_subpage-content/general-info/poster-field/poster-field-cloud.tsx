import Button from "@ui/button"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"
import { useFormContext } from "react-hook-form"

import { FormFields } from ".."

interface Props {
    poster: string
    name: string
}

const PosterFieldCloud: FunctionComponent<Props> = ({ poster, name }) => {
    /* hook form */
    const { setValue } = useFormContext<FormFields>()

    return (
        <div className="flex items-start justify-between rounded-md border border-neutral-200 p-6 outline-none">
            <div className="flex items-start space-x-6">
                <div className="aspect-[3/4] w-[120px] overflow-hidden rounded ">
                    <img src={poster} className="h-full w-full object-cover" />
                </div>

                <div>
                    <Typography size="xs" className="mb-1">
                        File poster telah terupload
                    </Typography>
                    <Typography size="sm">{name.split("/")[1]}</Typography>
                </div>
            </div>

            <Button
                label="Hapus"
                endIcon="heroicons:x-mark"
                size="sm"
                intent="danger-low"
                pill
                onClick={() => setValue("file", "")}
            />
        </div>
    )
}

export default PosterFieldCloud
