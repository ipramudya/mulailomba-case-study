import PosterCloud from "./poster-field-cloud"
import PosterUpload from "./poster-field-upload"

const PosterField = {
    Upload: PosterUpload,
    Cloud: PosterCloud,
}

export default PosterField
