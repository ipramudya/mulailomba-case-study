import { cva } from "class-variance-authority"

const posterStyles = cva("flex justify-between rounded-md p-6", {
    variants: {
        isUploaded: {
            true: "outline-none border border-neutral-200 items-start",
            false: "outline-dashed outline-2 outline-neutral-100 items-center",
        },
    },
    defaultVariants: {
        isUploaded: false,
    },
})

export default posterStyles
