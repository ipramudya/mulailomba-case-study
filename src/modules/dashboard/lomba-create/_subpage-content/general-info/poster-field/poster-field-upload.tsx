import formatBytes from "@functions/format-bytes"
import useDropImage from "@hooks/use-drop-image"
import { Icon } from "@iconify/react"
import Button from "@ui/button"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"
import { useFormContext } from "react-hook-form"
import { twMerge } from "tailwind-merge"

import { FormFields } from ".."
import posterStyles from "./poster-upload.styles"

const PosterUpload: FunctionComponent = () => {
    /* hook form */
    const { setValue } = useFormContext<FormFields>()

    /* dropzone */
    const { uploaded, onRemove, getRootProps, getInputProps, dropzoneVars } = useDropImage({
        noClick: true,
        onDropAccepted(file) {
            if (file) {
                setValue("file", file[0])
            }
        },
    })

    return (
        <div {...getRootProps()} className={posterStyles({ isUploaded: Boolean(uploaded) })}>
            <input {...getInputProps()} />

            <div className={twMerge("flex space-x-6", !uploaded ? "items-center" : "items-start")}>
                {/* 👇 upload icon */}
                {uploaded ? (
                    <div className="aspect-[3/4] w-[120px] overflow-hidden rounded ">
                        <img src={uploaded.preview} className="h-full w-full object-cover" />
                    </div>
                ) : (
                    <div className="aspect-square rounded bg-neutral-50 p-2">
                        <Icon
                            icon="heroicons:arrow-up-tray-solid"
                            className="h-5 w-5 text-neutral-400"
                        />
                    </div>
                )}

                {/* 👇 file upload descriptions */}
                {uploaded ? (
                    <div>
                        <Typography size="sm" className="mb-2">
                            {uploaded.file.name}
                        </Typography>
                        <Typography size="xs" className="mb-2">
                            Besar file: {formatBytes(uploaded.file.size)}
                        </Typography>
                    </div>
                ) : (
                    <Typography size="sm">
                        Unggah poster untuk mengilustrasikan alur lomba milikmu. <br />
                        Format file yang didukung JPG, PNG, JPEG.
                    </Typography>
                )}
            </div>

            {/* 👇 upload button */}
            <Button
                label={uploaded ? "Hapus" : "Unggah"}
                endIcon={uploaded ? "heroicons:x-mark" : undefined}
                size="sm"
                intent={uploaded ? "danger-low" : "bordered"}
                pill
                onClick={() => (uploaded ? onRemove() : dropzoneVars.open())}
            />
        </div>
    )
}

export default PosterUpload
