import { zodResolver } from "@hookform/resolvers/zod"
import useGetOrganizerLomba from "@organizerService/lomba/use-get-lomba-by-id"
import useUpsertLomba from "@organizerService/lomba/use-upsert-lomba"
import useUpsertPoster from "@organizerService/lomba/use-upsert-poster"
import deletePoster from "@organizerService/poster/delete-poster"
import getPosterPublicURL from "@organizerService/poster/get-poster-public-url"
import useUploadPoster from "@organizerService/poster/use-upload-poster"
import Button from "@ui/button"
import Form from "@ui/form"
import Typography from "@ui/typography"
import { isEmpty } from "lodash-es"
import { FunctionComponent, useEffect } from "react"
import { SubmitHandler, useForm } from "react-hook-form"
import { toast } from "react-hot-toast"
import { useLocation, useSearchParams } from "react-router-dom"
import { z } from "zod"

import CreatableMultiSelect from "./creatable-multi-select"
import PosterField from "./poster-field"

const formSchema = z.object({
    name: z.string().min(6, "*terlalu pendek"),
    category: z.string({ required_error: "*tidak boleh kosong" }).min(1, "*terlalu pendek"),
    file: z.any(),
    benefits: z.array(z.string()),
    eligibilities: z.array(z.string()),
})

export type FormFields = z.infer<typeof formSchema>

const GeneralInfo: FunctionComponent = () => {
    /* router stuffs */
    const [searchParams, setSearchParams] = useSearchParams()
    const { state: routerState } = useLocation()
    const lombaId = searchParams.get("lid") as string

    /* API calls */
    const { uploadPoster } = useUploadPoster()
    const { upsertLomba } = useUpsertLomba(lombaId)
    const { upsertPoster } = useUpsertPoster()
    const { data: lombaOnDB } = useGetOrganizerLomba(lombaId)
    const posterOnStorageDB =
        lombaOnDB && !isEmpty(lombaOnDB) && lombaOnDB.poster_storage_url
            ? getPosterPublicURL(lombaOnDB.poster_storage_url).data.publicUrl
            : null

    /* hook form */
    const methods = useForm<FormFields>({
        mode: "onChange",
        resolver: zodResolver(formSchema),
    })
    const fileFieldWatcher = methods.watch("file")

    /* set default value */
    useEffect(() => {
        if (lombaOnDB && !isEmpty(lombaOnDB)) {
            const { poster_public_url, name, category, benefits, eligibilities } = lombaOnDB

            methods.setValue("file", poster_public_url as string)
            methods.setValue("name", name as string)
            methods.setValue("category", category as string)
            methods.setValue("benefits", benefits as string[])
            methods.setValue("eligibilities", eligibilities as string[])
        }
    }, [lombaOnDB, methods])

    /* event handler */
    const onGeneralInfoSubmit: SubmitHandler<Omit<FormFields, "file"> & { file: File }> = async ({
        category,
        benefits,
        eligibilities,
        file,
        name,
    }) => {
        let uploadedPosterData, uploadedPosterError

        /*  update/insert poster ke dalam database
            -> url tempat poster disimpan atau error
         */
        if (posterOnStorageDB && lombaOnDB && lombaOnDB) {
            const response = await upsertPoster(lombaOnDB.poster_storage_url as string, file)
            uploadedPosterData = response.data
            uploadedPosterError = response.error
        } else {
            const response = await uploadPoster({
                file,
                fileName: file.name.replace(/\s+/g, "_"),
            })
            uploadedPosterData = response.data
            uploadedPosterError = response.error
        }

        if (uploadedPosterError || !uploadedPosterData) {
            return toast.error("Upload poster gagal, silahkan coba kembali")
        }

        /*  update/insert lomba ke dalam database
            -> url data lomba atau error
         */
        const { data: upsertLombaData, error: upsertLombaError } = await upsertLomba({
            posterStorageUrl: uploadedPosterData.path,
            posterPublicUrl: getPosterPublicURL(uploadedPosterData.path).data.publicUrl,
            name,
            category,
            benefits,
            eligibilities,
        })

        if (upsertLombaError || !upsertLombaData) {
            deletePoster(uploadedPosterData.path)
            return toast.error("Gagal menambahkan informasi tahap umum")
        }

        /* navigate to the next step */
        setSearchParams({ step: "2", lid: routerState.lombaId }, { state: routerState })
    }

    return (
        <Form {...methods} onSubmit={methods.handleSubmit(onGeneralInfoSubmit as any)}>
            <div className="grid grid-cols-3 gap-8">
                <hr className="col-span-3 border-neutral-100/50" />

                {/* 👇 upload poster */}
                <div className="col-span-1 pl-3">
                    <Typography as="h6" className="text-neutral-800" weight={500}>
                        Poster Lomba
                    </Typography>
                </div>
                <div className="col-span-2 pr-3">
                    {typeof fileFieldWatcher !== "string" ||
                    !fileFieldWatcher ||
                    !posterOnStorageDB ? (
                        <PosterField.Upload />
                    ) : (
                        <PosterField.Cloud
                            poster={posterOnStorageDB}
                            name={fileFieldWatcher as string}
                        />
                    )}
                </div>

                <hr className="col-span-3 border-neutral-100/50" />

                {/* 👇 lomba information */}
                <div className="col-span-1 pl-3">
                    <Typography as="h6" className="text-neutral-800" weight={500}>
                        Informasi Lomba
                    </Typography>
                </div>
                <div className="col-span-2 pr-3">
                    <div className="flex flex-col space-y-4">
                        <Form.TextField
                            label="Nama Lomba"
                            name="name"
                            placeholder="Masukan nama lomba"
                            required
                        />
                        <Form.SelectField
                            isClearable
                            label="Kategori"
                            name="category"
                            placeholder="Pilih kategori"
                            required
                            options={[
                                { label: "Musik", value: "Musik" },
                                { label: "Pendidikan", value: "Pendidikan" },
                                { label: "Olahraga", value: "Olahraga" },
                                { label: "Sastra dan Bahasa", value: "Sastra dan Bahasa" },
                                { label: "Esport Game", value: "Esport Game" },
                                {
                                    label: "Olimpiade Sains dan Teknologi",
                                    value: "Olimpiade Sains dan teknologi",
                                },
                            ]}
                        />
                    </div>
                </div>

                <hr className="col-span-3 border-neutral-100/50" />

                {/* 👇 benefit */}
                <div className="col-span-1 pl-3">
                    <div>
                        <Typography as="h6" className="text-neutral-800" weight={500}>
                            Benefit
                        </Typography>
                        <Typography size="sm" className="mt-1 max-w-[260px] ">
                            Keuntungan yang akan diperoleh peserta lomba
                        </Typography>
                    </div>
                </div>
                <div className="col-span-2 pr-3">
                    <CreatableMultiSelect
                        name="benefits"
                        options={[
                            { label: "Uang Tunai", value: "Uang Tunai" },
                            { label: "Sertifikat", value: "Sertifikat" },
                        ]}
                    />
                </div>

                <hr className="col-span-3 border-neutral-100/50" />

                {/* 👇 eligibility */}
                <div className="col-span-1 pl-3">
                    <div>
                        <Typography as="h6" className="text-neutral-800" weight={500}>
                            Kelayakan
                        </Typography>
                        <Typography size="sm" className="mt-1 max-w-[260px] ">
                            Tentukan peserta yang dapat mengikuti perlombaan
                        </Typography>
                    </div>
                </div>
                <div className="col-span-2 pr-3">
                    <CreatableMultiSelect
                        name="eligibilities"
                        options={[
                            { label: "Pelajar", value: "Pelajar" },
                            { label: "Umum", value: "Umum" },
                        ]}
                    />
                </div>

                <hr className="col-span-3 border-neutral-100/50" />
            </div>

            <div className="mt-8 flex w-full justify-end space-x-3 pr-3">
                <Button label="Batalkan" intent="bordered" className="text-neutral-400" />
                <Button
                    type="submit"
                    label="Simpan dan Lanjutkan"
                    endIcon="heroicons:document-check-20-solid"
                    loading={methods.formState.isSubmitting}
                    disabled={methods.formState.isSubmitting}
                />
            </div>
        </Form>
    )
}

export default GeneralInfo
