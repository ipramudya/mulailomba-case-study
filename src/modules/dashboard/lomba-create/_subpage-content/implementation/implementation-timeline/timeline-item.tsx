import { formatDate } from "@functions/format-date"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"

import { Timeline } from ".."
import TimelineItemMenu from "./timeline-item-menu"

interface Props {
    timeline: Timeline
    index: number
}

const TimelineItem: FunctionComponent<Props> = ({ timeline, index }) => {
    const { name, description, startDate, endDate } = timeline

    return (
        <div className="flex flex-col space-y-3 rounded-md border border-neutral-200 p-3">
            <div>
                <div className="flex items-center justify-between">
                    <Typography as="span" weight={500}>
                        {name}
                    </Typography>

                    {/* 👇 menu */}
                    <TimelineItemMenu index={index} timeline={timeline} />
                </div>
                <Typography size="sm" className="mt-1 max-w-[80%] line-clamp-4">
                    {description}
                </Typography>
            </div>

            <div className="grid grid-cols-2">
                <div>
                    <Typography size="xs" className="text-neutral-400">
                        Waktu dimulai
                    </Typography>
                    <Typography size="sm">{formatDate(startDate)}</Typography>
                </div>
                <div>
                    <Typography size="xs" className="text-neutral-400">
                        Waktu berakhir
                    </Typography>
                    <Typography size="sm">{formatDate(endDate)}</Typography>
                </div>
            </div>
        </div>
    )
}

export default TimelineItem
