import useGetOrganizerLomba from "@organizerService/lomba/use-get-lomba-by-id"
import Button from "@ui/button"
import Form from "@ui/form"
import Modal from "@ui/modal"
import { isEmpty } from "lodash-es"
import { FunctionComponent } from "react"
import { Control, SubmitHandler, useFieldArray, useForm } from "react-hook-form"
import { toast } from "react-hot-toast"
import { useSearchParams } from "react-router-dom"

import { FormFields, Timeline } from ".."

type TimelineFields = NonNullable<FormFields["timelines"]>[0]

interface Props {
    isOpen: boolean
    onOpenChange(val: boolean): void
    control: Control<FormFields>
    timeline?: Timeline
    index?: number
}

const TimelineModal: FunctionComponent<Props> = ({
    isOpen,
    onOpenChange,
    control,
    timeline,
    index = null,
}) => {
    const [searchParams] = useSearchParams()
    const lombaId = searchParams.get("lid") as string

    /* API calls */
    const { data: lombasOnDB } = useGetOrganizerLomba(lombaId)

    /* hook form */
    const methods = useForm<TimelineFields>({
        defaultValues: timeline && index !== null ? { ...timeline } : undefined,
    })
    const { append, update } = useFieldArray({
        control,
        name: "timelines",
    })

    /* append fields handler */
    const onTimelineAdded: SubmitHandler<TimelineFields> = (fields) => {
        console.log("fields", fields)
        if (!lombasOnDB || isEmpty(lombasOnDB) || !(lombasOnDB.start_date && lombasOnDB.end_date)) {
            return toast.error("Atur deadline lomba terlebih dahulu pada tab pendaftaran")
        }

        if (index !== null && timeline) {
            update(index, { ...fields })
        } else {
            append({ ...fields })
        }
        onOpenChange(false)
    }

    return (
        <Modal open={isOpen} onOpenChange={onOpenChange} title="Atur Timeline Pelaksanaan">
            <Form {...methods} onSubmit={methods.handleSubmit(onTimelineAdded)}>
                <Form.Layout>
                    <Form.TextField
                        name="name"
                        label="Nama Timeline"
                        required
                        placeholder="Masukan nama timeline"
                    />
                    <Form.TextareaField
                        name="description"
                        label="Deskripsi Timeline"
                        required
                        placeholder="Jelaskan mengenai timeline yang dibuat"
                        maxLength={256}
                        hasCount
                    />
                    <Form.RowLayout column={2}>
                        <Form.TextField
                            label="Waktu dimulai"
                            name="startDate"
                            required
                            type="datetime-local"
                        />
                        <Form.TextField
                            label="Waktu berakhir"
                            name="endDate"
                            required
                            type="datetime-local"
                        />
                    </Form.RowLayout>
                </Form.Layout>
                <Button type="submit" className="mt-6 w-full" label="Buat Timeline" />
            </Form>
        </Modal>
    )
}

export default TimelineModal
