import Menu from "@ui/menu"
import { FunctionComponent, useState } from "react"
import { useFieldArray, useFormContext } from "react-hook-form"

import { Timeline } from ".."
import TimelineModal from "./timeline-modal"

interface Props {
    index: number
    timeline: Timeline
}

const TimelineItemMenu: FunctionComponent<Props> = ({ timeline, index }) => {
    /* react states */
    const [isOpen, setOpen] = useState(false)

    /* hook form */
    const { control } = useFormContext()
    const { remove } = useFieldArray({
        control,
        name: "timelines",
    })

    return (
        <>
            {isOpen && (
                <TimelineModal
                    isOpen={isOpen}
                    onOpenChange={setOpen}
                    control={control as any}
                    timeline={timeline}
                    index={index}
                />
            )}
            <Menu.Root
                triggerType="iconButton"
                icon="heroicons:ellipsis-vertical"
                contentAlign="end"
            >
                <Menu.Item
                    label="Perbarui"
                    icon="heroicons:pencil-square"
                    onClick={() => setOpen(true)}
                />
                <Menu.Item
                    label="Hapus"
                    icon="heroicons:trash"
                    className="rd-highlighted:bg-danger-50 rd-highlighted:text-danger-600"
                    onClick={() => remove(index)}
                />
            </Menu.Root>
        </>
    )
}

export default TimelineItemMenu
