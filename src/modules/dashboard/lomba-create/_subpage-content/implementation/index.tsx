import plotToBoolean from "@functions/plot-to-boolean"
import { zodResolver } from "@hookform/resolvers/zod"
import useCreateTimeline from "@organizerService/lomba/timelines-lomba/use-create-timeline"
import useDeleteAllTimelinesLomba from "@organizerService/lomba/timelines-lomba/use-delete-all-timelines-lomba"
import useGetAllTimelinesLomba from "@organizerService/lomba/timelines-lomba/use-get-all-timelines-lomba"
import useGetOrganizerLomba from "@organizerService/lomba/use-get-lomba-by-id"
import useUpsertLomba from "@organizerService/lomba/use-upsert-lomba"
import Button from "@ui/button"
import Form from "@ui/form"
import Typography from "@ui/typography"
import { isEmpty } from "lodash-es"
import { FunctionComponent, useEffect, useMemo, useRef, useState } from "react"
import { SubmitHandler, useForm } from "react-hook-form"
import toast from "react-hot-toast"
import { useLocation, useNavigate, useSearchParams } from "react-router-dom"
import { z } from "zod"

import TimelineItem from "./implementation-timeline/timeline-item"
import TimelineModal from "./implementation-timeline/timeline-modal"

const formTimelineSchema = z.object({
    name: z.string().min(1, "*tidak boleh kosong"),
    description: z.string().min(1, "*tidak boleh kosong"),
    startDate: z.string().min(1, "*tidak boleh kosong"),
    endDate: z.string().min(1, "*tidak boleh kosong"),
})
export type Timeline = z.infer<typeof formTimelineSchema>

const formSchema = z.object({
    heldType: z.enum(["online", "offline"], { required_error: "*tidak boleh kosong" }),
    meetingLink: z.any(),
    location: z.any(),
    timelines: z.array(formTimelineSchema).optional(),
})

export type FormFields = z.infer<typeof formSchema>

const Implementation: FunctionComponent = () => {
    /* react states */
    const [isOpen, setOpen] = useState(false)
    const formRef = useRef()

    /* router stuffs */
    const navigate = useNavigate()
    const [searchParams, setSearchParams] = useSearchParams()
    const { state: routerState } = useLocation()
    const lombaId = searchParams.get("lid") as string

    /* API calls */
    const { upsertLomba } = useUpsertLomba(lombaId)
    const { deleteTimeline } = useDeleteAllTimelinesLomba()
    const { createTimeline } = useCreateTimeline(lombaId)
    const { data: lombasOnDB } = useGetOrganizerLomba(lombaId)
    const { data: timelinesOnDB } = useGetAllTimelinesLomba(lombaId)

    /* hook form */
    const methods = useForm<FormFields>({
        mode: "onChange",
        resolver: zodResolver(formSchema),
    })

    const navigateUsingStep = (step: string) => {
        setSearchParams({ step, lid: routerState.lombaId }, { state: routerState })
    }

    const checkBeforeNavigateHome = () => {
        if (lombasOnDB && !isEmpty(lombasOnDB)) {
            const lombaOnDB = lombasOnDB
            if (!lombaOnDB.name) {
                navigateUsingStep("1")
            } else if (!lombaOnDB.start_date) {
                navigateUsingStep("2")
            } else if (!lombaOnDB.description) {
                navigateUsingStep("3")
            } else {
                navigate("..", { relative: "path" })
            }
        }
    }

    /* event handler */
    const onImplementationSubmit: SubmitHandler<FormFields> = async (
        { heldType, location, meetingLink, timelines },
        event
    ) => {
        if ((event && event.target) !== formRef.current) return

        const { data, error } = await upsertLomba({
            isHeldOnline: Boolean(heldType === "online"),
            heldLocation: heldType === "offline" ? location : null,
            heldMeetingLink: heldType === "online" ? meetingLink : null,
        })

        if (error || !data) return toast.error("Gagal menambahkan informasi tahap pelaksanaan")

        if (timelines) {
            const promises = timelines.map(async (timeline, idx) => {
                // untuk membatasi pemanggilan tunggal
                if (idx === 0) {
                    await deleteTimeline(lombaId)
                }
                await createTimeline(timeline)
            })
            Promise.all(promises).then((values) => values)
        }

        checkBeforeNavigateHome()
    }

    const remapProperTimeline = useMemo(() => {
        if (timelinesOnDB && !isEmpty(timelinesOnDB)) {
            return timelinesOnDB.map(({ start_date, end_date, name, description }) => ({
                startDate: start_date as string,
                endDate: end_date as string,
                name: name as string,
                description: description as string,
            }))
        }

        return undefined
    }, [timelinesOnDB])

    useEffect(() => {
        if (lombasOnDB && !isEmpty(lombasOnDB)) {
            const { is_held_online, held_location, held_meeting_link } = lombasOnDB

            methods.setValue(
                "heldType",
                plotToBoolean({
                    considered: is_held_online,
                    truthy: "online",
                    falsy: "offline",
                }) as any
            )
            methods.setValue("meetingLink", held_meeting_link as string)
            methods.setValue("location", held_location as string)
            methods.setValue("timelines", remapProperTimeline)
        }
    }, [lombasOnDB, methods, remapProperTimeline, timelinesOnDB])

    return (
        <>
            {isOpen && (
                <TimelineModal isOpen={isOpen} onOpenChange={setOpen} control={methods.control} />
            )}
            <Form {...methods} onSubmit={onImplementationSubmit} ref={formRef}>
                <div className="grid grid-cols-3 gap-8">
                    <hr className="col-span-3 border-neutral-100/50" />

                    {/* 👇 held type */}
                    <div className="col-span-1 pl-3">
                        <Typography as="h6" className="text-neutral-800" weight={500}>
                            Tipe Pelaksanaan
                        </Typography>
                        <Typography size="sm" className="mt-1 max-w-[260px] ">
                            Atur bagaimana lomba akan diselenggarakan (daring / luring)
                        </Typography>
                    </div>
                    <div className="col-span-2 pr-3">
                        <div className="flex flex-col space-y-4">
                            <Form.RadioField
                                label={false}
                                name="heldType"
                                options={[
                                    { label: "Tatap Muka", value: "offline" },
                                    { label: "Daring", value: "online" },
                                ]}
                                split
                            />

                            {methods.watch("heldType") === "online" && (
                                <Form.TextField
                                    name="meetingLink"
                                    label="Link meeting"
                                    placeholder="Contoh: https://www.meeting.com/foo-bar-baz"
                                    required
                                />
                            )}

                            {methods.watch("heldType") === "offline" && (
                                <Form.TextareaField
                                    name="location"
                                    label="Lokasi Penyelenggaraan"
                                    placeholder="Masukan lokasi penyelenggaraan lomba"
                                    required
                                />
                            )}
                        </div>
                    </div>

                    <hr className="col-span-3 border-neutral-100/50" />
                    {/* 👇 timelines */}
                    <div className="col-span-1 pl-3">
                        <Typography as="h6" className="text-neutral-800" weight={500}>
                            Timeline Lomba <span className="text-xs">(opsional)</span>
                        </Typography>
                        <Typography size="sm" className="mt-1 max-w-[260px] ">
                            Pecah informasi pelaksanaan lomba berupa timeline agar memudahkan
                            pendaftar
                        </Typography>
                    </div>

                    <div className="col-span-2 pr-3">
                        <div className="flex flex-col space-y-4">
                            {!isEmpty(methods.watch("timelines")) &&
                                methods.watch("timelines") &&
                                methods
                                    .watch("timelines")
                                    ?.map((t, idx) => (
                                        <TimelineItem
                                            key={"timeline-" + idx}
                                            timeline={t}
                                            index={idx}
                                        />
                                    ))}
                            <Button
                                label="Buat dan tambahkan timeline baru"
                                startIcon="heroicons:plus-small"
                                intent="bordered"
                                thin
                                className="flex-grow justify-start"
                                onClick={() => setOpen(true)}
                            />
                        </div>
                    </div>
                </div>

                <div className="mt-8 flex w-full justify-end space-x-3 pr-3">
                    <Button label="Batalkan" intent="bordered" className="text-neutral-400" />
                    <Button
                        type="submit"
                        label="Selesaikan"
                        endIcon="heroicons:document-check-20-solid"
                        loading={methods.formState.isLoading}
                    />
                </div>
            </Form>
        </>
    )
}

export default Implementation
