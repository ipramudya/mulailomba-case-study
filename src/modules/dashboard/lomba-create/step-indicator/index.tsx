import { Icon } from "@iconify/react"
import useGetOrganizerLomba from "@organizerService/lomba/use-get-lomba-by-id"
import Typography from "@ui/typography"
import { isEmpty } from "lodash-es"
import { FunctionComponent } from "react"
import { useLocation, useSearchParams } from "react-router-dom"
import { twMerge } from "tailwind-merge"

import STEP_INDICATOR_STYLES from "./step-indicator.styles"

interface Props {
    stepNumber: number
    label: string
    identifier: "name" | "start_date" | "description" | "is_held_online"
}

const StepIndicator: FunctionComponent<Props> = ({ stepNumber, label, identifier }) => {
    /* router stuffs */
    const [searchParams, setSearchParams] = useSearchParams()
    const { state: routerState } = useLocation()
    const lombaId = searchParams.get("lid") as string
    const stepFromParams = Number(searchParams.get("step"))

    /* API calls */
    const { data } = useGetOrganizerLomba(lombaId)

    const getStatus = () => {
        let changeableStatus

        if (data && !isEmpty(data) && data[identifier] !== null) {
            changeableStatus = "done"
        } else if (stepFromParams === stepNumber) {
            changeableStatus = "active"
        } else {
            changeableStatus = "undone"
        }

        return changeableStatus
    }

    return (
        <div
            className="flex cursor-pointer items-center space-x-2"
            onClick={() =>
                setSearchParams(
                    { step: stepNumber.toString(), lid: routerState.lombaId },
                    { state: routerState }
                )
            }
        >
            <div
                className={twMerge(
                    STEP_INDICATOR_STYLES.get(getStatus())?.badgeNumber,
                    "flex aspect-square w-6 items-center justify-center rounded-full"
                )}
            >
                {getStatus() === "done" ? (
                    <Icon icon="heroicons:check" />
                ) : (
                    <Typography as="span" weight={500} size="xs" className="text-inherit">
                        {stepNumber}
                    </Typography>
                )}
            </div>
            <Typography
                as="span"
                weight={getStatus() === "active" ? 500 : 400}
                size="sm"
                className={STEP_INDICATOR_STYLES.get(getStatus())?.label}
            >
                {label}
            </Typography>
        </div>
    )
}

export default StepIndicator
