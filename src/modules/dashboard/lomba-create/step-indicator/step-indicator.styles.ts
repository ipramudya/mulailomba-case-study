const STEP_INDICATOR_STYLES = new Map([
    [
        "done",
        {
            badgeNumber: "bg-primary-500 text-white",
            label: "text-primary-700",
        },
    ],
    [
        "active",
        {
            badgeNumber: "bg-primary-50 text-primary-600",
            label: "text-primary-600",
        },
    ],
    [
        "undone",
        {
            badgeNumber: "bg-neutral-100 text-neutral-300",
            label: "text-neutral-300",
        },
    ],
])

export default STEP_INDICATOR_STYLES
