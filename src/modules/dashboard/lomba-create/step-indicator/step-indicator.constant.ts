const stepIndicatorConstant = [
    { stepNumber: 1, label: "Informasi Umum", identifier: "name" },
    { stepNumber: 2, label: "Pendaftaran", identifier: "start_date" },
    { stepNumber: 3, label: "Deskripsi dan Aturan", identifier: "description" },
    { stepNumber: 4, label: "Pelaksanaan", identifier: "is_held_online" },
] as const

export default stepIndicatorConstant
