import Typography from "@ui/typography"
import { FunctionComponent } from "react"

import ResetPasswordForm from "./reset-password-form"

const ResetPassword: FunctionComponent = () => {
    return (
        <div className="flex w-full max-w-[420px] flex-col space-y-6">
            <Typography as="h2" size="h2" weight={700} className="text-center">
                Atur Ulang Kata Sandi Organizer
            </Typography>

            <ResetPasswordForm />
        </div>
    )
}

export default ResetPassword
