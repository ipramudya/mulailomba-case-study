import { zodResolver } from "@hookform/resolvers/zod"
import useRegisterOrganizer from "@organizerService/user/auth/use-register-organizer"
import { AuthError } from "@supabase/supabase-js"
import Button from "@ui/button"
import Form from "@ui/form"
import { FunctionComponent } from "react"
import { SubmitHandler, useForm } from "react-hook-form"
import toast from "react-hot-toast"
import { useNavigate } from "react-router-dom"
import { z } from "zod"

const formSchema = z
    .object({
        name: z.string().min(6, "*nama tidak boleh kurang dari 6 karakter"),
        email: z.string().email("*email tidak valid"),
        password: z.string().min(6, "*terlalu pendek"),
        confirmPassword: z.string().min(6, "*terlalu pendek"),
    })
    .superRefine((schema, ctx) => {
        if (schema.password !== schema.confirmPassword) {
            ctx.addIssue({
                code: z.ZodIssueCode.custom,
                path: ["confirmPassword"],
                message: "*tidak sesuai",
            })
        }
    })
type FormFields = z.infer<typeof formSchema>

const RegisterForm: FunctionComponent = () => {
    /* api definitions */
    const organizerRegister = useRegisterOrganizer()

    /* router stuff */
    const navigate = useNavigate()

    /* 👇 form things */
    const methods = useForm<FormFields>({
        resolver: zodResolver(formSchema),
    })
    const onRegisterSubmit: SubmitHandler<FormFields> = async (fields) => {
        const { data, error } = await organizerRegister(fields)
        if (error) {
            if (error instanceof AuthError) {
                toast.error(
                    "Gagal melakukan proses registrasi akun organizer, server sedang tidak stabil"
                )
            } else {
                toast.error(error)
            }
        } else if (data && data.user) {
            return navigate("/dashboard/home", { replace: true })
        }
    }

    return (
        <Form {...methods} onSubmit={methods.handleSubmit(onRegisterSubmit)}>
            <Form.Layout>
                <Form.TextField
                    name="name"
                    label="Nama"
                    required
                    placeholder="Masukan nama lengkap kamu"
                />
                <Form.TextField
                    type="email"
                    name="email"
                    label="Email"
                    required
                    placeholder="Masukan email"
                />
                <Form.RowLayout column={2}>
                    <Form.TextField
                        type="password"
                        name="password"
                        label="Kata sandi"
                        required
                        placeholder="Masukan kata sandi"
                    />
                    <Form.TextField
                        type="password"
                        name="confirmPassword"
                        label="Konfirmasi kata sandi"
                        required
                        placeholder="Konfirmasi kata sandi"
                    />
                </Form.RowLayout>
                <Button
                    type="submit"
                    label="Buat MulaiLomba Organizer"
                    className="min-h-[40px] w-full bg-primary-800"
                    loading={methods.formState.isSubmitting}
                />
            </Form.Layout>
        </Form>
    )
}

export default RegisterForm
