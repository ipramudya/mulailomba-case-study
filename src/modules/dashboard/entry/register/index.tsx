import Typography from "@ui/typography"
import { FunctionComponent } from "react"

import RegisterForm from "./register-form"

const Register: FunctionComponent = () => {
    return (
        <div className="flex w-full max-w-[460px] flex-col space-y-6">
            <Typography as="h2" size="h2" weight={700} className="text-center">
                Gabung dan Kelola Organizer Kreatif Milikmu
            </Typography>

            <RegisterForm />
        </div>
    )
}

export default Register
