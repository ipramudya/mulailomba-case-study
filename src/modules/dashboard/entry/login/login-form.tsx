import { zodResolver } from "@hookform/resolvers/zod"
import useLoginOrganizer from "@organizerService/user/auth/use-login-organizer"
import { AuthError } from "@supabase/supabase-js"
import Button from "@ui/button"
import Form from "@ui/form"
import { FunctionComponent } from "react"
import { SubmitHandler, useForm } from "react-hook-form"
import { toast } from "react-hot-toast"
import { useNavigate } from "react-router-dom"
import { z } from "zod"

const formSchema = z.object({
    email: z.string().email("*email tidak valid"),
    password: z.string().min(6, "*kata sandi terlalu pendek"),
})
type FormFields = z.infer<typeof formSchema>

const LoginForm: FunctionComponent = () => {
    /* api definitions */
    const login = useLoginOrganizer()

    /* router stuff */
    const navigate = useNavigate()

    /* 👇 form things */
    const methods = useForm<FormFields>({
        resolver: zodResolver(formSchema),
    })
    const onLoginSubmit: SubmitHandler<FormFields> = async (fields) => {
        const { data, error } = await login(fields)

        if (error) {
            if (error instanceof AuthError) {
                toast.error("Email atau password anda salah")
            } else {
                toast.error(error)
            }
        } else if (data && data.user) {
            navigate("/dashboard/home", { replace: true })
        }
    }

    return (
        <Form {...methods} onSubmit={onLoginSubmit}>
            <Form.Layout>
                <Form.TextField name="email" label="Email" required placeholder="Masukan email" />
                <Form.TextField
                    name="password"
                    type="password"
                    label="Kata sandi"
                    required
                    placeholder="Masukan kata sandi"
                />
                <Button
                    type="submit"
                    label="Masuk sebagai Organizer"
                    className="min-h-[40px] w-full bg-primary-800"
                    disabled={methods.formState.isSubmitting}
                    loading={methods.formState.isSubmitting}
                />
            </Form.Layout>
        </Form>
    )
}

export default LoginForm
