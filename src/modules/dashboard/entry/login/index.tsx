import Typography from "@ui/typography"
import { FunctionComponent, useState } from "react"

import ConfirmationResetPasswordModal from "./confirmation-reset-password-modal"
import LoginForm from "./login-form"

const Login: FunctionComponent = () => {
    const [isConfirmationOpen, setConfirmationOpen] = useState(false)

    return (
        <div className="flex w-full max-w-[420px] flex-col space-y-6">
            <Typography as="h2" size="h2" weight={700} className="text-center">
                Selamat Datang <br /> Penyelenggara Kreatif
            </Typography>

            <LoginForm />
            <hr className="border-neutral-100" />

            {isConfirmationOpen && (
                <ConfirmationResetPasswordModal
                    isOpen={isConfirmationOpen}
                    onOpenChange={setConfirmationOpen}
                />
            )}

            <Typography
                size="sm"
                className="cursor-pointer text-center text-primary-500"
                onClick={() => setConfirmationOpen(true)}
            >
                Lupa kata sandi ?
            </Typography>
        </div>
    )
}

export default Login
