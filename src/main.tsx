import ReactQueryProvider from "@lib/react-query/react-query-provider"
import CustomToaster from "@lib/toaster"
import Feedback from "@utils/feedback"
import router from "@utils/router"
import React from "react"
import ReactDOM from "react-dom/client"
import { HelmetProvider } from "react-helmet-async"
import { SkeletonTheme } from "react-loading-skeleton"
import "react-loading-skeleton/dist/skeleton.css"
import { RouterProvider } from "react-router-dom"

// static assets
import "./assets/fonts/Inter-Bold.ttf"
import "./assets/fonts/Inter-ExtraBold.ttf"
import "./assets/fonts/Inter-Medium.ttf"
import "./assets/fonts/Inter-Regular.ttf"
import "./assets/fonts/Inter-SemiBold.ttf"
import "./assets/styles/index.css"

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
    <React.StrictMode>
        <HelmetProvider>
            <ReactQueryProvider>
                <SkeletonTheme baseColor="#e2e2e2" highlightColor="#F8F9F9" borderRadius="6px">
                    <CustomToaster />
                    <Feedback />
                    <RouterProvider router={router} />
                </SkeletonTheme>
            </ReactQueryProvider>
        </HelmetProvider>
    </React.StrictMode>
)
