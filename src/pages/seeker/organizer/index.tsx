import SeekerHeader from "@modules/seeker/_components/seeker-header"
import Nav, { HEADER_HEIGHT } from "@ui/nav"
import { FunctionComponent } from "react"
import { Helmet } from "react-helmet-async"
import { Outlet, useLocation } from "react-router-dom"

const OrganizerPage: FunctionComponent = () => {
    /* router stuffs */
    const { pathname } = useLocation()

    return (
        <>
            <Helmet>
                <title>MulaiLomba - Explore Organizer Menarik</title>
                <meta
                    name="description"
                    content="Temukan berbagai organizer favorit kamu untuk berpartisipasi pada lomba impianmu"
                />
            </Helmet>
            {pathname.includes("explore") ? (
                <div className="flex w-full flex-col">
                    {/* 👇 header */}
                    <Nav intent="light">
                        <SeekerHeader variant="primary" />
                    </Nav>

                    {/* 👇 main content -- could be explore or detail page */}
                    <div style={{ marginTop: HEADER_HEIGHT }}>
                        <Outlet />
                    </div>
                </div>
            ) : (
                <div className="flex w-full flex-col">
                    <div className="min-h-[196px] bg-primary-500">
                        <Nav intent="primary">
                            <SeekerHeader variant="secondary" />
                        </Nav>
                    </div>
                    <Outlet />
                </div>
            )}
        </>
    )
}

export default OrganizerPage
