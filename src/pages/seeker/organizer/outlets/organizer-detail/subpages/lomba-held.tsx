import LombaFilter from "@modules/seeker/organizer/detail/_subpages-content/lomba-held/lomba-filter"
import LombaGrids from "@modules/seeker/organizer/detail/_subpages-content/lomba-held/lomba-grids"
import { FunctionComponent } from "react"

const OrganizerDetailLombaHeldSubpages: FunctionComponent = () => {
    return (
        <div className="flex flex-col space-y-6">
            <LombaFilter />
            <LombaGrids />
        </div>
    )
}

export default OrganizerDetailLombaHeldSubpages
