import DescriptionAside from "@modules/seeker/organizer/detail/_subpages-content/description/description-aside"
import DescriptionPaper from "@modules/seeker/organizer/detail/_subpages-content/description/description-paper"
import { FunctionComponent } from "react"

const OrganizerDetailDescriptionSubpages: FunctionComponent = () => {
    return (
        <div className="flex space-x-[5rem]">
            {/* 👇 description */}
            <div className="w-full">
                <DescriptionPaper />
            </div>

            {/* 👇 side info */}
            <aside className="w-full max-w-[260px]">
                <DescriptionAside />
            </aside>
        </div>
    )
}

export default OrganizerDetailDescriptionSubpages
