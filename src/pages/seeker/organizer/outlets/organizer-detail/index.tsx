import BackButton from "@modules/seeker/organizer/detail/back-button"
import Profile from "@modules/seeker/organizer/detail/profile"
import TabNav from "@modules/seeker/organizer/detail/tab-nav"
import useGetAllLomba from "@publicService/use-get-all-lomba"
import useGetDetailOrganizer from "@publicService/use-get-detail-organizer"
import Container from "@ui/container"
import Typography from "@ui/typography"
import { FunctionComponent, useEffect } from "react"
import Skeleton from "react-loading-skeleton"
import { useLocation, useParams, useSearchParams } from "react-router-dom"

import OrganizerDetailDescriptionSubpages from "./subpages/description"
import OrganizerDetailLombaHeldSubpages from "./subpages/lomba-held"

const OrganizerDetailPage: FunctionComponent = () => {
    /* router stuffs */
    const [searchParams, setSearchParams] = useSearchParams()
    const search = searchParams.get("tab")
    const location = useLocation()
    const { organizerId } = useParams()

    /* API calls */
    const { data: organizerDetail, isLoading: isOrganizerDetailLoading } = useGetDetailOrganizer(
        organizerId as string
    )
    const { data: organizerLomba, isLoading: isOrganizerLombaLoading } = useGetAllLomba({
        organizerId,
    })

    /* react side effects */
    useEffect(() => {
        if (!search) {
            setSearchParams({ tab: "description" }, { state: location.state })
        }
    }, [location.state, search, setSearchParams])

    return (
        <Container as="main" className="relative z-10 flex flex-col space-y-6 pt-[60px] pb-6">
            {/* 👇 floating */}
            <div className="absolute top-[-94px] flex flex-col space-y-6">
                <BackButton />
                <Profile />
            </div>

            {/* 👇 name */}
            <div>
                {isOrganizerDetailLoading || !organizerDetail ? (
                    <>
                        <Skeleton className="mb-1 h-[32px] w-[250px] " />
                        <Skeleton className="h-[18px] w-[80px] " />
                    </>
                ) : (
                    <>
                        <Typography size="h2" as="h2" weight={600}>
                            {organizerDetail.name}
                        </Typography>
                        <Typography size="sm" as="span">
                            {organizerDetail.profile_status}
                        </Typography>
                    </>
                )}
            </div>

            {/* 👇 tab navigation */}
            <TabNav
                isLoading={!organizerLomba || isOrganizerLombaLoading}
                totalLomba={organizerLomba?.length as number}
            />

            {search === "description" && <OrganizerDetailDescriptionSubpages />}
            {search === "lomba" && <OrganizerDetailLombaHeldSubpages />}
        </Container>
    )
}

export default OrganizerDetailPage
