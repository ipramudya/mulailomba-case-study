import Organizers from "@modules/seeker/organizer/explore/organizers"
import ExploreTitle from "@modules/seeker/organizer/explore/title"
import Container from "@ui/container"
import { FunctionComponent } from "react"

const OrganizerExplorePage: FunctionComponent = () => {
    return (
        <Container as="main" className="flex flex-col space-y-6 py-8">
            <ExploreTitle />
            <Organizers />
        </Container>
    )
}

export default OrganizerExplorePage
