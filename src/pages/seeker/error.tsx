import Anchor from "@ui/anchor"
import Button from "@ui/button"
import Container from "@ui/container"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"
import { Helmet } from "react-helmet-async"
import { Link } from "react-router-dom"

const HEADER_HEIGHT = "66px"

const ErrorPage: FunctionComponent = () => {
    return (
        <>
            <Helmet>
                <title>MulaiLomba - Halaman Tidak Tersedia</title>
                <meta
                    name="description"
                    content="Halaman MulaiLomba yang anda cari tidak ditemukan"
                />
            </Helmet>
            <div className="flex min-h-screen w-full flex-col items-center justify-center">
                <nav className="fixed left-0 right-0 top-0 z-fixed bg-primary-500 transition duration-300">
                    <Container
                        size="lg"
                        as="div"
                        className=" py-4"
                        style={{ height: HEADER_HEIGHT }}
                    >
                        <div className="flex items-center justify-between">
                            {/* 👇 links */}
                            <div className="flex items-center">
                                {/* 👇 logo */}
                                <Link to="/">
                                    <Typography
                                        size="h3"
                                        weight={600}
                                        as="h2"
                                        className="text-white"
                                    >
                                        MulaiLomba
                                    </Typography>
                                </Link>

                                {/* 👇 links */}
                                <ul className="flex space-x-3 pl-8 text-white">
                                    <li>
                                        <Anchor
                                            to="/"
                                            intent="light"
                                            size="sm"
                                            weight={500}
                                            className="text-white"
                                        >
                                            Lomba
                                        </Anchor>
                                    </li>
                                    <li>
                                        <Anchor
                                            to="organizer"
                                            intent="light"
                                            size="sm"
                                            weight={500}
                                            className="text-white"
                                        >
                                            Organizer
                                        </Anchor>
                                    </li>
                                </ul>
                            </div>

                            {/* 👇 entry buttons */}
                            <div className="flex space-x-3">
                                <Button
                                    label="Masuk"
                                    intent="primary-darken"
                                    pill
                                    className="min-w-[80px]"
                                />
                                <Button
                                    label="Daftar"
                                    intent="primary-bordered"
                                    pill
                                    className="min-w-[80px] border-transparent"
                                />
                            </div>
                        </div>
                    </Container>
                </nav>
                <Container size="md" as="section">
                    <div className="flex flex-col space-y-4 border-l-2 pl-4">
                        <Typography
                            weight={600}
                            size="h1"
                            as="h3"
                            before="Halaman Tidak Tersedia"
                            className="relative z-[1] w-full text-neutral-800 before:absolute before:top-[2px] before:left-[2px] before:z-[-1] before:w-full before:text-3xl before:font-semibold before:text-neutral-200 before:content-[attr(before)]"
                        >
                            Halaman Tidak Tersedia
                        </Typography>
                        <Typography as="p">
                            Halaman yang sedang anda cari tidak dapat kami temukan atau bahkan
                            server kami sedang error. <br /> Mohon coba beberapa saat lagi{" "}
                            <span className="text-lg">😔</span>
                        </Typography>
                    </div>
                </Container>
            </div>
        </>
    )
}

export default ErrorPage
