import SeekerHeader from "@modules/seeker/_components/seeker-header"
import DetailLomba from "@modules/seeker/lomba/detail-lomba"
import Nav, { HEADER_HEIGHT } from "@ui/nav"
import { FunctionComponent } from "react"
import { Helmet } from "react-helmet-async"

const DetailLombaPage: FunctionComponent = () => {
    return (
        <>
            <Helmet>
                <title>MulaiLomba - Detail Lomba</title>
                <meta name="description" content="Detail lomba" />
            </Helmet>
            <div className="flex w-full flex-col">
                {/* 👇 header */}
                <Nav intent="light">
                    <SeekerHeader variant="primary" />
                </Nav>

                {/* 👇 main content */}
                <div style={{ marginTop: HEADER_HEIGHT }}>
                    <DetailLomba />
                </div>
            </div>
        </>
    )
}

export default DetailLombaPage
