import SeekerHeader from "@modules/seeker/_components/seeker-header"
import DropdownFilters from "@modules/seeker/landing/_subpages-content/explore/dropdowns"
import ExploreFilter from "@modules/seeker/landing/_subpages-content/explore/filter/explore-filter"
import LandingLomba from "@modules/seeker/landing/_subpages-content/explore/lomba"
import AllLombaTitle from "@modules/seeker/lomba/all-lomba/title"
import Container from "@ui/container"
import Nav, { HEADER_HEIGHT } from "@ui/nav"
import { FunctionComponent } from "react"
import { Helmet } from "react-helmet-async"

const AllLomba: FunctionComponent = () => {
    return (
        <>
            <Helmet>
                <title>MulaiLomba - Seluruh Lomba</title>
                <meta name="description" content="Temukan berbagai lomba menarik" />
            </Helmet>
            <div className="flex w-full flex-col">
                {/* 👇 header */}
                <Nav intent="light">
                    <SeekerHeader variant="primary" />
                </Nav>

                {/* 👇 main content -- could be explore or detail page */}
                <div style={{ marginTop: HEADER_HEIGHT }}>
                    <Container as="main" className="flex flex-col space-y-6 py-8">
                        <AllLombaTitle />
                        <ExploreFilter />
                        <DropdownFilters />
                        <LandingLomba />
                    </Container>
                </div>
            </div>
        </>
    )
}

export default AllLomba
