import Highlight from "@blocks/highlight"
import { Icon } from "@iconify/react"
import { BottomDots, TopDots } from "@images/index"
import organizerExampleConstant from "@modules/seeker/entry/organizers.constant"
import RouterLink from "@ui/router-link"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"
import { Outlet } from "react-router-dom"

const EntryPage: FunctionComponent = () => {
    return (
        <div className="relative flex h-screen w-full overflow-hidden">
            {/* 👇 billboard */}
            <div className="relative z-0 flex h-full w-full max-w-[512px] flex-col justify-center  bg-primary-500 py-4 pl-[3rem] pr-[4rem]">
                {/* 👇 logo */}
                <RouterLink to="/" className="absolute top-4 left-4 z-10">
                    <Typography size="h3" weight={600} as="h2" className=" text-white">
                        MulaiLomba
                    </Typography>
                </RouterLink>

                {/* 👇 dots decoration */}
                <div className="absolute top-4 right-6">
                    <img src={TopDots} alt="decoration" />
                </div>
                <div className="absolute bottom-4 left-6">
                    <img src={BottomDots} alt="decoration" />
                </div>

                {/* 👇 headline title */}
                <Typography
                    weight={700}
                    size="h1"
                    as="h1"
                    before="Temukan berbagai lomba menarik dari  Organizer Terpercaya"
                    className="relative z-[1] w-full text-white before:absolute before:top-[3px] before:left-[3px] before:z-[-1] before:w-full before:text-3xl before:font-bold before:text-primary-800 before:content-[attr(before)]"
                >
                    Temukan berbagai lomba menarik dari{" "}
                    <span className="text-secondary-200">Organizer Terpercaya</span>
                </Typography>

                {/* 👇 sample organizers */}
                <div className="mt-4 grid grid-cols-3 gap-3">
                    {organizerExampleConstant.map((img, idx) => (
                        <Highlight key={"sample-organizer-" + idx} className="col-span-1 ">
                            <div className="flex h-full w-full items-center justify-center">
                                <img
                                    src={img}
                                    alt="sample organizer"
                                    className="w-[unset] object-cover"
                                />
                            </div>
                        </Highlight>
                    ))}
                </div>

                {/* 👇 link to organizer's dashboard */}
                <RouterLink to="/dashboard/entry" className="mt-12 w-full">
                    <Highlight className="p-2">
                        <div className="flex w-full items-center justify-between">
                            <Typography as="span" weight={600} className="text-primary-700">
                                Masuk sebagai organizer
                            </Typography>
                            <div className="flex aspect-square w-8 items-center justify-center rounded-full border-[2px] border-primary-700 bg-primary-500">
                                <Icon
                                    icon="heroicons:arrow-long-right"
                                    className="h-6 w-6 text-white"
                                />
                            </div>
                        </div>
                    </Highlight>
                </RouterLink>
            </div>

            {/* 👇 outlet entries */}
            <main className="w-full ">
                <Outlet />
            </main>
        </div>
    )
}

export default EntryPage
