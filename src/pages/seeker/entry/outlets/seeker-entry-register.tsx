import Register from "@modules/seeker/entry/register"
import RouterLink from "@ui/router-link"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"
import { Helmet } from "react-helmet-async"

const RegisterPage: FunctionComponent = () => {
    return (
        <>
            <Helmet>
                <title>Bergabung Dengan MulaiLomba</title>
                <meta
                    name="description"
                    content="Gabung dengan MulaiLomba dan asah kemampuan serta keterampilan unik milikmu"
                />
            </Helmet>
            <div className="relative h-full w-full">
                <Typography size="sm" className="absolute top-4 right-4">
                    Sudah memiliki akun ?{" "}
                    <RouterLink to="../login" relative="path">
                        <Typography
                            className="text-primary-600 transition duration-300 hover:text-opacity-80"
                            as="span"
                            size="sm"
                        >
                            Masuk
                        </Typography>
                    </RouterLink>
                </Typography>

                {/* 👇 form */}
                <div className="flex h-full items-center justify-center">
                    <Register />
                </div>
            </div>
        </>
    )
}

export default RegisterPage
