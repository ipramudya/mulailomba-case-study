import useAuthData from "@/services/auth-user/use-get-auth-data"
import DropdownFilters from "@modules/seeker/landing/_subpages-content/explore/dropdowns"
import ExploreFilter from "@modules/seeker/landing/_subpages-content/explore/filter/explore-filter"
import LandingLomba from "@modules/seeker/landing/_subpages-content/explore/lomba"
import Typography from "@ui/typography"
import { type FunctionComponent } from "react"

const LandingExploreSubpage: FunctionComponent = () => {
    const { data } = useAuthData()

    return (
        <div className="flex flex-col space-y-6 py-3">
            {data ? (
                <>
                    {!data.user ? (
                        <>
                            <ExploreFilter />
                            <DropdownFilters />
                        </>
                    ) : (
                        <Typography as="h3" weight={600} size="h3">
                            Rekomendasi Lomba Untuk Anda
                        </Typography>
                    )}
                    <LandingLomba
                        shouldRecommendLomba={Boolean(data.user)}
                        interest={data.user?.user_metadata.interest}
                    />
                </>
            ) : (
                false
            )}
        </div>
    )
}

export default LandingExploreSubpage
