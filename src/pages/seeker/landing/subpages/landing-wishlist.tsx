import useAuthData from "@/services/auth-user/use-get-auth-data"
import LandscapePoster from "@blocks/poster/landscape"
import PosterSkeleton from "@blocks/poster/poster-skeleton"
import EmptyWishlist from "@modules/seeker/landing/_subpages-content/wishlist/empty-wishlist"
import useGetWishlist from "@seekerService/lomba/use-get-wishlist"
import Typography from "@ui/typography"
import { type FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"

const LandingWishlistSubpage: FunctionComponent = () => {
    const { data: authData } = useAuthData()
    const { data: wishlist, isLoading } = useGetWishlist()

    return (
        <div className="flex flex-col space-y-6 py-3">
            {/* title */}
            <div>
                <Typography as="h3" weight={600} size="h3">
                    Wishlist Kamu
                </Typography>
                {isLoading ? (
                    <Skeleton className="h-[20px] w-[120px] " />
                ) : (
                    <Typography as="span" size="sm">
                        {wishlist ? `Total ${wishlist.length} lomba` : "Wishlist tidak ditemukan"}
                    </Typography>
                )}
            </div>

            <div className="grid grid-cols-[repeat(auto-fill,_minmax(370px,_1fr))] gap-x-5 gap-y-8">
                {isLoading ? (
                    Array.from({ length: 12 }).map((_, idx) => (
                        <PosterSkeleton key={"poster-skeleton-explore-" + idx} />
                    ))
                ) : (
                    <>
                        {!wishlist ? (
                            <EmptyWishlist
                                status={authData?.user === null ? "not-logged" : "empty"}
                            />
                        ) : (
                            wishlist.map((w) => (
                                <LandscapePoster
                                    key={"poster-landing-" + w.id}
                                    id={w.id}
                                    price={w.is_free ? null : "-"}
                                    isOnline={Boolean(w.is_held_online)}
                                    title={w.name as string}
                                    startDate={w.start_date as string}
                                    endDate={w.end_date as string}
                                    image={w.poster_public_url as string}
                                />
                            ))
                        )}
                    </>
                )}
            </div>
        </div>
    )
}

export default LandingWishlistSubpage
