import RegisteredFilters from "@modules/seeker/landing/_subpages-content/registered/registered-filters"
import RegisteredList from "@modules/seeker/landing/_subpages-content/registered/registered-list"
import RegisteredSearchDropdowns from "@modules/seeker/landing/_subpages-content/registered/registered-search-dropdowns"
import Typography from "@ui/typography"
import { type FunctionComponent } from "react"

const LandingRegisteredSubpage: FunctionComponent = () => (
    <div className="flex flex-col space-y-6 py-3">
        <div className="flex items-center justify-between">
            <div>
                <Typography as="h3" weight={600} size="h3">
                    Lomba Terdaftar
                </Typography>
                <Typography as="span" size="sm">
                    Berikut merupakan lomba yang telah/sedang kamu ikuti
                </Typography>
            </div>
            {/* 👇 search and dropdowns */}
            <RegisteredSearchDropdowns />
        </div>
        {/* 👇 filters */}
        <RegisteredFilters />
        {/* 👇 list itself */}
        <RegisteredList />
    </div>
)

export default LandingRegisteredSubpage
