import SeekerHeader from "@modules/seeker/_components/seeker-header"
import Hero from "@modules/seeker/landing/hero"
import LombaHighlight from "@modules/seeker/landing/lomba-highlights"
import TabNav from "@modules/seeker/landing/tab-nav"
import Container from "@ui/container"
import Nav, { HEADER_HEIGHT } from "@ui/nav"
import { FunctionComponent, useEffect } from "react"
import { Helmet } from "react-helmet-async"
import { useInView } from "react-intersection-observer"
import { useSearchParams } from "react-router-dom"

import LandingExploreSubpage from "./subpages/landing-explore"
import LandingRegisteredSubpage from "./subpages/landing-registered"
import LandingWishlistSubpage from "./subpages/landing-wishlist"

const LandingPage: FunctionComponent = () => {
    /* router stuffs */
    const [searchParams, setSearchParams] = useSearchParams()
    const search = searchParams.get("tab")

    /* intersection observer */
    const { inView, entry, ref } = useInView({
        rootMargin: `-${HEADER_HEIGHT}`,
    })

    /* react side effects */
    useEffect(() => {
        if (!search) {
            setSearchParams({ tab: "explore" })
        }
    }, [search, setSearchParams])

    return (
        <>
            <Helmet>
                <title>MulaiLomba - Halaman Utama</title>
                <meta
                    name="description"
                    content="Daftarkan dirimu dan tunjukan bahwa kamu pemenangnya"
                />
            </Helmet>
            <div className="flex w-full flex-col ">
                {/* 👇 hero sections */}
                <div ref={ref} className="min-h-[310px] bg-primary-500">
                    <Nav
                        intent={!inView && entry !== undefined ? "light" : "primary"}
                        disableShadow
                    >
                        <SeekerHeader
                            variant={!inView && entry !== undefined ? "primary" : "secondary"}
                        />
                    </Nav>
                    <Hero />
                    <LombaHighlight />
                </div>

                {/* 👇 main content */}
                <Container size="md" as="main" className="space-y-6 py-8">
                    {/* 👇 tab nav */}
                    <TabNav />

                    {/* 👇 tab content registry */}
                    {search === "explore" && <LandingExploreSubpage />}
                    {search === "wishlist" && <LandingWishlistSubpage />}
                    {search === "registered" && <LandingRegisteredSubpage />}
                </Container>
            </div>
        </>
    )
}

export default LandingPage
