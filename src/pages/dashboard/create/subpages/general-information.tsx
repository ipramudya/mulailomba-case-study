import GeneralInfo from "@modules/dashboard/lomba-create/_subpage-content/general-info"
import PaperLayout from "@modules/dashboard/lomba-create/paper-layout"
import { FunctionComponent } from "react"

const GeneralInformationSubpage: FunctionComponent = () => {
    return (
        <PaperLayout title="Informasi Umum" stepNumber={1}>
            <GeneralInfo />
        </PaperLayout>
    )
}

export default GeneralInformationSubpage
