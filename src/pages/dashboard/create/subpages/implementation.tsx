import Implementation from "@modules/dashboard/lomba-create/_subpage-content/implementation"
import PaperLayout from "@modules/dashboard/lomba-create/paper-layout"
import { FunctionComponent } from "react"

const ImplementationSubpage: FunctionComponent = () => {
    return (
        <PaperLayout title="Pelaksanaan" stepNumber={4}>
            <Implementation />
        </PaperLayout>
    )
}

export default ImplementationSubpage
