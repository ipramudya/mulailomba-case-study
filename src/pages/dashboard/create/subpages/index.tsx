import Loader from "@ui/loader"
import { Fragment, FunctionComponent, lazy } from "react"
import { useSearchParams } from "react-router-dom"

const GeneralInformationSubpage = lazy(() => import("./general-information"))
const RegistrationSubpage = lazy(() => import("./registration"))
const DescAndRulesSubpage = lazy(() => import("./desc-and-rules"))
const ImplementationSubpage = lazy(() => import("./implementation"))

const SubpageRegistry: FunctionComponent = () => {
    /* router stuffs */
    const [searchParams] = useSearchParams()
    const search = searchParams.get("step")

    switch (search) {
        case "1":
            return (
                <Loader>
                    <GeneralInformationSubpage />
                </Loader>
            )
        case "2":
            return (
                <Loader>
                    <RegistrationSubpage />
                </Loader>
            )
        case "3":
            return (
                <Loader>
                    <DescAndRulesSubpage />
                </Loader>
            )
        case "4":
            return (
                <Loader>
                    <ImplementationSubpage />
                </Loader>
            )
        default:
            return <Fragment />
    }
}

export default SubpageRegistry
