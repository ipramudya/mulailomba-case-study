import Registration from "@modules/dashboard/lomba-create/_subpage-content/registration"
import PaperLayout from "@modules/dashboard/lomba-create/paper-layout"
import { FunctionComponent } from "react"

const RegistrationSubpage: FunctionComponent = () => {
    return (
        <PaperLayout title="Pendaftaran" stepNumber={2}>
            <Registration />
        </PaperLayout>
    )
}

export default RegistrationSubpage
