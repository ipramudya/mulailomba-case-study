import DescAndRules from "@modules/dashboard/lomba-create/_subpage-content/desc-and-rules"
import PaperLayout from "@modules/dashboard/lomba-create/paper-layout"
import { FunctionComponent } from "react"

const DescAndRulesSubpage: FunctionComponent = () => {
    return (
        <PaperLayout title="Deskripsi dan Aturan" stepNumber={3}>
            <DescAndRules />
        </PaperLayout>
    )
}

export default DescAndRulesSubpage
