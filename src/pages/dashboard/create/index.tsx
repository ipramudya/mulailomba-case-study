import CreateLombaHeader from "@modules/dashboard/lomba-create/create-lomba-header"
import Container from "@ui/container"
import Nav, { HEADER_HEIGHT } from "@ui/nav"
import Typography from "@ui/typography"
import { FunctionComponent, useEffect } from "react"
import { Helmet } from "react-helmet-async"
import { useLocation, useSearchParams } from "react-router-dom"

import SubpageRegistry from "./subpages"

const CreateLombaPage: FunctionComponent = () => {
    /* router stuffs */
    const [searchParams, setSearchParams] = useSearchParams()
    const search = searchParams.get("step")
    const { state: routerState } = useLocation()

    /* react side effects */
    useEffect(() => {
        if (!search) {
            setSearchParams({ step: "1", lid: routerState.lombaId }, { state: routerState })
        }
    }, [search, setSearchParams, routerState])

    return (
        <>
            <Helmet>
                <title>Organizer MulaiLomba - Buat Lomba</title>
                <meta name="description" content="Buat lomba untuk organizer" />
            </Helmet>
            <div className="flex w-full flex-col">
                {/* 👇 header */}
                <Nav intent="light" className="border-neutral-100 shadow-none">
                    <CreateLombaHeader />
                </Nav>

                <div className=" min-h-screen bg-neutral-50" style={{ marginTop: HEADER_HEIGHT }}>
                    <Container as="main" className="flex flex-col space-y-6 pt-[3rem] pb-[5rem] ">
                        <div className="flex flex-col space-y-2">
                            <Typography as="h2" size="h2" weight={600}>
                                Buat Perlombaan
                            </Typography>
                            <Typography size="sm">
                                Ikuti dan penuhi seluruh langkah-langkah yang telah disediakan agar
                                memudahkan <br /> pengguna dalam menemukan acara milikmu.
                            </Typography>
                        </div>

                        {/* 👇 paper content */}
                        <SubpageRegistry />
                    </Container>
                </div>
            </div>
        </>
    )
}

export default CreateLombaPage
