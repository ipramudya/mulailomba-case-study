import DashboardHeader from "@modules/dashboard/_components/dashboard-header"
import LombaDetail from "@modules/dashboard/lomba-detail"
import useGetDetailLomba from "@publicService/use-get-detail-lomba"
import Nav, { HEADER_HEIGHT } from "@ui/nav"
import { FunctionComponent } from "react"
import { Helmet } from "react-helmet-async"
import { useParams } from "react-router-dom"

const LombaDetailPage: FunctionComponent = () => {
    const { lombaId } = useParams()
    const { data } = useGetDetailLomba(lombaId as string)

    return (
        <>
            <Helmet>
                <title>Organizer MulaiLomba - Detail Lomba</title>
                <meta name="description" content={`Dashboard ${data?.name}`} />
            </Helmet>
            <div className="flex w-full flex-col">
                <Nav intent="none" className="border-b border-neutral-100">
                    <DashboardHeader />
                </Nav>

                <div style={{ marginTop: HEADER_HEIGHT }}>
                    <LombaDetail />
                </div>
            </div>
        </>
    )
}

export default LombaDetailPage
