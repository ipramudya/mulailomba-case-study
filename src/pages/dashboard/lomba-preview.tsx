import DashboardHeader from "@modules/dashboard/_components/dashboard-header"
import LombaPreview from "@modules/dashboard/lomba-preview"
import Nav, { HEADER_HEIGHT } from "@ui/nav"
import { FunctionComponent } from "react"
import { Helmet } from "react-helmet-async"

const LombaPreviewPage: FunctionComponent = () => {
    return (
        <>
            <Helmet>
                <title>Organizer MulaiLomba - Preview Lomba</title>
                <meta name="description" content="Preview lomba" />
            </Helmet>
            <div className="flex w-full flex-col">
                <Nav intent="none" className="border-b border-neutral-100">
                    <DashboardHeader />
                </Nav>
                <div style={{ marginTop: HEADER_HEIGHT }}>
                    <LombaPreview />
                </div>
            </div>
        </>
    )
}

export default LombaPreviewPage
