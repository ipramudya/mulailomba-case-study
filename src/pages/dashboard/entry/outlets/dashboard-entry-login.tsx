import Login from "@modules/dashboard/entry/login"
import RouterLink from "@ui/router-link"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"
import { Helmet } from "react-helmet-async"

const LoginPage: FunctionComponent = () => {
    return (
        <>
            <Helmet>
                <title>Selamat Datang Organizer</title>
                <meta
                    name="description"
                    content="Selamat datang kembali para penyelenggara kreatif"
                />
            </Helmet>
            <div className="relative h-full w-full">
                <Typography size="sm" className="absolute top-4 right-4">
                    Belum memiliki akun ?{" "}
                    <RouterLink to="../register" relative="path">
                        <Typography
                            className="text-primary-600 transition duration-300 hover:text-opacity-80"
                            as="span"
                            size="sm"
                        >
                            Daftarkan sekarang
                        </Typography>
                    </RouterLink>
                </Typography>

                {/* 👇 form */}
                <div className="flex h-full items-center justify-center">
                    <Login />
                </div>
            </div>
        </>
    )
}

export default LoginPage
