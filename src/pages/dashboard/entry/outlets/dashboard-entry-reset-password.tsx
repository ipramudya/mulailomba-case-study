import ResetPassword from "@modules/dashboard/entry/reset-password"
import RouterLink from "@ui/router-link"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"
import { Helmet } from "react-helmet-async"

const ResetPasswordPage: FunctionComponent = () => {
    return (
        <>
            <Helmet>
                <title>Atur Ulang Kata Sandi Organizer</title>
                <meta name="description" content="Atur ulang kata sandi organizer MulaiLomba" />
            </Helmet>

            <div className="relative h-[100dvh] w-full">
                <RouterLink to="/" className="absolute top-4 left-1/2 z-10 -translate-x-1/2">
                    <Typography size="h3" weight={600} as="h2">
                        MulaiLomba
                    </Typography>
                </RouterLink>

                {/* 👇 form */}
                <div className="flex h-full items-center justify-center">
                    <ResetPassword />
                </div>
            </div>
        </>
    )
}

export default ResetPasswordPage
