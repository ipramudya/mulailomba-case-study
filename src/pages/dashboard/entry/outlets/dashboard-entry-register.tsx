import Register from "@modules/dashboard/entry/register"
import RouterLink from "@ui/router-link"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"
import { Helmet } from "react-helmet-async"

const RegisterPage: FunctionComponent = () => {
    return (
        <>
            <Helmet>
                <title>Bergabung dan Kelola Organizer Unik Milikmu</title>
                <meta
                    name="description"
                    content="Gabung dengan MulaiLomba dan kembangkan organizer kreatif milikmu"
                />
            </Helmet>
            <div className="relative h-full w-full">
                <Typography size="sm" className="absolute top-4 right-4">
                    Sudah memiliki akun ?{" "}
                    <RouterLink to="../login" relative="path">
                        <Typography
                            className="text-primary-600 transition duration-300 hover:text-opacity-80"
                            as="span"
                            size="sm"
                        >
                            Masuk
                        </Typography>
                    </RouterLink>
                </Typography>

                {/* 👇 form */}
                <div className="flex h-full items-center justify-center">
                    <Register />
                </div>
            </div>
        </>
    )
}

export default RegisterPage
