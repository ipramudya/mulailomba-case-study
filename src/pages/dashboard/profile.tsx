import DashboardHeader from "@modules/dashboard/_components/dashboard-header"
import ProfileImage from "@modules/dashboard/profile/profile-image"
import ProfileLabel from "@modules/dashboard/profile/profile-label"
import TabNav from "@modules/dashboard/profile/tab-nav"
import Button from "@ui/button"
import Container from "@ui/container"
import Loader from "@ui/loader"
import Nav from "@ui/nav"
import { FunctionComponent, lazy, useEffect } from "react"
import { Helmet } from "react-helmet-async"
import { useLocation, useNavigate, useSearchParams } from "react-router-dom"

const DescriptionSubpage = lazy(
    () => import("@modules/dashboard/profile/_subpages-content/description")
)

const LombaHeldSubpage = lazy(
    () => import("@modules/dashboard/profile/_subpages-content/lomba-held")
)

const DashboardProfilePage: FunctionComponent = () => {
    /* router stuffs */
    const [searchParams, setSearchParams] = useSearchParams()
    const search = searchParams.get("tab")
    const location = useLocation()
    const navigate = useNavigate()

    /* react side effects */
    useEffect(() => {
        if (!search) {
            setSearchParams({ tab: "description" }, { state: location.state })
        }
    }, [location.state, search, setSearchParams])

    return (
        <>
            <Helmet>
                <title>Organizer MulaiLomba - Profile</title>
                <meta name="description" content="Dashboard profile organizer MulaiLonba" />
            </Helmet>
            <div className="flex w-full flex-col">
                <div className="min-h-[196px] bg-primary-500">
                    <Nav intent="primary">
                        <DashboardHeader variant="secondary" />
                    </Nav>
                </div>
                <Container
                    as="main"
                    className="relative z-10 flex flex-col space-y-6 pt-[60px] pb-6"
                >
                    {/* 👇 floating */}
                    <div className="absolute top-[-94px] flex flex-col space-y-6">
                        <Button
                            label="Kembali menuju home"
                            startIcon="heroicons:arrow-left-solid"
                            intent="unstyled"
                            onClick={() => navigate("../home", { relative: "path" })}
                            className="max-w-fit p-0 text-white hover:text-opacity-80"
                        />
                        <ProfileImage />
                    </div>
                    <ProfileLabel />
                    <TabNav />
                    {search === "description" && (
                        <Loader>
                            <DescriptionSubpage />
                        </Loader>
                    )}

                    {search === "lomba" && (
                        <Loader>
                            <LombaHeldSubpage />
                        </Loader>
                    )}
                </Container>
            </div>
        </>
    )
}

export default DashboardProfilePage
