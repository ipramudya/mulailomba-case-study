import Deadline from "@modules/dashboard/lomba-update/_subhash-content/registration/deadline"
import Prerequisites from "@modules/dashboard/lomba-update/_subhash-content/registration/prerequisites"
import VariantAndParticipant from "@modules/dashboard/lomba-update/_subhash-content/registration/variant-and-participant"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"
import { Element as ScrollELement } from "react-scroll"

const RegistrationSubhash: FunctionComponent = () => {
    return (
        <div className="flex flex-col space-y-6">
            <div className="flex flex-col space-y-4">
                <ScrollELement name="registration">
                    <Typography as="h2" size="lg" weight={500}>
                        Pendaftaran
                    </Typography>
                </ScrollELement>
                <VariantAndParticipant />
            </div>
            <Deadline />
            <Prerequisites />
        </div>
    )
}

export default RegistrationSubhash
