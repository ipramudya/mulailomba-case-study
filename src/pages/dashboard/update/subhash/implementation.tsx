import ImplementationType from "@modules/dashboard/lomba-update/_subhash-content/implementation/implementation-type"
import Timeline from "@modules/dashboard/lomba-update/_subhash-content/implementation/timeline"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"
import { Element as ScrollELement } from "react-scroll"

const ImplementationSubhash: FunctionComponent = () => (
    <div className="flex flex-col space-y-6">
        <div className="flex flex-col space-y-4">
            <ScrollELement name="implementation">
                <Typography as="h2" size="lg" weight={500}>
                    Pelaksanaan
                </Typography>
            </ScrollELement>
            <ImplementationType />
        </div>
        <Timeline />
    </div>
)

export default ImplementationSubhash
