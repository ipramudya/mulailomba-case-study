import Benefit from "@modules/dashboard/lomba-update/_subhash-content/general/benefit"
import Eligibility from "@modules/dashboard/lomba-update/_subhash-content/general/eligibility"
import PosterAndInfo from "@modules/dashboard/lomba-update/_subhash-content/general/poster-and-info"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"
import { Element as ScrollELement } from "react-scroll"

const GeneralSubhash: FunctionComponent = () => {
    return (
        <div className="flex flex-col space-y-6">
            <div className="flex flex-col space-y-4">
                <ScrollELement name="general">
                    <Typography as="h2" size="lg" weight={500}>
                        Informasi Umum
                    </Typography>
                </ScrollELement>
                <PosterAndInfo />
            </div>
            <Benefit />
            <Eligibility />
        </div>
    )
}

export default GeneralSubhash
