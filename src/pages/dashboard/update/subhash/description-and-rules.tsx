import Description from "@modules/dashboard/lomba-update/_subhash-content/description-and-rules/description"
import Rules from "@modules/dashboard/lomba-update/_subhash-content/description-and-rules/rules"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"
import { Element as ScrollELement } from "react-scroll"

const DescriptionAndRulesSubhash: FunctionComponent = () => (
    <div className="flex flex-col space-y-6">
        <div className="flex flex-col space-y-4">
            <ScrollELement name="description-and-rules">
                <Typography as="h2" size="lg" weight={500}>
                    Deskripsi dan Aturan
                </Typography>
            </ScrollELement>
            <Description />
        </div>
        <Rules />
    </div>
)

export default DescriptionAndRulesSubhash
