import DashboardHeader from "@modules/dashboard/_components/dashboard-header"
import { EditableTimelineAtom } from "@modules/dashboard/lomba-update/_subhash-content/implementation/timeline"
import { EditablePrerequisiteAtom } from "@modules/dashboard/lomba-update/_subhash-content/registration/prerequisites"
import Sidebar from "@modules/dashboard/lomba-update/sidebar"
import Container from "@ui/container"
import Nav, { HEADER_HEIGHT } from "@ui/nav"
import Typography from "@ui/typography"
import { useSetAtom } from "jotai"
import { FunctionComponent, useEffect } from "react"
import { Helmet } from "react-helmet-async"
import { useLocation, useNavigate } from "react-router-dom"

import DescriptionAndRulesSubhash from "./subhash/description-and-rules"
import GeneralSubhash from "./subhash/general"
import ImplementationSubhash from "./subhash/implementation"
import RegistrationSubhash from "./subhash/registration"

const UpdateLombaPage: FunctionComponent = () => {
    /* shareable states */
    const setIsPrerequisitesEdited = useSetAtom(EditablePrerequisiteAtom)
    const setIsTimelineEdited = useSetAtom(EditableTimelineAtom)

    /* router stuffs */
    const navigate = useNavigate()
    const { hash } = useLocation()

    // /* react side effects */
    useEffect(() => {
        if (!hash) {
            navigate("#general")
        }
    }, [hash, navigate])

    useEffect(() => {
        return function () {
            setIsPrerequisitesEdited(false)
            setIsTimelineEdited(false)
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <>
            <Helmet>
                <title>Organizer MulaiLomba - Perbarui Lomba</title>
                <meta name="description" content="Perbarui lomba organizer" />
            </Helmet>
            <div className="flex w-full flex-col">
                {/* 👇 header */}
                <Nav intent="light">
                    <DashboardHeader />
                </Nav>

                <div className="min-h-screen" style={{ marginTop: HEADER_HEIGHT }}>
                    <Container as="main" className="flex flex-col space-y-6 pt-[3rem] pb-8">
                        <div className="flex flex-col space-y-2">
                            <Typography as="h2" size="h2" weight={600}>
                                Perbarui Lomba
                            </Typography>
                            <Typography size="sm">
                                Anda dapat mengubah informasi lomba pada masing-masing komponen
                                lomba
                            </Typography>
                        </div>

                        <div className="flex h-fit w-full space-x-6">
                            <Sidebar />
                            <div className="flex grow flex-col space-y-10 border-l border-neutral-100 pl-8">
                                <GeneralSubhash />
                                <RegistrationSubhash />
                                <DescriptionAndRulesSubhash />
                                <ImplementationSubhash />
                            </div>
                        </div>
                    </Container>
                </div>
            </div>
        </>
    )
}

export default UpdateLombaPage
