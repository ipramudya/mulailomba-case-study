import DashboardHeader from "@modules/dashboard/_components/dashboard-header"
import Home from "@modules/dashboard/home"
import Nav, { HEADER_HEIGHT } from "@ui/nav"
import { FunctionComponent } from "react"
import { Helmet } from "react-helmet-async"

const DashboardHomePage: FunctionComponent = () => {
    return (
        <>
            <Helmet>
                <title>Organizer MulaiLomba - Home</title>
                <meta name="description" content="Home dashboard organizer MulaiLomba" />
            </Helmet>
            <div className="flex w-full flex-col">
                {/* 👇 header */}
                <Nav intent="light">
                    <DashboardHeader />
                </Nav>

                <div style={{ marginTop: HEADER_HEIGHT }}>
                    <Home />
                </div>
            </div>
        </>
    )
}

export default DashboardHomePage
