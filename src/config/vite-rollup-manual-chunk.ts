export default function manualChunks(id: string) {
    if (id.includes("node_modules")) {
        if (id.includes("@radix-ui")) {
            return "radix"
        }
        if (id.includes("lodash-es")) {
            return "lodash"
        }
        if (id.includes("@supabase")) {
            return "supabase"
        }
        if (id.includes("router")) {
            return "react-router-dom"
        }
        if (id.includes("react-select")) {
            return "react-select"
        }
        if (id.includes("@tanstack/query")) {
            return "react-query"
        }
        if (id.includes("date-fns")) {
            return "date-fns"
        }
        if (id.includes("tailwind-merge")) {
            return "tailwind-merge"
        }
        return "vendor"
    }
}
