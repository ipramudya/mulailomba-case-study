import useAuthData from "@/services/auth-user/use-get-auth-data"
import { FunctionComponent, useEffect } from "react"
import { Outlet, useLocation, useNavigate } from "react-router-dom"

const OrganizerMiddleware: FunctionComponent = () => {
    /* API definitions */
    const { data } = useAuthData()

    /* router stuffs */
    const { pathname } = useLocation()
    const navigate = useNavigate()

    /* navigate to appropriate page */
    useEffect(() => {
        if (!data) return

        if (pathname.includes("dashboard") && !pathname.includes("entry")) {
            if (pathname === "/dashboard/" || pathname === "/dashboard") {
                navigate("/dashboard/home/")
            }

            if (!data || !data.user) {
                return navigate("/dashboard/entry", { replace: true })
            } else if (data?.user?.user_metadata.access_role) {
                return
            } else {
                navigate("/", { replace: true })
            }
        } else if (Boolean(data) && Boolean(data?.user) && pathname.includes("entry")) {
            navigate("/dashboard/home", { replace: true })
        }
    }, [data, navigate, pathname])

    return <Outlet />
}

export default OrganizerMiddleware
