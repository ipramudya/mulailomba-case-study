import { Root } from "@radix-ui/react-portal"
import Button from "@ui/button"
import { FunctionComponent, useState } from "react"

import FeedbackModal from "./feedback-modal"

const Feedback: FunctionComponent = () => {
    const [isModalOpen, setIsModalOpen] = useState(false)

    return (
        <Root>
            <FeedbackModal isOpen={isModalOpen} onOpenChange={setIsModalOpen} />
            <div className="fixed bottom-0 left-0 z-10 flex w-full items-center justify-center ">
                <div className="mb-[1rem] ">
                    <Button
                        onClick={() => setIsModalOpen(true)}
                        label="Berikan Feedback"
                        className=" px-5 py-3 hover:bg-primary-50"
                        pill
                        endIcon="heroicons:heart-solid"
                        intent="primary-bordered"
                    />
                </div>
            </div>
        </Root>
    )
}

export default Feedback
