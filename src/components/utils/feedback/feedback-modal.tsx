import useSendFeedback from "@publicService/use-send-feedback"
import Button from "@ui/button"
import Form from "@ui/form"
import Modal from "@ui/modal"
import Typography from "@ui/typography"
import { FunctionComponent, useEffect } from "react"
import { SubmitHandler, useForm } from "react-hook-form"
import { toast } from "react-hot-toast"
import { twMerge } from "tailwind-merge"

import feedbackRating from "./rating.constant"

type FormFields = {
    emailOrUsername: string
    rating: string
    feedback: string
    category: string
    status: string
}

interface Props {
    isOpen: boolean
    onOpenChange(val: boolean): void
}

const FeedbackModal: FunctionComponent<Props> = ({ isOpen, onOpenChange }) => {
    const methods = useForm<FormFields>()
    const { setValue, watch } = methods
    const ratingFieldWatcher = watch("rating")

    const sendFeedback = useSendFeedback()

    const onFeedbackSubmit: SubmitHandler<FormFields> = async (fields) => {
        const { emailOrUsername, ...restFields } = fields

        let isFieldEmpty: true | null = null
        let emptyFieldErrorMessage = ""
        Object.keys(fields).forEach((key) => {
            // @ts-expect-error any
            if (!fields[key]) {
                isFieldEmpty = true
                emptyFieldErrorMessage = "Terdapat informasi yang masih belum terisi"
            }

            if (!ratingFieldWatcher) {
                isFieldEmpty = true
                emptyFieldErrorMessage = "Tolong pilih emoji terlebih dahulu ya"
            }
        })

        if (isFieldEmpty) return toast.error(emptyFieldErrorMessage)

        const { error } = await sendFeedback({ ...restFields, emailOrUsername })

        if (error) {
            if (typeof error !== "string") {
                toast.error("Server sedang tidak stabil")
            } else {
                toast.error(error)
            }
        } else {
            toast.success("Terima kasih telah mengirimkan feedback")
            onOpenChange(false)
            methods.reset()
        }
    }

    useEffect(() => {
        if (!isOpen) {
            methods.reset()
        }
    }, [isOpen, methods])

    return (
        <Modal open={isOpen} onOpenChange={onOpenChange} title={false}>
            <div className="mb-4 flex w-full flex-col items-center space-y-2 text-center">
                <Typography size="lg" weight={500} className="w-full text-center text-neutral-800">
                    Feedback
                </Typography>
                <Typography size="sm" className="w-fit">
                    Berikan umpan balik pada kami terkait pengalaman anda dalam menggunakan aplikasi
                    MulaiLomba.
                </Typography>
            </div>

            <Form {...methods} onSubmit={onFeedbackSubmit}>
                <div className="mb-8 flex items-center justify-center space-x-3">
                    {feedbackRating.map((d, idx) => {
                        const isSelected = ratingFieldWatcher === d.value
                        return (
                            <div
                                className={twMerge(
                                    "flex aspect-square cursor-pointer items-center justify-center rounded-full border border-neutral-100/70 bg-white p-1 transition-all hover:scale-125 hover:border-warning-600 hover:bg-warning-200/50",
                                    isSelected ? "border-warning-500 bg-warning-200/50" : undefined
                                )}
                                key={"feedback-rating-" + idx}
                                onClick={() => {
                                    if (isSelected) return setValue("rating", "")
                                    setValue("rating", d.value)
                                }}
                            >
                                <Typography size="h3">{d.label}</Typography>
                            </div>
                        )
                    })}
                </div>
                <Form.Layout>
                    <Form.TextField
                        name="emailOrUsername"
                        label="Email atau Username"
                        placeholder="Email atau Username sosial media kamu"
                        required
                    />
                    <Form.SelectField
                        name="category"
                        label="Kategori Feedback"
                        placeholder="pilih kategori feedback"
                        required
                        options={[
                            {
                                label: "Kemudahan Akses Informasi",
                                value: "Kemudahan Akses Informasi",
                            },
                            {
                                label: "Fitur Aplikasi",
                                value: "Fitur Aplikasi",
                            },
                            {
                                label: "Tampilan",
                                value: "Tampilan",
                            },
                        ]}
                    />
                    <Form.SelectField
                        name="status"
                        label="Status Pekerjaan"
                        placeholder="pilih status pekerjaan"
                        required
                        options={[
                            {
                                label: "Pelajar/Siswa",
                                value: "Pelajar/Siswa",
                            },
                            {
                                label: "Organisator",
                                value: "Organisator",
                            },
                            {
                                label: "Akademisi",
                                value: "Akademisi",
                            },
                            {
                                label: "Umum",
                                value: "Umum",
                            },
                        ]}
                    />
                    <Form.TextareaField
                        name="feedback"
                        label="Feedback"
                        required
                        resized
                        placeholder="tuliskan feedback"
                    />
                    <Form.RowLayout column={2} className="pt-4">
                        <Button
                            thin
                            label="Batalkan"
                            intent="bordered"
                            onClick={() => onOpenChange(false)}
                        />
                        <Button
                            thin
                            label="Kirimkan Feedback"
                            type="submit"
                            loading={methods.formState.isSubmitting}
                        />
                    </Form.RowLayout>
                </Form.Layout>
            </Form>
        </Modal>
    )
}

export default FeedbackModal
