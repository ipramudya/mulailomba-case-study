const feedbackRating = [
    {
        label: "🤩",
        value: "Sangat Baik",
    },
    {
        label: "🙂",
        value: "Baik",
    },
    {
        label: "😐",
        value: "Biasa Saja",
    },
    {
        label: "😢",
        value: "Buruk",
    },
    {
        label: "😩",
        value: "Sangat Buruk",
    },
]

export default feedbackRating
