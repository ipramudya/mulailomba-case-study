import useAuthData from "@/services/auth-user/use-get-auth-data"
import { FunctionComponent, useEffect } from "react"
import { Outlet, useLocation, useNavigate } from "react-router-dom"

interface Props {
    fallbackUrl: string
}

const RootElement: FunctionComponent<Props> = ({ fallbackUrl }) => {
    /* api definitions */
    const { data } = useAuthData()

    /* router stuffs */
    const { pathname } = useLocation()
    const navigate = useNavigate()

    /* navigate to home if logged-in user visits entry page */
    useEffect(() => {
        if (pathname.includes("entry") && data && data.user) {
            return navigate(fallbackUrl, { replace: true })
        }
    }, [data, fallbackUrl, navigate, pathname])

    return <Outlet />
}

export default RootElement
