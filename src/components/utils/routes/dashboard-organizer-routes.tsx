import Loader from "@ui/loader"
import { lazy } from "react"
import { Navigate, RouteObject } from "react-router-dom"

/* root component as middleware */
const OrganizerPageMiddleware = lazy(() => import("@utils/middlewares/organizer"))

const EntryPage = lazy(() => import("@pages/dashboard/entry"))
const LoginPage = lazy(() => import("@pages/dashboard/entry/outlets/dashboard-entry-login"))
const RegisterPage = lazy(() => import("@pages/dashboard/entry/outlets/dashboard-entry-register"))
const DashboardHomePage = lazy(() => import("@pages/dashboard/home"))
const CreateLombaPage = lazy(() => import("@pages/dashboard/create"))
const DashboardProfilePage = lazy(() => import("@pages/dashboard/profile"))
const LombaDetailPage = lazy(() => import("@pages/dashboard/lomba-detail"))
const LombaPreviewPage = lazy(() => import("@pages/dashboard/lomba-preview"))
const UpdateLombaPage = lazy(() => import("@pages/dashboard/update"))

// modules as pages
const MainDetailOutletPage = lazy(() => import("@modules/dashboard/lomba-detail/main-detail"))
const CancelationDetailOutletPage = lazy(
    () => import("@modules/dashboard/lomba-detail/detail-cancelation")
)

const dashboardOrganizerRoutes: RouteObject[] = [
    {
        path: "/dashboard",
        element: (
            <Loader>
                <OrganizerPageMiddleware />
            </Loader>
        ),
        children: [
            {
                path: "entry",
                element: (
                    <Loader>
                        <EntryPage />
                    </Loader>
                ),
                children: [
                    { index: true, element: <Navigate to="login" /> },
                    {
                        path: "login",
                        element: (
                            <Loader>
                                <LoginPage />
                            </Loader>
                        ),
                    },
                    {
                        path: "register",
                        element: (
                            <Loader>
                                <RegisterPage />
                            </Loader>
                        ),
                    },
                ],
            },
            {
                path: "home",
                children: [
                    {
                        index: true,
                        element: (
                            <Loader>
                                <DashboardHomePage />
                            </Loader>
                        ),
                    },
                    {
                        path: "create",
                        element: (
                            <Loader>
                                <CreateLombaPage />
                            </Loader>
                        ),
                    },
                    {
                        path: "lomba/:lombaId",
                        element: (
                            <Loader>
                                <LombaDetailPage />
                            </Loader>
                        ),
                        children: [
                            { index: true, element: <Navigate to="main" /> },
                            {
                                path: "main",
                                element: (
                                    <Loader>
                                        <MainDetailOutletPage />
                                    </Loader>
                                ),
                            },
                            {
                                path: "cancelation",
                                element: (
                                    <Loader>
                                        <CancelationDetailOutletPage />
                                    </Loader>
                                ),
                            },
                        ],
                    },
                    {
                        path: "lomba/:lombaId/preview",
                        element: (
                            <Loader>
                                <LombaPreviewPage />
                            </Loader>
                        ),
                    },
                    {
                        path: "lomba/:lombaId/update",
                        element: (
                            <Loader>
                                <UpdateLombaPage />
                            </Loader>
                        ),
                    },
                ],
            },
            {
                path: "profile",
                element: (
                    <Loader>
                        <DashboardProfilePage />
                    </Loader>
                ),
            },
        ],
    },
]

export default dashboardOrganizerRoutes
