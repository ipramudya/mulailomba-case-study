import ErrorPage from "@pages/seeker/error"
import LandingPage from "@pages/seeker/landing"
import Loader from "@ui/loader"
import { lazy } from "react"
import { Navigate, RouteObject } from "react-router-dom"

/* root component as middleware */
const RootElement = lazy(() => import("@utils/root-element"))
const AllLombaPage = lazy(() => import("@pages/seeker/lomba/all-lomba"))
const DetailLombaPage = lazy(() => import("@pages/seeker/lomba/detail-lomba"))
const OrganizerPage = lazy(() => import("@pages/seeker/organizer"))
const OrganizerDetailPage = lazy(() => import("@pages/seeker/organizer/outlets/organizer-detail"))
const OrganizerExplorePage = lazy(() => import("@pages/seeker/organizer/outlets/organizer-explore"))
const OrganizerResetPassword = lazy(
    () => import("@pages/dashboard/entry/outlets/dashboard-entry-reset-password")
)
const EntryPage = lazy(() => import("@pages/seeker/entry"))
const LoginPage = lazy(() => import("@pages/seeker/entry/outlets/seeker-entry-login"))
const RegisterPage = lazy(() => import("@pages/seeker/entry/outlets/seeker-entry-register"))
const ResetPasswordPage = lazy(
    () => import("@pages/seeker/entry/outlets/seeker-entry-reset-password")
)

const seekerRoutes: RouteObject[] = [
    {
        path: "/",
        errorElement: <ErrorPage />,
        element: (
            <Loader>
                <RootElement fallbackUrl="/" />
            </Loader>
        ),
        children: [
            {
                index: true,
                element: <LandingPage />,
            },

            /* 👇 page for lomba */
            {
                path: "lomba",
                element: (
                    <Loader>
                        <AllLombaPage />
                    </Loader>
                ),
            },
            {
                path: "lomba/:lombaId",
                element: (
                    <Loader>
                        <DetailLombaPage />
                    </Loader>
                ),
            },

            /* 👇 page for organizer */
            {
                path: "organizer",
                element: (
                    <Loader>
                        <OrganizerPage />
                    </Loader>
                ),
                children: [
                    {
                        index: true,
                        element: <Navigate to="explore" />,
                    },
                    {
                        path: "explore",
                        element: (
                            <Loader>
                                <OrganizerExplorePage />
                            </Loader>
                        ),
                    },
                    {
                        path: "detail/:organizerId",
                        element: (
                            <Loader>
                                <OrganizerDetailPage />
                            </Loader>
                        ),
                    },
                ],
            },
            {
                path: "entry",
                element: (
                    <Loader>
                        <EntryPage />
                    </Loader>
                ),
                children: [
                    {
                        index: true,
                        element: <Navigate to="login" />,
                    },
                    {
                        path: "login",
                        element: (
                            <Loader>
                                <LoginPage />
                            </Loader>
                        ),
                    },
                    {
                        path: "register",
                        element: (
                            <Loader>
                                <RegisterPage />
                            </Loader>
                        ),
                    },
                ],
            },
            {
                path: "seeker/reset-password",
                element: (
                    <Loader>
                        <ResetPasswordPage />
                    </Loader>
                ),
            },
            {
                path: "dashboard/reset-password",
                element: (
                    <Loader>
                        <OrganizerResetPassword />
                    </Loader>
                ),
            },
        ],
    },
]

export default seekerRoutes
