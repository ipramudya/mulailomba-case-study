import { createBrowserRouter } from "react-router-dom"

import dashboardOrganizerRoutes from "./routes/dashboard-organizer-routes"
import seekerRoutes from "./routes/seeker-routes"

export default createBrowserRouter([...seekerRoutes, ...dashboardOrganizerRoutes])
