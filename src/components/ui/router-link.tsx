import { FunctionComponent } from "react"
import { Link, LinkProps, useLocation } from "react-router-dom"

const RouterLink: FunctionComponent<LinkProps> = ({ children, ...restProps }) => {
    const location = useLocation()

    return (
        <Link {...restProps} state={{ from: location, ...restProps.state }}>
            {children}
        </Link>
    )
}

export default RouterLink
