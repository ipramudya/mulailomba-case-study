import { type VariantProps, cva } from "class-variance-authority"

const typographyStyles = cva("text-inherit", {
    variants: {
        intent: {
            primary: "text-neutral-800",
            secondary: "text-neutral-600",
        },
        weight: {
            400: "font-normal",
            500: "font-medium",
            600: "font-semibold",
            700: "font-bold",
        },
        size: {
            xxs: "text-[0.625rem] leading-3",
            xs: "text-xs",
            sm: "text-sm",
            base: "text-base leading-5",
            lg: "text-lg leading-6",
            h3: "text-xl",
            h2: "text-2xl",
            h1: "text-3xl",
            hero: "text-4xl leading-[46px]",
        },
    },
    compoundVariants: [
        { size: "h1", weight: 600 },
        { size: "h2", weight: 500 },
        { size: "h3", weight: 500 },
    ],
    defaultVariants: {
        weight: 400,
        size: "base",
    },
})

export default typographyStyles
export type TypographyStyles = VariantProps<typeof typographyStyles>
