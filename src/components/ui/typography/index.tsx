import { AllHTMLAttributes, ReactHTML, createElement, forwardRef } from "react"
import { twMerge } from "tailwind-merge"

import typographyStyles, { TypographyStyles } from "./typography.styles"

interface Props extends Omit<AllHTMLAttributes<ReactHTML>, "size">, TypographyStyles {
    as?: keyof ReactHTML
    before?: string
}

const Typography = forwardRef<any, Props>(
    (
        {
            weight,
            size,
            as = "p",
            className = "",
            children = [],
            onClick = undefined,
            ...restProps
        },
        forwardedRef
    ) =>
        createElement(
            as,
            {
                className: twMerge(
                    typographyStyles({
                        weight,
                        size,
                        intent: ["h1", "h2", "h3", "strong"].includes(as) ? "primary" : "secondary",
                    }),
                    onClick ? "cursor-pointer select-none" : "",
                    className
                ),
                ...restProps,
                onClick,
                ref: forwardedRef,
            },
            children
        )
)

Typography.displayName = "typography"
export default Typography
