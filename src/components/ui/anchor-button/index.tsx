import { Icon, IconifyIcon } from "@iconify/react"
import iconStyles from "@ui/_styles/icon.styles"
import buttonStyles, { ButtonStyles } from "@ui/button/button.styles"
import { AnchorHTMLAttributes, FunctionComponent, createElement } from "react"
import { Link } from "react-router-dom"
import { twMerge } from "tailwind-merge"

interface Props extends AnchorHTMLAttributes<HTMLAnchorElement>, Omit<ButtonStyles, "disabled"> {
    startIcon?: string | IconifyIcon
    endIcon?: string | IconifyIcon
    label: string
    loading?: boolean
    iconSize?: "xs" | "sm" | "md" | null
    thin?: boolean
    hasDot?: boolean
    tag?: "a" | "Link"
    newTab?: boolean
    relative?: "path"
}

const AnchorButton: FunctionComponent<Props> = ({
    intent,
    pill,
    size = "sm",
    label,
    loading = false,
    startIcon,
    endIcon,
    className,
    iconSize = null,
    thin = false,
    hasDot = false,
    tag = "a",
    newTab,
    href,
    ...restProps
}) => {
    const Slot = () => (
        <>
            {(startIcon || loading) && (
                <Icon
                    icon={loading ? "ri:loader-5-fill" : (startIcon as any)}
                    className={iconStyles({ iconSize: iconSize || size, loading })}
                />
            )}
            <span
                className={twMerge(
                    "relative max-w-fit text-ellipsis text-inherit",
                    thin ? "font-normal" : "font-medium",
                    fontSizes.get("sm"),
                    hasDot
                        ? "after:absolute after:-right-3 after:h-2 after:w-2 after:rounded-full after:bg-danger-400 after:content-[''] "
                        : undefined
                )}
            >
                {label}
            </span>
            {endIcon && !loading && (
                <Icon icon={endIcon} className={iconStyles({ iconSize: iconSize || size })} />
            )}
        </>
    )

    return createElement<any>(
        tag === "a" ? "a" : Link,
        {
            className: twMerge("cursor-pointer", buttonStyles({ intent, pill, size }), className),
            ...(tag === "Link" ? { to: href } : { href }),
            ...(newTab ? { target: "_blank", rel: "noopener noreferrer" } : {}),
            ...restProps,
        },
        <Slot />
    )
}

const fontSizes = new Map([
    ["sm", "text-sm"],
    ["md", "text-sm"],
    ["xs", "text-xs"],
    [null, "text-sm"],
])

export default AnchorButton
