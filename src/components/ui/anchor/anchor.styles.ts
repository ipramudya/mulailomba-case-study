import { VariantProps, cva } from "class-variance-authority"

const anchorStyles = cva(
    "text-inherit cursor-pointer transition duration-300 hover:underline hover:underline-offset-[6px] hover:opacity-80",
    {
        variants: {
            intent: {
                light: "text-neutral-300",
                dark: "text-neutral-300",
                primary: "text-primary-300",
            },
            active: {
                true: "underline underline-offset-[6px]",
            },
            ignoreUnderline: {
                true: "!no-underline",
            },
        },
        compoundVariants: [
            {
                intent: "light",
                active: true,
                className: "text-white",
            },
            {
                intent: "dark",
                active: true,
                className: "text-neutral-700",
            },
            {
                intent: "primary",
                active: true,
                className: "text-primary-500",
            },
        ],
        defaultVariants: {
            intent: "dark",
            active: false,
        },
    }
)

export default anchorStyles
export type AnchorStyles = VariantProps<typeof anchorStyles>
