import { FunctionComponent, PropsWithChildren } from "react"
import { NavLink, type NavLinkProps } from "react-router-dom"
import { twMerge } from "tailwind-merge"

import Typography from "../typography/index"
import { TypographyStyles } from "../typography/typography.styles"
import anchorStyles, { AnchorStyles } from "./anchor.styles"

interface Props
    extends PropsWithChildren,
        AnchorStyles,
        Omit<TypographyStyles, "intent">,
        Omit<NavLinkProps, "children"> {
    className?: string
    passCustomActive?: boolean
    active?: boolean
}

const Anchor: FunctionComponent<Props> = ({
    to,
    children,
    size,
    weight,
    className,
    intent,
    active = null,
    ...restProps
}) => {
    return (
        <NavLink end={to === "/"} to={to} {...restProps}>
            {({ isActive }) => (
                <Typography
                    as="span"
                    size={size}
                    weight={weight}
                    className={twMerge(
                        anchorStyles({
                            intent,
                            active: active || isActive, // pakai boolean value dari navlink atau component's param
                        }),
                        className
                    )}
                >
                    {children}
                </Typography>
            )}
        </NavLink>
    )
}

export default Anchor
