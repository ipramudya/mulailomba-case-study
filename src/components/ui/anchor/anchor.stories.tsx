import { ComponentMeta, ComponentStory } from "@storybook/react"

import Anchor from "."

export default {
    title: "Anchor",
    component: Anchor,
    argTypes: {
        backgroundColor: { control: "color" },
    },
} as ComponentMeta<typeof Anchor>

const Template: ComponentStory<typeof Anchor> = (args) => <Anchor {...args}>{args.children}</Anchor>

export const Main = Template.bind({})
Main.args = {
    children: "Going to homepage",
    className: "",
    active: false,
}
