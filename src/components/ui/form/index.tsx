import { ForwardRefExoticComponent, MutableRefObject, PropsWithChildren, forwardRef } from "react"
import { FormProvider, SubmitHandler, UseFormReturn } from "react-hook-form"

import FormCreatableSelectField from "./fields/creatable-select"
import FormFileField from "./fields/file"
import FormRadioField from "./fields/radio"
import FormSelectField from "./fields/select"
import FormTextField from "./fields/text"
import FormTextareaField from "./fields/textarea"
import FormLayout from "./form-layout"
import FormLayoutRow from "./form-layout-row"

interface Props extends UseFormReturn<any>, PropsWithChildren {
    onSubmit: SubmitHandler<any>
    className?: string
}

type FormEntriesType<T> = ForwardRefExoticComponent<T & { ref?: MutableRefObject<any> }> & {
    Layout: typeof FormLayout
    RowLayout: typeof FormLayoutRow
    TextField: typeof FormTextField
    SelectField: typeof FormSelectField
    CreatableSelectField: typeof FormCreatableSelectField
    RadioField: typeof FormRadioField
    TextareaField: typeof FormTextareaField
    FileField: typeof FormFileField
}

const Form: FormEntriesType<Props> = forwardRef<any, Props>(
    ({ children, onSubmit, className = "", ...restProps }, forwardedRef) => (
        <FormProvider {...restProps}>
            <form
                className={className}
                onSubmit={restProps.handleSubmit(onSubmit)}
                ref={forwardedRef}
            >
                {children}
            </form>
        </FormProvider>
    )
) as any

Form.Layout = FormLayout
Form.RowLayout = FormLayoutRow
Form.TextField = FormTextField
Form.SelectField = FormSelectField
Form.CreatableSelectField = FormCreatableSelectField
Form.RadioField = FormRadioField
Form.TextareaField = FormTextareaField
Form.FileField = FormFileField

Form.displayName = "form"
export default Form
