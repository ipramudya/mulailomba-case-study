import { Icon } from "@iconify/react"
import { FunctionComponent, PropsWithChildren } from "react"
import { twMerge } from "tailwind-merge"

import iconStyles from "../_styles/icon.styles"
import Typography from "../typography"
import formStyles, { FormHelperTextStyles } from "./form.styles"

interface Props extends PropsWithChildren, FormHelperTextStyles {
    className?: string
}

const FormHelperText: FunctionComponent<Props> = ({
    icon,
    children,
    className,
    intent,
    defaultColor,
}) => {
    return (
        <Typography
            size="xs"
            className={twMerge(
                "flex items-center justify-start space-x-1",
                formStyles.helperText({ intent, defaultColor }),
                className
            )}
        >
            {icon && (
                <Icon
                    icon={formStyles.helperText({ icon })}
                    className={iconStyles({ iconSize: "sm" })}
                />
            )}
            <span className="text-inherit">{children}</span>
        </Typography>
    )
}

export default FormHelperText
