import { FunctionComponent, PropsWithChildren } from "react"

import Typography from "../typography"
import formStyles, { FormRowLayoutStyles } from "./form.styles"

interface Props extends PropsWithChildren, FormRowLayoutStyles {
    headline?: string
    className?: string
}

const FormLayoutRow: FunctionComponent<Props> = ({
    headline,
    className,
    children,
    column,
    space,
}) => {
    return (
        <div className={className}>
            {headline && (
                <Typography size="sm" weight={500}>
                    {headline}
                </Typography>
            )}
            <div className={formStyles.rowLayout({ column, space })}>{children}</div>
        </div>
    )
}

export default FormLayoutRow
