import { FunctionComponent, PropsWithChildren } from "react"
import { twMerge } from "tailwind-merge"

import formStyles, { FormLayoutStyles } from "./form.styles"

interface Props extends PropsWithChildren, FormLayoutStyles {
    className?: string
}

const FormLayout: FunctionComponent<Props> = ({ children, className, space }) => (
    <div className={twMerge(formStyles.layout({ space }), className)}>{children}</div>
)

export default FormLayout
