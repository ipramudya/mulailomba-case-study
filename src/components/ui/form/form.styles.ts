import { VariantProps, cva } from "class-variance-authority"

const formLayoutStyles = cva("w-full", {
    variants: {
        space: {
            normal: "space-y-4",
            wider: "space-y-6",
        },
    },
    defaultVariants: {
        space: "normal",
    },
})

const formRowLayoutStyles = cva("grid w-full", {
    variants: {
        space: {
            normal: "gap-4",
            wider: "gap-6",
        },
        column: {
            1: "grid-cols-1",
            2: "grid-cols-2",
            3: "grid-cols-3",
        },
    },
    defaultVariants: {
        column: 1,
        space: "normal",
    },
})

const formHelperTextStyles = cva("", {
    variants: {
        intent: {
            error: "text-danger-600",
            success: "text-success-600",
            info: "text-primary-600",
        },
        icon: {
            error: "heroicons:exclamation-triangle",
            success: "heroicons:check-circle",
            info: "heroicons:information-circle",
        },
        defaultColor: {
            true: "!text-neutral-400",
            false: "",
        },
    },
})

const formFieldWrapper = cva(
    `
    flex border transition duration-300 min-h-[40px]
    focus-within:outline-1 focus-within:outline-dashed focus-within:outline-offset-2 
    `,
    {
        variants: {
            size: {
                xs: "px-2 py-1 rounded text-xs",
                sm: "px-[10px] py-[6px] rounded text-sm",
                md: "px-3 py-2 rounded-md tx-sm",
            },
            error: {
                true: "focus-within:outline-danger-400 border-danger-500",
                false: "focus-within:outline-neutral-400 border-neutral-200",
            },
            hasCount: {
                true: "pb-6",
            },
        },
        defaultVariants: {
            error: false,
            size: "sm",
        },
    }
)

const formFieldStyles = cva(
    `
        block w-full appearance-none outline-none border-none text-neutral-800
        placeholder:text-neutral-300 placeholder:font-normal
    `,
    {
        variants: {
            size: {
                xs: "text-xs placeholder:text-xs",
                sm: "text-sm placeholder:text-sm",
                md: "text-sm placeholder:text-sm",
            },
        },
        defaultVariants: {
            size: "sm",
        },
    }
)

const formTextareaFieldStyles = cva(
    " block w-full appearance-none outline-none border-none text-neutral-800 placeholder:text-neutral-300 placeholder:font-normal",
    {
        variants: {
            size: {
                xs: "text-xs placeholder:text-xs min-h-[62px]",
                sm: "text-sm placeholder:text-sm min-h-[86px]",
                md: "text-sm placeholder:text-sm min-h-[104px]",
            },
            resized: {
                false: "resize-none",
                true: "resize-y",
            },
        },
        defaultVariants: {
            resized: false,
            size: "sm",
        },
    }
)

export default {
    layout: formLayoutStyles,
    rowLayout: formRowLayoutStyles,
    helperText: formHelperTextStyles,
    fieldWrapper: formFieldWrapper,
    field: formFieldStyles,
    textarea: formTextareaFieldStyles,
}
export type FormLayoutStyles = VariantProps<typeof formLayoutStyles>
export type FormRowLayoutStyles = VariantProps<typeof formRowLayoutStyles>
export type FormHelperTextStyles = VariantProps<typeof formHelperTextStyles>
export type FormFieldWrapperStyles = VariantProps<typeof formFieldWrapper>
export type FormFieldStyles = VariantProps<typeof formFieldStyles>
export type FormTextareaStyles = VariantProps<typeof formTextareaFieldStyles>
