import { Icon } from "@iconify/react"
import iconStyles from "@ui/_styles/icon.styles"
import Typography from "@ui/typography"
import { isEmpty } from "lodash-es"
import { FunctionComponent, HTMLProps, useEffect } from "react"
import { DropzoneOptions, useDropzone } from "react-dropzone"
import { Controller, useFormContext } from "react-hook-form"
import { twMerge } from "tailwind-merge"

import FormHelperText from "../form-helper-text"
import formStyles, { FormFieldWrapperStyles } from "../form.styles"

interface Props
    extends Omit<HTMLProps<HTMLInputElement>, "size" | "label" | "className">,
        Pick<FormFieldWrapperStyles, "size"> {
    name: string
    label: string | false
    hint?: string
    hideHintIcon?: boolean
    fileIcon?: string
    ignoreError?: boolean
    dropzoneOptions?: DropzoneOptions
    isOptional?: boolean
    classNames?: {
        wrapper?: string
        input?: string
    }
}

const FormFileField: FunctionComponent<Props> = ({
    name,
    label,
    hint,
    hideHintIcon,
    required,
    fileIcon = "heroicons:paper-clip",
    ignoreError,
    dropzoneOptions,
    classNames,
    isOptional,
    size = "sm",
    ...restProps
}) => {
    const {
        unregister,
        control,
        formState: { errors },
    } = useFormContext()

    const { getInputProps, getRootProps, acceptedFiles } = useDropzone({
        accept: {
            "application/pdf": [".pdf"],
        },
        maxFiles: 1,
        noDrag: true,
        ...dropzoneOptions,
    })

    useEffect(() => {
        return () => {
            unregister(name)
        }
    }, [name, unregister])

    return (
        <div className="flex flex-col space-y-2">
            {/* 👇 input label */}
            <div className="flex flex-col space-y-1">
                {Boolean(label) && (
                    <Typography size="sm">
                        <span>{label}</span>
                        {required && (
                            <Typography as="span" className="pl-1 text-danger-500">
                                &#42;
                            </Typography>
                        )}
                        {isOptional && (
                            <Typography as="span" size="xs" className="pl-1 text-neutral-400">
                                (opsional)
                            </Typography>
                        )}
                    </Typography>
                )}
                {Boolean(hint) && (
                    <FormHelperText
                        intent="info"
                        icon={!hideHintIcon ? "info" : null}
                        className="text-neutral-400"
                    >
                        {hint}
                    </FormHelperText>
                )}
            </div>

            <Controller
                render={({ field: { onChange } }) => (
                    <div
                        {...getRootProps()}
                        className={twMerge(
                            "cursor-pointer items-center justify-between",
                            formStyles.fieldWrapper({ size, error: Boolean(errors.name) }),
                            classNames?.wrapper
                        )}
                    >
                        {/* 👇 field */}
                        <input
                            {...getInputProps({
                                onChange: (e) => {
                                    onChange(e.target.files?.[0] ?? null)
                                },
                            })}
                        />

                        <Typography
                            className={twMerge(
                                "text-sm",
                                isEmpty(acceptedFiles)
                                    ? "text-[rgb(186,186,186)]"
                                    : "text-neutral-800"
                            )}
                        >
                            {isEmpty(acceptedFiles) ? restProps.placeholder : acceptedFiles[0].name}
                        </Typography>

                        <div className="inline-flex items-center pr-3 text-neutral-600">
                            <Icon icon={fileIcon} className={iconStyles({ iconSize: size })} />
                        </div>
                    </div>
                )}
                name={name}
                control={control}
            />

            {/* 👇 error message */}
            {Boolean(errors[name]) && !ignoreError && (
                <FormHelperText intent="error" icon="error" className="flex items-center space-x-1">
                    {errors?.[name]?.message as any}
                </FormHelperText>
            )}
        </div>
    )
}

FormFileField.displayName = "file field"
export default FormFileField
