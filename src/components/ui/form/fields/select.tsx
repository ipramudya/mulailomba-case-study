import BaseSelect, { SelectProps } from "@ui/select"
import Typography from "@ui/typography"
import { FunctionComponent, useEffect } from "react"
import { Controller, useFormContext } from "react-hook-form"

import FormHelperText from "../form-helper-text"

interface Props extends SelectProps {
    name: string
    label: string | false
    hint?: string
    hideHintIcon?: boolean
    ignoreError?: boolean
    isOptional?: boolean
}

const FormSelectField: FunctionComponent<Props> = ({
    name,
    label,
    hint,
    options,
    hideHintIcon,
    required,
    ignoreError = false,
    defaultValue,
    isOptional,
    ...restProps
}) => {
    const {
        unregister,
        control,
        formState: { errors },
    } = useFormContext()

    useEffect(() => {
        return () => {
            unregister(name)
        }
    }, [name, unregister])

    return (
        <div className="flex flex-col space-y-2">
            {/* 👇 input label */}
            {(Boolean(label) || Boolean(hint)) && (
                <div>
                    {Boolean(label) && (
                        <Typography size="sm">
                            <span>{label}</span>
                            {required && (
                                <Typography as="span" className="pl-1 text-danger-500">
                                    &#42;
                                </Typography>
                            )}
                            {isOptional && (
                                <Typography as="span" size="xs" className="pl-1 text-neutral-400">
                                    (opsional)
                                </Typography>
                            )}
                        </Typography>
                    )}
                    {Boolean(hint) && (
                        <FormHelperText intent="info" icon={!hideHintIcon ? "info" : null}>
                            {hint}
                        </FormHelperText>
                    )}
                </div>
            )}

            {/* 👇 select input */}
            <Controller
                name={name}
                control={control}
                render={({ field: { value, onChange, ...restField } }) => (
                    <BaseSelect
                        {...restField}
                        {...restProps}
                        options={options}
                        value={options.find((c) => c.value === value)}
                        onChange={(val: any) => onChange(val?.value ?? "")}
                    />
                )}
                defaultValue={defaultValue}
            />

            {/* 👇 error message */}
            {Boolean(errors[name]) && !ignoreError && (
                <FormHelperText intent="error" icon="error">
                    {errors?.[name]?.message as any}
                </FormHelperText>
            )}
        </div>
    )
}

export default FormSelectField
