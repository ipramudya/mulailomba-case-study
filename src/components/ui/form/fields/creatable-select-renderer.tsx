import BaseSelect, { CreatableSelectProps } from "@ui/creatable-select"
import { useSetAtom } from "jotai"
import { FunctionComponent, useEffect } from "react"
import { ControllerRenderProps, FieldValues } from "react-hook-form"

import { ShareableOptionsAtom } from "./creatable-select"

interface Props extends CreatableSelectProps {
    field: ControllerRenderProps<FieldValues, string>
    onCreateChange?: (val: any) => void
}

const CreatableSelectRenderer: FunctionComponent<Props> = ({
    onCreateChange,
    options,
    field,
    ...restProps
}) => {
    const { onChange, ...rest } = field

    const setShareableAtom = useSetAtom(ShareableOptionsAtom)

    useEffect(() => {
        setShareableAtom(rest.value)
    }, [rest.value, setShareableAtom])

    return (
        <BaseSelect
            options={options}
            {...rest}
            onChange={(value: any) => {
                if (!value) return onChange("")

                if (Array.isArray(value)) {
                    const newValue = value.map((option: any) => {
                        if (typeof option === "string") {
                            return option
                        } else {
                            return option.value
                        }
                    })
                    onChange(newValue)

                    if (onCreateChange) {
                        onCreateChange(newValue)
                    }
                } else {
                    onChange(value.value)
                    if (onCreateChange) {
                        onCreateChange(value.value)
                    }
                }
            }}
            {...restProps}
        />
    )
}

export default CreatableSelectRenderer
