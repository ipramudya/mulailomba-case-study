import Typography from "@ui/typography"
import { HTMLProps, forwardRef, useEffect, useRef, useState } from "react"
import { useFormContext } from "react-hook-form"
import { mergeRefs } from "react-merge-refs"
import { twMerge } from "tailwind-merge"

import FormHelperText from "../form-helper-text"
import formStyles, { FormTextareaStyles } from "../form.styles"

interface Props
    extends Omit<HTMLProps<HTMLTextAreaElement>, "size" | "label" | "className">,
        FormTextareaStyles {
    name: string
    label: string | false
    hint?: string
    hideHintIcon?: boolean
    ignoreError?: boolean
    hasCount?: boolean
    isOptional?: boolean
    classNames?: {
        wrapper?: string
        input?: string
    }
}

const FormTextareaField = forwardRef<HTMLTextAreaElement, Props>(
    (
        {
            name,
            label,
            hint,
            hideHintIcon,
            ignoreError,
            required,
            classNames,
            size = "sm",
            resized = false,
            hasCount = false,
            isOptional,
            ...restProps
        },
        forwardedRef
    ) => {
        const [textareaCount, setTextareaCount] = useState(0)
        const cleanupFlag = useRef(false)

        const {
            register,
            unregister,
            formState: { errors },
            getValues,
        } = useFormContext()

        useEffect(() => {
            return () => {
                if (hasCount) {
                    cleanupFlag.current ? unregister(name) : (cleanupFlag.current = true)
                }
                unregister(name)
            }

            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [cleanupFlag.current])

        useEffect(() => {
            if (hasCount && getValues(name)) {
                const val = getValues(name)
                setTextareaCount(val.length)
            }
        }, [getValues, hasCount, name])

        return (
            <div className="flex flex-col space-y-2">
                {/* 👇 input label */}
                <div className="flex flex-col space-y-1">
                    {Boolean(label) && (
                        <Typography size="sm">
                            <span>{label}</span>
                            {required && (
                                <Typography as="span" className="pl-1 text-danger-500">
                                    &#42;
                                </Typography>
                            )}
                            {isOptional && (
                                <Typography as="span" size="xs" className="pl-1 text-neutral-400">
                                    (opsional)
                                </Typography>
                            )}
                        </Typography>
                    )}
                    {Boolean(hint) && (
                        <FormHelperText
                            intent="info"
                            icon={!hideHintIcon ? "info" : null}
                            className="text-neutral-400"
                        >
                            {hint}
                        </FormHelperText>
                    )}
                </div>

                {/* field */}
                <div
                    className={twMerge(
                        formStyles.fieldWrapper({ size, error: Boolean(errors.name), hasCount }),
                        "relative",
                        classNames?.wrapper
                    )}
                >
                    <textarea
                        {...register(name)}
                        {...restProps}
                        ref={mergeRefs([forwardedRef, register(name).ref])}
                        className={twMerge(
                            formStyles.textarea({ size, resized }),
                            classNames?.input
                        )}
                        onChange={
                            hasCount ? (e) => setTextareaCount(e.target.value.length) : undefined
                        }
                    />

                    {hasCount && restProps.maxLength && (
                        <Typography
                            as="span"
                            size="xxs"
                            className={twMerge(
                                "absolute right-0 bottom-0 mr-2 mb-2 select-none",
                                textareaCount >= restProps.maxLength ? "text-danger-600" : undefined
                            )}
                        >
                            {textareaCount} &#47; {restProps.maxLength}
                        </Typography>
                    )}
                </div>

                {/* 👇 error message */}
                {Boolean(errors[name]) && !ignoreError && (
                    <FormHelperText intent="error" icon="error">
                        {errors?.[name]?.message as any}
                    </FormHelperText>
                )}
            </div>
        )
    }
)

FormTextareaField.displayName = "textarea"
export default FormTextareaField
