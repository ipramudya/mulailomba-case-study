import { CreatableSelectProps } from "@ui/creatable-select"
import Typography from "@ui/typography"
import { atom, useAtomValue } from "jotai"
import { FunctionComponent, useEffect, useMemo } from "react"
import { Controller, ControllerProps, useFormContext } from "react-hook-form"

import FormHelperText from "../form-helper-text"
import CreatableSelectRenderer from "./creatable-select-renderer"

interface Props extends CreatableSelectProps {
    name: string
    label: string | false
    hint?: string
    hideHintIcon?: boolean
    control?: Pick<ControllerProps, "control">
    ignoreError?: boolean
    onCreateChange?: (val: any) => void
}

export const ShareableOptionsAtom = atom<null | string[]>(null)

const CreatableSelect: FunctionComponent<Props> = ({
    name,
    label,
    hint,
    options,
    hideHintIcon,
    required,
    ignoreError = false,
    defaultValue,
    onCreateChange,
    ...restProps
}) => {
    const shareableAtom = useAtomValue(ShareableOptionsAtom)

    const {
        unregister,
        control,
        formState: { errors },
    } = useFormContext()

    useEffect(() => {
        return () => {
            unregister(name)
        }
    }, [name, unregister])

    const getOptions = useMemo(() => {
        return options.filter((o) => !shareableAtom?.includes(o.value))
    }, [shareableAtom, options])

    return (
        <div className="flex flex-col space-y-2">
            {/* 👇 input label */}
            {(Boolean(label) || Boolean(hint)) && (
                <div>
                    {Boolean(label) && (
                        <Typography size="sm">
                            <span>{label}</span>
                            {required && (
                                <Typography as="span" className="pl-1 text-danger-500">
                                    &#42;
                                </Typography>
                            )}
                        </Typography>
                    )}
                    {Boolean(hint) && (
                        <FormHelperText intent="info" icon={!hideHintIcon ? "info" : null}>
                            {hint}
                        </FormHelperText>
                    )}
                </div>
            )}

            {/* 👇 select input */}
            <Controller
                name={name}
                control={control}
                defaultValue={defaultValue}
                render={({ field }) => (
                    <CreatableSelectRenderer
                        options={getOptions}
                        onCreateChange={onCreateChange}
                        field={field}
                        {...restProps}
                    />
                )}
            />

            {/* 👇 error message */}
            {Boolean(errors[name]) && !ignoreError && (
                <FormHelperText intent="error" icon="error">
                    {errors?.[name]?.message as any}
                </FormHelperText>
            )}
        </div>
    )
}

export default CreatableSelect
