import { Icon, IconProps } from "@iconify/react"
import { HTMLProps, forwardRef, useEffect } from "react"
import { useFormContext } from "react-hook-form"
import { mergeRefs } from "react-merge-refs"
import { twMerge } from "tailwind-merge"

import iconStyles from "../../_styles/icon.styles"
import Typography from "../../typography"
import FormHelperText from "../form-helper-text"
import formStyles, { FormFieldWrapperStyles } from "../form.styles"

interface Props
    extends Omit<HTMLProps<HTMLInputElement>, "size" | "label" | "className">,
        Pick<FormFieldWrapperStyles, "size"> {
    name: string
    label: string | false
    hint?: string
    hideHintIcon?: boolean
    startIcon?: string | IconProps["icon"]
    endIcon?: string | IconProps["icon"]
    ignoreError?: boolean
    isOptional?: boolean
    classNames?: {
        wrapper?: string
        input?: string
    }
}

const FormTextField = forwardRef<HTMLInputElement, Props>(
    (
        {
            name,
            label,
            hint,
            hideHintIcon,
            startIcon,
            endIcon,
            required,
            classNames,
            size = "sm",
            ignoreError = false,
            isOptional,
            ...restProps
        },
        forwardedRef
    ) => {
        const { register, unregister, formState } = useFormContext()

        useEffect(() => {
            return () => {
                unregister(name)
            }
        }, [name, unregister])

        return (
            <div className="flex flex-col space-y-2">
                {/* 👇 input label */}
                <div className="flex flex-col">
                    {Boolean(label) && (
                        <Typography size="sm">
                            <span>{label}</span>
                            {required && (
                                <Typography as="span" className="pl-1 text-danger-500">
                                    &#42;
                                </Typography>
                            )}
                            {isOptional && (
                                <Typography size="xs" as="span" className="pl-1 text-neutral-400">
                                    (opsional)
                                </Typography>
                            )}
                        </Typography>
                    )}
                    {Boolean(hint) && (
                        <FormHelperText
                            defaultColor
                            icon={!hideHintIcon ? "info" : null}
                            className="text-neutral-400"
                        >
                            {hint}
                        </FormHelperText>
                    )}
                </div>

                {/* 👇 input field */}
                <div
                    className={twMerge(
                        formStyles.fieldWrapper({ size, error: Boolean(formState.errors.name) }),
                        classNames?.wrapper
                    )}
                >
                    {startIcon && (
                        <div className="inline-flex items-center pr-3 text-neutral-600">
                            <Icon icon={startIcon} className={iconStyles({ iconSize: size })} />
                        </div>
                    )}

                    {/* 👇 field */}
                    <input
                        className={twMerge(formStyles.field({ size }), classNames?.input)}
                        {...register(name)}
                        ref={mergeRefs([forwardedRef, register(name).ref])}
                        {...restProps}
                    />

                    {endIcon && (
                        <div className="inline-flex items-center pr-3 text-neutral-600">
                            <Icon icon={endIcon} className={iconStyles({ iconSize: size })} />
                        </div>
                    )}
                </div>

                {/* 👇 error message */}
                {Boolean(formState.errors[name]) && !ignoreError && (
                    <FormHelperText
                        intent="error"
                        icon="error"
                        className="flex items-center space-x-1"
                    >
                        {formState.errors?.[name]?.message as any}
                    </FormHelperText>
                )}
            </div>
        )
    }
)

FormTextField.displayName = "text field"
export default FormTextField
