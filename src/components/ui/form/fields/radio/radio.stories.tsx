import { ComponentMeta, ComponentStory } from "@storybook/react"

import FormRadioField from "."

export default {
    title: "Radio",
    component: FormRadioField,
    argTypes: {
        backgroundColor: { control: "color" },
    },
} as ComponentMeta<typeof FormRadioField>

const Template: ComponentStory<typeof FormRadioField> = (args) => <FormRadioField {...args} />
export const Main = Template.bind({})
Main.args = {
    label: "Radio Button",
    defaultValue: "1",
    direction: "column",
    size: "md",
    options: [
        {
            label: "first option",
            value: "1",
        },
        {
            label: "second option",
            value: "2",
        },
        {
            label: "third option",
            value: "3",
        },
    ],
}
