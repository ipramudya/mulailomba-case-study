import { VariantProps, cva } from "class-variance-authority"

const radioRootStyles = cva("flex", {
    variants: {
        direction: {
            row: "flex-row space-x-2",
            column: "flex-col space-y-2",
        },
        split: {
            true: "grid grid-cols-[repeat(auto-fill,_minmax(200px,_1fr))]",
            false: "justify-start",
        },
    },
    defaultVariants: {
        direction: "row",
        split: false,
    },
})

const radioItemWrapper = cva("flex items-center", {
    variants: {
        size: {
            sm: "space-x-2",
            md: "space-x-3",
        },
    },
    defaultVariants: {
        size: "sm",
    },
})

const radioItem = cva(
    "bg-white border border-neutral-200 rounded-full transition duration-300 hover:bg-neutral-50/80 focus:outline-dashed focus:outline-1 focus:outline-offset-2 focus:outline-neutral-400 rd-disabled:border-neutral-100",
    {
        variants: {
            size: {
                sm: "w-5 h-5",
                md: "w-6 h-6",
            },
        },
        defaultVariants: {
            size: "sm",
        },
    }
)

const radioItemIndicator = cva(
    "flex items-center justify-center w-full h-full relative after:content-[''] after:block after:rounded-[50%] after:bg-primary-500",
    {
        variants: {
            size: {
                sm: "after:w-[12px] after:h-[12px]",
                md: "after:w-[14px] after:h-[14px]",
            },
        },
        defaultVariants: {
            size: "sm",
        },
    }
)

export type RadioRootStyles = VariantProps<typeof radioRootStyles>
export type RadioWrapperStyles = VariantProps<typeof radioItemWrapper>
export type RadioItemStyles = VariantProps<typeof radioItem>
export type RadioItemIndicatorStyles = VariantProps<typeof radioItemIndicator>

export default {
    root: radioRootStyles,
    wrapper: radioItemWrapper,
    item: radioItem,
    indicator: radioItemIndicator,
}
