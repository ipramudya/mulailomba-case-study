import { Indicator, Item, type RadioGroupProps, Root } from "@radix-ui/react-radio-group"
import FormHelperText from "@ui/form/form-helper-text"
import Typography from "@ui/typography"
import { FunctionComponent, useEffect } from "react"
import { Controller, useFormContext } from "react-hook-form"
import { twMerge } from "tailwind-merge"

import radioStyles, { RadioRootStyles, RadioWrapperStyles } from "./radio.styles"

interface Props extends RadioGroupProps, RadioRootStyles, RadioWrapperStyles {
    name: string
    label: string | false
    hint?: string
    hideHintIcon?: boolean
    ignoreError?: boolean
    options: {
        label: string
        value: string
        disabled?: boolean
    }[]
    classNames?: {
        root?: string
        wrapper?: string
        item?: string
        indicator?: string
    }
}

const FormRadioField: FunctionComponent<Props> = ({
    name,
    label,
    options,
    required,
    hint,
    hideHintIcon,
    classNames,
    direction,
    size = "sm",
    ignoreError = false,
    split = false,
    ...restProps
}) => {
    const {
        unregister,
        control,
        formState: { errors },
    } = useFormContext()

    useEffect(() => {
        return () => {
            unregister(name)
        }
    }, [name, unregister])

    return (
        <div className="flex flex-col space-y-3">
            {/* 👇 input label */}
            {(Boolean(label) || Boolean(hint)) && (
                <div>
                    {Boolean(label) && (
                        <Typography size="sm">
                            <span>{label}</span>
                            {required && (
                                <Typography as="span" className="pl-1 text-danger-500">
                                    &#42;
                                </Typography>
                            )}
                        </Typography>
                    )}
                    {Boolean(hint) && (
                        <FormHelperText
                            intent="info"
                            defaultColor
                            icon={!hideHintIcon ? "info" : null}
                        >
                            {hint}
                        </FormHelperText>
                    )}
                </div>
            )}

            {/* 👇 radio buttons */}
            <Controller
                name={name}
                control={control}
                defaultValue={restProps.defaultValue}
                render={({ field: { value, onChange, ...restField } }) => (
                    <Root
                        {...restProps}
                        {...restField}
                        value={value}
                        onValueChange={onChange}
                        className={twMerge(
                            radioStyles.root({ direction, split }),
                            classNames?.root
                        )}
                    >
                        {options.map(({ label, value, disabled }) => (
                            <div
                                key={"radio-" + value}
                                className={twMerge(
                                    radioStyles.wrapper({ size }),
                                    classNames?.wrapper
                                )}
                            >
                                <Item
                                    disabled={disabled}
                                    value={value}
                                    id={"radio-" + value}
                                    className={twMerge(
                                        radioStyles.item({ size }),
                                        classNames?.item,
                                        disabled && "hover:bg-white"
                                    )}
                                >
                                    <Indicator
                                        className={twMerge(
                                            radioStyles.indicator({ size }),
                                            classNames?.indicator
                                        )}
                                    />
                                </Item>

                                <Typography
                                    as="label"
                                    size={size === "md" ? "base" : size}
                                    className={disabled ? "text-neutral-200" : undefined}
                                    htmlFor={"radio-" + value}
                                >
                                    {label}
                                </Typography>
                            </div>
                        ))}
                    </Root>
                )}
            />

            {/* 👇 error message */}
            {Boolean(errors[name]) && !ignoreError && (
                <FormHelperText intent="error" icon="error">
                    {errors?.[name]?.message as any}
                </FormHelperText>
            )}
        </div>
    )
}

export default FormRadioField
