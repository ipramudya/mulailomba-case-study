import { type VariantProps, cva } from "class-variance-authority"

const buttonStyles = cva(
    "flex flex-shrink-0 items-center justify-center space-x-2 font-medium transition duration-300 focus:outline-dashed focus:outline-1 focus:outline-offset-2 focus:outline-neutral-400",
    {
        variants: {
            intent: {
                primary: "bg-primary-600 text-white hover:bg-primary-500 hover:shadow",
                "primary-low": "bg-primary-50 text-primary-700 hover:bg-primary-100 hover:shadow",
                "primary-darken":
                    "bg-primary-700 text-white border-transparent border hover:bg-primary-700/90 hover:shadow",
                "primary-bordered":
                    "bg-white border border-neutral-200 text-primary-500 hover:border-primary-500 hover:shadow",
                "primary-ghost": `
                        bg-white text-primary-600
                        hover:bg-primary-50 
                `,
                "danger-low": "bg-danger-50 text-danger-600 hover:bg-danger-100 hover:shadow",
                "danger-ghost": "bg-transparent text-danger-500 hover:bg-danger-50",
                "danger-bordered":
                    "bg-transparent text-danger-500 hover:bg-danger-50 border border-danger-200",
                "warning-low":
                    "bg-warning-100/70 text-warning-600 border border-transparent hover:border-warning-400 hover:bg-warning-100",
                "success-low":
                    "bg-success-100/70 text-success-600 border border-transparent hover:border-success-400 hover:bg-success-100",
                bordered:
                    "border border-neutral-100 text-neutral-600 bg-white hover:bg-neutral-50 hover:text-neutral-700 hover:border-neutral-200",
                default: "bg-white text-neutral-600 hover:bg-neutral-50",
                unstyled: "",
            },
            size: {
                xs: "px-2 py-1 rounded text-xs",
                sm: "px-[10px] py-[6px] rounded text-sm",
                md: "px-3 py-2 rounded-md tx-sm",
            },
            pill: {
                true: "rounded-full",
            },
            disabled: {
                true: "text-white cursor-not-allowed bg-neutral-200 hover:bg-neutral-300/90 border-none hover:border-none hover:text-white",
            },
        },
        defaultVariants: {
            intent: "primary",
            size: "sm",
            disabled: false,
        },
    }
)

export default buttonStyles
export type ButtonStyles = VariantProps<typeof buttonStyles>
