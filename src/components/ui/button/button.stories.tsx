import { ComponentMeta, ComponentStory } from "@storybook/react"

import Button from "."

export default {
    title: "Button",
    component: Button,
    argTypes: {
        backgroundColor: { control: "color" },
    },
} as ComponentMeta<typeof Button>

const Template: ComponentStory<typeof Button> = (args) => <Button {...args} />

export const Main = Template.bind({})
Main.args = {
    intent: "primary",
    label: "This is buttonnn",
    size: "sm",
    pill: false,
    loading: false,
}

export const Primary = Template.bind({})
Primary.args = {
    intent: "primary",
    label: "This is buttonnn",
}

export const Bordered = Template.bind({})
Bordered.args = {
    intent: "bordered",
    label: "Secondary buttonn",
}

export const Pill = Template.bind({})
Pill.args = {
    label: "Secondary buttonn",
    pill: true,
}

export const HasIcon = Template.bind({})
HasIcon.args = {
    label: "Button icon",
    startIcon: "",
    endIcon: "",
}
