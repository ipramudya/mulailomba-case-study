import { Icon, IconifyIcon } from "@iconify/react"
import { ButtonHTMLAttributes, forwardRef } from "react"
import { twMerge } from "tailwind-merge"

import iconStyles from "../_styles/icon.styles"
import buttonStyles, { ButtonStyles } from "./button.styles"

interface Props extends ButtonHTMLAttributes<HTMLButtonElement>, Omit<ButtonStyles, "disabled"> {
    startIcon?: string | IconifyIcon
    endIcon?: string | IconifyIcon
    label: string
    loading?: boolean
    iconSize?: "xs" | "sm" | "md" | null
    thin?: boolean
    hasDot?: boolean
}

const Button = forwardRef<any, Props>(
    (
        {
            intent,
            pill,
            size = "sm",
            label,
            loading = false,
            startIcon,
            endIcon,
            className,
            iconSize = null,
            thin = false,
            type = "button",
            hasDot = false,
            ...restProps
        },
        forwardedRef
    ) => (
        <button
            type={type}
            ref={forwardedRef}
            className={twMerge(
                buttonStyles({ intent, pill, size, disabled: loading || restProps.disabled }),
                className
            )}
            {...restProps}
        >
            {(startIcon || loading) && (
                <Icon
                    icon={loading ? "ri:loader-5-fill" : (startIcon as any)}
                    className={iconStyles({
                        iconSize: iconSize || size,
                        loading,
                        className: "flex-shrink-0",
                    })}
                />
            )}
            <span
                className={twMerge(
                    "relative max-w-fit text-sm text-inherit",
                    thin ? "font-normal" : "font-medium",
                    !hasDot && "truncate",
                    hasDot
                        ? " after:absolute after:-right-3 after:h-2 after:w-2 after:rounded-full after:bg-danger-400 after:content-[''] "
                        : undefined
                )}
            >
                {label}
            </span>
            {endIcon && !loading && (
                <Icon icon={endIcon} className={iconStyles({ iconSize: iconSize || size })} />
            )}
        </button>
    )
)

Button.displayName = "button"
export default Button
