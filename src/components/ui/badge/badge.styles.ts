import { VariantProps, cva } from "class-variance-authority"

const badgeStyles = cva("text-white rounded-full w-fit flex items-center justify-center", {
    variants: {
        intent: {
            primary: "bg-primary-500",
            "primary-low": "bg-primary-100 text-primary-600",
            "success-low": "bg-success-100 text-success-600",
            "danger-low": "bg-danger-100 text-danger-600",
            "warning-low": "bg-warning-100 text-warning-600",
            "kale-low": "bg-kale-100 text-kale-600",
            "neutral-low": "bg-neutral-100 text-neutral-600",
            bordered: "bg-white border border-neutral-100 text-neutral-400",
        },
        size: {
            xs: "px-[6px] py-[2px] text-[11px]",
            sm: "px-2 py-[2px] text-xs",
            md: "px-[10px] py-1 text-sm",
        },
    },
    defaultVariants: {
        intent: "primary",
        size: "sm",
    },
})

export default badgeStyles
export type BadgeStyles = VariantProps<typeof badgeStyles>
