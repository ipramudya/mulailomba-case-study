import { ComponentMeta, ComponentStory } from "@storybook/react"

import Badge from "."

export default {
    title: "Badge",
    component: Badge,
    argTypes: {
        backgroundColor: { control: "color" },
        intent: {
            options: ["primary", "success-low", "danger-low", "warning-low", "kale-low"],
            control: { type: "select" },
        },
        size: {
            options: ["sm", "md"],
            control: { type: "radio" },
        },
    },
} as ComponentMeta<typeof Badge>

const Template: ComponentStory<typeof Badge> = (args) => <Badge {...args} />

export const Main = Template.bind({})
Main.args = {
    label: "Fresh Graduate",
    size: "sm",
}
