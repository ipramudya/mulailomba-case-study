import { Icon, IconProps } from "@iconify/react"
import { FunctionComponent } from "react"
import { twMerge } from "tailwind-merge"

import iconStyles from "../_styles/icon.styles"
import badgeStyles, { BadgeStyles } from "./badge.styles"

interface Props extends BadgeStyles {
    label: string
    className?: string
    icon?: string | IconProps["icon"]
    startElement?: JSX.Element
    endElement?: JSX.Element
    onClick?: VoidFunction
    asElement?: "div" | "button"
}

const Badge: FunctionComponent<Props> = ({
    icon,
    intent,
    size,
    label,
    className,
    startElement,
    endElement,
    asElement = "div",
    onClick,
}) => {
    const CustomTag = asElement as keyof JSX.IntrinsicElements

    return (
        <CustomTag
            className={twMerge(badgeStyles({ intent, size }), className)}
            onClick={onClick}
            {...((asElement === "button" ? { type: "button" } : {}) as any)}
        >
            {icon && (
                <Icon icon={icon} className={twMerge(iconStyles({ iconSize: "xs" }), "mr-2")} />
            )}
            {Boolean(startElement) && startElement}
            <span className="font-semibold">{label}</span>
            {Boolean(endElement) && endElement}
        </CustomTag>
    )
}

export default Badge
