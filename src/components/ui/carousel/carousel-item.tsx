import { HTMLAttributes, PropsWithChildren, forwardRef, useMemo } from "react"
import { twMerge } from "tailwind-merge"

interface Props extends PropsWithChildren, HTMLAttributes<HTMLDivElement> {
    slidesToShow?: number
}

const CarouselItem = forwardRef<any, Props>(
    ({ className, slidesToShow = 1, children, ...restProps }, forwardedRef) => {
        const basis = useMemo(() => 100 / slidesToShow, [slidesToShow])
        return (
            <div
                style={{ flexBasis: basis + "%" }}
                className={twMerge(`relative shrink-0 grow-0 overflow-hidden pl-[16px]`, className)}
                {...restProps}
                ref={forwardedRef}
            >
                {children}
            </div>
        )
    }
)
CarouselItem.displayName = "carousel-item"
export default CarouselItem
