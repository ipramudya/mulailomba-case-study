import { LegacyRef, PropsWithChildren, forwardRef } from "react"

const CarouselRoot = forwardRef<any, PropsWithChildren>(({ children }, forwardedRef) => (
    <section className="w-full overflow-hidden" ref={forwardedRef as LegacyRef<HTMLDivElement>}>
        <div className="ml-[-16px] flex select-none">{children}</div>
    </section>
))
CarouselRoot.displayName = "carousel-root"

export default CarouselRoot
