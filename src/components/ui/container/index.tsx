import { CSSProperties, PropsWithChildren, createElement, forwardRef } from "react"
import { twMerge } from "tailwind-merge"

import containerStyles, { ContainerStyles } from "./container.styles"

interface Props extends PropsWithChildren, ContainerStyles {
    className?: string
    as?: "div" | "section" | "nav" | "main"
    style?: CSSProperties
}

const Container = forwardRef<any, Props>(
    ({ as = "div", className, children, size, style }, forwardedRef) =>
        createElement(
            as,
            {
                style,
                className: twMerge(containerStyles({ size }), className),
                ref: forwardedRef,
            },
            children
        )
)
Container.displayName = "container"

export default Container
