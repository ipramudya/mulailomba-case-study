import { VariantProps, cva } from "class-variance-authority"

const containerStyles = cva("mx-auto w-full", {
    variants: {
        size: {
            lg: "max-w-7xl",
            md: "max-w-6xl",
            sm: "max-w-4xl",
        },
    },
    defaultVariants: {
        size: "md",
    },
})

export default containerStyles
export type ContainerStyles = VariantProps<typeof containerStyles>
