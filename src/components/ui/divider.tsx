import { DetailedHTMLProps, FunctionComponent, HTMLAttributes } from "react"
import { twMerge } from "tailwind-merge"

interface Props extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
    variant?: "solid" | "dashed"
    orientation: "vertical" | "horizontal"
    label?: string
    className?: string
}

const Divider: FunctionComponent<Props> = ({
    variant = "solid",
    orientation,
    label,
    className,
    ...restProps
}) => {
    return (
        <>
            {label ? (
                <div
                    className={twMerge(
                        "before:mr-2 before:flex-shrink before:flex-grow before:basis-0 before:border-t before:border-dashed before:border-r-neutral-100",
                        "flex items-center border-neutral-100 text-xs text-neutral-600 ",
                        "after:ml-2 after:flex-shrink after:flex-grow after:basis-0 after:border-t after:border-dashed after:border-r-neutral-100",
                        className
                    )}
                    {...restProps}
                >
                    {label}
                </div>
            ) : (
                <div
                    className={twMerge(
                        "border-neutral-100",
                        variant === "solid" ? "border-solid" : "border-dashed",
                        orientation === "vertical"
                            ? "border-y-0 border-l border-r-0"
                            : "border-x-0 border-t border-b-0",
                        className
                    )}
                    {...restProps}
                />
            )}
        </>
    )
}

export default Divider
