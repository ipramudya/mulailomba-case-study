import { ComponentMeta, ComponentStory } from "@storybook/react"

import IconButton from "."

export default {
    title: "Icon Button",
    component: IconButton,
    argTypes: {
        backgroundColor: { control: "color" },
        intent: {
            options: ["primary", "primary-low", "primary-bordered", "bordered"],
            control: { type: "select" },
        },
        size: {
            options: ["xs", "sm", "md"],
            control: { type: "radio" },
        },
    },
} as ComponentMeta<typeof IconButton>

const Template: ComponentStory<typeof IconButton> = (args) => <IconButton {...args} />

export const Main = Template.bind({})
Main.args = {
    icon: "heroicons:arrow-trending-up-solid",
    circle: true,
    intent: "primary-low",
    size: "sm",
}
