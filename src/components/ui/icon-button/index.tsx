import { Icon, IconifyIcon } from "@iconify/react"
import { ButtonHTMLAttributes, forwardRef } from "react"
import { twMerge } from "tailwind-merge"

import iconStyles from "../_styles/icon.styles"
import iconButtonStyles, { IconButtonStyles } from "./icon-button.styles"

interface Props extends ButtonHTMLAttributes<HTMLButtonElement>, IconButtonStyles {
    icon: string | IconifyIcon
    iconSize?: "xs" | "sm" | "md"
}

const IconButton = forwardRef<any, Props>(
    ({ icon, loading, circle, size, intent, className, iconSize, ...restProps }, forwardedRef) => {
        return (
            <button
                ref={forwardedRef}
                className={twMerge(iconButtonStyles({ loading, circle, size, intent }), className)}
                type={restProps.type || "button"}
                {...restProps}
            >
                <Icon icon={icon} className={iconStyles({ iconSize: iconSize || size })} />
            </button>
        )
    }
)

IconButton.displayName = "icon-button"
export default IconButton
