import { type VariantProps, cva } from "class-variance-authority"

const iconButtonStyles = cva(
    `
    overflow-hidden relative transition duration-300 flex items-center justify-center
    focus:outline-dashed focus:outline-1 focus:outline-neutral-400 focus:outline-offset-2
    after:absolute after:content[""] after:w-full after:h-full after:bg-white after:top-0 after:left-0 after:-z-[1]
    `,
    {
        variants: {
            intent: {
                primary: `
                    bg-primary-600 text-white
                    hover:bg-primary-500 hover:shadow-md
                `,
                "primary-low": `
                    bg-primary-100 text-primary-700
                    hover:bg-primary-100 hover:shadow
                `,
                "primary-bordered": `
                    bg-white border border-neutral-200 text-primary-600
                    hover:border-primary-500 hover:shadow
                `,
                "primary-ghost": `
                        bg-white text-primary-700
                        hover:bg-primary-50 
                `,
                bordered: `
                    border border-neutral-100 text-neutral-600 bg-white
                    hover:bg-neutral-50 hover:text-neutral-700 hover:shadow
                `,
                "danger-ghost": "bg-white text-danger-600 hover:bg-danger-50",
                unstyled: "",
            },
            size: {
                xs: "w-6 h-6 rounded-sm",
                sm: "w-8 h-8 rounded",
                md: "w-10 h-10 rounded-md",
            },
            circle: {
                true: "rounded-full",
            },
            loading: {
                true: "text-white cursor-not-allowed bg-neutral-200 hover:bg-neutral-300/90 border-none hover:border-none hover:text-white",
            },
        },
        defaultVariants: {
            intent: "primary",
            size: "sm",
            circle: false,
        },
    }
)

export default iconButtonStyles
export type IconButtonStyles = VariantProps<typeof iconButtonStyles>
