import { Icon } from "@iconify/react"
import { FunctionComponent, PropsWithChildren } from "react"
import { twMerge } from "tailwind-merge"

import timelineStyles from "./timeline.styles"

interface Props extends PropsWithChildren {
    isStart?: boolean
    isEnd?: boolean
    spacing?: "sm" | "md"
    icon?: string
    classNames?: {
        marker?: string
        icon?: string
        contentWrapper?: string
    }
}

const TimelineItem: FunctionComponent<Props> = ({
    children,
    icon,
    classNames,
    isStart = false,
    isEnd = false,
    spacing = "sm",
}) => {
    return (
        <div className="relative flex">
            <div
                className={twMerge(
                    timelineStyles.marker({ spacing, isStart, isEnd }),
                    classNames?.marker
                )}
            >
                <Icon
                    icon={icon || "heroicons:check-circle"}
                    className={twMerge(
                        "z-10 box-content aspect-square w-7 bg-neutral-50 py-1 text-xl text-primary-600",
                        classNames?.icon
                    )}
                />
            </div>
            <div
                className={twMerge(
                    timelineStyles.contentWrapper({ spacing, isStart, isEnd }),
                    classNames?.contentWrapper
                )}
            >
                {children}
            </div>
        </div>
    )
}

export default TimelineItem
