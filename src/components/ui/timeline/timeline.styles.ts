import { cva } from "class-variance-authority"

const marker = cva(
    "relative flex justify-center after:absolute after:h-full after:border-l after:border-dashed after:border-primary-500 after:content-['']",
    {
        variants: {
            spacing: {
                sm: "pb-2",
                md: "pb-4",
            },
            strokeStyle: {
                dashed: "border-dashed",
                solid: "border-solid",
            },
            isStart: { true: "", false: "" },
            isEnd: {
                true: "after:border-none",
                false: "",
            },
        },
        defaultVariants: {
            spacing: "sm",
            strokeStyle: "dashed",
        },
    }
)

const contentWrapper = cva("flex items-center justify-center", {
    variants: {
        spacing: {
            sm: "ml-2 pb-2",
            md: "ml-4 pb-4",
        },
        isStart: { true: "", false: "" },
        isEnd: {
            true: "after:border-none",
            false: "",
        },
    },
    defaultVariants: {
        spacing: "sm",
    },
})

export default {
    marker,
    contentWrapper,
} as const
