import { FunctionComponent, PropsWithChildren } from "react"
import { twMerge } from "tailwind-merge"

interface Props extends PropsWithChildren {
    className?: string
}

const Callout: FunctionComponent<Props> = ({ className, children }) => {
    return (
        <div
            className={twMerge(
                "rounded-md border border-neutral-100 bg-white px-3 py-2 shadow-sm",
                className
            )}
        >
            {children}
        </div>
    )
}

export default Callout
