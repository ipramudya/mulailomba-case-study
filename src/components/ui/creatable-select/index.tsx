import { Icon } from "@iconify/react"
import Typography from "@ui/typography"
import { FunctionComponent, forwardRef, useId } from "react"
import {
    ClearIndicatorProps,
    ControlProps,
    DropdownIndicatorProps,
    IndicatorsContainerProps,
    InputProps,
    MultiValueProps,
    PlaceholderProps,
    Props as ReactSelectProps,
    SingleValueProps,
    ValueContainerProps,
    components,
} from "react-select"
import CreatableSelect from "react-select/creatable"
import { twMerge } from "tailwind-merge"

import modules from "./creatable-select.module.css"
import creatableSelectStyles, { ControlStyles } from "./creatable-select.styles"

type SelectOption = { label: string; value: string }
export interface CreatableSelectProps extends ReactSelectProps, ControlStyles {
    options: SelectOption[]
    icon?: string
}

const CreatebleSelect = forwardRef<HTMLSelectElement, CreatableSelectProps>(
    (
        { options, placeholder = "Pilih kategori", size = "sm", wide = false, icon, ...restProps },
        forwardedRef
    ) => {
        const selectId = useId()

        return (
            <CreatableSelect
                isMulti
                formatCreateLabel={(inputValue) => (
                    <p>
                        Buat opsi <span className="font-semibold">{inputValue}</span>
                    </p>
                )}
                noOptionsMessage={() => (
                    <Typography size="xs" className="py-1 text-center">
                        Opsi kosong, mulai mengetik dan tambahkan opsi baru
                    </Typography>
                )}
                instanceId={selectId}
                ref={forwardedRef as any}
                placeholder={placeholder}
                options={options}
                styles={{
                    multiValue: () => ({}),
                    multiValueLabel: () => ({
                        padding: "0",
                    }),
                    multiValueRemove: () => ({
                        padding: "0",
                    }),
                    control: () => ({}),
                    menu: (css) => ({
                        ...css,
                        margin: "12px 0",
                    }),
                    menuList: (css) => ({
                        ...css,
                        maxHeight: "420px",
                        padding: "6px",
                    }),
                    option: (_, { isFocused }) => ({
                        cursor: "pointer",
                        padding: "4px 8px",
                        borderRadius: "0.25rem",
                        color: "#6c6c6c",
                        fontSize: "14px",
                        display: "flex",
                        alignItems: "center",
                        backgroundColor: isFocused ? "#f1f2f8" : "white",
                    }),
                    noOptionsMessage: () => ({}),
                }}
                components={{
                    Control: (props) => <Control {...props} size={size} wide={wide} />,
                    ValueContainer: (props) => <ValueContainer {...props} />,
                    Placeholder: (props) => <Placeholder {...props} />,
                    SingleValue: (props) => <Value {...props} />,
                    MultiValue: (props) => <MultiValue {...props} />,
                    Input: (props) => <Input {...props} />,
                    IndicatorSeparator: null,
                    IndicatorsContainer: (props) => <IndicatorContainer {...props} />,
                    DropdownIndicator: (props) => <DropdownIndicator {...props} icon={icon} />,
                    ClearIndicator,
                }}
                {...restProps}
            />
        )
    }
)

type CProps = ControlProps & ControlStyles
const Control: FunctionComponent<CProps> = (props) => (
    <components.Control
        {...props}
        className={twMerge(
            creatableSelectStyles.control({ size: props.size }),
            props.menuIsOpen &&
                "border shadow-none outline-dashed outline-1 outline-offset-2 outline-neutral-400"
        )}
    >
        {props.children}
    </components.Control>
)

type VCProps = ValueContainerProps
const ValueContainer: FunctionComponent<VCProps> = (props) => (
    <components.ValueContainer
        {...props}
        className={"mr-3 !p-0 " + props.selectProps.controlShouldRenderValue && "space-x-2"}
    >
        {props.children}
    </components.ValueContainer>
)

type PProps = PlaceholderProps
const Placeholder: FunctionComponent<PProps> = (props) => (
    <components.Placeholder {...props} className="!m-0 flex items-center">
        <span className={"text-sm !text-[#BABABA]"}>{props.children}</span>
    </components.Placeholder>
)

type VProps = SingleValueProps
const Value: FunctionComponent<VProps> = (props) => (
    <components.SingleValue
        {...props}
        className="!m-0 flex max-h-[20px] items-center !p-0 !text-neutral-800"
    >
        <span className={" text-sm text-neutral-800"}>{props.children}</span>
    </components.SingleValue>
)

type MVProps = MultiValueProps
const MultiValue: FunctionComponent<MVProps> = (props) => (
    <components.MultiValue
        {...props}
        className="flex items-center space-x-1 rounded-full bg-primary-100 px-[6px] py-[2px] text-primary-600"
    >
        <span className=" text-[11px] text-primary-500">{props.children}</span>
    </components.MultiValue>
)

type InProps = InputProps
const Input: FunctionComponent<InProps> = (props) => (
    <components.Input {...props} className={"!m-0 !p-0 " + modules.rs_input}>
        {props.children}
    </components.Input>
)

type IndProps = IndicatorsContainerProps
const IndicatorContainer: FunctionComponent<IndProps> = (props) => (
    <components.IndicatorsContainer {...props} className="max-h-fit space-x-3">
        {props.children}
    </components.IndicatorsContainer>
)

type DdIndProps = DropdownIndicatorProps & { icon?: string }
const DropdownIndicator: FunctionComponent<DdIndProps> = ({ icon, ...props }) => (
    <components.DropdownIndicator {...props} className="aspect-square !p-0">
        <Icon icon={icon || "heroicons:chevron-down"} className=" h-4 w-4 text-neutral-600" />
    </components.DropdownIndicator>
)

type ClsIndProps = ClearIndicatorProps
const ClearIndicator: FunctionComponent<ClsIndProps> = (props) => (
    <components.ClearIndicator {...props} className="aspect-square !p-0">
        <Icon icon="heroicons:x-mark" className=" h-4 w-4 text-danger-600" />
    </components.ClearIndicator>
)

CreatebleSelect.displayName = "creatable-select"
export default CreatebleSelect
