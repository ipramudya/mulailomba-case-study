import { ComponentMeta, ComponentStory } from "@storybook/react"

import CreatableSelect from "."

export default {
    title: "CreatableSelect",
    component: CreatableSelect,
    argTypes: {
        backgroundColor: { control: "color" },
    },
} as ComponentMeta<typeof CreatableSelect>

const Template: ComponentStory<typeof CreatableSelect> = (args) => <CreatableSelect {...args} />
export const Main = Template.bind({})
Main.args = {
    options: [
        {
            label: "first option",
            value: "1",
        },
        {
            label: "second option",
            value: "2",
        },
        {
            label: "third option",
            value: "3",
        },
    ],
}
