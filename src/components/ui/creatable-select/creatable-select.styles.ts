import { VariantProps, cva } from "class-variance-authority"

const creatableSelectControlStyles = cva(
    "border-neutral-200 flex items-center rounded border cursor-pointer min-h-[40px]",
    {
        variants: {
            size: {
                xs: "px-2 py-1 rounded",
                sm: "px-[10px] py-[6px] rounded",
                md: "px-3 py-2 rounded-md",
            },
            wide: {
                true: "w-full",
            },
        },
        defaultVariants: {
            size: "sm",
            wide: false,
        },
    }
)

export type ControlStyles = VariantProps<typeof creatableSelectControlStyles>
export default { control: creatableSelectControlStyles }
