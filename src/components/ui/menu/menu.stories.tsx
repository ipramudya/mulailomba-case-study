import { ComponentMeta, ComponentStory } from "@storybook/react"

import MenuItem from "./menu-item"
import Menu from "./menu-wrapper"

export default {
    title: "Menu",
    component: Menu,
    argTypes: {
        backgroundColor: { control: "color" },
    },
} as ComponentMeta<typeof Menu>

export const MenuItemStory: ComponentStory<typeof Menu> = (args) => (
    <Menu {...args}>
        <MenuItem label="Item one" />
        <MenuItem label="Item two" />
        <MenuItem label="Item three" />
    </Menu>
)
MenuItemStory.args = {
    buttonLabel: "Dropdown Item",
}
MenuItemStory.storyName = "Menu Item"
