import { Icon } from "@iconify/react"
import * as BaseMenu from "@radix-ui/react-dropdown-menu"
import { MenuCheckboxItemProps } from "@radix-ui/react-dropdown-menu"
import { FunctionComponent } from "react"
import { twMerge } from "tailwind-merge"

import iconStyles from "../_styles/icon.styles"
import menuStyles, { MenuItemStyles } from "./menu.styles"

interface Props extends MenuCheckboxItemProps, MenuItemStyles {
    label: string
    className?: string
}

const MenuItemCheckbox: FunctionComponent<Props> = ({ label, className, size, ...restProps }) => {
    return (
        <BaseMenu.CheckboxItem
            className={twMerge(menuStyles.item({ size }), "cursor-pointer", className)}
            {...restProps}
        >
            <span>{label}</span>
            <BaseMenu.ItemIndicator>
                <Icon
                    icon="heroicons:check-20-solid"
                    className={iconStyles({
                        iconSize: size,
                        className: "text-success-600 ",
                    })}
                />
            </BaseMenu.ItemIndicator>
        </BaseMenu.CheckboxItem>
    )
}

MenuItemCheckbox.displayName = "checkbox menu"
export default MenuItemCheckbox
