import {
    Content,
    type MenuContentProps,
    Portal,
    Root,
    Trigger,
} from "@radix-ui/react-dropdown-menu"
import IconButton from "@ui/icon-button"
import { FunctionComponent, PropsWithChildren } from "react"
import { twMerge } from "tailwind-merge"

import Button from "../button"
import menuStyles, { MenuStyles } from "./menu.styles"

interface Props extends PropsWithChildren, MenuStyles {
    buttonLabel?: string
    triggerType?: "button" | "iconButton"
    icon?: string
    contentAlign?: MenuContentProps["align"]
    triggerButton?: JSX.Element | null
    classNames?: {
        button?: string
    }
    disabled?: boolean
}

const MenuWrapper: FunctionComponent<Props> = ({
    children,
    size,
    buttonLabel = "",
    triggerType = "button",
    icon,
    contentAlign = "center",
    triggerButton = null,
    classNames,
    disabled = false,
}) => (
    <Root modal={false}>
        {triggerButton && <Trigger asChild>{triggerButton}</Trigger>}
        {triggerButton === null && (
            <>
                {triggerType === "iconButton" && icon ? (
                    <Trigger asChild>
                        <IconButton
                            icon={icon}
                            size={size}
                            intent="primary-ghost"
                            className={classNames?.button}
                            disabled={disabled}
                        />
                    </Trigger>
                ) : (
                    <Trigger asChild>
                        <Button
                            label={buttonLabel}
                            intent="bordered"
                            endIcon="heroicons:chevron-down-20-solid"
                            size={size}
                            className={twMerge("rd-state-open:bg-neutral-50", classNames?.button)}
                            disabled={disabled}
                        />
                    </Trigger>
                )}
            </>
        )}

        <Portal>
            <Content className={menuStyles.menu({ size })} sideOffset={8} align={contentAlign}>
                {children}
            </Content>
        </Portal>
    </Root>
)

export default MenuWrapper
