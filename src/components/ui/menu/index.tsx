import { Separator } from "@radix-ui/react-dropdown-menu"

import MenuItem from "./menu-item"
import MenuItemCheckbox from "./menu-item-checkbox"
import MenuItemRadio from "./menu-item-radio"
import MenuRadioGroup from "./menu-radio-group"
import MenuWrapper from "./menu-wrapper"

export type MenuType = typeof MenuWrapper
export default {
    Root: MenuWrapper,
    Item: MenuItem,
    Checkbox: MenuItemCheckbox,
    Radio: MenuItemRadio,
    RadioGroup: MenuRadioGroup,
    Separator,
}
