import { VariantProps, cva } from "class-variance-authority"

const menuStyles = cva(" border border-neutral-100 bg-white z-dropdowns shadow-menu", {
    variants: {
        size: {
            sm: "space-y-0 p-[6px] rounded min-w-[160px]",
            md: "space-y-1 p-2 rounded-md min-w-[200px]",
        },
    },
    defaultVariants: {
        size: "md",
    },
})

const menuItemStyles = cva(
    "outline-none flex items-center justify-between rd-highlighted:bg-neutral-50 text-neutral-600 transition duration-200",
    {
        variants: {
            size: {
                sm: "px-[6px] py-1 text-xs rounded-sm",
                md: "px-2 py-1 text-sm rounded",
            },
        },
        defaultVariants: {
            size: "md",
        },
    }
)

export default {
    menu: menuStyles,
    item: menuItemStyles,
}

export type MenuStyles = VariantProps<typeof menuStyles>
export type MenuItemStyles = VariantProps<typeof menuItemStyles>
