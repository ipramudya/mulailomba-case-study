import { Icon, IconProps } from "@iconify/react"
import { Item, MenuItemProps } from "@radix-ui/react-dropdown-menu"
import { FunctionComponent } from "react"
import { twMerge } from "tailwind-merge"

import iconStyles from "../_styles/icon.styles"
import menuStyles, { MenuItemStyles } from "./menu.styles"

interface Props extends MenuItemStyles, MenuItemProps {
    label: string
    className?: string
    icon?: string | IconProps["icon"]
}

const MenuItem: FunctionComponent<Props> = ({ label, size, icon, className, ...restProps }) => {
    return (
        <Item
            className={twMerge("cursor-pointer", menuStyles.item({ size }), className)}
            {...restProps}
        >
            <span>{label}</span>
            {icon && <Icon icon={icon} className={iconStyles({ iconSize: size })} />}
        </Item>
    )
}

export default MenuItem
