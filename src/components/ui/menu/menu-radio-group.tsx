import * as BaseMenu from "@radix-ui/react-dropdown-menu"
import { MenuRadioGroupProps } from "@radix-ui/react-dropdown-menu"
import { FunctionComponent } from "react"

const MenuRadioGroup: FunctionComponent<MenuRadioGroupProps> = ({ children, ...restProps }) => {
    return <BaseMenu.RadioGroup {...restProps}>{children}</BaseMenu.RadioGroup>
}

export default MenuRadioGroup
