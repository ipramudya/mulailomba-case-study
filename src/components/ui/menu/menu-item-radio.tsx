import * as BaseMenu from "@radix-ui/react-dropdown-menu"
import { MenuRadioItemProps } from "@radix-ui/react-dropdown-menu"
import { FunctionComponent } from "react"
import { twMerge } from "tailwind-merge"

import menuStyles, { MenuItemStyles } from "./menu.styles"

interface Props extends MenuRadioItemProps, MenuItemStyles {
    label: string
    className?: string
}

const MenuItemRadio: FunctionComponent<Props> = ({ label, className, size, ...restProps }) => {
    return (
        <BaseMenu.RadioItem
            className={twMerge(
                menuStyles.item({ size }),
                "cursor-pointer border border-transparent rd-state-checked:border-success-600 rd-state-checked:bg-success-50 rd-state-checked:text-success-700",
                className
            )}
            {...restProps}
        >
            <span>{label}</span>
        </BaseMenu.RadioItem>
    )
}

export default MenuItemRadio
