import { FunctionComponent, PropsWithChildren } from "react"
import { twMerge } from "tailwind-merge"

interface Props extends PropsWithChildren {
    className?: string
}

const Paper: FunctionComponent<Props> = ({ children, className }) => {
    return (
        <div
            className={twMerge(
                "boder flex flex-col space-y-2 rounded-md border border-neutral-100 bg-neutral-50 p-3",
                className
            )}
        >
            {children}
        </div>
    )
}

export default Paper
