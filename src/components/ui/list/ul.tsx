import { FunctionComponent, PropsWithChildren } from "react"
import { twMerge } from "tailwind-merge"

interface Props extends PropsWithChildren {
    className?: string
}

const UL: FunctionComponent<Props> = ({ className, children }) => {
    return <ul className={twMerge("flex flex-col space-y-1", className)}>{children}</ul>
}

export default UL
