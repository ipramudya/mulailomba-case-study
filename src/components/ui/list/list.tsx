import Typography from "@ui/typography"
import { FunctionComponent, PropsWithChildren } from "react"
import { twMerge } from "tailwind-merge"

interface Props extends PropsWithChildren {
    className?: string
}

const List: FunctionComponent<Props> = ({ children, className }) => {
    return (
        <li className={twMerge("flex space-x-2", className)}>
            <div className="mt-[10px] h-1 w-1 flex-shrink-0 rounded-full bg-neutral-500" />
            <Typography as="span" size="sm" className="leading-6">
                {children}
            </Typography>
        </li>
    )
}

export default List
