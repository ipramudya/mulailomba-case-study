import { Icon } from "@iconify/react"
import { type FunctionComponent, type PropsWithChildren, Suspense } from "react"

const Loader: FunctionComponent<PropsWithChildren> = ({ children }) => {
    return (
        <Suspense
            fallback={
                <div className="flex min-h-screen min-w-full items-center justify-center">
                    <Icon
                        icon="lucide:loader-2"
                        className="h-12 w-12 animate-spin text-primary-500"
                    />
                </div>
            }
        >
            {children}
        </Suspense>
    )
}

export default Loader
