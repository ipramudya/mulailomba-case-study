import { Content, type DialogProps, Overlay, Portal, Root, Title } from "@radix-ui/react-dialog"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"
import { twMerge } from "tailwind-merge"

interface Props extends DialogProps {
    title: string | false
    ignoreBluring?: boolean
    classNames?: {
        content?: string
        overlay?: string
    }
}

const Modal: FunctionComponent<Props> = ({
    open,
    children,
    title,
    classNames,
    ignoreBluring = false,
    ...restProps
}) => {
    return (
        <Root open={open} {...restProps}>
            <Portal>
                <Overlay
                    className={twMerge(
                        "fixed inset-0 z-overlay bg-neutral-800/25",
                        classNames?.overlay,
                        !ignoreBluring && "backdrop-blur-[3px]"
                    )}
                />
                <Content
                    className={twMerge(
                        "fixed top-[50%] left-[50%] z-dialog translate-x-[-50%] translate-y-[-50%] overflow-hidden rounded-lg bg-white focus:outline-none"
                    )}
                >
                    <div
                        className={twMerge(
                            "max-h-[85vh] w-[90vw] max-w-[550px] p-6",
                            classNames?.content
                        )}
                    >
                        {title && (
                            <Title className="mb-4" asChild>
                                <Typography as="h3" size="lg" weight={600}>
                                    {title}
                                </Typography>
                            </Title>
                        )}
                        {children}
                    </div>
                </Content>
            </Portal>
        </Root>
    )
}

export default Modal
