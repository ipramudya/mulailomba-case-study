import { type VariantProps, cva } from "class-variance-authority"

const navStyles = cva("fixed left-0 right-0 top-0 z-fixed transition duration-300", {
    variants: {
        intent: {
            primary: "bg-primary-500",
            light: "border-b bg-white",
            none: "bg-white",
        },
        disableShadow: {
            true: "border-neutral-100",
            false: "shadow-nav border-neutral-50",
        },
    },
    defaultVariants: {
        intent: "primary",
    },
})

export default navStyles
export type NavStyles = VariantProps<typeof navStyles>
