import { FunctionComponent, PropsWithChildren } from "react"
import { twMerge } from "tailwind-merge"

import navStyles, { NavStyles } from "./nav.styles"

export const HEADER_HEIGHT = "66px"

interface Props extends PropsWithChildren, NavStyles {
    className?: string
}

const Nav: FunctionComponent<Props> = ({ children, className, intent, disableShadow = false }) => {
    return (
        <nav
            className={twMerge(navStyles({ intent, disableShadow }), className)}
            style={{ height: HEADER_HEIGHT }}
        >
            {children}
        </nav>
    )
}

export default Nav
