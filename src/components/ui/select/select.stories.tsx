import { ComponentMeta, ComponentStory } from "@storybook/react"

import Select from "."

export default {
    title: "Select",
    component: Select,
    argTypes: {
        backgroundColor: { control: "color" },
    },
} as ComponentMeta<typeof Select>

const Template: ComponentStory<typeof Select> = (args) => <Select {...args} />
export const Main = Template.bind({})
Main.args = {
    options: [
        {
            label: "first option",
            value: "1",
        },
        {
            label: "second option",
            value: "2",
        },
        {
            label: "third option",
            value: "3",
        },
    ],
}
