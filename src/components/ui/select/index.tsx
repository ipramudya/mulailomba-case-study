import { Icon } from "@iconify/react"
import Typography from "@ui/typography"
import { FunctionComponent, forwardRef, useId } from "react"
import ReactSelect, {
    ClearIndicatorProps,
    type ControlProps,
    type DropdownIndicatorProps,
    type IndicatorsContainerProps,
    type InputProps,
    type PlaceholderProps,
    type Props as ReactSelectProps,
    type SingleValueProps,
    type ValueContainerProps,
    components,
} from "react-select"
import { twMerge } from "tailwind-merge"

import modules from "./select.module.css"
import selectStyles, { ControlStyles } from "./select.styles"

type SelectOption = { label: string; value: string }
export interface SelectProps extends ReactSelectProps, ControlStyles {
    options: SelectOption[]
}

const Select = forwardRef<HTMLSelectElement, SelectProps>(
    (
        { options, placeholder = "Pilih kategori", size = "sm", wide = false, ...restProps },
        forwardedRef
    ) => {
        const selectId = useId()

        return (
            <ReactSelect
                ref={forwardedRef as any}
                placeholder={placeholder}
                options={options}
                instanceId={selectId}
                noOptionsMessage={({ inputValue }) => (
                    <Typography size="sm" className="py-1">
                        Opsi <span className="font-semibold">{inputValue}</span> tidak dapat
                        ditemukan
                    </Typography>
                )}
                styles={{
                    control: () => ({}),
                    menu: (css) => ({
                        ...css,
                        margin: "12px 0",
                    }),
                    menuList: (css) => ({
                        ...css,
                        maxHeight: "420px",
                        padding: "6px",
                    }),
                    option: (_, { isFocused }) => ({
                        cursor: "pointer",
                        padding: "4px 8px",
                        borderRadius: "0.25rem",
                        color: "#6c6c6c",
                        fontSize: "14px",
                        display: "flex",
                        alignItems: "center",
                        backgroundColor: isFocused ? "#f1f2f8" : "white",
                    }),
                    noOptionsMessage: () => ({}),
                }}
                components={{
                    Control: (props) => <Control {...props} size={size} wide={wide} />,
                    ValueContainer: (props) => <ValueContainer {...props} />,
                    Placeholder: (props) => <Placeholder {...props} />,
                    SingleValue: (props) => <Value {...props} />,
                    Input: (props) => <Input {...props} />,
                    IndicatorSeparator: null,
                    IndicatorsContainer: (props) => <IndicatorContainer {...props} />,
                    DropdownIndicator: (props) => <DropdownIndicator {...props} />,
                    ClearIndicator,
                }}
                {...restProps}
            />
        )
    }
)

type CProps = ControlProps & ControlStyles
const Control: FunctionComponent<CProps> = (props) => (
    <components.Control
        {...props}
        className={twMerge(
            selectStyles.control({ size: props.size }),
            props.menuIsOpen &&
                "border shadow-none outline-dashed outline-1 outline-offset-2 outline-neutral-400"
        )}
    >
        {props.children}
    </components.Control>
)

type VCProps = ValueContainerProps
const ValueContainer: FunctionComponent<VCProps> = (props) => (
    <components.ValueContainer {...props} className="mr-3 !p-0">
        {props.children}
    </components.ValueContainer>
)

type PProps = PlaceholderProps
const Placeholder: FunctionComponent<PProps> = (props) => (
    <components.Placeholder {...props} className="!m-0 flex items-center">
        <span className={"text-sm !text-[#BABABA]"}>{props.children}</span>
    </components.Placeholder>
)

type VProps = SingleValueProps
const Value: FunctionComponent<VProps> = (props) => (
    <components.SingleValue
        {...props}
        className="!m-0 flex max-h-[20px] items-center !p-0 !text-neutral-800"
    >
        <span className={" text-sm text-neutral-800"}>{props.children}</span>
    </components.SingleValue>
)

type InProps = InputProps
const Input: FunctionComponent<InProps> = (props) => (
    <components.Input {...props} className={"!m-0 !p-0 " + modules.rs_input}>
        {props.children}
    </components.Input>
)

type IndProps = IndicatorsContainerProps
const IndicatorContainer: FunctionComponent<IndProps> = (props) => (
    <components.IndicatorsContainer {...props} className="max-h-fit space-x-3">
        {props.children}
    </components.IndicatorsContainer>
)

type DdIndProps = DropdownIndicatorProps
const DropdownIndicator: FunctionComponent<DdIndProps> = (props) => (
    <components.DropdownIndicator {...props} className="aspect-square !p-0">
        <Icon icon="heroicons:chevron-down" className=" h-4 w-4 text-neutral-600" />
    </components.DropdownIndicator>
)

type ClsIndProps = ClearIndicatorProps
const ClearIndicator: FunctionComponent<ClsIndProps> = (props) => (
    <components.ClearIndicator {...props} className="aspect-square !p-0">
        <Icon icon="heroicons:x-mark" className=" h-4 w-4 text-danger-600" />
    </components.ClearIndicator>
)

Select.displayName = "select"
export default Select
