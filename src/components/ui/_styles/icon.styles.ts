import { VariantProps, cva } from "class-variance-authority"

const iconStyles = cva("text-inherit", {
    variants: {
        iconSize: {
            xs: "w-4 h-4",
            sm: "w-5 h-5",
            md: "w-6 h-6",
        },
        loading: {
            true: "animate-spin",
        },
    },
})

export default iconStyles
export type IconStyles = VariantProps<typeof iconStyles>
