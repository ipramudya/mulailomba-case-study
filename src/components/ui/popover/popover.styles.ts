import { VariantProps, cva } from "class-variance-authority"

const popoverStyles = cva("bg-white flex items-center justify-center z-dropdowns", {
    variants: {
        size: {
            sm: "min-w-[80px] rounded p-2",
            md: "min-w-[120px] rounded-md p-3",
        },
    },
    defaultVariants: {
        size: "sm",
    },
})

export type PopoverStyles = VariantProps<typeof popoverStyles>
export default popoverStyles
