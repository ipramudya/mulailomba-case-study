import {
    Content,
    PopoverContentProps,
    type PopoverProps,
    Root,
    Trigger,
} from "@radix-ui/react-popover"
import { FunctionComponent, PropsWithChildren } from "react"
import { twMerge } from "tailwind-merge"

import popoverStyles, { PopoverStyles } from "./popover.styles"

interface Props extends PropsWithChildren<PopoverProps>, PopoverStyles {
    triggerEl: JSX.Element
    align?: PopoverContentProps["align"]
    className?: string
}

const Popover: FunctionComponent<Props> = ({
    triggerEl,
    children,
    align,
    className,
    size,
    ...restProps
}) => {
    return (
        <Root {...restProps}>
            <Trigger asChild>{triggerEl}</Trigger>
            <Content
                sideOffset={8}
                align={align}
                className={twMerge(popoverStyles({ size }), className)}
            >
                {children}
            </Content>
        </Root>
    )
}

export default Popover
