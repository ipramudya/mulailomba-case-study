import { Icon } from "@iconify/react"
import Paper from "@ui/paper"
import Typography from "@ui/typography"
import { FunctionComponent, PropsWithChildren } from "react"
import { twMerge } from "tailwind-merge"

interface Props extends PropsWithChildren {
    message?: string
    spacingY?: boolean
    icon?: string
}

const NotFound: FunctionComponent<Props> = ({
    message,
    children,
    icon = "heroicons:archive-box-x-mark",
    spacingY = true,
}) => {
    return (
        <div className={twMerge("w-full", spacingY && "py-[2rem]")}>
            <Paper className="items-center space-y-4 py-6">
                <Icon icon={icon} className="h-8 w-8 text-neutral-400" />
                {message ? (
                    <Typography size="sm" className="text-center">
                        {message}
                    </Typography>
                ) : (
                    <Typography size="sm" className="text-center">
                        Tidak ada ajang perlombaan yang dapat anda kelola <br />
                        silahkan membuat terlebih dahulu
                    </Typography>
                )}
                {children}
            </Paper>
        </div>
    )
}

export default NotFound
