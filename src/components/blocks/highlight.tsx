import { FunctionComponent, PropsWithChildren } from "react"
import { twMerge } from "tailwind-merge"

interface Props extends PropsWithChildren {
    className?: string
}

const Highlight: FunctionComponent<Props> = ({ children, className }) => {
    return (
        <div
            className={twMerge(
                "relative rounded-lg border-[2px] border-primary-800 bg-white p-3 before:absolute before:top-[8px] before:left-[8px] before:z-[-1] before:block before:h-full before:w-full before:rounded-lg before:border-[2px] before:border-primary-800 before:bg-secondary-200 before:content-['']",
                className
            )}
        >
            {children}
        </div>
    )
}

export default Highlight
