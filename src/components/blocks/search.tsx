import useDebounce from "@hooks/use-debounce"
import { Icon } from "@iconify/react"
import { ChangeEvent, type FunctionComponent, useEffect, useState } from "react"
import { twMerge } from "tailwind-merge"

interface Props {
    wide?: boolean
    onSearchChange?: (val: string) => void
    className?: string
    iconPlacing?: "start" | "end"
    pill?: boolean
    placeholder?: string
}

const Search: FunctionComponent<Props> = ({
    onSearchChange,
    className,
    wide = false,
    pill = false,
    iconPlacing = "start",
    placeholder = "Cari lomba...",
}) => {
    const [searchState, setSearchState] = useState<string>()
    const debouncedSearchValue = useDebounce(searchState)

    const onInputChange = (e: ChangeEvent<HTMLInputElement>) => {
        setSearchState(e.target.value)
    }

    useEffect(() => {
        if (onSearchChange && debouncedSearchValue !== undefined) {
            onSearchChange(debouncedSearchValue)
        }
    }, [debouncedSearchValue, onSearchChange])

    return (
        <div
            className={twMerge(
                "flex  border border-neutral-100 px-[10px] py-[6px] text-sm transition duration-300 focus-within:outline-dashed focus-within:outline-1 focus-within:outline-offset-2 focus-within:outline-neutral-400",
                wide ? "w-full" : "min-w-[280px]",
                pill ? "rounded-full" : "rounded",
                className
            )}
        >
            <Icon
                icon="heroicons:magnifying-glass"
                className={`h-5 w-5 text-neutral-600 ${
                    iconPlacing === "start" ? "order-first mr-2" : "order-last ml-2"
                }`}
            />
            <input
                placeholder={placeholder}
                type="text"
                onChange={onInputChange}
                className={`block w-full appearance-none border-none text-sm outline-none placeholder:text-sm placeholder:font-normal placeholder:text-neutral-300`}
            />
        </div>
    )
}

export default Search
