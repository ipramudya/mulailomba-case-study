import { Icon } from "@iconify/react"
import Button from "@ui/button"
import Modal from "@ui/modal"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"
import { twMerge } from "tailwind-merge"

interface Props {
    cb(): void
    isOpen: boolean
    onOpenChange(val: boolean): void
    variant: "delete" | "accept"
    title: string
    description: string
    confimationLabel?: string
    icon?: string
}

const VARIANT = new Map([
    [
        "accept",
        {
            color: "border-success-500",
            text: "text-success-500",
            icon: "heroicons:check-circle",
            button: "success-low",
        },
    ],
    [
        "delete",
        {
            color: "border-danger-500",
            text: "text-danger-500",
            icon: "heroicons:no-symbol",
            button: "danger-low",
        },
    ],
])

const Confirmation: FunctionComponent<Props> = ({
    cb,
    isOpen,
    onOpenChange,
    variant,
    title,
    description,
    confimationLabel = "Konfirmasi",
    icon = VARIANT.get(variant)?.icon,
}) => (
    <Modal
        title={false}
        open={isOpen}
        onOpenChange={onOpenChange}
        classNames={{ content: "max-w-[486px]" }}
    >
        <div className="flex flex-col items-center space-y-3">
            <div
                className={twMerge(
                    "flex h-[56px] w-[56px] items-center justify-center rounded-full border p-2",
                    VARIANT.get(variant)?.color
                )}
            >
                <Icon
                    icon={icon as string}
                    className={twMerge("h-8 w-8", VARIANT.get(variant)?.text)}
                />
            </div>
            <div className="flex w-fit flex-col space-y-1 text-center">
                <Typography as="span" weight={500}>
                    {title}
                </Typography>
                <Typography size="sm" as="span" className="text-neutral-500">
                    {description}
                </Typography>
            </div>
            <div className="flex w-full flex-col space-y-2 pt-4">
                <Button
                    size="sm"
                    label="Batalkan"
                    intent="bordered"
                    onClick={() => onOpenChange(false)}
                />
                <Button
                    size="sm"
                    label={confimationLabel}
                    intent={VARIANT.get(variant)?.button as any}
                    className={twMerge(
                        "border",
                        variant == "delete" ? "border-danger-200" : "border-primary-200"
                    )}
                    onClick={() => {
                        cb()
                        onOpenChange(false)
                    }}
                />
            </div>
        </div>
    </Modal>
)

export default Confirmation
