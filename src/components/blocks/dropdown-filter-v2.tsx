import Menu from "@ui/menu"
import { FunctionComponent } from "react"

interface Props {
    value: string
    valueSetter(val: string): void
    options: {
        label: string
        value: string
    }[]
    buttonLabel: string
    contentAlign?: "center" | "start" | "end"
    ignoreValueAsLabel?: boolean
    disabled?: boolean
    preventSetEmptyWhenValueSame?: boolean
}

const DropdownFilterV2: FunctionComponent<Props> = ({
    value,
    valueSetter,
    options,
    buttonLabel,
    contentAlign = "center",
    ignoreValueAsLabel = false,
    disabled = false,
    preventSetEmptyWhenValueSame = false,
}) => {
    const getLabel = (value: string) => {
        if (ignoreValueAsLabel) return buttonLabel

        if (["1", "0"].includes(value)) {
            return options.find((o) => o.value === value)?.label
        }
        return value
    }

    return (
        <Menu.Root
            buttonLabel={getLabel(value) || buttonLabel}
            classNames={
                !ignoreValueAsLabel
                    ? {
                          button: value ? "border-success-600 bg-success-50" : undefined,
                      }
                    : undefined
            }
            contentAlign={contentAlign}
            disabled={disabled}
        >
            <Menu.RadioGroup
                value={value}
                onValueChange={(val) => {
                    if (val === value && !preventSetEmptyWhenValueSame) return valueSetter("")
                    valueSetter(val)
                }}
            >
                {options.map((o, idx2) => (
                    <Menu.Radio label={o.label} value={o.value} key={"filter-" + o.label + idx2} />
                ))}
            </Menu.RadioGroup>
        </Menu.Root>
    )
}

export default DropdownFilterV2
