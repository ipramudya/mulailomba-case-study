import { FunctionComponent } from "react"
import { twMerge } from "tailwind-merge"

import styles from "./pattern.module.css"

const Pattern: FunctionComponent = () => {
    return (
        <div className="fixed bottom-0 left-0 -z-[1] h-[50dvh] w-full">
            <div
                className={twMerge(
                    styles.pattern,
                    "h-full w-full bg-white bg-fixed bg-left-bottom bg-repeat shadow-inner shadow-white"
                )}
                style={{
                    backgroundSize: "4%",
                }}
            ></div>
        </div>
    )
}

export default Pattern
