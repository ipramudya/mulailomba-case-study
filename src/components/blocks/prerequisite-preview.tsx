import Button from "@ui/button"
import Form from "@ui/form"
import Modal from "@ui/modal"
import { Fragment, FunctionComponent, useId } from "react"
import { useForm } from "react-hook-form"

interface Props {
    prerequisitesOnDB: {
        description: string | null
        id: string
        is_required: number | null
        lomba_id: string
        name: string | null
        variant: string | null
    }[]
    isOpen: boolean
    onOpenChange(val: boolean): void
}

const PrerequisitePreview: FunctionComponent<Props> = ({
    prerequisitesOnDB,
    isOpen,
    onOpenChange,
}) => {
    const id = useId()
    const methods = useForm()

    return (
        <Modal open={isOpen} onOpenChange={onOpenChange} title="Lihat Prasyarat">
            <Form
                {...methods}
                onSubmit={() => {
                    return null
                }}
            >
                <Form.Layout>
                    {prerequisitesOnDB.map((prerequisite) => (
                        <Fragment key={prerequisite.id}>
                            {prerequisite.variant === "Paragraf" && (
                                <Form.TextareaField
                                    key={prerequisite.id}
                                    label={prerequisite.name as string}
                                    required={Boolean(prerequisite.is_required)}
                                    name={`${prerequisite.name}-${id}`}
                                    maxLength={1000}
                                    hasCount
                                    classNames={{ input: "min-h-[120px] resize-y" }}
                                    placeholder={`Masukan ${prerequisite.name}`}
                                    hint={prerequisite.description || undefined}
                                    hideHintIcon
                                />
                            )}

                            {prerequisite.variant === "Teks Singkat" && (
                                <Form.TextField
                                    key={prerequisite.id}
                                    hint={prerequisite.description || undefined}
                                    label={prerequisite.name as string}
                                    required={Boolean(prerequisite.is_required)}
                                    name={`${prerequisite.name}-${id}`}
                                    placeholder={`Masukan ${prerequisite.name}`}
                                    hideHintIcon
                                />
                            )}

                            {prerequisite.variant === "File" && (
                                <Form.FileField
                                    key={prerequisite.id}
                                    hint={prerequisite.description || undefined}
                                    label={prerequisite.name as string}
                                    required={Boolean(prerequisite.is_required)}
                                    name={`${prerequisite.name}-${id}`}
                                    placeholder={`Silahkan ${prerequisite.name}`}
                                    hideHintIcon
                                />
                            )}
                        </Fragment>
                    ))}
                    <Button
                        label="Tutup"
                        intent="bordered"
                        onClick={() => onOpenChange(false)}
                        className="ml-auto"
                    />
                </Form.Layout>
            </Form>
        </Modal>
    )
}

export default PrerequisitePreview
