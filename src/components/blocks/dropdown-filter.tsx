import Menu from "@ui/menu"
import { find } from "lodash-es"
import { type FunctionComponent, useMemo, useState } from "react"

interface Props {
    options: { label: string; value: string }[]
    onOptionSelected?: (value: string) => void
}

const DropdownFilter: FunctionComponent<Props> = ({ options, onOptionSelected }) => {
    /* react states */
    const [selectedFilter, setSelectedFilter] = useState<(typeof options)[number]["value"]>("")

    /* react memoization */
    const buttonLabel = useMemo(() => {
        return find(options, { value: selectedFilter })
    }, [options, selectedFilter])

    const onFilterSelected = (val: string) => {
        if (selectedFilter !== val) {
            setSelectedFilter(val as (typeof options)[number]["value"])

            if (onOptionSelected) {
                onOptionSelected(val)
            }
            return
        }

        setSelectedFilter("")
        if (onOptionSelected) {
            onOptionSelected("")
        }
    }

    return (
        <Menu.Root buttonLabel={buttonLabel?.label as string}>
            <Menu.RadioGroup value={selectedFilter} onValueChange={onFilterSelected}>
                {options.map(({ label, value }, idx) => {
                    if (!value) return

                    return (
                        <Menu.Radio
                            label={label}
                            value={value}
                            key={`grade-dropdown-filter-${idx}`}
                        />
                    )
                })}
            </Menu.RadioGroup>
        </Menu.Root>
    )
}

export default DropdownFilter
