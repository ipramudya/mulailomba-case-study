import { FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"

const PosterSkeleton: FunctionComponent = () => {
    return (
        <div className="grid grid-cols-3 space-x-4 overflow-hidden rounded-md border border-neutral-100 px-2 py-3">
            <Skeleton className="col-span-1 aspect-[3/4] h-full" />
            <div className="col-span-2 flex h-full flex-col space-y-3">
                <div className="flex items-center justify-between">
                    <Skeleton className="h-[24px] w-[140px]" />
                    <Skeleton className="aspect-square w-6 " />
                </div>
                <Skeleton count={3} className="mb-1 h-[20px] w-full" />
                <Skeleton className="h-[20px] w-[100px]" />
            </div>
        </div>
    )
}

export default PosterSkeleton
