import PLOT_STATUS_LOMBA from "@constant/plot-status-lomba"
import { formatStartAndEndDate, getRemainingTime } from "@functions/format-date"
import useAddToWishlist from "@hooks/use-add-to-wishlist"
import { Icon } from "@iconify/react"
import Badge from "@ui/badge"
import IconButton from "@ui/icon-button"
import RouterLink from "@ui/router-link"
import Typography from "@ui/typography"
import { type FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"

interface Props {
    image?: string
    id: string
    price: string | null
    isOnline: boolean
    title: string
    startDate: string
    endDate: string
}

const LandscapePoster: FunctionComponent<Props> = ({
    id,
    image,
    price,
    isOnline,
    title,
    startDate,
    endDate,
}) => {
    const { handleAddToFavorite, isAlreadySaved, isRenderable } = useAddToWishlist(id)

    /* formatter */
    let formattedDate, remainingTime
    if (!!startDate || !!endDate) {
        formattedDate = formatStartAndEndDate(startDate, endDate)
        remainingTime = getRemainingTime(startDate, endDate)
    } else {
        formattedDate = "-"
        remainingTime = null
    }

    return (
        <RouterLink to={`/lomba/${id}`} className="block">
            <div className="grid cursor-pointer grid-cols-3 space-x-4 overflow-hidden rounded-md border border-neutral-100 px-2 py-3 transition duration-300 hover:shadow-card">
                {/* 👇 poster image */}
                {image ? (
                    <div className="relative col-span-1 h-0 overflow-hidden rounded-md bg-neutral-100 pt-[133%]">
                        <img
                            src={image}
                            alt={`poster-${title}`}
                            className="absolute left-0 top-0 h-full w-full"
                        />
                    </div>
                ) : (
                    <div className="col-span-1 flex aspect-square items-center justify-center bg-secondary-50">
                        <Icon icon="heroicons:photo" className="h-6 w-6 text-neutral-500" />
                    </div>
                )}

                <div className="col-span-2 flex h-full flex-col space-y-3">
                    {/* 👇 title */}
                    <div className="relative flex flex-grow items-center justify-between space-x-2">
                        <Typography as="h5" className="line-clamp-2" weight={500}>
                            {title}
                        </Typography>

                        {/* 👇 bookmark button */}
                        {isRenderable ? (
                            <IconButton
                                size="xs"
                                iconSize="sm"
                                intent="primary-ghost"
                                className="shrink-0"
                                icon={
                                    isAlreadySaved
                                        ? "heroicons:bookmark-20-solid"
                                        : "heroicons:bookmark"
                                }
                                onClick={handleAddToFavorite}
                            />
                        ) : (
                            <Skeleton className="h-5 w-5" />
                        )}
                    </div>

                    <div className="flex flex-col space-y-[6px]">
                        {/* 👇 date */}
                        <div className="flex items-center space-x-2">
                            <Icon
                                icon="lucide:calendar-range"
                                className="h-4 w-4 flex-shrink-0 text-neutral-500"
                            />
                            <Typography as="span" size="xs">
                                {formattedDate}
                            </Typography>
                        </div>

                        {/* 👇 location */}
                        <div className="flex items-center space-x-2">
                            <Icon
                                icon="lucide:map-pin"
                                className="h-4 w-4 flex-shrink-0 text-neutral-500"
                            />
                            <Typography as="span" size="xs">
                                {isOnline ? "Online" : "Tatap muka"}
                            </Typography>
                        </div>

                        {/* 👇 price */}
                        <div className="flex items-center space-x-2">
                            <Icon
                                icon="heroicons:banknotes"
                                className="h-4 w-4 flex-shrink-0 text-neutral-500"
                            />
                            <Typography as="span" size="xs">
                                {!price ? "Tidak dipungut biaya" : "Berbayar"}
                            </Typography>
                        </div>
                    </div>

                    {/* 👇 badge */}
                    <Badge
                        label={remainingTime?.message as string}
                        intent={
                            PLOT_STATUS_LOMBA.get(remainingTime?.status as string)
                                ?.badgeIntent as any
                        }
                        size="xs"
                    />
                </div>
            </div>
        </RouterLink>
    )
}

export default LandscapePoster
