// ! DEPRECATED
import PLOT_STATUS_LOMBA from "@constant/plot-status-lomba"
import { formatStartAndEndDate, getRemainingTime } from "@functions/format-date"
import useAddToWishlist from "@hooks/use-add-to-wishlist"
import { Icon } from "@iconify/react"
import Badge from "@ui/badge"
import IconButton from "@ui/icon-button"
import RouterLink from "@ui/router-link"
import Typography from "@ui/typography"
import { type FunctionComponent } from "react"
import Skeleton from "react-loading-skeleton"

interface Props {
    image?: string
    id: string
    price: string | null
    isOnline: boolean
    title: string
    startDate: string
    endDate: string
}

const PotraitPoster: FunctionComponent<Props> = ({
    image,
    id,
    price,
    isOnline,
    title,
    startDate,
    endDate,
}) => {
    const { handleAddToFavorite, isAlreadySaved, isRenderable } = useAddToWishlist(id)

    /* formatter */
    let formattedDate, remainingTime
    if (!!startDate || !!endDate) {
        formattedDate = formatStartAndEndDate(startDate, endDate)
        remainingTime = getRemainingTime(startDate, endDate)
    } else {
        formattedDate = "-"
        remainingTime = null
    }

    return (
        <RouterLink to={`/lomba/${id}`} className="block">
            <div className="flex h-full cursor-pointer flex-col overflow-hidden rounded-md border border-neutral-100 transition duration-300 hover:shadow-card">
                {/* 👇 poster image */}
                {image ? (
                    <div className="relative h-0 overflow-hidden bg-neutral-100 pt-[150%] ">
                        <img
                            src={image}
                            className="absolute top-0 left-0 h-full w-full"
                            alt={`poster-${title}`}
                        />
                    </div>
                ) : (
                    <div className="flex aspect-square items-center justify-center bg-secondary-50">
                        <Icon icon="heroicons:photo" className="h-6 w-6 text-neutral-500" />
                    </div>
                )}
                {/* 👇 content */}
                <div className="flex h-full flex-col space-y-[10px] px-3 pt-2 pb-4">
                    <div className="flex items-center justify-between">
                        {/* 👇 badges */}
                        <Badge
                            label={remainingTime?.message as string}
                            intent={
                                PLOT_STATUS_LOMBA.get(remainingTime?.status as string)
                                    ?.badgeIntent as any
                            }
                            size="xs"
                        />

                        {/* 👇 bookmark button */}
                        {isRenderable && (
                            <>
                                {isAlreadySaved === null ? (
                                    <Skeleton className="h-5 w-5" />
                                ) : (
                                    <IconButton
                                        size="xs"
                                        iconSize="sm"
                                        intent="primary-ghost"
                                        icon={
                                            isAlreadySaved
                                                ? "heroicons:bookmark-20-solid"
                                                : "heroicons:bookmark"
                                        }
                                        onClick={handleAddToFavorite}
                                    />
                                )}
                            </>
                        )}
                    </div>

                    {/* 👇 title */}
                    <Typography as="h5" size="sm" className="flex-grow line-clamp-2" weight={500}>
                        {title}
                    </Typography>

                    <div className="flex flex-col space-y-[6px]">
                        {/* 👇 date */}
                        <div className="flex items-center space-x-2">
                            <Icon
                                icon="lucide:calendar-range"
                                className="h-5 w-5 flex-shrink-0 text-neutral-500"
                            />
                            <Typography as="span" size="xs">
                                {formattedDate}
                            </Typography>
                        </div>

                        {/* 👇 location */}
                        <div className="flex items-center space-x-2">
                            <Icon
                                icon="lucide:map-pin"
                                className="h-5 w-5 flex-shrink-0 text-neutral-500"
                            />
                            <Typography as="span" size="xs">
                                {isOnline ? "Tatap muka" : "Online"}
                            </Typography>
                        </div>

                        {/* 👇 price */}
                        <div className="flex items-center space-x-2">
                            <Icon
                                icon="heroicons:banknotes"
                                className="h-5 w-5 flex-shrink-0 text-neutral-500"
                            />
                            <Typography as="span" size="xs">
                                {!price ? "Tidak dipungut biaya" : "Berbayar"}
                            </Typography>
                        </div>
                    </div>
                </div>
            </div>
        </RouterLink>
    )
}

export default PotraitPoster
