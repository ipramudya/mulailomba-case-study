import { QueryClient, QueryClientProvider } from "@tanstack/react-query"
import { ReactQueryDevtools } from "@tanstack/react-query-devtools"
import { FunctionComponent, PropsWithChildren } from "react"

const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            retry: 4,
        },
        mutations: {
            retry: 4,
        },
    },
})

const ReactQueryProvider: FunctionComponent<PropsWithChildren> = ({ children }) => {
    return (
        <QueryClientProvider client={queryClient}>
            <ReactQueryDevtools />
            {children}
        </QueryClientProvider>
    )
}

export default ReactQueryProvider
