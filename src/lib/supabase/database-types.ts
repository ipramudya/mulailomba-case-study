export type Json = string | number | boolean | null | { [key: string]: Json | undefined } | Json[]

export interface Database {
    public: {
        Tables: {
            executions: {
                Row: {
                    created_at: string | null
                    description: string
                    file_public_url: string | null
                    file_storage_url: string | null
                    id: string
                    is_eliminate: number | null
                    is_passed: number | null
                    is_winner: number | null
                    on_timeline: string | null
                    registration_id: string | null
                    seeker_id: string | null
                    timeline_name: string | null
                    updated_at: string | null
                    winner_as: number | null
                }
                Insert: {
                    created_at?: string | null
                    description?: string
                    file_public_url?: string | null
                    file_storage_url?: string | null
                    id?: string
                    is_eliminate?: number | null
                    is_passed?: number | null
                    is_winner?: number | null
                    on_timeline?: string | null
                    registration_id?: string | null
                    seeker_id?: string | null
                    timeline_name?: string | null
                    updated_at?: string | null
                    winner_as?: number | null
                }
                Update: {
                    created_at?: string | null
                    description?: string
                    file_public_url?: string | null
                    file_storage_url?: string | null
                    id?: string
                    is_eliminate?: number | null
                    is_passed?: number | null
                    is_winner?: number | null
                    on_timeline?: string | null
                    registration_id?: string | null
                    seeker_id?: string | null
                    timeline_name?: string | null
                    updated_at?: string | null
                    winner_as?: number | null
                }
                Relationships: [
                    {
                        foreignKeyName: "executions_registration_id_fkey"
                        columns: ["registration_id"]
                        isOneToOne: false
                        referencedRelation: "registrations"
                        referencedColumns: ["id"]
                    },
                    {
                        foreignKeyName: "executions_seeker_id_fkey"
                        columns: ["seeker_id"]
                        isOneToOne: false
                        referencedRelation: "seeker"
                        referencedColumns: ["id"]
                    }
                ]
            }
            feedback: {
                Row: {
                    category: string
                    created_at: string
                    email_or_username: string
                    feedback: string
                    id: string
                    rating: string
                    status: string
                }
                Insert: {
                    category: string
                    created_at?: string
                    email_or_username: string
                    feedback: string
                    id?: string
                    rating: string
                    status: string
                }
                Update: {
                    category?: string
                    created_at?: string
                    email_or_username?: string
                    feedback?: string
                    id?: string
                    rating?: string
                    status?: string
                }
                Relationships: []
            }
            lomba: {
                Row: {
                    benefits: string[] | null
                    category: string | null
                    created_at: string | null
                    description: string | null
                    eligibilities: string[] | null
                    end_date: string | null
                    end_registration: string | null
                    held_location: string | null
                    held_meeting_link: string | null
                    id: string
                    is_archived: number | null
                    is_free: number | null
                    is_held_online: number | null
                    is_unlimited_participants: number | null
                    name: string | null
                    organizer_id: string
                    poster_public_url: string | null
                    poster_storage_url: string | null
                    rules: string | null
                    start_date: string | null
                    start_registration: string | null
                    total_participants: number | null
                    updated_at: string | null
                }
                Insert: {
                    benefits?: string[] | null
                    category?: string | null
                    created_at?: string | null
                    description?: string | null
                    eligibilities?: string[] | null
                    end_date?: string | null
                    end_registration?: string | null
                    held_location?: string | null
                    held_meeting_link?: string | null
                    id?: string
                    is_archived?: number | null
                    is_free?: number | null
                    is_held_online?: number | null
                    is_unlimited_participants?: number | null
                    name?: string | null
                    organizer_id: string
                    poster_public_url?: string | null
                    poster_storage_url?: string | null
                    rules?: string | null
                    start_date?: string | null
                    start_registration?: string | null
                    total_participants?: number | null
                    updated_at?: string | null
                }
                Update: {
                    benefits?: string[] | null
                    category?: string | null
                    created_at?: string | null
                    description?: string | null
                    eligibilities?: string[] | null
                    end_date?: string | null
                    end_registration?: string | null
                    held_location?: string | null
                    held_meeting_link?: string | null
                    id?: string
                    is_archived?: number | null
                    is_free?: number | null
                    is_held_online?: number | null
                    is_unlimited_participants?: number | null
                    name?: string | null
                    organizer_id?: string
                    poster_public_url?: string | null
                    poster_storage_url?: string | null
                    rules?: string | null
                    start_date?: string | null
                    start_registration?: string | null
                    total_participants?: number | null
                    updated_at?: string | null
                }
                Relationships: [
                    {
                        foreignKeyName: "lomba_organizer_id_fkey"
                        columns: ["organizer_id"]
                        isOneToOne: false
                        referencedRelation: "organizer"
                        referencedColumns: ["id"]
                    }
                ]
            }
            lomba_prerequisites: {
                Row: {
                    description: string | null
                    id: string
                    is_required: number | null
                    lomba_id: string | null
                    name: string | null
                    variant: string | null
                }
                Insert: {
                    description?: string | null
                    id?: string
                    is_required?: number | null
                    lomba_id?: string | null
                    name?: string | null
                    variant?: string | null
                }
                Update: {
                    description?: string | null
                    id?: string
                    is_required?: number | null
                    lomba_id?: string | null
                    name?: string | null
                    variant?: string | null
                }
                Relationships: [
                    {
                        foreignKeyName: "lomba_prerequisites_lomba_id_fkey"
                        columns: ["lomba_id"]
                        isOneToOne: false
                        referencedRelation: "lomba"
                        referencedColumns: ["id"]
                    }
                ]
            }
            lomba_timelines: {
                Row: {
                    description: string | null
                    end_date: string | null
                    id: string
                    lomba_id: string | null
                    name: string | null
                    start_date: string | null
                }
                Insert: {
                    description?: string | null
                    end_date?: string | null
                    id?: string
                    lomba_id?: string | null
                    name?: string | null
                    start_date?: string | null
                }
                Update: {
                    description?: string | null
                    end_date?: string | null
                    id?: string
                    lomba_id?: string | null
                    name?: string | null
                    start_date?: string | null
                }
                Relationships: [
                    {
                        foreignKeyName: "lomba_timelines_lomba_id_fkey"
                        columns: ["lomba_id"]
                        isOneToOne: false
                        referencedRelation: "lomba"
                        referencedColumns: ["id"]
                    }
                ]
            }
            organizer: {
                Row: {
                    about: string | null
                    created_at: string | null
                    email: string | null
                    id: string
                    location: string | null
                    name: string | null
                    phone: string | null
                    profile_public_url: string | null
                    profile_status: string | null
                    profile_storage_url: string | null
                    social_instagram: string | null
                    social_whatsapp: string | null
                    updated_at: string | null
                }
                Insert: {
                    about?: string | null
                    created_at?: string | null
                    email?: string | null
                    id: string
                    location?: string | null
                    name?: string | null
                    phone?: string | null
                    profile_public_url?: string | null
                    profile_status?: string | null
                    profile_storage_url?: string | null
                    social_instagram?: string | null
                    social_whatsapp?: string | null
                    updated_at?: string | null
                }
                Update: {
                    about?: string | null
                    created_at?: string | null
                    email?: string | null
                    id?: string
                    location?: string | null
                    name?: string | null
                    phone?: string | null
                    profile_public_url?: string | null
                    profile_status?: string | null
                    profile_storage_url?: string | null
                    social_instagram?: string | null
                    social_whatsapp?: string | null
                    updated_at?: string | null
                }
                Relationships: [
                    {
                        foreignKeyName: "organizer_id_fkey"
                        columns: ["id"]
                        isOneToOne: true
                        referencedRelation: "users"
                        referencedColumns: ["id"]
                    }
                ]
            }
            registrations: {
                Row: {
                    created_at: string | null
                    id: string
                    is_cancelation_approved: number
                    is_canceled: number
                    is_verified: number
                    lomba_id: string | null
                    seeker_id: string | null
                    updated_at: string | null
                }
                Insert: {
                    created_at?: string | null
                    id?: string
                    is_cancelation_approved?: number
                    is_canceled?: number
                    is_verified?: number
                    lomba_id?: string | null
                    seeker_id?: string | null
                    updated_at?: string | null
                }
                Update: {
                    created_at?: string | null
                    id?: string
                    is_cancelation_approved?: number
                    is_canceled?: number
                    is_verified?: number
                    lomba_id?: string | null
                    seeker_id?: string | null
                    updated_at?: string | null
                }
                Relationships: [
                    {
                        foreignKeyName: "registrations_lomba_id_fkey"
                        columns: ["lomba_id"]
                        isOneToOne: false
                        referencedRelation: "lomba"
                        referencedColumns: ["id"]
                    },
                    {
                        foreignKeyName: "registrations_seeker_id_fkey"
                        columns: ["seeker_id"]
                        isOneToOne: false
                        referencedRelation: "seeker"
                        referencedColumns: ["id"]
                    }
                ]
            }
            registrations_answer: {
                Row: {
                    created_at: string | null
                    id: string
                    label: string | null
                    registration_id: string | null
                    value: string | null
                }
                Insert: {
                    created_at?: string | null
                    id?: string
                    label?: string | null
                    registration_id?: string | null
                    value?: string | null
                }
                Update: {
                    created_at?: string | null
                    id?: string
                    label?: string | null
                    registration_id?: string | null
                    value?: string | null
                }
                Relationships: [
                    {
                        foreignKeyName: "registrations_answer_registration_id_fkey"
                        columns: ["registration_id"]
                        isOneToOne: false
                        referencedRelation: "registrations"
                        referencedColumns: ["id"]
                    }
                ]
            }
            registrations_upload: {
                Row: {
                    created_at: string | null
                    file_public_url: string | null
                    file_storage_url: string | null
                    id: string
                    label: string | null
                    registration_id: string | null
                }
                Insert: {
                    created_at?: string | null
                    file_public_url?: string | null
                    file_storage_url?: string | null
                    id?: string
                    label?: string | null
                    registration_id?: string | null
                }
                Update: {
                    created_at?: string | null
                    file_public_url?: string | null
                    file_storage_url?: string | null
                    id?: string
                    label?: string | null
                    registration_id?: string | null
                }
                Relationships: [
                    {
                        foreignKeyName: "registrations_upload_registration_id_fkey"
                        columns: ["registration_id"]
                        isOneToOne: false
                        referencedRelation: "registrations"
                        referencedColumns: ["id"]
                    }
                ]
            }
            seeker: {
                Row: {
                    created_at: string | null
                    email: string | null
                    id: string
                    interest: string[] | null
                    is_generated: number
                    location: string | null
                    name: string | null
                    phone: string | null
                    profile_public_url: string | null
                    profile_storage_url: string | null
                    registered_lomba: string[] | null
                    saved_lomba: string[] | null
                    updated_at: string | null
                }
                Insert: {
                    created_at?: string | null
                    email?: string | null
                    id?: string
                    interest?: string[] | null
                    is_generated?: number
                    location?: string | null
                    name?: string | null
                    phone?: string | null
                    profile_public_url?: string | null
                    profile_storage_url?: string | null
                    registered_lomba?: string[] | null
                    saved_lomba?: string[] | null
                    updated_at?: string | null
                }
                Update: {
                    created_at?: string | null
                    email?: string | null
                    id?: string
                    interest?: string[] | null
                    is_generated?: number
                    location?: string | null
                    name?: string | null
                    phone?: string | null
                    profile_public_url?: string | null
                    profile_storage_url?: string | null
                    registered_lomba?: string[] | null
                    saved_lomba?: string[] | null
                    updated_at?: string | null
                }
                Relationships: [
                    {
                        foreignKeyName: "seeker_id_fkey"
                        columns: ["id"]
                        isOneToOne: true
                        referencedRelation: "users"
                        referencedColumns: ["id"]
                    }
                ]
            }
        }
        Views: {
            accounts_view: {
                Row: {
                    created_at: string | null
                    email: string | null
                    id: string | null
                    location: string | null
                    name: string | null
                    phone: string | null
                    profile_public_url: string | null
                    profile_storage_url: string | null
                    status: string | null
                }
                Relationships: []
            }
            user_emails_view: {
                Row: {
                    email: string | null
                }
                Relationships: []
            }
        }
        Functions: {
            [_ in never]: never
        }
        Enums: {
            [_ in never]: never
        }
        CompositeTypes: {
            [_ in never]: never
        }
    }
}
