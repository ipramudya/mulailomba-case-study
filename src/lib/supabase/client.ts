import { createClient } from "@supabase/supabase-js"

import { Database } from "./database-types"

const supabaseClient = createClient<Database>(
    import.meta.env.VITE_SUPABASE_PROJECT_URL,
    import.meta.env.VITE_SUPABASE_PUBLIC_ANON_KEY
)

export default supabaseClient
