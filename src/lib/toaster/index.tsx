import { Icon } from "@iconify/react"
import Typography from "@ui/typography"
import { FunctionComponent } from "react"
import { ToastBar, Toaster, toast } from "react-hot-toast"

const variant = new Map([
    [
        "error",
        {
            text: "text-danger-600",
            emoji: "☹️",
        },
    ],
    [
        "success",
        {
            text: "text-success-600",
            emoji: "😉",
        },
    ],
    [
        "blank",
        {
            text: "text-neutral-600",
            emoji: "",
        },
    ],
])

const CustomToaster: FunctionComponent = () => {
    return (
        <Toaster
            toastOptions={{
                duration: 5000,
                success: {
                    className: "bg-success-50",
                    iconTheme: {
                        primary: "#16a34a",
                        secondary: "#FFFFFF",
                    },
                },
                error: {
                    className: "bg-danger-50",
                    iconTheme: {
                        primary: "#c32734",
                        secondary: "#FFFFFF",
                    },
                },
                blank: {
                    className: "bg-neutral-50",
                },
            }}
        >
            {(t) => {
                const emoji = variant.get(
                    !["error", "success"].includes(t.type) ? "blank" : t.type
                )?.emoji
                const textColor = variant.get(
                    !["error", "success"].includes(t.type) ? "blank" : t.type
                )?.text

                return (
                    <ToastBar toast={t}>
                        {({ icon }) => (
                            <>
                                {icon}
                                <Typography size="sm" as="span" className={`mx-3 ${textColor}`}>
                                    <>
                                        {t.message} {emoji}
                                    </>
                                </Typography>
                                <button onClick={() => toast.dismiss(t.id)}>
                                    <Icon icon="heroicons:x-mark-solid" className={textColor} />
                                </button>
                            </>
                        )}
                    </ToastBar>
                )
            }}
        </Toaster>
    )
}

export default CustomToaster
