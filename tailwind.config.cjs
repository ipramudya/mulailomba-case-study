/* eslint-disable no-undef */
/* eslint-disable @typescript-eslint/no-var-requires */

const colors = require("tailwindcss/colors")
const theme = require("tailwindcss/defaultTheme")

/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
    theme: {
        extend: {
            boxShadow: {
                menu: "rgba(0, 0, 0, 0.08) 0px 4px 12px",
                card: "rgba(17, 17, 26, 0.1) 0px 0px 16px",
                accent: "rgba(0, 0, 0, 0.15) 0px 5px 15px 0px",
                nav: "0px 20px 30px rgba(128, 128, 128, 0.05)",
            },
            zIndex: {
                toast: 1000,
                dialog: 999,
                overlay: 998,
                fixed: 997,
                dropdowns: 99,
            },
        },
        fontFamily: {
            sans: ["Inter", ...theme.fontFamily.sans],
            serif: [...theme.fontFamily.serif],
            body: ["Inter", ...theme.fontFamily.sans],
        },
        colors: {
            transparent: "transparent",
            inherit: colors.inherit,
            current: colors.current,
            white: colors.white,
            black: colors.black,
            warning: colors.amber,
            success: colors.emerald,
            primary: {
                50: "#f1f2f8",
                100: "#d5d8ea",
                200: "#b8bedc",
                300: "#9ca4cd",
                400: "#808abf",
                500: "#5564AA",
                600: "#4d5a99",
                700: "#323a63",
                800: "#232947",
                900: "#15192a",
            },
            secondary: {
                50: "#F8F5F2",
                100: "#edd8d1",
                200: "#e2bfb2", // mark
                300: "#d6a593",
                400: "#ca8b75",
                500: "#a95d41",
                600: "#a95d41",
                700: "#a95d41",
                800: "#6c3b29",
                900: "#2e1912",
            },
            neutral: {
                50: "#F8F9F9",
                100: "#e2e2e2",
                200: "#cecece",
                300: "#bababa",
                400: "#939393",
                500: "#808080",
                600: "#6c6c6c",
                700: "#585858",
                800: "#454545",
                900: "#1d1d1d",
            },
            kale: {
                50: "#f2f7f7",
                100: "#f2f7f7",
                200: "#bdd7d7",
                300: "#a3c6c6",
                400: "#88b6b6",
                500: "#6ea6a6",
                600: "#599191",
                700: "#497777",
                800: "#385c5c",
                900: "#284242",
            },
            danger: {
                50: "#fbedee",
                100: "#f4cacd",
                200: "#eda6ac",
                300: "#e6838b",
                400: "#d83c49",
                500: "#c32734",
                600: "#a0202a",
                700: "#7c1921",
                800: "#591218",
                900: "#36090d",
            },
        },
    },
    plugins: [
        require("tailwindcss-radix")({
            variantPrefix: "rd",
        }),
    ],
}
