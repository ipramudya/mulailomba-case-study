/* eslint-disable no-undef */
/* eslint-disable @typescript-eslint/no-var-requires */
const pluginSortImports = require("@trivago/prettier-plugin-sort-imports")
const pluginTailwindcss = require("prettier-plugin-tailwindcss")

/** @type {import("prettier").Parser}  */
const myParser = {
    ...pluginSortImports.parsers.typescript,
    parse: pluginTailwindcss.parsers.typescript.parse,
}

/** @type {import("prettier").Plugin}  */
const myPlugin = {
    parsers: {
        typescript: myParser,
    },
}

module.exports = {
    plugins: [myPlugin],
    tabWidth: 4,
    printWidth: 100,
    semi: false,
    singleQuote: false,
    importOrder: ["^[./]"],
    importOrderSeparation: true,
    importOrderSortSpecifiers: true,
    endOfLine: "lf",
    // your settings
}
