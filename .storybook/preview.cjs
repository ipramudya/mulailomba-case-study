import "../src/assets/fonts/Inter-Bold.ttf"
import "../src/assets/fonts/Inter-ExtraBold.ttf"
import "../src/assets/fonts/Inter-Medium.ttf"
import "../src/assets/fonts/Inter-Regular.ttf"
import "../src/assets/fonts/Inter-SemiBold.ttf"
import "../src/assets/styles/index.css"

export const parameters = {
    actions: { argTypesRegex: "^on[A-Z].*" },
    layout: "centered",
    controls: {
        matchers: {
            color: /(background|color)$/i,
            date: /Date$/,
        },
    },
    backgrounds: {
        default: "one",
        values: [
            {
                name: "one",
                value: "#eff0f3",
            },
            {
                name: "two",
                value: "#f8f5f2",
            },
        ],
    },
}
